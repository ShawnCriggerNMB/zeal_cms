<?php

// ** MySQL settings ** //
define('DB_NAME', 'cms_blank');    // The name of the database
define('DB_USER', 'cms_user');     // Your MySQL username
define('DB_PASSWORD', 'test123'); // ...and password
define('DB_HOST', 'localhost');    // 99% chance you won't need to change this value

$base_path = str_replace('/home/', '', dirname(__FILE__) );
$len       = strpos ( $base_path, '/');
$base_path = substr ( $base_path, 0, $len);

define('DBCONNECT', '/home/' . $base_path . '/Globals/dbconnect.php');
define('DB_PREFIX'  , 'cms_');

define('ADMIN_TABLE'  , 'admin'); //don't remove. is used in functions, can't pull from $_GET
define('ADMIN_DIR'  , 'admin');
define('SAINT', 2);
define('DEBUG', 0);

unset( $base_path, $len );


// IMAP settings - uncomment and set if you plan to use bin/spress-imap-pull.php
// sample settings are valid for Gmail, just fill in IMAP_USER and IMAP_PASSWORD
#define('IMAP_HOST', 'imap.gmail.com');
#define('IMAP_PORT', '993');
#define('IMAP_USER', 'xxx@xxx.xxx');
#define('IMAP_PASSWORD', 'xxx');
#define('IMAP_MAILBOX', 'INBOX');

// SMTP settings - uncomment and set if you want to use an exteral SMTP server to send email
// sample settings are valid for Gmail, just fill in IMAP_USER and IMAP_PASSWORD
#define('SMTP_HOST', 'ssl://smtp.gmail.com');
#define('SMTP_PORT', '465');
#define('SMTP_USER', 'xxx@xxx.xxx');
#define('SMTP_PASSWORD', 'xxx');

/**
 * base URL of the website
 * @var string
 */
$site_domain = 'http://cms.dev/';
// base path of the support administration site
$site_path   = '/';

/**
 * the full URL of the website site
 * @var string
 */
$site_url    = $site_domain . $site_path;

/**
 * admin email - used for occasional administrative notices
 * @var string
 */
$admin_email = 'nobody@cms.dev';

/**
 * support email - this is used as the From address when replying to tickets, it should usually match the SMTP_USER
 * @var string
 */
$support_email = 'support@cms.dev';

/**
 * used as the sender domain in email replies
 * @var string
 */
$email_domain = 'cms.dev';

// You can have multiple installations in one database if you give each a unique prefix
$table_prefix  = 'cms_';   // Only numbers, letters, and underscores please!

// Leave this line intact until installation is complete.
// Comment it out by adding a # at the beginning of the next line when instructed by the installer.
if ( ! defined( 'IS_INSTALLING' ) )
{
	define( 'IS_INSTALLING', false );
}

/* That's all, stop editing! */

if ( !defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname(__FILE__) );
}

if ( !defined( 'BACKPRESS_PATH' ) ) {
	define( 'BACKPRESS_PATH', ABSPATH . '/includes/' );
}

if ( !defined( 'CORE_PLUGIN_DIR' ) ) {
	define( 'CORE_PLUGIN_DIR', ABSPATH . '/includes/core-modules/' );
}

if ( !defined( 'CMS_PLUGIN_DIR' ) ) {
	define( 'CMS_PLUGIN_DIR', ABSPATH . '/includes/modules/' );
}

if ( !defined( 'ZEAL_PATH' ) ) {
	define( 'ZEAL_PATH', ABSPATH . '/includes/zeal/' );
}


// use these to avoid repeated function_exists/extension_loaded calls
define('HAS_EXT_IMAP', extension_loaded('imap'));
define('HAS_EXT_MBSTRING', extension_loaded('mbstring'));
define('HAS_EXT_ICONV', defined('ICONV_VERSION'));

// assorted useful stuff
define('MYSQL_STRFTIME_FORMAT', '%Y-%m-%d %H:%M:%S');

/**
 * Add define('CMS_DEBUG', true); to config.php to enable display of notices during development.
 */
if ( !defined('CMS_DEBUG') ) define( 'CMS_DEBUG', TRUE );

/**
 *  Add define('CMS_DEBUG_DISPLAY', null); to config.php use the globally configured setting for
 *  display_errors and not force errors to be displayed. Use false to force display_errors off.
 */
if ( !defined('CMS_DEBUG_DISPLAY') ) define( 'CMS_DEBUG_DISPLAY', true );

/**
 * Add define('CMS_DEBUG_LOG', true); to enable error logging to lib/debug.log.
 */
if ( !defined('CMS_DEBUG_LOG') ) define('CMS_DEBUG_LOG', true);

if ( !defined('CMS_LOG_FILE') ) define( 'CMS_LOG_FILE', './debug.log' );
/**
 * Whether to use a object cache to limit DB calls.
 */
if ( !defined('CMS_CACHE') )
	define('CMS_CACHE', false);

/**
 * @since 1.5.0
 */
if ( !defined('SITECOOKIEPATH') ) {
	$site_url = ( ! function_exists( 'site_url' ) ) ? $site_url : site_url();
	define('SITECOOKIEPATH', preg_replace('|https?://[^/]+|i', '', $site_url . '/' ) );
}


