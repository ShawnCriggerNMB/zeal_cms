function show_results ( data )
{
  $('ajax-content').dialog({
            resizable: false,
            height:180,
            modal: true,
            draggable: false,
            buttons: {
             "Close": function() {
              $( this ).dialog( "close" );
             }
            }
           });  
}

function ajax_remove ( url )
{
  $.ajax({
        url: 'ajax_common.php',
        global: true,
        type: "POST",
        cache: false,
        data: ({'params':url }),
        dataType: "html",
        async   :false,
        success : function(data) { $('ajax-content').attr('title','Item has been deleted.'); $('ajax-content').html(data); show_results(); },              
        complete: function(){ /*$('ajax-content').html('');*/ }
        });  
}

function deleteListingImage (img )
{
    if(confirm("Are you sure you want to delete '"+ img +"'?")){
        $.ajax({
            url: 'ajax_common.php',
            global: true,
            type: "POST",
            cache: false,
            data: ({
                'filename':img,
                'func': 'delete_listing_image'
            }),
            dataType: "html",
            async   :false,
            success : function(data) {
                if(data == 1){
                    $("a:contains("+img+")").closest("div.image").html("<input type='file' name='listing_images[]' />");
                }
                else{
                    $("a:contains("+img+")").closest("div.image").append("&nbsp;Delete unsuccessful.");
                }
            }
        });
    }

}

$(window).ready(function () { 
   equalCols('#contentwrapper','#leftcolumn');
});


$(window).ready(function () { 
  $('a.lightbox').lightBox();
  
  
  $('a.delete_item').click(function (event)
  {
    event.stopPropagation();
    event.preventDefault();
    $("ajax-content").attr('title', 'Delete Item Confirmation');
    $( "#ajax-content" ).html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>');
    $( "#ajax-content" ).dialog({
     resizable: false,
     height:180,
     modal: true,
     draggable: false,
     buttons: {
      "Delete this Item": function() {
       var url = $('a.delete_item').attr('href');
       $( "#ajax-content" ).html(''); 
       $( this ).dialog( "close" );
       ajax_remove ( url );         
      },
      Cancel: function() {
       $( "#ajax-content" ).html('');
       $( this ).dialog( "close" );
      }
     }
    });
    
    return false;
  });

  $('a.delete_image').click(function (event)
  {
    event.stopPropagation();
    event.preventDefault();
    $("ajax-content").attr('title', 'Delete Image Confirmation');
    $( "#ajax-content" ).html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This image will be permanently deleted and cannot be recovered. Are you sure?</p>');
    $( "#ajax-content" ).dialog({
     resizable: false,
     height:180,
     modal: true,
     draggable: false,
     buttons: {
      "Delete Image": function() {
       var url = $('a.delete_item').attr('href');
       $( "#ajax-content" ).html(''); 
       $( this ).dialog( "close" );
       ajax_remove ( url );         
      },
      Cancel: function() {
       $( "#ajax-content" ).html('');
       $( this ).dialog( "close" );
      }
     }
    });
    
    return false;
  });
  
  
  $('.tooltip').qtip({
   position: { corner: { target: 'topRight', tooltip: 'bottomLeft' } },
   style:{ name : 'dark',
           border: { width: 1, radius: 8 }
   }
  });  
  
});
