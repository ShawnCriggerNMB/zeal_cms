<?php

//****************************************************/
// Class      : Constants
// Written By : Shawn Crigger of EWD
// Written On : May 10, 2010
// Updated On : May 3, 2014
// Updated By : Cory Shaw of EWD
// Copyright Zeal Technologies
// Revision 164.
//***************************************************/

define('name'   , 'VMS');
define('version', 'v2.0BE');

$base_path = str_replace('/home/', '', dirname(__FILE__) );
$len       = strpos ( $base_path, '/');
$base_path = substr ( $base_path, 0, $len);

define('dbconnect', '/home/' . $base_path . '/Globals/dbconnect.php');
define('db_prefix'  , 'cms_');
define('admin_table'  , 'admin'); //don't remove. is used in functions, can't pull from $_GET
define('admin_dir'  , 'admin');
define('saint', 2);
define('debug', 0);

unset ( $base_path );
unset ( $len );

require( dbconnect );

$identifier = (isset($_GET['tool'])) ? $_GET['tool'] : ((isset($_POST['tool'])) ? $_POST['tool'] : $_POST['tool']);

$id = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);

$module_name = get_module_name();

$dbtbl = db_prefix. $identifier;
?>