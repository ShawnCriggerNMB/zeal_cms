<?php
// Begin a Session
session_start();
define( 'BASE', TRUE );
require_once("functions.inc.php");

$sitename      = cms_get_option( 'site_name' );
$site_base_url = cms_get_option( 'site_url' );
$error = NULL;
$hook_suffix = NULL;

$module_html = View::show( 'login', array(), FALSE );

View::set_tags( get_defined_vars() );
View::show( 'layout' );
