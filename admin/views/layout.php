<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo apply_filters( 'admin_page_title', $module_title ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex, nofollow">

<?php do_admin_head( $hook_suffix ); ?>

</head>
<body>
  <div id="wrapper">
  <?php do_action( 'left_sidebar', $hook_suffix  ); ?>

    <div id="page-wrapper">

			<div class="affix-top">
				<div class="area-top clearfix">
					<div class="pull-left header">
						<h3 class="title">
							<i class="fa fa-dashboard"></i><?php echo apply_filters( 'module_header_title', 'Dashboard' ) ?>

						</h3>
						<h5><span><?php echo apply_filters( 'module_header_subtitle', 'Dashboard' ) ?></span></h5>
					</div>
					<?php do_action( 'area-top-right', $hook_suffix ); ?>
				</div>
			</div>

		<div id="page-inner">


			<div class="row">
<?= $module_html ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="span12">
			<div id="footer" style="min-height:20px !important; height:20px; "><?php echo $time; ?><a style="float:right;padding-right:35px; color: #C8DC27;" href="http://www.zealtechnologies.com" target="_blank">Powered by Zeal Technologies</a></div>
		</div>
	</div>
</div>

<?php do_admin_footer( $hook_suffix ); ?>
<?php cms_print_scripts(); ?>

<script>

$(document).ready(function() {

	if ( $.fn.dataTable && jQuery("#data-table").length > 0 )
	{
		jQuery("#data-table").dataTable();
	}

	if ( $.fn.tagsManager && jQuery(".tm-input").length > 0 )
	{
		jQuery(".tm-input").tagsManager({
			prefilled: tagmanager.keywords,
			CapitalizeFirstLetter: false,
			delimiters: [9, 13, 44], // tab, enter, comma
			backspace: [8],
			maxTags: 16,
			tagsContainer: null,tagCloseIcon: 'x',
			tagClass: 'tm-input-success tm-input-small',
			replace: 'keywords'
	    });
	}
});

tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: 680,
    height: 300,
    convert_fonts_to_spans: true,
    statusbar: false,
//    menubar: "tools table format view insert edit",
	menu : { // this is the complete default configuration
//	    file   : {title : 'File'  , items : 'newdocument'},
	    edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
	    insert : {title : 'Insert', items : 'link media | template hr'},
	    view   : {title : 'View'  , items : 'visualaid'},
	    format : {title : 'Format', items : 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
	    table  : {title : 'Table' , items : 'inserttable tableprops deletetable | cell row column'},
	    tools  : {title : 'Tools' , items : 'spellchecker code'}
	},
    plugins: [
         "advlist autolink link image lists charmap hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
         "table contextmenu directionality emoticons paste textcolor responsivefilemanager spellchecker code"
   ],
   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | code | paste",
   image_advtab: true ,

   external_filemanager_path:"/admin/filemanager/",
   filemanager_title:"Filemanager" ,
   external_plugins: { "filemanager" : "/admin/filemanager/plugin.min.js"}
 });

</script>
</body>
</html>
