<?php if ( is_user_logged_in() ) : ?>
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo admin_url(); ?>">Admin Area</a>
            </div>
            <ul class="nav navbar-right top-nav">
                <li class="dropdown" style="padding-top: 10px;padding-bottom: 0px;">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?=$current_user->display_name?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?= admin_url( '?tool=user_profile' ); ?>"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?= admin_url( '?action=logout' ); ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
      <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="<?= admin_url() ; ?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
<?php echo create_sidebar(); ?>
<?php if ( ! $current_user->has_cap( 'superadmin' ) ) : ?>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#config"><i class="fa fa-fw fa-arrows-v"></i> Config <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="config" class="collapse">
                            <li>
                                <a href="<?= admin_url('/?tool=config'); ?>">Website Config</a>
                            </li>
                            <li>
                                <a href="#">Another config item....</a>
                            </li>
                        </ul>
                    </li>
<?php endif; ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
            </nav>
<?php endif; ?>
