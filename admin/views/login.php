<section id="login">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="form-wrap">
				<h1>Log in with your email account</h1>
					<form role="form" action="<?= admin_url( '/index.php' )?>" method="post" id="login-form" autocomplete="off">
						<div class="form-group">
							<label for="username" class="sr-only">Email</label>
							<input type="email" name="username" id="username" class="form-control" placeholder="somebody@example.com">
						</div>
						<div class="form-group">
							<label for="password" class="sr-only">Password</label>
							<input type="password" name="password" id="password" class="form-control" placeholder="Password">
						</div>
						<div class="checkbox">
							<!-- <span class="character-checkbox" onclick="showPassword()"></span> -->
							<!-- <span class="label">Show password</span> -->
						</div>
						<input type="submit" id="btn-login" class="btn btn-custom btn-lg btn-block" value="Log in">
						<input type="hidden" name="action" value="login">
						<input type="hidden" name="redirect_to" value="<?=admin_url( '/index.php' )?>" >
					</form>
					<a href="javascript:;" class="forget" data-toggle="modal" data-target=".forget-modal">Forgot your password?</a>
					<hr>
				</div>
			</div> <!-- /.col-xs-12 -->
		</div> <!-- /.row -->
	</div> <!-- /.container -->
</section>

<div class="modal fade forget-modal" tabindex="-1" role="dialog" aria-labelledby="myForgetModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title">Recovery password</h4>
			</div>
			<div class="modal-body">
				<p>Type your email account</p>
				<input type="email" name="recovery-email" id="recovery-email" class="form-control" autocomplete="off">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-custom">Recovery</button>
			</div>
		</div> <!-- /.modal-content -->
	</div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->
