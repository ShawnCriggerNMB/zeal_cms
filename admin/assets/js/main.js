(function ($) {
    "use strict";
    var App = {

        menus: function () {
            /*====================================
            METIS MENU
            ======================================*/
            $('#main-menu').metisMenu();

            /*====================================
              LOAD APPROPRIATE MENU BAR
           ======================================*/
            $(window).bind("load resize", function () {
                if ($(this).width() < 768) {
                    $('div.sidebar-collapse').addClass('collapse');
                } else {
                    $('div.sidebar-collapse').removeClass('collapse');
                }
            });
        },
        initialization: function () {
            App.menus();

        }

};
    // Initializing ///

    $(document).ready(function () {
//        App.menus();
    });

}(jQuery));



var Nav = function () {

    return { init: init };

    function init () {
        var mainnav = $('#main-nav'),
            openActive = mainnav.is ('.open-active'),
            navActive = mainnav.find ('> .active');

        mainnav.find ('> .dropdown > a').bind ('click', navClick);

        if (openActive && navActive.is ('.dropdown')) {
            navActive.addClass ('opened').find ('.sub-nav').show ();
        }
    }

    function navClick (e) {
        e.preventDefault ();

        var li = $(this).parents ('li');

        if (li.is ('.opened')) {
            closeAll ();
        } else {
            closeAll ();
            li.addClass ('opened').find ('.sub-nav').slideDown ();
        }
    }

    function closeAll () {
        $('.sub-nav').slideUp ().parents ('li').removeClass ('opened');
    }
}();


$(document).ready(function () {
    $('html').removeClass ('no-js');
    Nav.init();

	if ( $(".delete-record").length > 0 )
	{

		$(".delete-record").click(function(e){
			var $id = $(this).data('id');
			var $parent = $(this).parent().parent();
			bootbox.confirm("Are you sure you want to delete this record?", function(result){
				if ( true === result )
				{
					var module = Site_Config.module;
					module.toLowerCase();

					var data = {
						id    : $id,
						module: Site_Config.module,
						action: 'delete-' + module + '-record'
					};

					$elem = $(this).parent().parent();
					var request = $.ajax({
						url: Site_Config.ajax_url,
						type: "POST",
						data: data,
						dataType: "json"
					});

					request.done(function( data ) {
						if ( 1 === data || '1' === data )
						{
							$parent.fadeOut().remove();
						}
					});
				}

			});
			e.preventDefault();
		});
	}


});


