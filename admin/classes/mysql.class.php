<?php
/**
 * Class      : Mysql Wrapper Class for Admin Center
 * Written By : Shawn Crigger of EWD
 * Written On : Dec 28, 2010
 * Revision   : 1
 * Copyright Eagle Web Designs & S-Vizion Software
 *
 *<p>
 *    _____      __      __ _       _              
 *  / ____|     \ \    / /(_)     (_)              
 * | (___  ______\ \  / /  _  ____ _   ___   _ __  
 *  \___ \|______|\ \/ /  | ||_  /| | / _ \ | '_ \ 
 *  ____) |        \  /   | | / / | || (_) || | | |
 * |_____/          \/    |_|/___||_| \___/ |_| |_|
 *</p>
 *
 *  @author     Shawn Crigger <shawn@eaglewebdesigns.com>
 *  @version    1.0.5 (last revision: Dec 28, 2009)
 *  @copyright  (c) 2010 - 2010 Eagle Web Designs & S-Vizion Software
 *  @package    VisionManagementSystems
 *
 **/  
class MySQL {

  /**
   *  Holds the last mysql error message 
   *
   *  @var    string
   */
  var $sLastError;		

  /**
   *  Holds the last query
   *
   *  @var    string
   */
  var $sLastQuery;		

  /**
   *  Holds the MySQL query result
   *
   *  @var    object
   */
  var $aResult;			

  /**
   *  Holds the total number of records returned
   *
   *  @var    integer
   */
  var $iRecords;			

  /**
   *  Holds the total number of Queries Executed
   *
   *  @var    integer
   */
  var $iQueries;			

  /**
   *  Holds the total number of records affected
   *
   *  @var    integer
   */
  var $iAffected;		

  /**
   *  Holds raw 'arrayed' results
   *
   *  @var    object
   */
  var $aRawResults;	

  /**
   *  Holds a single 'arrayed' result
   *
   *  @var    array
   */
  var $aArrayedResult;	 

  /**
   *  Holds multiple 'arrayed' results (usually with a set key)
   *
   *  @var    array
   */
  var $aArrayedResults;	 

  /**
   *  MySQL Hostname Defaults to 'localhost'
   *
   *  @var    string
   */
  var $sHostname; 	

  /**
   *  MySQL Login Username
   *
   *  @var    string
   */
  var $sUsername;

  /**
   *  MySQL User Login Password
   *
   *  @var    string
   */
  var $sPassword;	

  /**
   *  MySQL Database Name
   *
   *  @var    string
   */
  var $sDatabase;	
  
  /**
   *  Database Connection Link
   *
   *  @var    object
   */
  var $sDBLink;			
	
  /**
   *  Constructor of the class.
   *
   *  @access private
   */
  function MySQL()
  {
		global $db_base;
		
		$this->iQueries  = ( intval ( $this->iQueries ) > 0 ) ? $this->iQueries : 0;
		$this->sHostname = 'localhost';	
		$this->sUsername = $db_base['user'];	// MySQL Username
		$this->sPassword = $db_base['pass'];	// MySQL Password
		$this->sDatabase = $db_base['name'];	// MySQL Database
			
		$this->Connect();
  }
		
  /**
   *  Connects class to database 
   *
   *  @access private
   *  @return boolean     TRUE on success, FALSE on error.
   *                      If FALSE is returned, check the {@link sLastError} and {@link sLastQuery} property to see what went wrong
   */
  function Connect()
  {
			
    if ( $this->sDBLink )
      mysql_close($this->sDBLink);
  
		$this->sDBLink = mysql_connect($this->sHostname, $this->sUsername, $this->sPassword);
  
    if (!$this->sDBLink)
    {
      $this->sLastError = 'Could not connect to server: ' . mysql_error();
			//echo $this->sHostname . '<br />' . $this->sUsername. '<br />' . $this->sPassword;
      return false;
    }
  
    if(!$this->UseDB())
    {
      $this->sLastError = 'Could not connect to database: ' . mysql_error();
      return false;
    }
    return true;
  }
	

  /**
   *  Select database to use
   *
   *  @access private
   *  
   *  @return boolean 	Returns True on Success and False on Error, Error Message is Stored in {link sLastError}
   */
  function UseDB()
  {
    if (!mysql_select_db($this->sDatabase))
    {
      $this->sLastError ='Cannot select database: ' . mysql_error();
      return false;
    } else {
      return true;
    }
  }
	
  /**
   *  Executes MySQL query
   *
   *
   *  If Successful,
   *  	The Query Result is Stored in {link aResult}
   *  	Mysql Num Rows is Stored in   {link iRecords}
   *  	Affected Rows is Stored in    {link iAffected}
   *  	
   *  If False Error Message is Stored in {link sLastError}
   *
   *  @param		string 		$sSQLQuery	 Mysql Query String to Execute
   *
   *  @access 	public
   *
   *  @return 	boolean 	Returns True on Success and False on Error, Error Message is Stored in {link sLastError}
   */
  function Query($sSQLQuery)
  {
    $this->sLastQuery 	 = $sSQLQuery;
   	$this->iQueries      = $this->iQueries + 1;
    if($this->aResult 	 = mysql_query($sSQLQuery))
    {
      $this->iRecords 	= @mysql_num_rows($this->aResult);
      $this->iAffected	= @mysql_affected_rows();
      return true;
    } else {
      $this->sLastError = mysql_error();
      return false;
    }
  }

  /**
   *  Executes MySQL query and returns array of results
   *
   *
   *  If Successful returns Array of Query
   *  If False Error Message is Stored in {link sLastError}
   *
   *  @param   	string 		$sql	 Mysql Query String to Execute
   *
   *  @access 	public
   *
   *  @return 	mixed 	Returns Array on Success or False on Error, Error Message is Stored in {link sLastError}
   */  
  function Query_first( $sql )
  {
    $res = $this->Query ( $sql );
    
    if ( $res )
      return $this->ArrayResults();      
    
    return false;
  }
  /**
   *  Adds a record to the database based on the array key names
   *
   *  @param	array		$aVars		Array of Database Fields and Values to Insert Into New Row
   *  @param	string		$sTable		Table to Select From
   *  @param    array		$aExclude	Array of Fields to Exclude from the SQL Query
   *  
   *  @access 	public
   *
   *  @return 	boolean		Returns True or False Depending on Successful Query
   *  
   */  
  function Insert($aVars, $sTable, $aExclude = '')
  {
    // Catch Exceptions
    if($aExclude == '')
    {
      $aExclude = array();
    }

		if ( $aVars == '' || !is_array ( $aVars ) )
			return false;
		
    array_push($aExclude, 'MAX_FILE_SIZE');
    // Prepare Variables
    $aVars = $this->SecureData($aVars);
    
    $sSQLQuery = 'INSERT INTO `' . db_prefix . $sTable . '` SET ';
    foreach($aVars as $iKey=>$sValue)
    {
      if(in_array($iKey, $aExclude))
      {
        continue;
      }
      $sSQLQuery .= '`' . $iKey . '` = "' . $sValue . '", ';
    }
    
    $sSQLQuery = substr($sSQLQuery, 0, -2);
    		
    if($this->Query($sSQLQuery))
    {
      return true;
    } else {
      return false;
    }
  }
	
  /**
   *  Deletes a record from the database
   *
   *  @param	string		$sTable		Table to Select From
   *  @param	array		$aWhere		Array of Conditions That Must be Met
   *  @param    string		$sLimit		Number of Rows to Pull
   *  @param	boolean		$bLike		Use Like Statements or Equal To Statements, Defaults to Equal To
   *  
   *  @access 	public
   *
   *  @return 	boolean		Returns True or False Depending on Successful Query
   *  
   */  
  function Delete($sTable, $aWhere='', $sLimit='', $bLike=false)
  {

    $sSQLQuery = 'DELETE FROM `' . db_prefix . $sTable . '` WHERE ';
    if(is_array($aWhere) && $aWhere != '')
    {
      // Prepare Variables
      $aWhere = $this->SecureData($aWhere);
      
      foreach($aWhere as $iKey=>$sValue)
      {
        $sSQLQuery .= ($bLike) ? '`' . $iKey . '` LIKE "%' . $sValue . '%" AND ' : '`' . $iKey . '` = "' . $sValue . '" AND ';
      }
  
      $sSQLQuery = substr($sSQLQuery, 0, -5);
    }
  
    if($sLimit != '')
      $sSQLQuery .= ' LIMIT ' .$sLimit;
  
    if ( $this->Query( $sSQLQuery ))
      return true;
    else
      return false;
  
  }
	
  /**
   *  Selects a $aWhat from $sFrom Where $aWhere is True
   *
   *  @param		string		$sFrom		Table to Select From
   *  @param		array			$aWhere		Array of Conditions That Must be Met
   *  @param		array			$aWhat		Array of Fields to Select Defaults to All
   *  @param		string		$sOrderBy	Field to Order By
   *  @param 		string		$sLimit		Number of Rows to Pull
   *  @param		boolean		$bLike		Use Like Statements or Equal To Statements, Defaults to Equal To
   *  @param  	string		$sOperand	The AND Operator Defaults to AND
   *  
   *  @access 	public
   *
   *  @return 	boolean		Returns True or False Depending on Successful Query
   *  
   */
  function Select($sFrom, $aWhere='', $aWhat='*', $sOrderBy='', $sLimit='', $bLike=false, $sOperand='AND')
  {
  // Catch Exceptions
    if(trim($sFrom) == '')
      return false;
  
		if ( is_array ($aWhat) )
		{
			$what  = '';
			$aWhat = $this->SecureData($aWhat);

			foreach ( $aWhat as $k=>$v )
			{
				$what .= ( $v != '' ) ? "$v, " : '';		
			}
			$what = substr ( $what, 0, -2);	  
		} else {
			$what = ' * ';
		}
  
    $sSQLQuery = 'SELECT ' . $what . ' FROM `' . db_prefix . $sFrom . '` WHERE ';
  
    if( is_array( $aWhere ) && $aWhere != '' )
    {
  // Prepare Variables
      $aWhere = $this->SecureData($aWhere);
  
      foreach($aWhere as $iKey=>$sValue)
      {
        $sSQLQuery .= ( $bLike ) ? '`' . $iKey . '` LIKE "%' . $sValue . '%" ' . $sOperand . ' ' : '`' . $iKey . '` = "' . $sValue . '" ' . $sOperand . ' ';
      }
  
      $sSQLQuery = substr($sSQLQuery, 0, -5);
  
    } else {
  
      $sSQLQuery = substr($sSQLQuery, 0, -7);
    }
  
    if ( $sOrderBy != '' )
      $sSQLQuery .= ' ORDER BY ' .$sOrderBy;
  
  
    if( $sLimit != '' )
      $sSQLQuery .= ' LIMIT ' .$sLimit;
  
  
    if ( $this->Query( $sSQLQuery ))
    {
//      if( $this->iRecords > 0)      
//        $this->ArrayResults();
      return true;
    } else {
      return false;
    }
  
  }
	
	
	  /**
   *  Selects a Single Row from a Table Where Condition is True
   *
   *  @param		string		$sFrom		Table to Select From
   *  @param		array			$aWhere		Array of Conditions That Must be Met
   *  @param		array			$aWhat		Array of Fields to Select Defaults to All
   *  @param		string		$sOrderBy	Field to Order By
   *  
   *  @access 	public
   *
   *  @return 	mixed 		Returns Array of Results or False Depending on Successful Query
   *  
   */
	function First ( $sFrom, $aWhere='', $aWhat='', $sOrderBy='' )
	{
		$this->Select($sFrom, $aWhere, $aWhat, $sOrderBy, 1);
		
		if ( $this->iRecords > 0 )
			return $this->ArrayResults();
		
		return false;
	}
	
	
  /**
   *  Updates a record in the database based on WHERE
   *
   *  @access public
   */
  function Update ( $sTable, $aSet, $aWhere, $aExclude = '' )
  {
  // Catch Exceptions
    if( trim( $sTable ) == '' || !is_array( $aSet ) || !is_array( $aWhere ))
      return false;
  
    if($aExclude == '')
      $aExclude = array();
  
    array_push($aExclude, 'MAX_FILE_SIZE');
  
    $aSet 	= $this->SecureData($aSet);
    $aWhere = $this->SecureData($aWhere);
  
    // SET
  
    $sSQLQuery = 'UPDATE `' . db_prefix . $sTable . '` SET ';
  
    foreach ( $aSet as $iKey=>$sValue )
    {
      if ( in_array( $iKey, $aExclude ))      
        continue;
      
      $sSQLQuery .= '`' . $iKey . '` = "' . $sValue . '", ';
    }
  
    $sSQLQuery = substr($sSQLQuery, 0, -2);
  
    // WHERE
  
    $sSQLQuery .= ' WHERE ';
  
    foreach( $aWhere as $iKey=>$sValue )
      $sSQLQuery .= '`' . $iKey . '` = "' . $sValue . '" AND ';
  
  
    $sSQLQuery = substr($sSQLQuery, 0, -5);
  
    if ( $this->Query( $sSQLQuery ))
      return true;
    else
      return false;
  
  }
	
  /**
   *  Fetchs One Single Query Result
   *
   *  @access private
   *
   *  @return	array 	
   */
  function ArrayResults()
  {
    $this->aArrayedResult = mysql_fetch_array ($this->aResult ) or trigger_error ( mysql_error() );
    return $this->aArrayedResult;
  }
	
  
  /**
   *  'Arrays' multiple results with a key
   *
   *  @param	string		$sKey	Key to to a 3D Array of Mysql Results
   *  @access   private
   *
   *  @return	array 	
   */
  function ArrayResultsWithKey($sKey='id')
  {
    if( isset( $this->aArrayedResults ))
      unset($this->aArrayedResults);
  
    $this->aArrayedResults = array();
    while( $aRow = mysql_fetch_assoc($this->aResult) )
    {
      foreach( $aRow as $sTheKey => $sTheValue)
        $this->aArrayedResults[$aRow[$sKey]][$sTheKey] = $sTheValue;
    }
  
    return $this->aArrayedResults;
  }
	
  
  /**
   *  Performs a 'mysql_real_escape_string' on the entire array/string
   *
   *  @param  	mixed 		$aData		Data to be Secured for Mysql Query
   *  @access 	private
   *
   *  @return	mixed
   */
  function SecureData( $aData )
  {
    if ( is_array( $aData ))
    {
      foreach ( $aData as $iKey=>$sVal )
      {
        if ( !is_array($aData[$iKey] ))
          $aData[$iKey] = mysql_real_escape_string( stripslashes ( $aData[$iKey] ));        
      }
    } else {			
      $aData = mysql_real_escape_string( stripslashes ( $aData ) );
    }
  
    return $aData;
  }


}
?>