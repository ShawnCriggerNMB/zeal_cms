<?php
/**
 * Abstract class for extending the Admin Modules from.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage admin
 * @category modules
 * @since  3.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */

if ( !defined('BASE') ) die('No Direct Script Access');

// ------------------------------------------------------------------------

abstract class Module_Base {

	/**
	 * Placeholder for any form errors.
	 * @access protected
	 * @var array|null
	 */
	protected $_errors = NULL;

	/**
	 * Placeholder for any form validation rules.
	 * @access protected
	 * @var array|null
	 */
	protected $_rules = NULL;

	/**
	 * Placeholder for human readable field names.
	 * @access protected
	 * @var array|null
	 */
	protected $_human_field_names = NULL;

	/**
	 * Placeholder for any $_POST variable filtering rules, like strtolower, trim, etc.
	 * @access protected
	 * @var array|null
	 */
	protected $_filter_rules = NULL;

	/**
	 * Placeholder for Record ID.
	 * @var integer|null
	 */
	protected $_id = NULL;

	/**
	 * Placeholder for action, either 'insert' or 'update'
	 * @var string
	 */
	protected $_action = 'manage';

	protected $_db = NULL;

	public $form_validator = NULL;

	// ------------------------------------------------------------------------

	/**
	 * Sets class variables, can be overrode in the extending class.
	 * @param  object  $pdo  Instance of PDO DB connection for now.
	 */
	public function __construct( )
	{
//		$this->_db =& $pdo;

		$this->_action = 'manage';
		if ( isset( $_REQUEST['action'] ) )
		{
			$this->_action = $_REQUEST['action'];
		}

		if ( isset( $_REQUEST['id'] ) )
		{
			$this->_id = (int) $_REQUEST['id'];
		}

		if ( ! class_exists( 'GUMP' ) ) {
			throw new BadMethodCallException( 'Form validation class "GUMP" does not exist.', 404 );
		}

		$this->form_validator = new GUMP();
		if ( count( $this->_human_field_names ) > 0)
		{
			foreach ( $this->_human_field_names as $field => $readable_name )
			{
				$this->form_validator->set_field_name( $field, $readable_name );
			}
		}

		// Load datatable assets.
		if ( 'manage' === $this->_action OR 'datatable' === $this->_action )
		{
			add_action( 'admin_enqueue_scripts', function() {
				cms_enqueue_style ( 'admin-dataTables', site_url( '/admin/assets/css/dataTables.bootstrap.css') );
				cms_enqueue_script( 'admin-dataTables', site_url( '/admin/assets/js/jquery.dataTables.min.js') );
				cms_enqueue_script( 'admin-dataTables-bootstrap', site_url( '/admin/assets/js/dataTables.bootstrap.js') );
			});
		}

		if ( 'POST' === $_SERVER['REQUEST_METHOD'] && isset( $_POST['submit'] ) )
		{
			$this->handle_validation();
			if ( ! empty( $this->_errors ) OR count ( $this->_errors ) > 0 )
			{
//				$this->_action = 'update';
			} else {

				return $this->handle_submit();
			}
		}

/*
		switch ( $this->_action )
		{
			case 'update':
			case 'insert':
				$this->handle_form();
				break;
			case 'delete':
				$this->handle_delete();
				break;
			case 'submit':
				$this->handle_submit();
				break;
			default:
			case 'manage':
				$this->handle_datatable();
				break;
		}
*/

	}


	// ------------------------------------------------------------------------

	/**
	 * Handles form validation.
	 * @return Module_Base
	 */
	public function handle_validation()
	{
		if ( 'POST' !== $_SERVER['REQUEST_METHOD'] ) return $this;
		if ( ! is_array( $this->_rules ) OR FALSE === count( $this->_rules ) ) return $this;

		GUMP::add_validator( 'valid_path', function($field, $input, $param = NULL) {
			ChromePhp::log( $input[$field] );
		    return (bool) is_dir( $input[$field] );
		});

		$this->form_validator->validation_rules( $this->_rules );

		if ( isset( $this->_filter_rules ) && count( $this->_filter_rules ) > 0 )
		{
			$this->form_validator->filter_rules( $this->_filter_rules );
		}
		$_POST = $this->form_validator->sanitize( $_POST );

		$is_valid = $this->form_validator->run( $_POST );
		if ( FALSE === $is_valid )
		{
			$this->_errors = $this->form_validator->get_readable_errors();
		}

		return $this;
	}

	// ------------------------------------------------------------------------

	/**
	 * Displays data-table of records.
	 * @abstract
	 * @return void
	 */
	abstract function handle_datatable();

	// ------------------------------------------------------------------------

	/**
	 * Displays view file for form.
	 * @abstract
	 * @return void
	 */
	abstract function handle_form();

	// ------------------------------------------------------------------------

	/**
	 * Handles Deleting record from database.
	 * @abstract
	 * @return void
	 */
	abstract function handle_delete();

	// ------------------------------------------------------------------------

	/**
	 * Handles Submitting form and any Database methods.
	 * @abstract
	 * @return void
	 */
	abstract function handle_submit();


	// ------------------------------------------------------------------------


	// ------------------------------------------------------------------------



}
