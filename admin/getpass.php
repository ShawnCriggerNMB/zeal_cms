<?php	
	//if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Page       : Reset Admin Password Page
// Written By : Shawn Crigger of EWD
// Written On : Jun 8, 2010
// Updated By : Chuck Bunnell of EWD
// Updated On : Jan 6, 2014
// Copyright Zeal Technologies
//***************************************************/

  session_start();

  require_once('functions.inc.php');

	$config        = fetch_config_data();
	$sitename      = $config['site_name'];
  $message       = '';
	$site_base_url = $config['site_base_url'];
  $dnremail      = 'donotreply@'.substr ( $site_base_url, 11);

  function create_password()
  {    
    $newpassword=''; 
		srand((double)microtime()*1000000); 
		$chars = array ( 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0'); 

		for ($rand = 0; $rand <= 7; $rand++)
    { 
			$random       = rand(0, count($chars) -1); 
			$newpassword .= $chars[$random]; 
		} //end for

    return $newpassword;     
  }

  if ( !empty ( $_POST ))
  {    
    if (empty($_POST["email"]) )
    {
      $error = "An email address is required to send a password to.";
    } else {
      $email = $_POST['email'];

			$stmt = $db->prepare("SELECT id FROM ".db_prefix . admin_table." WHERE username = ? OR email = ?");
			$stmt->execute(array($email,$email));
      
			if ($stmt->rowCount() == 0)
			{		
				$user = false;
			} else {
				$user = true;
				$r = $stmt->fetch(PDO::FETCH_ASSOC);
				$id = $r['id'];
			}
			
			
      if ( $user != false )
      {
        $newpassword = create_password();

				$errno = 0;
		
				try {
					$stmt = $db->prepare("UPDATE " . db_prefix . "admin SET
					pass = ?
					WHERE id = ?");
		
					$stmt->execute(array(
						md5($newpassword),
						$id
					));
				}
				catch(PDOException $ex){
					$errno = $ex->getCode();
				}
		
				//print_mysql_message ( $errno , 'password', $user, $type ) ;
				
				$message .= "Your new password for the admin login is ".$newpassword.".\n\n Use this password to login and change this password to a password you can remember.";

    		mail($email, "Password Reset", $message, "From: ".$dnremail."");

        $message = 'Your password was successfully reset and sent to your email address on file.<br /><br />' . "\n" .' <span class="red"><strong><a href="'.$site_base_url.'/admin">click here to return to the login page.</a></strong></span>';

				session_write_close();
        //header("Location: index.php");

      } else {

        $error = "You have entered an invalid email address. Please try again.";

      }//end if
    }//end if
  } //end if    
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $sitename; ?> - Reset Password</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="javascript/resize.js"></script>
<script type="text/javascript">
$(window).load(function () { 
   equalCols('#contentwrapper','#leftcolumn');
});
</script>
</head>

<body onLoad="document.lostpw.email.focus()" style="background:#36393D;">
  <div id="maincontainer">
    <div id="topsection" >
			<img src="images/header_logo.png" alt="" />
      <p class="coname"><?php echo $sitename; ?></p>
      <p class="adminarea">Administration Area</p>
    </div>

   	<div style="background:#303137;min-height:500px;padding-top:25px;">
      <div style="width:400px;margin:0 auto;border:1px solid #CCC;padding:10px;background:#fff;"> 
      	<p style="color:#B02B2C;font-size:140%; text-align:center; margin-bottom:15px;"><?php echo $sitename; ?><br /><br />
        	<span style="">Administrators: Retrieve Password</span></p>

        <hr style="margin:0 30px;" />

				<?php
          if ( $error != '' )
            echo '<p style="text-align:center; margin-top:15px;">' . $error . '</p>';

          if ( $message != '' )
            echo '<p style="text-align:center; margin-top:15px;">' . $message . '</p>';
        ?>

          <div class="spacer" style="margin-bottom:30px;">&nbsp;</div>

          <?php
						if ($message == '')
						{ ?>

            <form action="<?PHP $_SERVER['PHP_SELF']; ?>" method="post" name="lostpw" id="lostpw">
              <table style="width:90%; border:none">
                <tr>
                  <td style="width:51%; text-align:right; padding-right:2px;">Email Address:</td>
                  <td style="width:49%; text-align:left; padding-left:2px;">
                    <input name="email" type="text" id="email" size="30" value="<?php echo $_POST['email']; ?>" /></td>
                </tr>

                <tr>
                  <td colspan="2" style="text-align:center; padding-top:15px;">
                  <input style="padding:0 5px;" name="submit" type="submit" id="submit" value="Retrieve" /></td>
                </tr>
              </table>
            </form> 

          <?php } ?>       

          <p style="text-align:center; margin-top:25px;" ><a href="<?php echo $site_base_url; ?>">click here to return to <?php echo $site_base_url; ?></a></p>
			</div> 

		</div>
		
		<div id="footer" style="min-height:20px !important; height:20px; "><a style="float:right;padding-right:35px; color: #C8DC27;" href="http://www.eaglewebdesigns.com/">Powered by Zeal Technologies</a></div>

  </div>  
</body>
</html>