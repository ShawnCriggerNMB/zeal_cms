<?php
/**
 * Core functions for Zeal Technologies CMS Admin area only.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage admin
 * @category helper_functions
 * @since  1.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */

// written on May 10, 2010, updated on May 03, 2014, current re-write on Oct 10, 2014.

require( __DIR__ . '/../includes/cms-load.php' );
require( ABSPATH . '/includes/init.php' );
if ( ! defined( 'MODULE_PATH' ) )
{
	define( 'MODULE_PATH', ABSPATH . '/includes/modules/' );
}

function create_form_actions( $id = NULL )
{
	$vars = array(
		'tool'   => $_GET['tool'],
		'action' => 'update',
		'id'     => $id
	);
	$link   = site_url( '/admin/?' . http_build_query( $vars ) );
	$html   = "<a href=\"{$link}\" data-id=\"{$id}\" title=\"Update this record\">UPDATE</a> | ";

	$vars['action'] = 'delete';
	$link   = site_url( '/admin/?' . http_build_query( $vars ) );
	$html   .= "<a href=\"{$link}\" class=\"delete-record\" data-id=\"{$id}\" title=\"Update this record\">DELETE</a>";
	return $html;
}


View::set_view_dir(  'admin/views/' );

require( ABSPATH . '/' . ADMIN_DIR . '/core-actions.php' );
require( ABSPATH . '/' . ADMIN_DIR . '/classes/module.class.php' );

//! global $db,Variables Used in Most Modules
$known_photo_types = array('image/pjpeg' => 'jpg','image/jpeg'  => 'jpg','image/gif'   => 'gif','image/bmp'   => 'bmp','image/x-png' => 'png','image/png'   => 'png');
$known_file_types  = array('application/pdf' => 'pdf','application/msword' => 'doc','application/rtf' => 'rtf');
$cms_uploads = '/uploads/';
ini_set('display_errors', 'STDOUT');
define('SEND_ERROR_EMAIL_TO', false);

error_reporting(E_ALL);
//set_error_handler('output_all_errors');
ini_set('docref_root', 'http://www.php.net/manual/en/');
ini_set('docref_ext', '.php');


// ------------------------------------------------------------------------

/**
 * Sanitize data from db
 * @param  string $data string to sanitize
 * @return string
 */
function sanitizeFromdb ( $data )
{
	$r_data = htmlspecialchars ( $data );

	return $r_data;
}

// ------------------------------------------------------------------------

/**
 * Sanitize data for display (from form), runs stripsslashes and strip_tags on string.
 * Not used for db input
 * @param  string $data  string to be sanitized
 * @return string
 */
function sanitize4user ( $data )
{
	$r_data = stripslashes ( $data );
	$r_data = strip_tags ( $r_data );
	return $r_data;
}


// ------------------------------------------------------------------------

/**
 * Displays Left Side Menu
 *
 * Displays the Left Side Menu based on user access levels as well as available applications on this site.
 *
 * @return  string
 */
function display_menu()
{
	global $wpdb, $current_user;
	$results = $wpdb->get_results("SELECT * FROM {$wpdb->modules} WHERE active = 1 AND display = 1 ORDER BY weight,identifier");

//	$results = $q->fetchAll( PDO::FETCH_OBJ );
	$menu = NULL;

	foreach ( $results as $row )
	{
		$cls = ( 'superadmin' === $row->level ) ? " style=\"background:#B28500;\" " : '';
		$indent = $row->identifier;
		$name   = $row->name;
		$icon   = isset( $row->icon ) ? "fa {$row->icon} " : '';
		$link   = admin_url() . "index.php?tool={$row->identifier}";
//		if ( check_level ( $row['identifier'] ) )
		$menu .= "\t<li>\n<a {$cls} href=\"{$link}\" >\n<i class=\"{$icon} fa-3x\"></i> {$name}</a>\n</li>\n";
//		echo '<li><a ' . $cls . ' href=\'index.php?tool=' . $row['identifier'] . '\' >' . $row['name'] . '</a></li>' . "\n";

    }//end while

    return $menu;

/*
    $data = get_module_settings ('admin_nav');

    foreach ( $data as $k=>$v)
    {
    	echo '<li><a href="' . $v . '" target="_blank">' . ucwords ( $k ) . '</a></li>' . "\n";
    }
*/

}//end function


/**
* Return HTML string for Top Navigation
*
*
* @return string String of Links for Top Navigation
*/
function create_top_nav()
{
    global $userinfo, $site_base_url;

    $top_nav     = '<ul>';
    $clock			 = '<span id="clock">&nbsp;</span>';
    $dashboard	 = '<li><a href="index.php?tool=home" title="Admin Dashboard" >Dashboard</a></li>';
    $home_page   = '<li><a href="' . $site_base_url . '/" target="_blank" title="Open Website in a New Window or Tab" >My Website</a></li>';
    $cur_login   = '<li>Logged in as : ' . $userinfo['name'] . '</li>';
    $my_profile  = '<li><a href="index.php?tool=admin&action=update&id=' . $userinfo['id'] . '" title="Edit your user account, including changing your password">My Profile</a></li>';
    $log_out     = '<li><a href="index.php?tool=logout" title="Log out of current session">Logout</a></li>';

    $top_nav    .= $dashboard . $home_page . $my_profile . $cur_login . $log_out . ' | ' . $clock;

    $top_nav    .= '</ul>' . "\n";

    return $top_nav;
}


// ------------------------------------------------------------------------

function load_module( $module = 'dashboard' )
{
	$path = MODULE_PATH . $module;
	$file = $path . '/' . $module . '_admin.php';
	if ( ! is_dir( $path ) ) die( $path );
	if ( ! is_file( $file ) ) die( $file );

	ob_start();
	require( $file );
	if ( 'dashboard' !== $module )
	{
		$class = ucfirst( strtolower( $module ) ) . '_Admin';
		$GLOBALS["current_module"] = new $class( $GLOBALS['wpdb'] );
		handle_request();
	}
	$module_html = ob_get_clean();
	return $module_html;
}

function handle_request()
{
	$action = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] : 'datatable';
	$action = strtolower( $action );
	$action = 'handle_' . $action;
	if ( method_exists( $GLOBALS['current_module'], $action ) )
	{
		call_user_func( array( $GLOBALS['current_module'], $action ) );
	}

}

// ------------------------------------------------------------------------

function create_sidebar()
{
	global $wpdb, $current_user;

	$modules = $wpdb->get_results("SELECT identifier, name, level, icon FROM {$wpdb->modules} WHERE `active` = '1' AND `installed` = '1' AND `display` = '1' ORDER BY weight");
	$sidebar = NULL;
	$default_icon = 'fa fa-fw fa-gear';

	foreach ( $modules as $module )
	{

		if ( ! $current_user->has_cap( 'superadmin' ) && ! $current_user->has_cap( $module->level ) ) continue;

		$link     = admin_url( "/?tool={$module->identifier}");
		$icon     = empty( $module->icon ) ? $default_icon : $module->icon;
		$active   = ( isset( $_REQUEST['tool'] ) && $module->identifier == $_REQUEST['tool'] ) ? ' active ' : '';
		$sidebar .= <<<EOL
                    <li class="{$active}">
                        <a href="{$link}"><i class="{$icon}"></i> {$module->name}</a>
                    </li>
EOL;

	}

	return $sidebar;
}


// ------------------------------------------------------------------------


if ( ! function_exists( 'dump') ) :

  /**
    * Debug Helper
    *
    * Outputs the given variable(s) with formatting and location
    *
    * @access        public
    * @param        mixed    variables to be output
    */
  function dump()
  {
      list($callee) = debug_backtrace();
      $arguments = func_get_args();
      $total_arguments = count($arguments);

      echo '<fieldset style="background: #fefefe !important; border:2px red solid; padding:5px">';
      echo '<legend style="background:lightgrey; padding:5px;">'.$callee['file'].' @ line: '.$callee['line'].'</legend><pre>';
      $i = 0;
      foreach ($arguments as $argument)
      {
          echo '<br/><strong>Debug #'.(++$i).' of '.$total_arguments.'</strong>: ';
          var_dump($argument);
      }

      echo "</pre>";
      echo "</fieldset>";
  }

endif;

// ------------------------------------------------------------------------


