<?php if ( !defined('BASE') ) die('No Direct Script Access');

// ------------------------------------------------------------------------

/**
 * Sets the meta title tag for the admin area.
 * @param  [string] $module_title Module Name
 * @return string
 */
function admin_page_title( $module_title = NULL )
{
	$title = cms_get_option( 'site_name' );

	if ( ! is_null ( $module_title ) && ! empty( $module_title ) )
	{
		$title .= " {$module_title}";
	}
	$title .= " - Administrative Tools";
	return $title;
}

add_filter( 'admin_page_title', 'admin_page_title', 10, 1 );

// ------------------------------------------------------------------------

function admin_default_assets()
{
	global $module_title;
	$js_vars = array(
		'site_url' => site_url(),
		'ajax_url' => site_url('/do-ajax.php'),
		'module'   => $module_title,
	);

	cms_enqueue_script( 'jquery' );
	cms_enqueue_script( 'tinyMCE' );
	cms_enqueue_style ( 'fontawesome' );

	cms_enqueue_script( 'admin-bootstrap' );
	cms_enqueue_style ( 'admin-bootstrap' );
	cms_enqueue_script( 'admin-bootbox', site_url( '/admin/assets/js/vendor/bootbox.js') );
	cms_enqueue_style ( 'sidebar-nav', site_url( '/admin/assets/css/sidenav.css') );
	cms_enqueue_script( 'metisMenu', site_url( '/admin/assets/js/jquery.metisMenu.js') );
	cms_enqueue_script( 'main', site_url( '/admin/assets/js/main.js') );
	cms_localize_script( 'main', 'Site_Config', $js_vars );
}

add_action( 'admin_enqueue_scripts', 'admin_default_assets' );

// ------------------------------------------------------------------------

add_action( 'admin_sidebar', 'display_menu' );

// ------------------------------------------------------------------------

// ------------------------------------------------------------------------

// ------------------------------------------------------------------------

function do_admin_footer( $hook_suffix = NULL )
{
	/**
	 * Print scripts or data before the default footer scripts.
	 *
	 * @since 1.2.0
	 *
	 * @param string $data The data to print.
	 */
	do_action( 'admin_footer', '' );

	/**
	 * Prints any scripts and data queued for the footer.
	 *
	 * @since 2.8.0
	 */
	do_action( 'admin_print_footer_scripts' );

	/**
	 * Print scripts or data after the default footer scripts.
	 *
	 * The dynamic portion of the hook name, $GLOBALS['hook_suffix'],
	 * refers to the global hook suffix of the current page.
	 *
	 * @since 2.8.0
	 *
	 * @param string $hook_suffix The current admin page.
	 */
	do_action( "admin_footer-" . $GLOBALS['hook_suffix'] );

	// get_site_option() won't exist when auto upgrading from <= 2.7
	if ( function_exists('get_site_option') ) {
		if ( false === get_site_option('can_compress_scripts') )
			compression_test();
	}
}

// ------------------------------------------------------------------------

/**
 * Called at the end of the admin <head> element to fire all the different script loading actions.
 * @param  [string] $hook_suffix
 * @return void
 */
function do_admin_head( $hook_suffix = NULL )
{
	/**
	 * Enqueue scripts for all admin pages.
	 *
	 * @since 2.8.0
	 *
	 * @param string $hook_suffix The current admin page.
	 */
	do_action( 'admin_enqueue_scripts', $hook_suffix );

	/**
	 * Fires when styles are printed for a specific admin page based on $hook_suffix.
	 *
	 * @since 2.6.0
	 */
	do_action( "admin_print_styles-$hook_suffix" );

	/**
	 * Fires when styles are printed for all admin pages.
	 *
	 * @since 2.6.0
	 */
	do_action( 'admin_print_styles' );

	/**
	 * Fires when scripts are printed for a specific admin page based on $hook_suffix.
	 *
	 * @since 2.1.0
	 */
	do_action( "admin_print_scripts-$hook_suffix" );

	/**
	 * Fires when scripts are printed for all admin pages.
	 *
	 * @since 2.1.0
	 */
	do_action( 'admin_print_scripts' );

	/**
	 * Fires in <head> for a specific admin page based on $hook_suffix.
	 *
	 * @since 2.1.0
	 */
	do_action( "admin_head-$hook_suffix" );

	cms_print_styles();
	/**
	 * Fires in <head> for all admin pages.
	 *
	 * @since 2.1.0
	 */
	do_action( 'admin_head' );

}

add_action( 'left_sidebar', 'print_left_sidebar' );

function print_left_sidebar( $vars = array() )
{
	View::show( 'sidebar-new', $vars );
}


add_filter( 'datatable_yes_no', 'cms_yes_no', 10, 2 );

function cms_yes_no( $val = 0 )
{
	$val = (int) $val;
	return ( 0 === $val ) ? '<span class="text-danger">No</span>' : '<span class="text-success">Yes</span>';
}


