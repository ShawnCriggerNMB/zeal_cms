 <?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Coupons Module

// Written By : Darrell Bessent of EWD

// Written On : May 16, 2013

// Revision   : 164

// Updated by : Darrell Bessent of EWD

// Updated On : May 16, 2013

// Description: This module is used with websites that 

//							need to keep track of businesses or use

//              other modules that rely on this businesses module. 

// Copyright Zeal Technologies 

//***************************************************/



/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);





$mod_config  = get_module_settings ( 'businesses' );



$upload_file_url  = $site_base_url . '/cmsuploads/businesses/';

$upload_file_dir  = $site_base_dir . '/cmsuploads/businesses/';



$upload_image_url  = $site_base_url . '/cmsuploads/businesses/';

$upload_image_dir  = $site_base_dir . '/cmsuploads/businesses/';



$width = '';

$height = '100';



function execute()

{

	switch($_GET['action'])

	{

		case 'update':

			update();

			break;

		case 'add':

			update();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Slider Content

 *

 *

 **************************************************************/



function manage()

{



  global $upload_image_dir,$upload_image_url,$upload_file_dir,$upload_file_url;

  

  $i    = 0;

  $link = '<a href="./?tool=businesses&action=add">Add Client Business</a>';

  

  print_header('Manage Client Businesses',$link);

  

  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 

       ' 	  <thead>' . "\n" . 

       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" .

       ' 			  <th><h3>Business</h3></th>' . "\n" . 

			 ' 			  <th><h3>Locations</h3></th>' . "\n" .	

			 ' 			  <th><h3>Website</h3></th>' . "\n" .

			 ' 			  <th><h3>Image</h3></th>' . "\n" .    

       ' 			  <th style="width:50px; text-align:center;"><h3>Order</h3></th>' . "\n" . 

       ' 			  <th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . 

       ' 			  <th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" . 

       '		 </tr></thead><tbody>' . "\n";

  

  $results = mysql_query('SELECT * FROM ' . db_prefix . 'businesses ORDER BY weight,label');

  $pages   = mysql_num_rows($results);

  while ($row = mysql_fetch_array($results))

  {    



    $i++;

    

    $image_link 				 = 'No File Uploaded';

		$file_link 					 = 'No File Uploaded';

    $label							 = $row['label'];

		$notes							 = $row['notes'];

		$website						 = $row['website'];

		

    $image_filename = $upload_image_dir . $row['image'];

		$file_filename = $upload_file_dir . $row['file'];

    

    $image_link = ( file_exists ( $image_filename ) ) ? '<a href="' . $upload_image_url . $row['image'] . '" class="lightbox">' . $row['image'] . '</a>' : $image_link;

    $file_link = ( file_exists ( $file_filename ) ) ? '<a href="' . $upload_file_url . $row['file'] . '" target="_blank">' . $row['file'] . '</a>' : $file_link;

			          

    $act = yes_no ( $row['active'] );

    $notes = substr($notes, 0, 60) . '...';

		

    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

    echo '<td style="padding-left:20px;">' . $label . '</td>' . "\n"; 	

		echo '<td style="padding-left:20px;">' . $notes . '</td>' . "\n"; 

		echo '<td style="padding-left:20px;">' . $website . '</td>' . "\n";	

		echo '<td style="padding-left:20px;">' . $image_link . '</td>' . "\n"; 		

    echo '<td text-align:center;">' . $row['weight'] . '</td>' . "\n"; 

    echo '<td text-align:center;">' . $act . '</td>' . "\n"; 

    echo '<td valign="middle"><strong><a href="./?tool=businesses&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=businesses&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

    echo '</tr>' . "\n";

	  

  }//end while



  echo '</tbody></table>';



  $pages = ( $pages > 20 ) ? true : false;

	

  echo_js_sorter ( $pages );

             

}//end function



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Banner Form for User Input

 *

 *

 **************************************************************/



function add( $errors = '' )

{

  global $id, $action, $site_base_url, $site_base_dir, $upload_image_url, $upload_image_dir, $upload_file_url, $upload_file_dir, $mod_config, $height, $width;

   

   

  if ( $errors )

  {

   echo '<div class="error_message">';

   if ( in_array('label', $errors ) )

     echo '* You must fill out a business name.<br/>';

   echo '</br></div>' . "\n";

  } else {

    $c = ( $action == 'update' ) ? 1 : 0;

    print_notice_message ( $c );

  }

  

  if ($action == "update")

  {

   // Run the query for the existing data to be displayed.

   $results = mysql_query('SELECT * FROM ' . db_prefix . 'businesses WHERE id = "' . $id . '"');

   $row 				= mysql_fetch_array($results);

  } 

	if (!empty ($_POST))

	{	

   $row     = sanitize_vars ($_POST);

  }//end if

  

	echo '<div class="notice_message">NOTES : <br /> 

					<ul>

						<li>The `BUSINESS` is the label for the banner ad post.</li>

						<li>The `ADDRESS` is the address of the business.</li>

						<li>The `CITY` is the city of the business.</li>

						<li>The `STATE` is the state of the business.</li>

						<li>The `ZIP` is the zip of the business.</li>

						<li>The `PHONE #1` is the main phone number of the business.</li>

						<li>The `PHONE #2` can be used for fax, cell, or any other form of phone contact.</li>

						<li>The `LOCATIONS` are alternate address locations you may wish to add about the business.</li>

						<li>The `IMAGE` is for when you want to display an image with the business.</li>

						<li>The `WEBSITE` is the business website url.</li>

						<li>The `MAP` is use to insert the iframe code from google (Notice: Map currently only supports google maps).</li>

						<li>The `ORDER` is the order weight in which you wish for the businesses to be listed.</li>

						<li>The `ACTIVE` is used to control an advertisments status or disable an advertisment before the ending date.</li>

					<ul>	

				</div>';

	

  echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

  $t_index = 1;

  echo '   <tr>' . "\n".

       '      <td > <label for="label">Business</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="label" id="label" value="'. $row['label'] .'" size="45" tabindex="' . $t_index . '" maxlength="64" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

			 

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="address">Address</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="address" id="address" value="'. $row['address'] .'" size="45" tabindex="' . $t_index . '" maxlength="256" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

	

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="city">City</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="city" id="city" value="'. $row['city'] .'" size="45" tabindex="' . $t_index . '" maxlength="64" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

	

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="state">State</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="state" id="state" value="'. $row['state'] .'" size="45" tabindex="' . $t_index . '" maxlength="2" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

	

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="zip">Zip</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="zip" id="zip" value="'. $row['zip'] .'" size="45" tabindex="' . $t_index . '" maxlength="5" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

	

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="phone1">Phone #1</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="phone1" id="phone1" value="'. $row['phone1'] .'" size="45" tabindex="' . $t_index . '" maxlength="12" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

			 

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="phone2">Phone #2</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="phone2" id="phone2" value="'. $row['phone2'] .'" size="45" tabindex="' . $t_index . '" maxlength="12" /></td>  ' . "\n" . 

       '   </tr>' . "\n";		 

	

	$t_index++;

	$r_notes = str_replace('<br />', '\n', $row['notes']);

	echo '   <tr>' . "\n".

       '      <td > <label for="notes">Locations</label></td>  ' . "\n" . 

       '      <td > <textarea name="notes" id="notes" rows="5" cols="34" tabindex="' . $t_index . '">'.$r_notes.'</textarea></td>  ' . "\n" . 

       '   </tr>' . "\n";		 



	

  /****************************

  * File Uploading Section

  ***************************/

  

	// image upload handler

  if ( $row['image'] != '' )

  {

    

   $image_filename      = $upload_image_dir . $row['image'];

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="current_image">Current Image</label></td>' . "\n";

   echo '<td ><a href="' . $upload_image_url . $row['image'] . '" class="lightbox">' . $row['image'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="current_image" value="' . $row['image'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="image">Upload Image</label></td>' ."\n";

  echo '<td ><input name="image" type="file" id="image" size="15">' . "\n";

  echo '<span class="note">( File must be a *.JPG, *.PNG, or *.GIF )</span></td>' . "\n";

  echo '</tr>' . "\n";

 

 	// file upload handler

/*   if ( $row['file'] != '' )

  {

    

   $file_filename      = $upload_file_dir . $row['file'];

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="current_file">Current File</label></td>' . "\n";

   echo '<td ><a href="' . $upload_file_url . $row['file'] . '">' . $row['file'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="current_file" value="' . $row['file'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="file">Upload File</label></td>' ."\n";

  echo '<td ><input name="file" type="file" id="file" size="15">' . "\n";

  echo '<span class="note">( File must be a *.DOC, *DOCX, *.PDF or *.RTF )</span></td>' . "\n";

  echo '</tr>' . "\n"; */

	

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="website">Website</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="website" id="website" value="'. $row['website'] .'" size="45" tabindex="' . $t_index . '" placeholder="http://www.somebusiness.com" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

			 

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="map">Map</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="map" id="map" value="'. $row['map'] .'" size="45" tabindex="' . $t_index . '" placeholder="<iframe> code goes here from your map provider </iframe>" /></td>  ' . "\n" . 

       '   </tr>' . "\n";		 

	

  $t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="weight">Order</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />' . "\n" .

       ' 						<span class="note" style="padding-left:5px;"> ( Recommend increments of 10 ) </span>' . "\n" .

       '      </td>' . "\n" .

       '   </tr>' . "\n";

  

  $t_index++;

		echo '   <tr>' . "\n" .

							' 						<td ><label for="active">Active</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="active" name="active">' . "\n" . 

							' 									<option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 

							' 									<option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 

							' 						</select>' . "\n" . 

						 ' 						<span class="note" style="padding-left:5px;"> ( Set option to `Yes` to display on the website. ) </span>' . "\n" . 

       ' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";		 

	

 // This is our submit

  $t_index++;

  echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";  

  echo '</table></form>' . "\n";

  echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>' . "\n";



}





/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

  global $upload_file_dir, $upload_image_dir, $mod_config, $known_photo_types, $known_file_types;

  

  $r_data['label'] 	 					= mysql_real_escape_string ( $data['label'] );

	$r_data['address'] 	 				= mysql_real_escape_string ( $data['address'] );

	$r_data['city'] 	 					= mysql_real_escape_string ( $data['city'] );

	$r_data['state'] 	 					= mysql_real_escape_string ( $data['state'] );

	$r_data['zip'] 	 						= mysql_real_escape_string ( $data['zip'] );

	$r_data['phone1'] 	 				= mysql_real_escape_string ( $data['phone1'] );

	$r_data['phone2'] 	 				= mysql_real_escape_string ( $data['phone2'] );

	$r_data['notes']  	 				= mysql_real_escape_string ( $data['notes'] );

  $r_data['image']  				  = $data['current_image'];

	$r_data['file']  					  = $data['current_file'];

	$r_data['website']  				= mysql_real_escape_string ( $data['website'] );

	$r_data['map'] 	 						= htmlspecialchars ( $data['map'] );

	$r_data['weight']  					= mysql_real_escape_string ( $data['weight'] );

	$r_data['active'] 				  = mysql_real_escape_string ( $data['active'] );

			

  $tid = ( intval ( $data['id'] ) == 0 ) ? fetch_lastid ('businesses') : $data['id'];

      		 

				 

  if ( !empty ( $_FILES['file']['name'] ) )

  {

            

    $old_file        = $r_data['file'];

    $file            = $_FILES['file'];

		

		$r_data['file'] = process_upload_file ( $tid , $file, $known_file_types, $upload_file_dir);

						

    if ( ( $old_file != $r_data['file'] ) && ( file_exists ( $upload_file_dir . $old_file ) ) && ( $old_file != '' ))

		{

			unlink ( $upload_file_dir . $old_file );

		}

  

	}

	

  if ( !empty ( $_FILES['image']['name'] ) )

  {

            

    $old_image        = $r_data['image'];

    $image            = $_FILES['image'];

    

		$r_data['image'] = process_upload ( $tid , '', $image, $known_photo_types, $upload_image_dir, 1, $width, $height );

			

    if ( ( $old_image != $r_data['image'] ) && ( file_exists ( $upload_image_dir . $old_image ) ) && ( $old_image != '' ))

		{

			unlink ( $upload_image_dir . $old_image );

		}

  

	}

			 

  if ( empty($_POST['weight']) )

  {

    $r_data['weight']  = fetch_weight('businesses', 'weight' );

  }

  

  return $r_data;



}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 * Unlinks Existing Photo

 *

 **************************************************************/



function remove()

{



	global $id, $upload_file_dir, $upload_image_dir, $module_name;

	

	$result = mysql_query("SELECT * FROM " . db_prefix . "businesses WHERE id = '$id'");

	$row    = mysql_fetch_array($result);

	

	print_header('Delete Client Business - ' . $row['label']);

	

	if ( !empty($_POST ))

	{		

					

		if ( file_exists ( $upload_file_dir . $row['file'] ))

		{

				@unlink( $upload_file_dir . $row['file'] );

		}

		

		if ( file_exists ( $upload_image_dir . $row['image'] ))

		{

				@unlink( $upload_image_dir . $row['image'] );

		}

		

		

		$result = mysql_query('DELETE FROM ' . db_prefix . "businesses WHERE id = '$id'");

		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

			

	} else {

		

		$result = mysql_query("SELECT * FROM " . db_prefix . "coupons WHERE bus_id = '$id'");

		$totrow = mysql_num_rows($result);

		

		if ($totrow > 0)

		{

			echo '<form action="./?tool=businesses&action=remove" method="post" name="form">' . "\n" .

									'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

									'<tr> <td><div align="center">This business still has attached coupons! Please remove all coupons from this business first.</div></td></tr>' . "\n" .

									'<tr> <td><div align="center">' . "\n" .

									'<input name="Ok" type="button" value="Ok" onClick="window.location = \'./?tool=coupons\'"></div></td></tr>' . "\n" .

									'</table></form>';

		} else 

			{

				echo '<form action="./?tool=businesses&action=remove" method="post" name="form">' . "\n" .

									'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

									'<tr> <td><div align="center">Are you sure you want to delete this business?</div></td></tr>' . "\n" .

									'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

									'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=businesses\'"></div></td></tr>' . "\n" .

									'</table></form>';

			}

	}//end if

}//end function





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

		global $action, $id, $module_name;

		

		if ( $action == 'update' )

				print_header('Update Client Business');

		else

				print_header('Add Client Business');

 

		if ( array_key_exists ('submit',$_POST))

		{				

    require_once("classes/validation.php");

  

    $rules = array();    

    

    $rules[] = "required,label,label";

		$rules[] = "required,address,address";

		$rules[] = "required,city,city";

		$rules[] = "required,state,state";

		$rules[] = "required,zip,zip";

		$rules[] = "required,phone1,phone1";

    

    $errors = validateFields($_POST, $rules);

    

		}

		



		

		

		

		

		if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

	 {

		

				$data  = sanitize_vars( $_POST );			

		 	  $type  = ( $action == 'update' ) ? 0 : 1;

				$notes = str_replace('\n', '<br />', $data['notes']);

				

				

				if ($data['active'] == '0')

				{

					$result = mysql_query("SELECT * FROM " . db_prefix . "coupons WHERE `bus_id`='$id' AND `active`='1'");

					$totrow = mysql_num_rows($result);

					if ($totrow > 0)

					{

						echo '<div class="error_message">This business still has active coupons.<br />This business and related coupons will remain visible on the website until time expires or related coupons are deactived.</div>';

					}

				}

								

				

				if ( $action == 'update' )

						$sql = "UPDATE " . db_prefix . "businesses SET label = '" . $data['label'] . "', address = '" . $data['address'] . 

						"', city = '" . $data['city'] . "', state = '" . $data['state'] . "', zip = '" . $data['zip'] . 

						"', phone1 = '" . $data['phone1'] . "', phone2 = '" . $data['phone2'] . "', notes = '" . $notes . 

						"', image = '" . $data['image'] . "', file = '" . $data['file'] . "', website = '" . $data['website'] . 

						"', map = '" . $data['map'] . "', weight = '" . $data['weight'] . "', active = '" . $data['active'] . 

						"'	WHERE id = '$id'";

 		 else

					 $sql = 'INSERT INTO ' . db_prefix . 'businesses (label, address, city, state, zip, phone1, phone2, notes, image, file, website, map, weight, active) VALUES ' .

									"('" . $data['label'] . "', '" . $data['address'] . "', '" . $data['city'] . "', '" . $data['state'] . "', '" . 

									$data['zip'] . "', '" . $data['phone1'] . "', '" . $data['phone2'] . "', '" . $notes . "', '" . 

									$data['image'] . "', '" . $data['file'] . "', '" . $data['website'] . "', '" . $data['map'] . "', '" . 

									$data['weight'] . "', '" . $data['active'] . "')";

											

//    echo $sql;

    

				mysql_query ( $sql );

    

    if ( is_saint() && mysql_errno != 0 )

      echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

   			

				print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;   

		

  } else {

		add( $errors );

	}//end if	

}//end function







?>