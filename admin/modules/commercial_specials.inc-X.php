 <?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Commercial Specials Module

// Written By : Darrell Bessent of EWD

// Written On : March 29, 2013

// Revision   : 164

// Updated by : Darrell Bessent of EWD

// Updated On : March 29, 2013

// Description: This module is used with websites that 

//							advertise specials to commercial buyers

//							with a special login area.

// Copyright Zeal Technologies 

//***************************************************/



/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);





$mod_config  = get_module_settings ( 'commercial_specials' );



$upload_url  = $site_base_url . '/uploads/commercial/';

$upload_dir  = $site_base_dir . '/uploads/commercial/';





function execute()

{

	switch($_GET['action'])

	{

		case 'view':

			view();

			break;

		case 'add':

			update();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Slider Content

 *

 *

 **************************************************************/



function manage()

{



  global $upload_dir,$upload_url;

  

  $i    = 0;

  $link = '<a href="./?tool=commercial_specials&action=add">Add A Commercial Special</a>';

  

  print_header('Manage Commercial Specials',$link);

  

  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 

       ' 	  <thead>' . "\n" . 

       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" . 

       ' 			  <th><h3>Commercial Special</h3></th>' . "\n" . 		// Display our commercial special name title

			 ' 			  <th><h3>Username</h3></th>' . "\n" . 		// Display our commercial username title

			 ' 			  <th><h3>Password</h3></th>' . "\n" . 		// Display our commercial password title

			 ' 			  <th><h3>Description</h3></th>' . "\n" . // Display our description title

			 ' 			  <th><h3>Start Date</h3></th>' . "\n" .  // Display our start date title

       ' 			  <th><h3>End Date</h3></th>' . "\n" .    // Display our end date title

			 ' 			  <th><h3>Special Date</h3></th>' . "\n" . // Display our special date title

			// ' 			  <th><h3>File Name</h3></th>' . "\n" .  // Display our file url title

			 ' 			  <th><h3>Price</h3></th>' . "\n" .  // Display our price title

      // ' 			  <th style="width:50px; text-align:center;"><h3>Order</h3></th>' . "\n" . // Display our order weight title

      // ' 			  <th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . // Display the active title

       ' 			  <th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" . // Display the title for the tools

       '		 </tr></thead><tbody>' . "\n";

  

  $results = mysql_query('SELECT * FROM ' . db_prefix . 'commercial_specials ORDER BY weight,label');

  $pages   = mysql_num_rows($results);

  while ($row = mysql_fetch_array($results))

  {    



    $i++;

    

    $link 					 = 'No File Uploaded';

    $label					 = stripslashes ( $row['label'] );

		$descr					 = stripslashes ( $row['descr'] );

		$start_date			 = $row['start_date'];

		$end_date	 			 = $row['end_date'];

		$special_date		 = $row['special_date'];

		$commercial_user = $row['commercial_user'];

		$commercial_pass = $row['commercial_pass'];

		$price 					 = $row['price'];

		

		

    $filename = $upload_dir . $row['file'];

    

    $link = ( file_exists ( $filename ) ) ? '<a href="' . $upload_url . $row['file'] . '" target="_blank">' . $row['file'] . '</a>' : $link;

                

    $act = yes_no ( $row['active'] );

    $descr = substr($descr, 0, 60) . '...';

		

    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

    echo '<td style="padding-left:20px;">' . $label . '</td>' . "\n"; 	// Display our special name (label)

		echo '<td style="padding-left:20px;">' . $commercial_user . '</td>' . "\n"; 	// Display our comm. user (commercial_user)

		echo '<td style="padding-left:20px;">' . $commercial_pass . '</td>' . "\n"; 	// Display our comm. pass (commercial_pass)

		echo '<td style="padding-left:20px;">' . $descr . '</td>' . "\n"; 	// Display our description (descr)

		echo '<td style="padding-left:20px;">' . $start_date . '</td>' . "\n"; 	// Display our start_date (start_date)

		echo '<td style="padding-left:20px;">' . $end_date . '</td>' . "\n"; 	// Display our start_date (end_date)

		echo '<td style="padding-left:20px;">' . $special_date . '</td>' . "\n"; 	// Display our special_date (special_date)

    //echo '<td style="padding-left:20px;">' . $link . '</td>' . "\n";  	// Display our file url (link)

		echo '<td style="padding-left:20px;">' . $price . '</td>' . "\n";  	// Display our price(price)

    //echo '<td text-align:center;">' . $row['weight'] . '</td>' . "\n"; // Display our order weight(weight)

    //echo '<td text-align:center;">' . $act . '</td>' . "\n"; // Display if the special is active (act)

    echo '<td valign="middle"><strong><a href="./?tool=commercial_specials&action=view&id='.$row['id'].'">View</a>&nbsp;|&nbsp;<a href="./?tool=commercial_specials&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

		//echo '<td style="width:100px;" valign="middle"><strong><a href="./?tool=commercial_specials&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

    echo '</tr>' . "\n";

	  

  }//end while



  echo '</tbody></table>';



  $pages = ( $pages > 20 ) ? true : false;

	

  echo_js_sorter ( $pages );

             

}//end function



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Banner Form for User Input

 *

 *

 **************************************************************/



function add( $errors = '' )

{

  global $id, $action, $site_base_url, $upload_url, $upload_dir, $mod_config, $height, $width, $invalid_user;

   

//  if ($invalid_user === TRUE)

//	{

//		echo '<div class="error_message">';

//		echo '* The username is already in use. Please select a different username.<br/>';

//		echo '</br></div>' . "\n";

//	}

	

	 

  if ( $errors || $invalid_user === TRUE)

  {

   echo '<div class="error_message">';

   if ( in_array('label', $errors ) )

     echo '* You must fill out a commerical special name.<br/>';

	if ( in_array('price', $errors ) )

     echo '* You must fill out a commerical special price.<br/>';

	if ($invalid_user === TRUE)

		echo '* This usernmame is already in use. Please select a different username.<br/>';

		

   echo '</br></div>' . "\n";

  } else {

    $c = ( $action == 'update' ) ? 1 : 0;

    print_notice_message ( $c );

  }

  

  if ($action == "update")

  {

   // Run the query for the existing data to be displayed.

   $results = mysql_query('SELECT * FROM ' . db_prefix . 'commercial_specials WHERE id = "' . $id . '"');

   $row 				= mysql_fetch_array($results);

  } 

	if (!empty ($_POST))

	{	

   $row     = sanitize_vars ($_POST);

  }//end if

  

	echo '<div class="notice_message">NOTES : <br /> 

					<ul>

						<li>The `COMMERCIAL SPECIAL` is the special name.</li>

						<li>The `COMMERCIAL USER` is the commercial special username.</li>

						<li>The `COMMERCIAL PASSWORD` is the commercial special password.</li>

						<li>The `DESCRIPTION` is a short description of the brand.</li>

						<li>The `START DATE` is when the special will start to show on the website.</li>

						<li>The `END DATE` is when the special will be removed from the website.</li>

						<li>The `SPECIAL DATE` is the actual sale date/dates.</li>

						<li>The `FILE` may be a *.DOC, *.DOCX or *.PDF.</li>

						<li>The `PRICE` is the actual price of the special.</li>

					<ul>	

				</div>';

	

  echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

  $t_index = 1;

  echo '   <tr>' . "\n".

       '      <td > <label for="label">Commercial Special</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="label" id="label" value="'. $row['label'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

 	

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="commercial_user">Commercial Username</label></td>  ' . "\n" . 

       '      <td ><input type="text" name="commercial_user" id="commercial_user" value="'. $row['commercial_user'] .'" size="45" tabindex="' . $t_index . '"  />

			 <span class="note" style="padding-left:5px;"> ( A commercial username is required ) </span>

						</td>  ' . "\n" . 

       '   </tr>' . "\n";	




  echo '   <tr>' . "\n".

       '      <td > <label for="commercial_pass">Commercial Password</label></td>  ' . "\n" . 

       '      <td ><input type="text" name="commercial_pass" id="commercial_pass" value="'. $row['commercial_pass'] .'" size="45" tabindex="' . $t_index . '"  />

			 <span class="note" style="padding-left:5px;"> ( A commercial password is required ) </span>

						</td>  ' . "\n" . 

       '   </tr>' . "\n";	

	

	

	  $t_index++;

		$r_descr = str_replace('<br />', '\n', $row['descr']);

	echo '   <tr>' . "\n".

       '      <td valign="top"> <label for="descr">Description</label></td>  ' . "\n" . 

       '      <td > <textarea name="descr" id="descr" rows="5" cols="40" tabindex="' . $t_index . '">'.$r_descr.'</textarea></td>  ' . "\n" . 

       '   </tr>' . "\n";		 

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="start_date">Start Date</label></td>  ' . "\n" . 

       '      <td > <a href="javascript:show_calendar(\'signup.start_date\',\'YYYY-MM-DD\');" onmouseover="window.status=\'Date Picker\';return true;" onmouseout="window.status=\'\';return true;">

			 <input type="text" name="start_date" id="start_date" value="'. $row['start_date'] .'" size="45" tabindex="' . $t_index . '"  readonly />

			 <span class="note" style="padding-left:5px;"> ( A date is required ) </span>

			 

<img src="images/show-calendar.gif" width=18 height=18 border=0 style="vertical-align:-4px;"></a></td>  ' . "\n" . 

       '   </tr>' . "\n";	

		


  echo '   <tr>' . "\n".

       '      <td > <label for="end_date">End Date</label></td>  ' . "\n" . 

       '      <td > <a href="javascript:show_calendar(\'signup.end_date\',\'YYYY-MM-DD\');" onmouseover="window.status=\'Date Picker\';return true;" onmouseout="window.status=\'\';return true;">

			 <input type="text" name="end_date" id="end_date" value="'. $row['end_date'] .'" size="45" tabindex="' . $t_index . '" readonly />

			 

			 <span class="note" style="padding-left:5px;"> ( A date is required ) </span>

			 

			 <img src="images/show-calendar.gif" width=18 height=18 border=0 style="vertical-align:-4px;"></a></td>  ' . "\n" . 

       '   </tr>' . "\n";		  

		


  echo '   <tr>' . "\n".

       '      <td > <label for="special_date">Special Date</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="special_date" id="special_date" value="'. $row['special_date'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";		  	 



  

  

  /****************************

  * File Uploading Section

  ***************************/

  

  if ( $row['file'] != '' )

  {

    

   $filename      = $upload_dir . $row['file'];

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="current_file">Current File</label></td>' . "\n";

   echo '<td ><a href="' . $upload_url . $row['file'] . '">' . $row['file'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="current_file" value="' . $row['file'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="file">Upload File</label></td>' ."\n";

  echo '<td ><input name="file" type="file" id="image" size="15">' . "\n";

  echo '<span class="note">( File must be a *.DOC, *.DOCX or *.PDF )</span></td>' . "\n";

  echo '</tr>' . "\n";

 




  echo '   <tr>' . "\n".

       '      <td > <label for="price">Price</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="price" id="price" value="'. $row['price'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";		



  $t_index++;

  echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";  

  echo '</table></form>' . "\n";

  echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>' . "\n";



}



//***************************************************************

// VIEW FUNCTION

//***************************************************************

function view()

{

	global $id, $action;

	

	// Run the query for the existing data to be displayed.

   $results = mysql_query('SELECT * FROM ' . db_prefix . 'commercial_specials WHERE id = "' . $id . '"');

   $row 				= mysql_fetch_array($results);

	 

	  echo '<form name="signup" id="signup"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

  $t_index = 1;

  echo '   <tr>' . "\n".

       '      <td > <label for="label">Commercial Special</label></td>  ' . "\n" . 

       '      <td > '.$row['label'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";

 	

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="commercial_user">Commercial Username</label></td>  ' . "\n" . 

       '      <td >'. $row['commercial_user'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	




  echo '   <tr>' . "\n".

       '      <td > <label for="commercial_pass">Commercial Password</label></td>  ' . "\n" . 

       '      <td >'. $row['commercial_pass'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

	

	

	  $t_index++;

		$r_descr = str_replace('<br />', '\n', $row['descr']);

	echo '   <tr>' . "\n".

       '      <td valign="top"> <label for="descr">Description</label></td>  ' . "\n" . 

       '      <td>' .$r_descr.'</td>  ' . "\n" . 

       '   </tr>' . "\n";		 

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="start_date">Start Date</label></td>  ' . "\n" . 

       '      <td >'. $row['start_date'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

		


  echo '   <tr>' . "\n".

       '      <td > <label for="end_date">End Date</label></td>  ' . "\n" . 

       '      <td > '. $row['end_date'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";		  

		


  echo '   <tr>' . "\n".

       '      <td > <label for="special_date">Special Date</label></td>  ' . "\n" . 

       '      <td > '. $row['special_date'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";		

			 

			 
  echo '   <tr>' . "\n".

       '      <td > <label for="price">Price</label></td>  ' . "\n" . 

       '      <td > '. $row['price'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";		



  $t_index++;

  echo '' . "\n";  

  echo '</table></form>' . "\n";

}

/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

  global $upload_dir, $mod_config, $known_photo_types;

  

  

  $r_data['label'] 									 = mysql_real_escape_string ( $data['label'] );

	$r_data['descr']  								 = mysql_real_escape_string ( $data['descr'] );

	$r_data['start_date']  						 = mysql_real_escape_string ( $data['start_date'] );

	$r_data['end_date']  							 = mysql_real_escape_string ( $data['end_date'] );

	$r_data['special_date']					   = mysql_real_escape_string ( $data['special_date'] );

	$r_data['commercial_user']  			 = $data['commercial_user'];

	$r_data['commercial_pass']  			 = $data['commercial_pass'];

  $r_data['weight']  								 = intval ( $data['weight'] );

  $r_data['active'] 								 = intval ( $data['active'] ) ;

  $r_data['file']  									 = $data['current_file'];

	$r_data['price']  								 = $data['price'];

		

  $tid = ( intval ( $data['id'] ) == 0 ) ? fetch_lastid ('commercial_specials') : $data['id'];

      		 

  if ( !empty ( $_FILES['file']['name'] ) )

  {

            

    $old_file        = $r_data['file'];

    $file            = $_FILES['file'];

    $sufx 					 = substr($old_file, -4);

		

		$r_data['file'] = process_upload_file ( $tid , $file, $known_file_types, $upload_dir);

						

    if ( ( $old_file != $r_data['file'] ) && ( file_exists ( $upload_dir . $old_file ) ) && ( $old_file != '' ))

		{

			unlink ( $upload_dir . $old_file );

		}

  

	}

			 

  if ( $data['active'] == 0 && empty($_POST['active']) )

  {

    $r_data['active'] = 1;

    $r_data['weight']  = fetch_weight('commercial_specials', 'weight' );

  }

  

  return $r_data;



}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 * Unlinks Existing Photo

 *

 **************************************************************/



function remove()

{



	global $id, $upload_dir, $module_name;

	

	$result = mysql_query("SELECT * FROM " . db_prefix . "commercial_specials WHERE id = '$id'");

	$row    = mysql_fetch_array($result);

	

	print_header('Delete Commercial Special - ' . $row['label']);

	

	if ( !empty($_POST ))

	{		

					

		if ( file_exists ( $upload_dir . $row['file'] ))

		{

				@unlink( $upload_dir . $row['file'] );

				

				//$sufx = substr($row['file'], -4);

				

		}

		

		

		$result = mysql_query('UPDATE ' . db_prefix . "commercial_specials SET active='0' WHERE id = '$id'");

		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

			

	} else {

	

		echo '<form action="./?tool=commercial_specials&action=remove" method="post" name="form">' . "\n" .

							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

							'<tr> <td><div align="center">Are you sure you want to delete this special?</div></td></tr>' . "\n" .

							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=commercial_specials\'"></div></td></tr>' . "\n" .

							'</table></form>';

	}//end if

}//end function





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

		global $action, $id, $module_name, $invalid_user;

		

		if ( $action == 'update' )

				print_header('Update Comemrcial Special');

		else

				print_header('Add New Commercial Special');

 

		if ( array_key_exists ('submit',$_POST))

		{				

    require_once("classes/validation.php");

  

    $rules = array();    

    

    $rules[] = "required,label,label";

		$rules[] = "required,start_date,start_date";

		$rules[] = "required,end_date,end_date";

		$rules[] = "required,commercial_user,commercial_user";

		$rules[] = "required,commercial_pass,commercial_pass";

    $rules[] = "required,price,price";

		

    $errors = validateFields($_POST, $rules);

		

		$qry_exuser = mysql_query("SELECT id FROM cms_commercial_specials WHERE `active`='1' AND `commercial_user`='".$_POST['commercial_user']."'");	 	 

			$userrows = mysql_num_rows($qry_exuser);

			if ($userrows > 0)

				$invalid_user = TRUE; 

		}

		

		

				

		

		

		if ( ( empty ( $errors ) && $invalid_user != TRUE) && ( array_key_exists ('submit',$_POST)) )

	 {

		

				$data  = sanitize_vars( $_POST );			

		 	  $type  = ( $action == 'update' ) ? 0 : 1;

				$descr = str_replace('\n', '<br />', $data['descr']);

				

				if ( $action == 'update' )

						$sql = "UPDATE " . db_prefix . "commercial_specials SET label = '" . $data['label'] . "', descr = '" . $descr . "', weight = '" . $data['weight'] . "', active = '" . $data['active'] . "', file = '" . $data['file'] ."', price = '" . $data['price'] . "', start_date = '" . $data['start_date'] . "', end_date = '" . $data['end_date'] . "', special_date = '" . $data['special_date'] . "', commercial_user = '" . $data['commercial_user'] . "', commercial_pass = '" . $data['commercial_pass'] . "'  WHERE id = '$id'";

 		 else

					 $sql = 'INSERT INTO ' . db_prefix . 'commercial_specials (label, active, weight, file, descr, start_date, end_date, special_date, commercial_user, commercial_pass, price) VALUES ' . "('" . $data['label'] ."', '" . $data['active'] . "', '" . $data['weight'] . "', '" . $data['file'] . "', '" . $descr . "', '" . $data['start_date'] . "', '" . $data['end_date'] . "', '" . $data['special_date'] . "', '" . $data['commercial_user'] . "', '" . $data['commercial_pass'] . "', '" . $data['price'] ."')";

											

//    echo $sql;

    

				mysql_query ( $sql );



				

    if ( is_saint() && mysql_errno != 0 )

      echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

   			

				print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;   

		

  } else {

		add( $errors );

	}//end if	

}//end function







?>