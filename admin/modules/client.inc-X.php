<?php
//****************************************************/
// Module: Client Management System
// Written By : Chuck Bunnell of EWD
// Written On : Dec 23, 2011
// Copyright Zeal Technologies & S-Vizion Software
//***************************************************/

if ( !defined('BASE') ) die('No Direct Script Access');

/***************************************************************
 *
 * Sanitize variables for use in forms and whatnots
 *
 *
 **************************************************************/

$id          = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);
$action      = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);
$module_name = get_module_name();

$dbtbl = db_prefix. 'client';


function execute()
{
	switch($_GET['action'])
 {
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}
}


/***************************************************************
 *
 * function manage
 * Querrys DB and Displays Clients
 *
 *
 **************************************************************/

function manage()
{
  global $userinfo, $dbtbl;
 
  $i    = 0;
	$link = '<a href="./?tool=client&action=add">Add a Client</a>';
	
/* commented out because any level of admin can add a client 
 $lvl  = $userinfo['level'];

 if ( $lvl == 0 )
 {
			$link = '';
	}
*/ 
	
	print_header('Manage Clients',$link);
	
	$sql = '';
	
	echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table" >' . "\n" .
		'<thead><tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" . 
			'<th><h3>Client</h3></th>' . "\n" .
			'<th><h3>Contact</h3></th>' . "\n" .
			'<th><h3>Email</h3></th>' . "\n" .
			'<th><h3>Last Login</h3></th>' . "\n" .
			'<th><h3>Active</h3></th>' . "\n" .
			'<th class="nosort" style="width:160px;"><h3>Tools</h3></th>' . "\n" .
		'</tr></thead>
		<tbody>';

			$results = mysql_query('SELECT * FROM ' . $dbtbl . ' ORDER BY coname');
  		$pages   = mysql_num_rows($results);
			while ($row = mysql_fetch_array($results))
			{    
				$i++;
	
				if ( $row['last_login_ts'] == '' )
					$lt = 'Never Logged';
				else
					$lt  = date("m/d/Y, g:i a", $row['last_login_ts'] );
 			
				$act = ( $row['active'] == '1' ) ? 'Y' : 'N';
				
				echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";
					echo '<td>' . $row['coname'] . '</td>' . "\n";
					echo '<td>' . $row['fname'] . '&nbsp;' . $row['lname'] . '</td>' . "\n";
					echo '<td>' . $row['email'] . '</td>' . "\n";
					echo '<td>' . $lt . '</td>' . "\n";
					echo '<td>' . $act . '</td>' . "\n";
					echo '<td valign="middle"><strong><a href="./?tool=client&action=update&id='.$row['id'].'">Update</a>
						&nbsp;|&nbsp;<a href="./?tool=client&action=remove&id='.$row['id'].'">Remove</a>&nbsp;|&nbsp;<a 
						href="./?tool=files&clientid='.$row['id'].'">Files</a></strong></td>';
				echo '</tr>' . "\n";
 	 	
     	}
 
		echo '</tbody>
	</table>';

	if ( $pages > 20 )
		$pages = true;
	else
		$pages = false;
	
	echo_js_sorter ( $pages );
             
}

/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 * 
 *
 **************************************************************/

function add( $errors = '' )
{
	global $id, $action, $dbtbl;
	
	if ( $errors )
	{
		echo '<div class="error_message">';
			echo 'Please fill in the required fields.';
			if ( in_array('coname', $errors ) )
			{
				echo '<br />* The Company Name is required.';
				$val_coname = ' class="form_field_error" ';
			}
			
			if ( in_array('fname', $errors ) )
			{
				echo '<br />* A Contact Persons First Name is required.';
				$val_fname = ' class="form_field_error" ';
			}
			
			if ( in_array('lname', $errors ) )
			{
				echo '<br />* A Contact Persons Last Name is required.';
				$val_lname = ' class="form_field_error" ';     
			}
	
			if ( in_array('address', $errors ) )
			{
				echo '<br />* The Company Address is required.';
				$val_address = ' class="form_field_error" ';     
			}
	
			if ( in_array('city', $errors ) )
			{
				echo '<br />* The City is required.';
				$val_city = ' class="form_field_error" ';     
			}
	
			if ( in_array('state', $errors ) )
			{
				echo '<br />* The State is required.';
				$val_state = ' class="form_field_error" ';     
			}
	
			if ( in_array('zip', $errors ) )
			{
				echo '<br />* The Zip Code is required.';
				$val_zip = ' class="form_field_error" ';     
			}
	
			if ( in_array('phone', $errors ) )
			{
				echo '<br />* The Contact Phone Number is required.';
				$val_phone = ' class="form_field_error" ';     
			}
	
			if ( in_array('email', $errors ) )
			{
				echo '<br />* The Email Address does not appear to be valid.';
				$val_email = ' class="form_field_error" ';      
			}
					
			if ( in_array('used_email', $errors ) )
			{
				echo '<br />* This email address is already being used';
				$val_email = ' class="form_field_error" ';      
			}
			
			if ( in_array('username', $errors ) )
			{
				echo '<br />* A Username is required for login.';
				$val_username = ' class="form_field_error" ';
			}
					
			if ( in_array('used_username', $errors ) )
			{
				echo '<br />* This Username is already being used';
				$val_username = ' class="form_field_error" ';      
			}
	
			if ( in_array('pass', $errors ) )
			{
				echo '<br />* A Password is required for login';
				$val_pass = ' class="form_field_error" ';      
			}
	
		echo '</div>' . "\n";
	}
		
	if ($action == "update")
	{
		// Run the query for the existing data to be displayed.
		$results = mysql_query('SELECT * FROM ' . $dbtbl . ' WHERE id = "' . $id . '"');
		$row 		 = mysql_fetch_array($results);
	} else {
		$row     = sanitize_vars ( $_POST );
	}
	
	$req  = '<span class="req">*</span>';
	$preq = ($action == 'add') ? $req : '';
/********************************************************
 * Start Building Form
 *******************************************************/
		
	echo '<form name="signup" id="signup" method="post" action="#" ><table>' . "\n";			
		echo '<input type="hidden" name="id" value="' . $id . '" />';
		
		$passnote = tooltip('Recommended: 8-10 characters using uppercase, lowercase, and numbers.');
		if ($action == 'update')
			$passnote = tooltip('Leave blank to keep current password.<br />
													Recommended: 8-10 characters using uppercase, lowercase, and numbers.');
		
		echo '<tr>' . "\n" .
			'<td> ' .$req. ' <label for="coname">Company Name</label></td>' . "\n" .
			'<td> <input ' . $val_coname . ' type="text" name="coname" id="coname" value="'. $row['coname'] .'" 
				size="45" /></td>  ' . "\n" .
		'</tr>' . "\n";

		$t_index++;		
		echo '<tr>' . "\n" .
			'<td> ' .$req. ' <label for="fname">Contact First Name</label></td>' . "\n" .
			'<td> <input ' . $val_fname . ' type="text" name="fname" id="fname" value="'. $row['fname'] .'" size="45" 
				tabindex="' . $t_index . '" /></td>  ' . "\n" .
		'</tr>' . "\n";

		echo '<tr>' . "\n" .
			'<td> <label for="lname">Contact Last Name</label></td>' . "\n" .
			'<td> <input ' . $val_lname . ' type="text" name="lname" id="lname" value="'. $row['lname'] .'" size="45" 
				tabindex="' . $t_index . '" /></td>  ' . "\n" .
		'</tr>' . "\n";

		echo '<tr>' . "\n" .
			'<td> <label for="address">Company Address</label></td>' . "\n" .
			'<td> <input ' . $val_address . ' type="text" name="address" id="address" value="'. $row['address'] .'" size="45" 
				tabindex="' . $t_index . '" /></td>  ' . "\n" .
		'</tr>' . "\n";

		echo '<tr>' . "\n" .
			'<td> <label for="city">City</label></td>' . "\n" .
			'<td> <input ' . $val_city . ' type="text" name="city" id="city" value="'. $row['city'] .'" size="45" 
				tabindex="' . $t_index . '" /></td>  ' . "\n" .
		'</tr>' . "\n";

		echo '<tr>' . "\n" .
			'<td> <label for="state">State</label></td>' . "\n" .
			'<td> <input ' . $val_state . ' type="text" name="state" id="state" value="'. $row['state'] .'" size="45" 
				tabindex="' . $t_index . '" /></td>  ' . "\n" .
		'</tr>' . "\n";

		echo '<tr>' . "\n" .
			'<td> <label for="zip">Zip Code</label></td>' . "\n" .
			'<td> <input ' . $val_zip . ' type="text" name="zip" id="zip" value="'. $row['zip'] .'" size="10" 
				tabindex="' . $t_index . '" /></td>  ' . "\n" .
		'</tr>' . "\n";

		echo '<tr>' . "\n" .
			'<td> <label for="phone">Contact Phone</label></td>' . "\n" .
			'<td> <input ' . $val_phone . ' type="text" name="phone" id="phone" value="'. $row['phone'] .'" size="45" 
				tabindex="' . $t_index . '" /></td>  ' . "\n" .
		'</tr>' . "\n";

		echo '<tr>' . "\n" .
			'<td> ' .$req. ' <label for="email">Email</label></td>' . "\n" .
			'<td> <input ' . $val_email . ' type="text" name="email" id="email" value="'. $row['email'] .'" size="45" 
				tabindex="' . $t_index . '" /></td>  ' . "\n" .
		'</tr>' . "\n";

		echo '<tr>' . "\n" .
			'<td> ' .$req. ' <label for="username">Username</label></td>' . "\n" .
			'<td> <input ' . $val_username . ' type="text" name="username" id="username" value="'. $row['username'] .'" 
				size="45" /></td>  ' . "\n" .
		'</tr>' . "\n";

		
		echo '<tr>' . "\n" .
			'<td> ' .$preq. ' <label for="pass">Password</label></td>' . "\n" .
			'<td> <input ' . $val_pass . ' type="text" name="pass" id="pass" value="" size="45" 
				tabindex="' . $t_index . '" />
				' .$passnote. ' </td>' . "\n" .
		'</tr>' . "\n";
									
		echo '<tr>' . "\n" .
			'<td><label for="active">Active</label></td>  ' . "\n" . 
		 	'<td>' . "\n" . 
				create_slist ( $list, 'active', $row['active'], 1, $t_index ) . 
		 		tooltip('Set to Yes to Allow Login') . "\n" . 
		 	'</td>  ' . "\n" . 
		'</tr>  '. "\n";
		
		echo '<tr><td colspan="2" style="text-align:center; margin:0 auto;" ><input type="submit" name="submit" 
			value="Submit" /></td></tr>' . "\n";  
 	echo '</table></form>' . "\n";
	echo '<script type="text/javascript">document.getElementById(\'coname\').focus();</script>' . "\n";

}



/***************************************************************
 *
 * function sanitize_vars
	* @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 *
 **************************************************************/


function sanitize_vars( $data )
{
	
	$r_data['coname'] 	= mysql_real_escape_string ( $data['coname'] );
	$r_data['fname']    = mysql_real_escape_string ( $data['fname'] );
	$r_data['lname']    = mysql_real_escape_string ( $data['lname'] );
	$r_data['address']  = mysql_real_escape_string ( $data['address'] );
	$r_data['city']     = mysql_real_escape_string ( $data['city'] );
	$r_data['state']    = mysql_real_escape_string ( $data['state'] );
	$r_data['zip']    	= mysql_real_escape_string ( $data['zip'] );
	$r_data['phone']    = mysql_real_escape_string ( $data['phone'] );
	$r_data['email']    = mysql_real_escape_string ( $data['email'] );
	$r_data['username'] = mysql_real_escape_string ( $data['username'] );
	$r_data['pass']     = $data['pass'];
	$r_data['active']		= intval ( $data['active'] );
	
 	if ( !array_key_exists('active', $_POST ) )
		$r_data['active'] = 1;
 
 return $r_data;
}

/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $id, $module_name, $userinfo, $config;
		
	$unldir = $config['site_base_dir']. '/uploads/spreadsheets/';

	$result = mysql_query("SELECT * FROM " . db_prefix . "client WHERE id = '".$id."'");
	$row    = mysql_fetch_array($result);
	
	print_header('Delete Client  - ' . $row['coname']);
	
	// are there any files attached to this client
	$fqry = mysql_query("SELECT id,file FROM " . db_prefix . "files WHERE clientid = '{$id}'");
	$ftot = mysql_num_rows($fqry);
	$fnum = ($ftot > 0) ? 'There are '.$ftot.' file(s) associated with this client. All of these files will also be 
		deleted!<br /><br />' : '';
	
	if ( !empty($_POST ))
	{		
		$result = mysql_query("DELETE FROM " . db_prefix . "client WHERE id = '$id'");
		
		if ($ftot > 0)
		{
			while ($frow = mysql_fetch_array($fqry))
			{ 
				@unlink($unldir . $frow['file']);
			}
			
			$res = mysql_query("DELETE FROM " . db_prefix . "files WHERE clientid = '$id'");
		}
			

		print_mysql_message ( mysql_errno() , $module_name, $id, '2' ) ;
	} else {
		
		echo '<form action="./?tool=client&action=remove" method="post" name="form">' . "\n" .
			'<input type="hidden" name="id" value="' . $id . '">' . "\n" .
			'<table width="100%" border="0">' . "\n" .
				'<tr> <td><div align="center">'.$fnum.'Are you sure you want to delete this record?</div></td></tr>' . "\n" .
				'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;
					<input name="No" type="button" value="No" onClick="window.location = \'./?tool=client\'"></div></td>
				</tr>' . "\n" .
			'</table>
		</form>';
	}
}


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of page contents
 *
 **************************************************************/

function update()
{
	global $action, $id, $module_name;
	
	if ( $action == 'update' )
		print_header('Update Client');
	else
		print_header('Add New Client');
	
	if ( array_key_exists ('submit',$_POST))
	{				
		require_once("classes/validation.php");
	
		$rules = array();    
		
		$rules[] = "required,coname,coname";
		$rules[] = "required,fname,fname";
//		$rules[] = "required,lname,lname";
		$rules[] = "required,email,email";
		$rules[] = "valid_email,email,email";
		$rules[] = "required,username,username";
	
		if ( $action != 'update' )
			$rules[] = "required,pass,pass";
		
		$errors = validateFields($_POST, $rules);
	
		if ( $action != 'update' )
		{
			$results = mysql_query("SELECT id FROM " . db_prefix . "client WHERE 
														 email = '" . mysql_real_escape_string ($_POST['email']) . "'");
			if ( mysql_num_rows($results) != 0 )
				$errors[] = 'used_email';
		
			$results = mysql_query("SELECT id FROM " . db_prefix . "client WHERE 
														 username = '" . mysql_real_escape_string ($_POST['username']) . "'");
			if ( mysql_num_rows($results) != 0 )
				$errors[] = 'used_username';
		}
	
	}
	
	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$data  = sanitize_vars( $_POST );			
															
		if ( $action == 'update' ) 
		{
			$sql = 'UPDATE ' . db_prefix . 'client SET ';
		
			$xsql[] .= "coname = '" . $data['coname'] . "' ";
			$xsql[] .= "fname = '" . $data['fname'] . "' ";  
			$xsql[] .= "lname = '" . $data['lname'] . "' ";
			$xsql[] .= "address = '" . $data['address'] . "' ";
			$xsql[] .= "city = '" . $data['city'] . "' ";
			$xsql[] .= "state = '" . $data['state'] . "' ";
			$xsql[] .= "zip = '" . $data['zip'] . "' ";
			$xsql[] .= "phone = '" . $data['phone'] . "' ";
			$xsql[] .= "email = '" . $data['email'] . "' ";
			$xsql[] .= "username = '" . $data['username'] . "' ";
			$xsql[] .= "active = '" . $data['active'] . "' ";

			if ( $data['pass'] != '' )
				$xsql[] .= " pass = '" . md5( $data['pass'] ) . "' ";
							
			$slen = count ( $xsql );
	 
			for($i=0;$i<$slen;$i++)
			{
				$sql .= $xsql[$i];
				if ( $i != ( $slen - 1 ) )
					$sql .= ', ';      
			}
			
			$sql .= " WHERE id = '$id'";
			$type = 0;
		} else {
			$data = sanitize_vars( $_POST );
					
		 	$sql = 'INSERT INTO ' . db_prefix . 'client (coname, fname, lname, address, city, state, zip, phone, email, 
																									 username, pass, active) 
							VALUES ' . "('" . $data['coname'] ."', '" .$data['fname'] ."', '" .$data['lname'] ."', 
													 '" .$data['address'] ."', '" .$data['city'] ."', '" .$data['state'] ."', 
													 '" .$data['zip'] ."', '" .$data['phone'] ."', '" . $data['email'] . "', 
													 '" . $data['username'] ."', '" . md5( $data['pass'] ) . "', '" . $data['active'] . "')";
													 
	
			$type = 1;
		}	
			
		mysql_query ( $sql );
		
		if ( ( mysql_errno() != 0 ) && ( is_saint() ) )
			print_debug($sql);
			
		print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;
				
  } else {
		add( $errors );
	}//end if	
}//end function
