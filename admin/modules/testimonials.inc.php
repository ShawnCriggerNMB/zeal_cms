<?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module: Testimonials Management System 

// OrigModule : Admin Management System

// Written By : Shawn Crigger of EWD

// Written On : May 11, 2010

// Updated by : Cory Shaw of EWD

// Updated On : May 9, 2013

// Copyright Zeal Technologies

//***************************************************/

// if update make sure this id exists
update_verify();

function execute()

{

	switch($_GET['action'])

 {

		case 'update':

			update();

			break;

		case 'add':

			update();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Testimonial info

 *

 *

 **************************************************************/



function manage()

{

  global $dbtbl, $identifier, $module_name, $db, $userinfo;

 

  $i    = 0;

	$link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';

	

  $lvl  = $userinfo['level'];



  if ( $lvl == 0 )

  {

			$link = '';

	}

/********************************************************

 * Extra Info 

 *******************************************************/



 

	print_header('Manage '.$module_name,$link);

	

 $sql = '';

	

	echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table" >' . "\n" .

						'<thead><tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" . 

						'<th><h3>Testimonal</h3></th>' . "\n" .

      			'<th><h3>Name</h3></th>' . "\n" .

						'<th style="width:50px; text-align:center;"><h3>Order</h3></th>' . "\n" . 

      			'<th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . 

      $hdr .      

						'<th class="nosort" style="width:112px;"><h3>Tools</h3></th>' . "\n" .

	'</tr></thead><tbody>';



				

		$stmt = $db->prepare('SELECT * FROM ' . $dbtbl.' ORDER BY weight');

        $stmt->execute();

		$pages   = $stmt->rowCount();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC))

		{    

   		$i++;

 

 			$act = yes_no ( $row['active'] );

     

		  echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

				echo '<td style="padding-left:20px;">' . substr($row['content'], 0, 175) . '</td>' . "\n";

				echo '<td style="padding-left:20px;">' . $row['name'] . '&nbsp;' . $row['lname'] . '</td>' . "\n";

				echo '<td style="text-align:center;">' . $row['weight'] . '</td>' . "\n";

				echo '<td style="text-align:center;">' . $act . '</td>' . "\n";

				//echo $tbl;

				echo '<td valign="middle"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

  		echo '</tr>' . "\n";

     

		}//end while

  echo '</tbody></table>';



	$pages = ( $pages > 20 ) ? true : false;

	

	echo_js_sorter ( $pages );

             

}//end function



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Form for User Input

 * 

 *

 **************************************************************/



function add( $errors = '' )

{

		global $dbtbl, $identifier, $module_name, $db, $id, $action, $site_base_url, $content, $name, $mod_config;

		

	$base_path = dirname(__FILE__);

	

	include_once $base_path . '/ckeditor/ckeditor.php' ;

	$option = '';

				

		if ( $errors )

		{

			echo '<div class="error_message">';

				echo 'Please fill in the required fields.';

				if ( in_array('content', $errors ) )

				{

					echo '<br />* You must fill out a testimonial.';

					$val_content = ' class="form_field_error" ';

				}

		

			echo '</div>' . "\n";

		}

			

		if ($action == "update")

		{

			// Run the query for the existing data to be displayed.

			$stmt = $db->prepare('SELECT * FROM ' . $dbtbl.' WHERE id = ?');

            $stmt->execute(array($id));

			$row 				= $stmt->fetch(PDO::FETCH_ASSOC);

		} 

		

	if (!empty ($_POST))

	{

   $row     = sanitize_vars ($_POST); 

  }//end if





/********************************************************

 * Start Building the Testimonial Form

 *******************************************************/

	

			echo '<div class="notice_message">NOTES : <br /> 

					<ul>

						<li>The `TESTIMONIAL` is the customers testimonial.<br />

								<span style="color:#FF0000;">WARNING:</span> ONLY PASTE TEXT FROM WORD DOCUMENTS USING THE WORD TOOLBAR BUTTON!</li>

						<li>The `NAME` is the customers name that submitted the testimonial.</li>

						<li>Setting the `ORDER` will tell the testimonial where to appear within the list on the testimonial page.</li>

					<ul>	

				</div>';

	

	

		

	echo '<form name="signup" id="signup" method="post" action="#" ><table>' . "\n";			

		echo '<input type="hidden" name="id" value="' . $id . '" />';

		

		


		echo '<tr>' . "\n" .

			'<td> <label for="content">Testimonial</label></td>' . "\n" .

			'<td> <textarea ' . $val_content . ' cols="45" rows="5" name="content" id="content" 

				size="45" />'. $row['content'] .'</textarea></td>  ' . "\n" .

		'</tr>' . "\n";

		

	$ckeditor  = new CKEditor( ) ;

	$ck_config = set_fck_config();

	$ck_events = set_fck_events();

	

	$ck_config['toolbar'] = set_fck_toolbar_pastefromword();

	$ck_config['height']  = 200;

	$ck_config['width']   = '102%';



// EDIT BELOW LINE TO ADD YOUR WEBSITE STYLESHEET

//		$ck_config['contentsCss'] = $base_site_url . '/styles/main.css';

	$ckeditor->replace("content", $ck_config, $ck_events);





		$t_index++;		

		echo '<tr>' . "\n" .

			'<td> <label for="name">Name</label></td>' . "\n" .

			'<td> <input ' . $val_name . ' type="text" name="name" id="name" value="'. htmlspecialchars($row['name']) .'" size="45"

				tabindex="' . $t_index . '" /></td>  ' . "\n" .

		'</tr>' . "\n";




		echo '<tr>' . "\n" .

			'<td> <label for="weight">Order</label></td>' . "\n" .

			'<td> <input ' . $val_weight . ' type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="45" 

				tabindex="' . $t_index . '" />' . "\n" .

				tooltip('Setting the `Order` will tell the testimonial where to appear within the list on the testimonial page.') . "\n" . 

				'</td>' . "\n" .

		'</tr>' . "\n";

									



  $t_index++;

		echo '   <tr>' . "\n" .

							' 						<td ><label for="active">Active</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="active" name="active">' . "\n" . 

							' 									<option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 

							' 									<option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 

							' 						</select>' . "\n" . 

						 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to Display on Website ) </span>' . "\n" . 

       ' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";

		

			


		echo '<tr><td colspan="2" style="text-align:center; margin:0 auto;" ><input type="submit" name="submit" 

			value="Submit" /></td></tr>' . "\n";  

 	echo '</table></form>' . "\n";

	echo '<script type="text/javascript">document.getElementById(\'content\').focus();</script>' . "\n";



}







/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

	global $dbtbl, $identifier;

    

	$r_data['content']      = $data['content'] ;

	$r_data['name']         = strip_fck_strip_value($data['name']) ;

	$r_data['weight']       = $data['weight'] ;

	$r_data['active']		= intval ( $data['active'] );

	

		 

  if ( empty($_POST['weight']) )

  {

    $r_data['weight']  = fetch_weight($identifier, 'weight' );

  }

 

	return $r_data;

}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 *

 **************************************************************/



function remove()

{

	global $dbtbl, $identifier, $module_name, $db, $id, $module_name, $userinfo;

		

	$stmt = $db->prepare("SELECT * FROM " . $dbtbl ." WHERE id = ?");

    $stmt->execute(array($id));

	$row    = $stmt->fetch(PDO::FETCH_ASSOC);

	

 

	print_header('Delete '.$module_name.' From - ' . $row['name']);

	

	if ( !empty($_POST ))

	{		

        $errno = 0;

        try{

            $stmt = $db->prepare("DELETE FROM " . $dbtbl ." WHERE id = ?");

            $stmt->execute(array($id)); 

        }

		catch(PDOException $ex){

            $errno = $ex->getCode();

        }

		print_mysql_message ( $errno , $module_name, $id, '2' ) ;

	} else {

		echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">' . "\n" .

			'<input type="hidden" name="id" value="' . $id . '">' . "\n" .

			'<div class="center">Are you sure you want to delete this testimonial?</div>' . "\n" .

			'<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;

				<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div>' . "\n" .

		'</form>';

	}

}





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

	global $dbtbl, $identifier, $module_name, $db, $action, $id, $module_name;

	

	if ( $action == 'update' )

		print_header('Update '.$module_name);

	else

		print_header('Add New '.$module_name);

	

	if ( array_key_exists ('submit',$_POST) )

	{				

		require_once("classes/validation.php");

	

		$rules = array();    

		

		$rules[] = "required,name,name";

		$rules[] = "required,content,content";

	

		

		$errors = validateFields($_POST, $rules);

	

		if ( $action != 'update' )

		{

			$stmt = $db->prepare("SELECT id FROM " . $dbtbl ." WHERE 

														 name = ?");

            $stmt->execute(array($_POST['name']));

			if ( $stmt->rowCount() != 0 )

				$errors[] = 'used_name';

		

			$stmt = $db->prepare("SELECT id FROM " . $dbtbl ." WHERE 

														 content = ?");

            $stmt->execute(array($_POST['content']));

			if ( $stmt->rowCount() != 0 )

				$errors[] = 'used_content';

		}

	

	}

	

	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

	{

		$data  = sanitize_vars( $_POST );



        $array = array();

															

		if ( $action == 'update' ) 

		{

			$sql = 'UPDATE ' . $dbtbl.' SET

			            content = ?,

			            name    = ?,

			            weight  = ?,

			            active  = ?

                    WHERE id = ?

			            ';



            array_push($array, strip_fck_strip_value($data['content']));

            array_push($array, strip_fck_strip_value($data['name']));

            array_push($array, $data['weight']);

            array_push($array, $data['active']);

            array_push($array, $id);



			$type = 0;

		} else {

					

		 	$sql = 'INSERT INTO ' . $dbtbl.' (content, name, weight, active) 

							VALUES (?, ?, ?, ?)';



            array_push($array, strip_fck_strip_value( $data['content']));

            array_push($array, strip_fck_strip_value( $data['name']));

            array_push($array, $data['weight']);

            array_push($array, $data['active']);

			$type = 1;

		}	



        $errno = 0;

        try{

            $stmt = $db->prepare($sql);

            $stmt->execute($array);

        }

		catch(PDOException $ex){

            $errno = $ex->getCode();

        }

		

		if ( ( $errno != 0 ) )

			print_debug($sql);

			

		print_mysql_message ( $errno , $module_name, $id, $type ) ;

				

  } else {

		add( $errors );

	}//end if	

}//end function

