<?php	
if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module: Module Activation Controller
// Written By : Shawn Crigger of EWD
// Written On : May 27, 2010
// Updated By : Cory Shaw of EWD
// Updated On : May 3, 2014
// Copyright Zeal Technologies
//***************************************************/


function execute()
{
	switch($_GET['action'])
 {
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}
}


/***************************************************************
 *
 * function manage
 * Querrys DB and Displays Info
 *
 **************************************************************/

function manage()
{
	global $db, $identifier, $module_name, $dbtbl;

	$link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';
  
  print_header('Manage '.$module_name,$link);
  
	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sortable" id="table">
		<thead>
			<tr>
				<th><h3>Identifier</h3></th>
				<th><h3>Name</h3></th>
				<th style="width:70px;"><h3>Active</h3></th>
				<th style="width:110px;"><h3>Level</h3></th>
				<th style="width:70px;"><h3>Weight</h3></th>
				<th class="nosort"><h3>Tools</h3></th>
      </tr>
		</thead>
		<tbody>';

    $stmt = $db->prepare('SELECT * FROM '.$dbtbl);
    $stmt->execute();
    $pages = $stmt->rowCount();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{    
			$act  = yes_no ( $row['active'] );				
			$alvl = ( $row['level'] == '1' ) ? 'Super-Admin' : 'Admin';
    
			if ( $row['level'] >= 2 )
				$alvl = 'Programmer';
      
			echo '<tr align="left" valign="middle">
				<td>' . $row['identifier'] . '</td>
				<td>' . $row['name'] . '</td>
				<td>' . $act . '</td>
				<td>' . $alvl . '</td>
				<td>' . $row['weight'] . '</td>
				<td style="padding:0;text-align:center;"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&action=remove&id='.$row['id'].'">Remove</a></strong></td>
			</tr>';
		}
  
		echo '</tbody>
	</table>';  
  
	$pages = ( $pages > 20 ) ? true : false;
  
  echo_js_sorter ( $pages, 2 );
		           
	echo '<div class="spacer">&nbsp;</div>';
}//end function


/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 * 
 **************************************************************/

function add( $errors = '' )
{
	global $db, $identifier, $module_name, $id, $action, $dbtbl;
				
	if ( $errors )
	{
		echo '<ul class="error_message">
			<strong>Please fill in the required fields.</strong>';
			// set error messages for required fields
			if ( in_array('identifier', $errors ) )
				echo '<li>An Identifier is required.</li>';
				$val_identifier = ' class="form_field_error" ';								
			
			if ( in_array('used_identifier', $errors ) )
			{
				echo '<li>This Identifier is already in use.</li>';
				$val_email = ' class="form_field_error" ';
			}

			if ( in_array('name', $errors ) )
				echo '<li>A Name is required.</li>';
				$val_name = ' class="form_field_error" ';								

			if ( in_array('used_name', $errors ) )
			{
				echo '<li>This Name is already in use.</li>';
				$val_email = ' class="form_field_error" ';
			}

			if ( in_array('description', $errors ) )
				echo '<li>A Description is required.</li>';
				$val_descr = ' class="form_field_error" ';								

		echo '</ul>';
	
	} else {
		
		$c = ( $action == 'update' ) ? 'update this' : 'add a new';
	
		echo  '<ul class="notice_message">';
			// set instructions here
			echo '<strong>To ' . $c . ' record, fill out the form and click submit.</strong>
			<li>All fields are required except the Weight.</li>
			<li>No two modules can have the same Identifier or Name.</li>
			<li>The Identifier is the php file (minus the ".inc.php")</li>
			<li>The Name is the name of the module. It will appear in various places.</li>
			<li>Weight is the order you want to display the modules in the left sidebar.</li>
			<li>If you don\'t want the module to display in the left sidebar, set the weight to "999".</li>
			<li>Select which Level of administrator can access this module.</li>
			<li>The Description of the module will display on the Instructions page.</li>
		</ul>';
	}
   
	if ($action == "update")
	{
		// Run the query for the existing data to be displayed.
		$stmt = $db->prepare('SELECT * FROM '.$dbtbl.' WHERE id = ?');
		$stmt->execute(array($id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
	} 
		
	if (!empty ($_POST))
	{
	 $row = sanitize_vars ($_POST);
	}
		
/********************************************************
 * Start Building Form
 *******************************************************/

	$r = required();
	echo '<form name="form" id="form" method="post" action="#">
		<table>
			<input type="hidden" name="id" value="' . $id . '" />';
	
			echo '<tr>
				<td><label for="identifier">'.$r.'Identifier</label></td>  
					<td><input ' . $val_identifier . ' type="text" name="identifier" id="identifier" value="'. htmlspecialchars($row['identifier']) .'" size="45" /></td>
			</tr>';

			echo '<tr>
				<td><label for="name">'.$r.'Name</label></td>  
					<td><input ' . $val_name . ' type="text" name="name" id="name" value="'. htmlspecialchars($row['name']) .'" size="45" /></td>
			</tr>';

			echo '<tr>
				<td><label for="weight">Weight</label></td>  
					<td><input ' . $val_weight . ' type="text" name="weight" id="weight" value="'. htmlspecialchars($row['weight']) .'" size="45" /></td>
			</tr>';

			echo '<tr>
				<td><label for="active">'.$r.'Active</label></td>  
        <td>
        '.create_slist ( $list, 'active', $row['active'], 1) .
        tooltip('Set to Yes to Allow Module').'
        </td>
			</tr>';

			echo '<tr>
				<td><label for="level">'.$r.'Level</label></td>  
				<td>
					<select id="level" name="level">
						<option value="0" '; if ( $row['level'] == '0' ) echo ' SELECTED'; echo ' >Admin</option>
						<option value="1" '; if ( $row['level'] == '1' ) echo ' SELECTED'; echo ' >Super-Admin</option>
						<option value="2" '; if ( $row['level'] == '2' ) echo ' SELECTED'; echo ' >Programmer</option>
					</select> '.tooltip('Select administrative level').'
				</td>  
			</tr>';

			echo '<tr>
				<td><label for="description">'.$r.'Description</label></td>  
				<td><textarea '.$val_descr.' id="description" name="description" cols="40" rows="5" wrap="SOFT">' . htmlentities ( $row['description'] ) . '</textarea></td>  
			</tr>';

			echo '<tr>
				<td colspan="2" style="padding:3px;"><input type="submit" name="submit" value="Submit" /></td>
			</tr>';

    echo '</table>
	</form>';
	
	echo '<script type="text/javascript">document.getElementById(\'identifier\').focus();</script>';

}


/***************************************************************
 *
 * function sanitize_vars
 * @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 **************************************************************/

function sanitize_vars( $data )
{
	$r_data['description'] = stripslashes ( $data['description'] );
	$r_data['identifier'] = stripslashes ( $data['identifier'] );
	$r_data['name'] = stripslashes ( $data['name'] );
	$r_data['active'] = intval ( $data['active'] ) ;
	$r_data['installed'] = 1;
	$r_data['level'] = intval ( $data['level'] ) ;
	$r_data['weight'] = intval ( $data['weight'] ) ;
	
	return $r_data;
}

/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $db, $identifier, $module_name, $id, $dbtbl;

	$stmt = $db->prepare("SELECT * FROM ".$dbtbl." WHERE id = ?");
	$stmt->execute(array($id));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	print_header('Delete '.$module_name.' - ' . $row['name']);
	
	if ( !empty($_POST ))
	{
		$errno = 0;
		try
		{
			$stmt = $db->prepare('DELETE FROM ' . db_prefix . $identifier." WHERE id = ?");
			$stmt->execute(array($id));
		}
		
		catch(PDOException $ex)
		{
			$errno = $ex->getCode();
		}
            
		print_mysql_message ( $errno , $module_name, $id, 2 ) ;
		
	} else {
		
		echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">
    	<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to delete this record?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div>
    </form>';
	}
}


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of contents
 *
 **************************************************************/

function update()
{
	global $db, $identifier, $module_name, $action, $id, $dbtbl;
		
	if ( $action == 'update' )
	{
		print_header('Update '.$module_name);
	}	else {
		print_header('Add New '.$module_name);
	}
		
	if ( array_key_exists ('submit',$_POST))
	{
		require_once("classes/validation.php");
			
		// set rules for required fields
		$rules = array();
		$rules[] = "required,name,name";
		$rules[] = "required,identifier,identifier";
		$rules[] = "required,description,description";
			
		$errors = validateFields($_POST, $rules);								
	
		// make sure identifier and name are not used more than once
		if ( $action != 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE identifier = ?");
			$stmt->execute(array($_POST['identifier']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_identifier';

			$stmt2 = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE name = ?");
			$stmt2->execute(array($_POST['name']));
			if ( $stmt2->rowCount() != 0 )
				$errors[] = 'used_name';
		}

		if ( $action == 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE identifier = ? AND id != ".$id);
			$stmt->execute(array($_POST['identifier']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_identifier';
		
			$stmt2 = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE name = ? AND id != ".$id);
			$stmt2->execute(array($_POST['name']));
			if ( $stmt2->rowCount() != 0 )
				$errors[] = 'used_name';
		}
	
	}
		
	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$vars = array();
		$data = sanitize_vars( $_POST );
		$type = ( $action == 'update' ) ? 0 : 1;
		
		if ( $action == 'update' ) 
		{
			$sql = "UPDATE ".$dbtbl." SET 
				identifier=?, 
				name=?, 
				installed=?, 
				active=?,
				level=?,  
				weight=?,
				description=?	
				WHERE id = ?";
			array_push($vars, $data['identifier']);
			array_push($vars, $data['name']);
			array_push($vars, $data['installed']);
			array_push($vars, $data['active']);
			array_push($vars, $data['level']);
			array_push($vars, $data['weight']);
			array_push($vars, $data['description']);
			array_push($vars, $id);
		
		} else {
			
			$sql = "INSERT INTO ".$dbtbl." (`identifier`, `name`, `installed`, `active`, `level`, `weight`, `description`) 
				VALUES (?, ?, ?, ?, ?, ?, ?)";
			array_push($vars, $data['identifier']);
			array_push($vars, $data['name']);
			array_push($vars, $data['installed']);
			array_push($vars, $data['active']);
			array_push($vars, $data['level']);
			array_push($vars, $data['weight']);
			array_push($vars, $data['description']);
		}

		$errono = 0;
		try
		{
			$stmt = $db->prepare($sql);
			$stmt->execute($vars);
		}
		
		catch(PDOException $ex)
		{
			$errono = $ex->getCode();
		}

		if ( is_saint() && $errono != 0 )
			echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

    print_mysql_message ( $errono , $module_name, $id, $type ) ;

  } else {
		add( $errors );
	}

}//end function
?>
