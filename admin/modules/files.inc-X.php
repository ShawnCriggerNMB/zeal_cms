<?php	
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module: Client Files Management System
// Written By : Chuck Bunnell of EWD
// Written On : Jan 05, 2012
// Copyright Zeal Technologies & S-Vizion Software
//***************************************************/

/***************************************************************
 *
 * Sanitize variables for use in forms and whatnots
 *
 *
 **************************************************************/

$id          = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);
$action      = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);
$module_name = get_module_name();

// make sure clientid is an integer
$clientid = ($_GET['clientid'] + 0 > 0) ? $_GET['clientid'] : '0';

if ($clientid > 0)
{
	$dbtbl = db_prefix. 'files';
	
	$cidqry	= mysql_query ("SELECT coname FROM " .db_prefix. "client WHERE id = '{$clientid}'");
	$cidtot = mysql_num_rows($cidqry);
	$cidrow = mysql_fetch_array ($cidqry);
}

// must have a valid clientid to access these functions
if (!$clientid || $clientid == '' || $clientid == 0 || ($cidtot == 0 && !isset($_POST)) )
  header('Location: ' .$config['site_base_url']. '/admin/index.php?tool=client');            


function execute()
{
	switch($_GET['action'])
 {
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}//end switch
}//end function


/***************************************************************
 *
 * function manage
 * Querrys DB and Displays Clients Files
 *
 *
 **************************************************************/

function manage()
{
	global $cidrow, $clientid, $config, $dbtbl, $site_base_dir;
 	
	$coname  = $cidrow['coname'];
	$siteurl = $config['site_base_url'];
	
	$i    = 0;
	$link = '<a href="./?tool=files&action=add&clientid='.$clientid.'">Add a File</a>';
	
	print_header('Manage Files for ' .$coname, $link);
	
	$sql = '';
	
	echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table" >' . "\n" .
		'<thead><tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" . 
			'<th><h3>File</h3></th>' . "\n" .
			'<th><h3>Date</h3></th>' . "\n" .
			'<th><h3>Active</h3></th>' . "\n" .
			'<th class="nosort" style="width:130px;"><h3>Tools</h3></th>' . "\n" .
		'</tr></thead>
		<tbody>';

			$results = mysql_query("SELECT * FROM " . $dbtbl . " WHERE clientid = '{$clientid}' ORDER BY file");
  		$pages   = mysql_num_rows($results);
			while ($row = mysql_fetch_array($results))
			{    
				$i++;
	
				$date  = date("M j, Y", strtotime($row['date']));
 			
				$act = ( $row['active'] == '1' ) ? 'Y' : 'N';
				
				echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";
					echo '<td><a href="' .$siteurl. '/file.php?file=' .$row['file']. '" target="_blank">' .$row['file']. '</a>
					</td>' . "\n";
					echo '<td>' . $date . '</td>' . "\n";
					echo '<td>' . $act . '</td>' . "\n";
					echo '<td valign="middle"><strong><a href="./?tool=files&action=update&id='.$row['id'].'&clientid='.$clientid.'">Update</a>&nbsp;|&nbsp;<a href="./?tool=files&action=remove&id='.$row['id'].'&clientid='.$clientid.'">Remove</a></strong></td>';
				echo '</tr>' . "\n";
 	 	
     	}
 
		echo '</tbody>
	</table>';

	if ( $pages > 20 )
		$pages = true;
	else
		$pages = false;
	
	echo_js_sorter ( $pages );
             
}

/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 * 
 *
 **************************************************************/

function add( $errors = '' )
{
	global $id, $action, $config, $clientid, $dbtbl;
	
	if ( $errors )
	{
		echo '<div class="error_message">';
			echo 'Please fill in the required fields.';
			
			if ( in_array('ufile', $errors ) )
			{
				echo '<br />* Browse for a File.';
				$val_ufile = ' class="form_field_error" ';
			}
			
			if ( in_array('ufiletype', $errors ) )
			{
				echo '<br />* The file type must be pdf or xls.';
				$val_ufile = ' class="form_field_error" ';
			}
			
		echo '</div>' . "\n";
	}
		
	if ($action == "update")
	{
		// Run the query for the existing data to be displayed.
		$results = mysql_query('SELECT * FROM ' .$dbtbl. ' WHERE id = "' . $id . '"');
		$row 		 = mysql_fetch_array($results);
		$pfile	 = $row['file'];
	} 
	if (!empty ($_POST))
	{
   $row     = sanitize_vars ($_POST);
  }//end if
	

/********************************************************
 * Start Building Form
 *******************************************************/
		
	echo '<form name="addfile" id="addfile" method="post" action="#" enctype="multipart/form-data" ><table>' . "\n";			
		echo '<input type="hidden" name="id" value="' . $id . '" />';
		
		$filenote = tooltip('Browse your computer for the file you want to upload.');
		if ($action == 'update')
			$filenote = tooltip('Uploading a new file will remove the present one!');
		
		
		/* use this if we need a dropdown of clients to select from
				good if admin needs to move file to a different client
		$cqry = mysql_query ("SELECT id,coname FROM " . db_prefix . "client WHERE active = '1' ORDER BY coname");
		echo '<tr>' . "\n" .
			'<td> <label for="clientid">Company Name</label></td>' . "\n" .
		 	'<td>' . "\n" . 
				'<select name="clientid" id="clientid" tabindex="' . $t_index . '">' . "\n"; 
				while ($crow = mysql_fetch_array($cqry) )
				{
					$sel = ($crow['id'] == $clientid && !$_POST ) ? ' selected ' : '';
					if (isset($_POST['clientid'])  )
						$sel = ($crow['id'] == $_POST['clientid']) ? ' selected ' : '';
					
					echo '<option value="' .$crow['id']. '" '.$sel.'>' .$crow['coname']. '</option>' . "\n"; 
				}
				echo '</select>' . "\n" . 
		 	'</td>  ' . "\n" . 
		'</tr>' . "\n";
*/
		if ($action == 'update')
		{
			$t_index++;		
			echo '<tr>' . "\n" .
				'<td> <label for="pfile">Present File</label></td>' . "\n" .
				'<td> <a href="' .$fileurl . $pfile. '" target="_blank">' .$pfile. '</a></td>  ' . "\n" .
			'</tr>' . "\n";
		}

		$t_index++;		
		echo '<tr>' . "\n" .
			'<td> <label for="ufile">File</label></td>' . "\n" .
			'<td> <input ' . $val_ufile . ' type="file" name="ufile" id="ufile" size="45" 
				tabindex="' . $t_index . '" /> ' .$filenote. '</td>  ' . "\n" .
		'</tr>' . "\n";

		echo '<tr>' . "\n" .
			'<td><label for="active">Active</label></td>  ' . "\n" . 
		 	'<td>' . "\n" . 
				create_slist ( $list, 'active', $row['active'], 1, $t_index ) . 
		 		tooltip('Set to Yes to Allow Client to view and download this file') . "\n" . 
		 	'</td>  ' . "\n" . 
		'</tr>  '. "\n";
		
		echo '<tr><td colspan="2" style="text-align:center; margin:0 auto;" ><input type="submit" name="submit" 
			value="Submit" /></td></tr>' . "\n";  
 	echo '</table></form>' . "\n";
	echo '<script type="text/javascript">document.getElementById(\'ufile\').focus();</script>' . "\n";

}



/***************************************************************
 *
 * function sanitize_vars
	* @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 *
 **************************************************************/


function sanitize_vars( $data )
{
	
	$r_data['active']		= intval ( $data['active'] );
	$r_data['ufile']    = ($data['ufile']['name'] != '') ? $data['ufile']['name'] : '';
	
	if ( !array_key_exists('active', $_POST ) )
		$r_data['active'] = 1;
				
  return $r_data;
}

/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $id, $module_name, $config, $clientid, $cidrow;
	
	$coname  = $cidrow['coname'];
	
	$sitebasdir = $config['site_base_dir'];
	$unldir 		= substr($sitebasdir,0,-12). '/spreadsheets/';
	
	$result = mysql_query("SELECT * FROM " . db_prefix . "files WHERE id = '{$id}'");
	$row    = mysql_fetch_array($result);
	
	print_header('Delete File  - ' . $row['file']);
	
	if ( !empty($_POST ))
	{		
		$result = mysql_query("DELETE FROM " . db_prefix . "files WHERE id = '$id'");
		@unlink($unldir . $row['file']);

		print_mysql_message ( mysql_errno() , 'client', $clientid, '2', 'This file was successfully removed.', 
			'<br /><a href="./?tool=files&clientid='.$clientid.'">Click here to manage the files for ' .$coname .'.</a>' ) ;
	} else {
		echo '<form action="./?tool=files&action=remove&clientid='.$clientid.'" method="post" name="form">' . "\n" .
			'<input type="hidden" name="id" value="' . $id . '">' . "\n" .
			'<div class="center">Are you sure you want to delete this record?</div>' . "\n" .
			'<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;
				<input name="No" type="button" value="No" onClick="window.location = \'./?tool=files&clientid='.$clientid.'\'"></div>' . "\n" .
		'</form>';
	}
}


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of page contents
 *
 **************************************************************/

function update()
{
	global $action, $id, $module_name, $config, $clientid, $cidrow, $dbtbl;
	
	$coname  = $cidrow['coname'];
		
	$sitebasdir = $config['site_base_dir'];
	$unldir 		= substr($sitebasdir,0,-12). '/spreadsheets/';
	
	if ( $action == 'update' )
		print_header('Update File for '.$coname);
	else
		print_header('Add New File for '.$coname);
	
	if ( array_key_exists ('submit',$_POST))
	{				
		require_once("classes/validation.php");
	
		$rules = array();    
		
//		$rules[] = "required,ufile,ufile";
	
		$errors = validateFields($_POST, $rules);
	
		if ($action != 'update' && $_FILES['ufile']['type'] == "")
			$errors[] = 'ufile';
		
		/* Below checks actual extension type and MIME type */
		$filename = basename($_FILES['ufile']['name']);
		$ext 			= substr($filename, strrpos($filename, '.') + 1);
		// $ext is the extension
	
		if ($_FILES['ufile']['type'] != "" && ($_FILES['ufile']['type'] != "pdf" && $ext != "pdf" && 
			$_FILES['ufile']['type'] != "xls" && $ext != "xls"))
		{
			$errors[] = 'ufiletype';
		}
			
	}
	
	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$data  = sanitize_vars( $_POST );			
															
		if ($_FILES['ufile']['name'] != '')
		{
			// remove characters from filename
			$ufile = rembadchar ($_FILES['ufile']['name']);
			
			// add client id and timestamp to filename
			$ufile = $clientid. '_' .$_SERVER['REQUEST_TIME']. '_' .$ufile;
		}
		
		if ( $action == 'update' ) 
		{
			//query present file
			$unlqry = mysql_query("SELECT file FROM " . db_prefix . "files WHERE id = '{$id}'");
			$unlrow = mysql_fetch_array($unlqry);
			$olfile = $unlrow['file'];

			if ($_FILES['ufile']['name'] != '')
			{
				//unlink old file
				@unlink($unldir . $olfile);
				copy($_FILES['ufile']['tmp_name'], "$unldir" . $ufile);
				$newfile = $ufile;
			} else {
				$newfile = $olfile;
			}

			$sql = 'UPDATE ' .$dbtbl. ' SET ';
		
			$xsql[] .= "file	 	 = '" . $newfile . "' ";
			$xsql[] .= "active	 = '" . $data['active'] . "' ";

			$slen = count ( $xsql );
	 
			for($i=0; $i<$slen; $i++)
			{
				$sql .= $xsql[$i];
				if ( $i != ( $slen - 1 ) )
					$sql .= ', ';      
			}
			
			$sql .= " WHERE id = '$id'";
			
			echo 'file: '.$newfile;									 
			
			$type = 0;
		} else {
			$data = sanitize_vars( $_POST );
			
					
		 	$sql = 'INSERT INTO ' .$dbtbl. ' (clientid, file, date, active) 
							VALUES ' . "('" . $clientid ."', '" .$ufile ."', '" .date('Y-m-d')."', 
													 '" . $data['active'] . "')";
			
			copy($_FILES['ufile']['tmp_name'], "$unldir" . $ufile);
			echo 'file: '.$ufile;									 
			$type = 1;
		}	
			
		mysql_query ( $sql );
		
		if ( ( mysql_errno() != 0 ) && ( is_saint() ) )
			print_debug($sql);
			
		$rlink = '<a href="./?tool='.$_GET['tool'].'&clientid='.$clientid.'" >Click here to manage the files</a> 
			for '.$coname.'.<br />
			<a href="./?tool='.$_GET['tool'].'&action=add&clientid='.$clientid.'">Click here to add new files</a> 
			for '.$coname.'.';
		
		print_mysql_message ( mysql_errno() , $module_name, $id, $type, '', $rlink ) ;
				
  } else {
		add( $errors );
	}//end if	
}//end function
