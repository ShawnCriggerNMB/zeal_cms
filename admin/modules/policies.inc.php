 <?php	
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : Security Module v0.0.1
// Written By : Darrell Bessent of EWD
// Written On : May 07, 2013
// Revision   : 001
// Updated by : Darrell Bessent of EWD
// Updated On : May 08, 2013
// Description: This module is used to create security
//							policies and configurations within the CMS.
// Copyright Zeal Technologies & BlackSuit I.T.
//***************************************************/

/***************************************************************
 *
 * Sanitize variables for use in forms and whatnots
 *
 *
 **************************************************************/

$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);
$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);


$mod_config  = get_module_settings ( 'policies' );


function execute()
{
	switch($_GET['action'])
	{
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}//end switch
}//end function


/***************************************************************
 *
 * function manage
 * Querrys DB and Displays Slider Content
 *
 *
 **************************************************************/

function manage()
{	

	// Server Stats: Server Online/Offline ?
	
	// Connection Stats: Recent connections, Current connections
	
	// Prevention Stats: Host Prevention, Injection Prevention
	
	// Change Module Level Permissions (LOCK DOWN | `Change all to Programmer` for lock down / development mode)
	
	// Display already configured policies
  $i    = 0;
  $link = '<a href="./?tool=policies&action=add">Add New Policy</a>';
  
  print_header('Manage Security Policies',$link);
  
  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 
       ' 	  <thead>' . "\n" . 
       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" . 
       ' 			  <th><h3>ID</h3></th>' . "\n" . 	
			 ' 			  <th><h3>Policy</h3></th>' . "\n" . 
			 ' 			  <th><h3>Value</h3></th>' . "\n" .
			 ' 			  <th><h3>Policy Note</h3></th>' . "\n" .    
       ' 			  <th style="width:50px; text-align:center;"><h3>Order</h3></th>' . "\n" . 
       ' 			  <th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . 
       ' 			  <th style="width:100px;" class="nosort"><h3>Tools</h3></th>' . "\n" . 
       '		 </tr></thead><tbody>' . "\n";
  
  $results = mysql_query('SELECT * FROM ' . db_prefix . 'security ORDER BY policy');
  $pages   = mysql_num_rows($results);
  while ($row = mysql_fetch_array($results))
  {    

    $i++;
    
    $id				= $row['id'];
		$policy	  = $row['policy'];
		$value		= $row['value'];
		$pnote	  = $row['pnote'];
		          
    $act = yes_no ( $row['active'] );
		
    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";
    echo '<td style="padding-left:20px;">' . $id . '</td>' . "\n"; 	
		echo '<td style="padding-left:20px;">' . $policy . '</td>' . "\n";
		echo '<td style="padding-left:20px;">' . $value . '</td>' . "\n";
		echo '<td style="padding-left:20px;">' . $pnote . '</td>' . "\n"; 
    echo '<td style="width:50px; text-align:center;">' . $row['weight'] . '</td>' . "\n"; 
    echo '<td style="width:50px; text-align:center;">' . $act . '</td>' . "\n"; 
    echo '<td style="width:100px;" valign="middle"><strong><a href="./?tool=policies&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=policies&action=remove&id='.$row['id'].'">Remove</a></strong></td>';
    echo '</tr>' . "\n";
	  
  }//end while

  echo '</tbody></table>';

  $pages = ( $pages > 20 ) ? true : false;
	
  echo_js_sorter ( $pages );
             
}//end function

/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Banner Form for User Input
 *
 *
 **************************************************************/

function add( $errors = '' )
{
  global $id, $action, $site_base_url, $site_base_dir, $mod_config;
   
   
  if ( $errors )
  {
   echo '<div class="error_message">';
   if ( in_array('policy', $errors ) )
     echo '* You must fill out a policy.<br/>';
   echo '</br></div>' . "\n";
  } else {
    $c = ( $action == 'update' ) ? 1 : 0;
    print_notice_message ( $c );
  }
  
  if ($action == "update")
  {
   // Run the query for the existing data to be displayed.
   $results = mysql_query('SELECT * FROM ' . db_prefix . 'security WHERE id = "' . $id . '"');
   $row 				= mysql_fetch_array($results);
  } 
	if (!empty ($_POST))
	{	
   $row     = sanitize_vars ($_POST);
  }//end if
  
	echo '<div class="notice_message">NOTES : <br /> 
					<ul>
						<li>The `NONE` If you need this your are a noobster.</li>
					<ul>	
				</div>';
	
  echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";			
  echo '<input type="hidden" name="id" value="' . $id . '" />';
  
  $t_index = 1;
		echo '   <tr>' . "\n" .
							' 						<td ><label for="policy">Active</label></td>  ' . "\n" . 
							' 						<td >' . "\n" . 
							' 						<select id="policy" name="policy">' . "\n" . 
							' 									<option value="IP_BLOCK" '; if ( $row['policy'] == 'IP_BLOCK' ) echo 'SELECTED'; echo ' >IP_BLOCK</option>' . "\n" . 
							' 									<option value="STRING_BLOCK" '; if ( $row['policy'] == 'STRING_BLOCK' ) echo ' SELECTED'; echo ' >STRING_BLOCK</option>																' . "\n" . 
							' 						</select>' . "\n" . 
       ' 						</td>  ' . "\n" . 
							'				</tr>  '. "\n";
			 	  
  echo '   <tr>' . "\n".
       '      <td > <label for="value">Value</label></td>  ' . "\n" . 
       '      <td > <input type="text" name="value" id="value" value="'. $row['value'] .'" size="45" /></td>  ' . "\n" . 
       '   </tr>' . "\n";		  	
			 
		  $t_index++;
		$r_pnote = str_replace('<br />', '\n', $row['pnote']);
	echo '   <tr>' . "\n".
       '      <td > <label for="pnote">Policy Note</label></td>  ' . "\n" . 
       '      <td > <textarea name="pnote" id="pnote" rows="5" cols="40" tabindex="' . $t_index . '">'.$r_pnote.'</textarea></td>  ' . "\n" . 
       '   </tr>' . "\n";			  


  $t_index++;
  echo '   <tr>' . "\n".
       '      <td > <label for="weight">Order</label></td>  ' . "\n" . 
       '      <td > <input type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />' . "\n" .
       ' 						<span class="note" style="padding-left:5px;"> ( Recommend increments of 10 ) </span>' . "\n" .
       '      </td>' . "\n" .
       '   </tr>' . "\n";
   
  $t_index++;
		echo '   <tr>' . "\n" .
							' 						<td ><label for="active">Active</label></td>  ' . "\n" . 
							' 						<td >' . "\n" . 
							' 						<select id="active" name="active">' . "\n" . 
							' 									<option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 
							' 									<option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 
							' 						</select>' . "\n" . 
						 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to Display on Website ) </span>' . "\n" . 
       ' 						</td>  ' . "\n" . 
							'				</tr>  '. "\n";
	
 // This is our submit
  $t_index++;
  echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";  
  echo '</table></form>' . "\n";
  echo '<script type="text/javascript">document.getElementById(\'title\').focus();</script>' . "\n";

}


/***************************************************************
 *
 * function sanitize_vars
	* @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 *
 **************************************************************/


function sanitize_vars( $data )
{
  global $mod_config;  
  
  $r_data['policy']  = mysql_real_escape_string ( $data['policy'] );
	$r_data['value']   = mysql_real_escape_string ( $data['value'] );
	$r_data['pnote']   = mysql_real_escape_string ( $data['pnote'] );
	$r_data['weight']  = mysql_real_escape_string ( $data['weight'] );
	$r_data['active']  = mysql_real_escape_string ( $data['active'] );
			
  $tid = ( intval ( $data['id'] ) == 0 ) ? fetch_lastid ('security') : $data['id'];
			 
  if ( $data['active'] == 0 && empty($_POST['active']) )
  {
    $r_data['active'] = 1;
    $r_data['weight']  = fetch_weight('news', 'weight' );
  }
  
  return $r_data;

}

/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 * Unlinks Existing Photo
 *
 **************************************************************/

function remove()
{

	global $id, $module_name;
	
	$result = mysql_query("SELECT * FROM " . db_prefix . "security WHERE id = '$id'");
	$row    = mysql_fetch_array($result);
	
	print_header('Delete Security Policy - ' . $row['policy']. ' : ' . $row['value']);
	
	if ( !empty($_POST ))
	{		
					

		$result = mysql_query('DELETE FROM ' . db_prefix . "security WHERE id = '$id'");
		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;
			
	} else {
	
		echo '<form action="./?tool=policies&action=remove" method="post" name="form">' . "\n" .
							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .
							'<tr> <td><div align="center">Are you sure you want to delete this news post?</div></td></tr>' . "\n" .
							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .
							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=policies\'"></div></td></tr>' . "\n" .
							'</table></form>';
	}//end if
}//end function


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of page contents
 *
 **************************************************************/

function update()
{
		global $action, $id, $module_name;
		
		if ( $action == 'update' )
				print_header('Update Security Policy');
		else
				print_header('Add Security Policy');
 
		if ( array_key_exists ('submit',$_POST))
		{				
    require_once("classes/validation.php");
  
    $rules = array();    
    
    $rules[] = "required,policy,policy";
		$rules[] = "required,value,value";
    
    $errors = validateFields($_POST, $rules);
    
		}
		
		if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	 {
		
				$data  = sanitize_vars( $_POST );			
		 	  $type  = ( $action == 'update' ) ? 0 : 1;
				$pnote = str_replace('\n', '<br />', $data['pnote']);
				
				if ( $action == 'update' )
						$sql = "UPDATE " . db_prefix . "security SET policy = '" . $data['policy'] . "', value = '" . $data['value'] . "', pnote = '" . $pnote . "', weight = '" . $data['weight'] . "', active = '" . $data['active'] . "'	WHERE id = '$id'";
 		 else
					 $sql = 'INSERT INTO ' . db_prefix . 'security (policy, value, pnote, weight, active) VALUES ' .
												 "('" . $data['policy'] ."', '" . $data['value'] . "', '" . $pnote . "', '" . $data['weight'] . "', '" . $data['active'] . "')";
											
//    echo $sql;
    
				mysql_query ( $sql );
    
    if ( is_saint() && mysql_errno != 0 )
      echo '<div class="error_message"><pre>' . $sql . '</pre></div>';
   			
				print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;   
		
  } else {
		add( $errors );
	}//end if	
}//end function



?>