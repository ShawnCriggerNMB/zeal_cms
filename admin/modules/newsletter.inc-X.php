<?php
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : Newsletter Module
// Written By : Darrell Bessent
// Written On : May 04, 2013
// Updated by : Darrell Bessent of EWD
// Updated On : May 04, 2013
// Copyright Zeal Technologies & S-Vizion Software
//***************************************************/


/***************************************************************
 *
 * Sanitize variables for use in forms and whatnots
 *
 *
 **************************************************************/

$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);
$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);

$mod_config  = get_module_settings ( 'newsletter' );
	// connect to db
	include ('../Globals/dbconnect.php');
	
	// Fetch config data from database
	$foo    = mysql_query ('SELECT * FROM cms_config WHERE id=1');
	$config = mysql_fetch_array ( $foo );

	// basic variables
	$sitename		 = $config['site_name'];

function execute()
{
	switch($_GET['action'])
	{
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}//end switch
}//end function


/***************************************************************
 *
 * function manage
 *
 **************************************************************/

function manage()
{
	global $action, $id;

	$link = '<a href="./?tool=newsletter&action=add">Send A Newsletter</a>';
	print_header('Newsletter Email Address Submissions', $link );

	$th  = "\n";


	echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" .
			 '<thead>' . "\n" .
			 '	<tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" .
			 $th .
			 '		<th><h3>ID</h3></th>' . "\n" .
			 '		<th><h3>Email Address</h3></th>' . "\n" .
			 '		<th style="width:100px" class="nosort" colspan="3" ><h3>Tools</h3></th>'.  "\n" .
			 '	</tr></thead><tbody>' . "\n";

	$query = mysql_query("SELECT * FROM " . db_prefix . "newsletter");
	$pages = mysql_num_rows ( $query );
	while ( $row = mysql_fetch_array ( $query ))
	{

		$td = '';
		echo '<tr>' ."\n" . $td .
							'  <td style="padding-left:20px;" >'.$row['id'].'</td>' ."\n" .
							'  <td style="padding-left:20px;" >'.$row['email'].'</td>' ."\n" .
							'  <td style="width:50px; text-align:center;"><a href="./?tool=newsletter&action=remove&id='.$row['id'].'">remove</a></td>' ."\n";

		echo '</td></tr>' ."\n";
	} // end while

	echo '</tbody></table>';

	$pages = ( $pages > 20 ) ? true : false;

	echo_js_sorter ( $pages, $s );

	echo '<div class="spacer">&nbsp;</div>' . "\n";

}


/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $id, $module_name;

	$query = mysql_query("SELECT id,email FROM " . db_prefix . "newsletter WHERE id = '".$id."'");
	$row = mysql_fetch_assoc($query);
	$totalRows = mysql_num_rows($query);

	print_header('Delete Newsletter Submission From - ' . $row['email'] );



		if(!empty($_POST))
		{
			$deleteSQL = mysql_query("DELETE FROM " . db_prefix . "newsletter WHERE id = '".$row['id']."'");
			print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;
		} else {
				echo '<form action="./?tool=newsletter&action=remove" method="post" name="form">' . "\n" .
						 '<input type="hidden" name="id" value="' . $id . '"><table width="80%" border="0">' . "\n" .
						 '<tr> <td><p style="text-align:center"><strong>You are about to Delete the "'.$row['email'].'" submission.</strong></p></td></tr>' .
						 '<tr> <td><div align="center">Are you sure you want to delete this record?</div></td></tr>' . "\n" .
						 '<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .
						 '<input name="No" type="button" value="No" onClick="window.location = \'./?tool=pages\'"></div></td></tr>' . "\n" .
						 '</table></form>';
		} // end if POST

}


/***************************************************************
 *
 * function sanitize_vars
	* @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 *
 **************************************************************/


function sanitize_vars( $data )
{
	$r_data['label']       = mysql_real_escape_string ( $data['label'] );
	$r_data['content'] = mysql_real_escape_string ( stripslashes ( $data['content'] ));
	$r_data['meta_title']  = mysql_real_escape_string ( $data['meta_title'] );
	$r_data['meta_kw']     = mysql_real_escape_string ( $data['meta_kw'] );
	$r_data['meta_descr']  = mysql_real_escape_string ( $data['meta_descr'] );

	return $r_data;
}



function add( $errors )
{
	global $action, $id, $sitename;

	$base_path = dirname(__FILE__);

	include_once $base_path . '/ckeditor/ckeditor.php' ;
	$option = '';

	if ( $errors != '' )
	{
	//$errormsg = implode(' ', $errors);
		echo '<div class="error_message"><strong>ERROR:</strong><br />'.$errors.'</div>';
	} else {
		echo  '<div class="notice_message">To send a newsletter create the letter in the provided area or paste in a word document using the Word toolbar button.<br />' . "\n" .
					'<ul><strong>NOTES:</strong>' . "\n" .
					'<li> The email addresses are already filled out for you.</li>' . "\n" .
					'<li> Hold <strong>Shift</strong> and press <strong>Enter</strong> at same time to Start a new paragraph.</li>' . "\n" .
					'</ul></div>' . "\n";
	}


				$fnavqry = mysql_query("SELECT * FROM cms_newsletter");
				$ftotnum = mysql_num_rows($fnavqry);
				
				for ($i=1; $i<=$ftotnum; $i++)
				{
					$spacer = ($i == $ftotnum) ? '' : ', ';
					
					$fnavrow = mysql_fetch_assoc($fnavqry);
					$emails .= $fnavrow['email'].$spacer;
				}
	$r = required();

	echo '<form name="signup" id="signup" method="post" action="#" ><table>' . "\n";
	echo '<input type="hidden" name="id"   value="' . $id . '" />';

	echo '   <tr>' . "\n" .
						'					<td  class="labelcell"> <label for="to">'.$r.' TO: </label></td>' . "\n" .
						'				 <td > <input ' . $val_label . ' type="text" name="to" id="to" value="'. $emails .'" size="110" tabindex="1" />' . "\n" .
						'			 </tr>' . "\n";
						
		echo '   <tr>' . "\n" .
						'					<td  class="labelcell"> <label for="subject">'.$r.' Subject: </label></td>' . "\n" .
						'				 <td > <input ' . $val_label . ' type="text" name="subject" id="subject" value="'.$sitename.' Newsletter" size="110" tabindex="1" />' . "\n" .
						'			 </tr>' . "\n";					

	echo '<tr><td colspan="2" style="width:100%">&nbsp;</td></tr>' . "\n";

	echo  '<tr><td colspan="2" style="text-align:center; width:100%">' . "\n" .
				'<textarea style="min-height:270px; height:270px; width:100%;" name="content" id="content"></textarea>' . "\n" .
				'</td></tr>' . "\n";

	$ckeditor  = new CKEditor( ) ;
	$ck_config = set_fck_config();
	$ck_events = set_fck_events();

	$ck_config['toolbar'] = set_fck_toolbar();
	$ck_config['height']  = 200;
	$ck_config['width']   = '102%';

// EDIT BELOW LINE TO ADD YOUR WEBSITE STYLESHEET
//		$ck_config['contentsCss'] = $base_site_url . '/styles/main.css';
	$ckeditor->replace("content", $ck_config, $ck_events);


	echo '<tr><td colspan="2" style="width:100%">&nbsp;</td></tr>' . "\n";

	echo '<tr><td colspan="2" style="width:100%">&nbsp;</td></tr>'. "\n";
	echo '<tr><td colspan="2" style="width:100%; text-align:center; margin:0 auto; padding:3px; "><input name="Submit" type="submit" value="Submit" class="button"> </td></tr>'. "\n";
	echo '</table></form>'. "\n";
	echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>' . "\n";
	echo '<div class="spacer">&nbsp;</div>';

}//end if empty get id


function update()
{
	global $action, $id, $module_name;

	$data = $_POST;
		
			if ( ( $action == 'add' ) && ( !empty($_POST) ) )
	{

			$error = false;
			if ($data['to'] == "")
			{
				$error = true;
				$errormsg .= "The emails are required.<br />";
			}//end if
			
			if ($data['subject'] == "")
			{
				$error = true;
				$errormsg .= "The emails are required.<br />";
			}//end if
			
			if ($data['content'] == "")
			{
				$error = true;
				$errormsg .= "The newsletter is required.<br />";
			}//end if

			if ( $error == false )
			{
				
				$data['content'] = strip_fck_strip_value( $data['content']) ;
				
				
						$sql = "SELECT email FROM cms_newsletter WHERE email !='' AND email != 'NULL'";
						$result = mysql_query($sql);
						$totrow = mysql_num_rows($result);

				while ($row = mysql_fetch_assoc($result)) 
				{
					// Create the emails and send them.
					// Adjust your $rows accordingly
					$to = $row['email'];
					$subject = $data['subject'];
					$emsg = $data['content'];
					$headers = 'FROM: staff@lostatsesamemorial.com';
					// Send the mail using PHPs mail() function
					mail($to, $subject, $emsg, $headers);
				
				}
			echo '<div class="success_message"><strong>This message was sent to:</strong> ' 
							. $totrow . ' newsletter subscribers.</div><br /><br />';
				//print_mysql_message ( mysql_errno() , $module_name, $id, 1, $msg ) ;
			} else {
				add($errormsg);
			}// end if !$error
		} else {

			add($errormsg);
		}
}


?>