<?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Rotating Images Module

// Written By : Shawn Crigger of EWD

// Written On : May 1, 2010

// Revision   : 163

// Updated by : Darrell Bessent of EWD

// Updated On : Feb. 14, 2013

// Copyright Zeal Technologies & S-Vizion Software

//***************************************************/



/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);





$mod_config  = get_module_settings ( 'rotating_comm' );



$upload_url  = $site_base_url . $cms_uploads . 'rotating/communities/';

$upload_dir  = $site_base_dir . $cms_uploads . 'rotating/communities/';

$height = '';

$width  = '771';

//$tn_height = '180';

//$tn_width = '150';



   



function execute()

{

	switch($_GET['action'])

	{

		case 'update':

			update();

			break;

		case 'add':

			update();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Slider Content

 *

 *

 **************************************************************/



function manage()

{



  global $upload_dir,$upload_url;

  

  $i    = 0;

  $link = '<a href="./?tool=rotating_comm&action=add">Add Rotating Image</a>';

  

  print_header('Manage Rotating Community Images',$link);

  

  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 

       ' 	  <thead>' . "\n" . 

       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" .

			 ' 			  <th><h3>Community</h3></th>' . "\n" . 

       ' 			  <th><h3>Image Label</h3></th>' . "\n" . 

       ' 			  <th><h3>Image Name</h3></th>' . "\n" . 

			// ' 			  <th><h3>ALT Tag</h3></th>' . "\n" .

       ' 			  <th style="width:50px; text-align:center;"><h3>Order</h3></th>' . "\n" . 

       ' 			  <th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . 

       ' 			  <th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" . 

       '		 </tr></thead><tbody>' . "\n";

  

  $results = mysql_query('SELECT * FROM ' . db_prefix . 'rotating_comm ORDER BY weight,label');

  $pages   = mysql_num_rows($results);

  while ($row = mysql_fetch_array($results))

  {    



    $i++;

    

    $link  = 'No Image Uploaded';

    $label = stripslashes ( $row['label'] );

		

		$alttag = stripslashes ( $row['alttag'] );

		

		$community = $row['community'];

    

    $filename = $upload_dir . $row['image'];

    

    $link = ( file_exists ( $filename ) ) ? '<a href="' . $upload_url . $row['image'] . '" alt="' . $alttag . '" title="' . $label . '" class="lightbox" >' . $row['image'] . '</a>' : $link;

                

    $act = yes_no ( $row['active'] );



	// Display selected Community

	$qry_com = mysql_query("SELECT label FROM cms_communities WHERE `id`='$community'");

	$totnum = mysql_num_rows($qry_com);

	$row_com = mysql_fetch_assoc($qry_com);

	$community2 = $row_com['label'];

    

    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

		echo '<td style="padding-left:20px;">' . $community2 . '</td>' . "\n";

    echo '<td style="padding-left:20px;">' . $label . '</td>' . "\n";

    echo '<td style="padding-left:20px;">' . $link . '</td>' . "\n";

		//echo '<td style="padding-left:20px;">' . $alttag . '</td>' . "\n";

    echo '<td style="text-align:center;">' . $row['weight'] . '</td>' . "\n";

    echo '<td style="text-align:center;">' . $act . '</td>' . "\n";

    echo '<td valign="middle"><strong><a href="./?tool=rotating_comm&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=rotating_comm&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

    echo '</tr>' . "\n";

	  

  }//end while



  echo '</tbody></table>';



  $pages = ( $pages > 20 ) ? true : false;

	

  echo_js_sorter ( $pages );

             

}//end function



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Banner Form for User Input

 *

 *

 **************************************************************/



function add( $errors = '' )

{

  global $id, $action, $site_base_url, $upload_url, $upload_dir, $mod_config, $height, $width;

   

   

  if ( $errors )

  {

   echo '<div class="error_message">';

   if ( in_array('label', $errors ) )

     echo '* You must fill out a label name.<br/>';

   echo '</br></div>' . "\n";

  } else {

    $c = ( $action == 'update' ) ? 1 : 0;

    print_notice_message ( $c );

  }

  

  if ($action == "update")

  {

   // Run the query for the existing data to be displayed.

   $results = mysql_query('SELECT * FROM ' . db_prefix . 'rotating_comm WHERE id = "' . $id . '"');

   $row 				= mysql_fetch_array($results);

  } 

	if (!empty ($_POST))

	{	

   $row     = sanitize_vars ($_POST);

  }//end if

  

	

		echo '<div class="notice_message">NOTES : <br /> 

					<ul>

						<li>The `IMAGE LABEL` is to help you keep your images organized.</li>

						<li>The `ALT Tag` is to help you set alternate image tags.</li>

						<li>The `Community` will assign the image to the community. Each rotating community image may only appear in one community.</li>

						<li>The `ORDER` controls the image display order for the rotating images on the home page.</li>

						<li>The `ACTIVE` is to set the image post visability to on or off.</li>

						<li>Image should be `RECTANGLE` and `771w X 213h`.</li>

						<li>Larger images will be down-sized to fit properly. <br />

							<span style="color:#FF0000;">WARNING:</span> Images smaller than 771w X 213h will be stretched and not look proper or clean.</li>

					<ul>	

				</div>';

	

  echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

  $t_index = 1;

  echo '   <tr>' . "\n".

       '      <td > <label for="label">Label</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="label" id="label" value="'. $row['label'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

			 

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="alttag">ALT Tag</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="alttag" id="alttag" value="'. $row['alttag'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

	

		// Build Community Option Boxes

	$qry_com = mysql_query("SELECT id,label FROM cms_communities WHERE `active`='1' ORDER BY weight");

	$totnum = mysql_num_rows($qry_com);

	$com_options = '';

	

	// Check if selected

		$qry2 = mysql_query("SELECT community FROM cms_rotating_comm WHERE `id`='$id'");

		$row2 = mysql_fetch_array($qry2); // fetch array

		$iscom = explode(',', $row2['community']); // explode it

	while ($row_com = mysql_fetch_assoc($qry_com))

	{



		if (in_array($row_com['id'], $iscom)) //use in_array to check if in array

		{

			$checked = 'checked="checked"';

		} else

			{

				$checked = '';

			}

		// End check if selected



		$com_options .= '<input name="comm_id[]" type="radio" value="'.$row_com['id'].'" '.$checked.' />&nbsp;<label for="'.$row_com['label'].'">'.$row_com['label'].'</label><br />';

	}

	// End of Build Community Options Boxes

	// Print out the community options boxes

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="comm_id">Community</label></td>  ' . "\n" . 

       '      <td >'.$com_options.'</td>  ' . "\n" . 

       '   </tr>' . "\n";

	// end print out		 



  $t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="weight">Order</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />' . "\n" .

       ' 						<span class="note" style="padding-left:5px;"> ( Recommend increments of 10 ) </span>' . "\n" .

       '      </td>' . "\n" .

       '   </tr>' . "\n";

   

  $t_index++;

		echo '   <tr>' . "\n" .

							' 						<td ><label for="active">Active</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="active" name="active">' . "\n" . 

							' 									<option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 

							' 									<option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 

							' 						</select>' . "\n" . 

						 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to Display on Website ) </span>' . "\n" . 

       ' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";

  

  /****************************

  * Image Uploading Section

  ***************************/

  

  if ( $row['image'] != '' )

  {

    

   $filename      = $upload_dir . $row['image'];

   list ( $w, $h ) = img_size ( $filename );

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="current_file">Current Image</label></td>' . "\n";

   echo '<td ><a href="' . $upload_url . $row['image'] . '" alt="' . $row['alttag'] . '" title="' . $row['label'] . '" target="_blank" class="lightbox" >' . $row['image'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="current_file" value="' . $row['image'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="image">Upload Image</label></td>' ."\n";

  echo '<td ><input name="image" type="file" id="image" size="15">' . "\n";

  echo '<span class="note">( Image will be auto-magicly resized to 771w X 213h pixels )</span></td>' . "\n";

  echo '</tr>' . "\n";

 



  $t_index++;

  echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";  

  echo '</table></form>' . "\n";

  echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>' . "\n";



}





/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

  global $upload_dir, $mod_config, $known_photo_types, $height, $width, $tn_height, $tn_width;

  

  

  $r_data['label']  = mysql_real_escape_string ( $data['label'] );

	$r_data['alttag']  = mysql_real_escape_string ( $data['alttag'] );

  $r_data['weight']  = intval ( $data['weight'] );

  $r_data['active'] = intval ( $data['active'] ) ;

	$r_data['community']  = $data['community'];

  $r_data['image']  = $data['current_file'];

  $r_data['image_tn']  = $data['current_file'];

		

  $tid = ( intval ( $data['id'] ) == 0 ) ? fetch_lastid ('rotating_comm') : $data['id'];

      		 

  if ( !empty ( $_FILES['image']['name'] ) )

  {

            

    $old_file        = $r_data['image'];

    $image           = $_FILES['image'];

    

		

		$r_data['image'] = process_upload ( $tid , '', $image, $known_photo_types, $upload_dir, 1, $width, $height );

		

		//$r_data['image_tn'] = process_upload ( $tid , 'tn', $image, $known_photo_types, $upload_dir, 1, $tn_width, $tn_height );

						

    if ( ( $old_file != $r_data['image'] ) && ( file_exists ( $upload_dir . $old_file ) ) && ( $old_file != '' ))

		{

			unlink ( $upload_dir . $old_file );

			

			//$sufx = substr($old_file, -4);

			//$timg = substr($old_file,0,-4);

			//$timg = $timg.'_tn'.$sufx;

				

			//unlink( $upload_dir . $timg );

		}

  

	}

			 

  if ( empty($_POST['weight']) )

  {

    $r_data['weight']  = fetch_weight('rotating_comm', 'weight' );

  }

  

  return $r_data;



}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 * Unlinks Existing Photo

 *

 **************************************************************/



function remove()

{



	global $id, $upload_dir, $module_name;

	

	$result = mysql_query("SELECT * FROM " . db_prefix . "rotating_comm WHERE id = '$id'");

	$row    = mysql_fetch_array($result);

	

	print_header('Delete Rotating Image - ' . $row['label']);

	

	if ( !empty($_POST ))

	{		

					

		if ( file_exists ( $upload_dir . $row['image'] ))

		{

				@unlink( $upload_dir . $row['image'] );

				

				//$sufx = substr($row['image'], -4);

				//$timg = substr($row['image'],0,-4);

				//$timg = $timg.'_tn'.$sufx;

				

				//@unlink( $upload_dir . $timg );

		}

		//echo 'File:'.$timg;

		

		$result = mysql_query('DELETE FROM ' . db_prefix . "rotating_comm WHERE id = '$id'");

		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

			

	} else {

	

		echo '<form action="./?tool=rotating_comm&action=remove" method="post" name="form">' . "\n" .

							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

							'<tr> <td><div align="center">Are you sure you want to delete this image?</div></td></tr>' . "\n" .

							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=rotating_comm\'"></div></td></tr>' . "\n" .

							'</table></form>';

	}//end if

}//end function





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

		global $action, $id, $module_name;

		

		if ( $action == 'update' )

				print_header('Update Rotating Image');

		else

				print_header('Add New Rotating Image');

 

		if ( array_key_exists ('submit',$_POST))

		{				

    require_once("classes/validation.php");

  

    $rules = array();    

    

    $rules[] = "required,label,label";

    

    $errors = validateFields($_POST, $rules);

    

		}

		

		if(!empty($_POST['comm_id'])) {

			$allcoms = implode(",", $_POST['comm_id']);

		}

		

		if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

	 {



				$data  = sanitize_vars( $_POST );			

		  $type  = ( $action == 'update' ) ? 0 : 1;

				if ( $action == 'update' )

						$sql = "UPDATE " . db_prefix . "rotating_comm SET label = '" . $data['label'] . "', weight = '" . $data['weight'] . "', active = '" . $data['active'] . "', image = '" . $data['image'] . "', alttag = '" . $data['alttag'] . "', community = '" . $allcoms . "'	WHERE id = '$id'";

 		 else

					 $sql = 'INSERT INTO ' . db_prefix . 'rotating_comm (label, active, weight, image, alttag, community) VALUES ' .

												 "('" . $data['label'] ."', '" . $data['active'] . "', '" . $data['weight'] . "', '" . $data['image'] . "', '" . $data['alttag'] . "', '" . $allcoms . "')";

											

//    echo $sql;

    

				mysql_query ( $sql );

    

    if ( is_saint() && mysql_errno != 0 )

      echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

   			

				print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;   

		

  } else {

		add( $errors );

	}//end if	

}//end function







?>