<?php
// PHP Anti Virus v1.0 BETA
// BlackSuit Information Technology
// Zeal Technologies
// Eagle Web Designs
// *****************************************************************************************
// This script is planned to be a security assessment tool
// used to help secure websites. This script will search for all
// files on the server and scan each file's contents for
// known keywords and source code used in popular trojans
// and backdoors that are being uploaded to breeched sites.
// The script will also attempt to compare file SHA256 hashes
// to public databases to identify viruses, trojans, and other infected
// or potentially harmful files.

// PHPAV is Developed by 
// BlackSuit Information Technology
// Project started : Saturday July 13, 2013 @ 3:19am

// main()

// initialize the script
$avFile = 'anti-virus.php';
$keyFile = 'key-database.txt';
$shaFile = 'sha-database.txt';
$avDir = '/home/hostzeal/public_html/admin/modules/phpav/';
$siteDir = '/home/hostzeal/public_html';

main($siteDir, $avDir, $avFile, $keyFile, $shaFile);
// *****************************************************************************************

// main() function

function main($siteDir, $avDir, $avFile, $keyFile, $shaFile) {
	
	// This is the welcoming banner
	echo '<img src="/admin/modules/phpav/logo.png" /><br /><br />';
	echo '<form method="post" action="index.php?tool=antivirus"><input style="width: 400px;" name="cmd" type="submit" value="Start" /></form><br />';


	// Now initialize our scan
	if ($_POST['cmd'] == 'Start') { init_scan($siteDir, $avDir, $avFile, $keyFile, $shaFile);}
}
// *****************************************************************************************

// init_scan() function
function init_scan($siteDir, $avDir, $avFile, $keyFile, $shaFile)
{
	
	echo '<div style="width: 800px; height:300px; overflow:scroll; border: 1px solid #000">';
	find_files($siteDir, '/$/', 'my_handler', $avDir, $avFile, $keyFile, $shaFile);
	echo '</div>';
}
// *****************************************************************************************

// find_files($path, $pattern, $callback) function

// Find files on the server, and drop to a callback function

function find_files($path, $pattern, $callback, $avDir, $avFile, $keyFile, $shaFile) {
	$path = rtrim(str_replace("\\", "/", $path), '/') . '/';
	$matches = Array();
	$entries = Array();
	$dir = dir($path);
	
	while (false !== ($entry = $dir->read())) {
		$entries[] = $entry;
	}
	
	$dir->close();
	
	foreach ($entries as $entry) 
	{
		$fullname = $path . $entry;
		if ($entry != '.' && $entry != '..' && is_dir($fullname)) {
		find_files($fullname, $pattern, $callback, $avDir, $avFile, $keyFile, $shaFile);
	} else if (is_file($fullname) && preg_match($pattern, $entry)) 
		{ 
			call_user_func($callback, $fullname, $avDir, $avFile, $keyFile, $shaFile); 
		}
	}
}
// *****************************************************************************************

// my_handler($filename) function

// With the found files from find_files
// perform the handler function called in find_files 
// as a $callback function. This handles each
// of the required routines to scan a file.

function my_handler($filename, $avDir, $avFile, $keyFile, $shaFile) {
	// Routine 1 - Indentify and Analyze
	$file = $filename;
	$contents = file_get_contents($file);
	$fSHA256 = hash_file ('sha256', $file);
	
	if ($file == $avDir.$avFile || $file == $avDir.$keyFile || $file == $avDir.$shaFile)
	{
		// These are AV files that contain the keys and hashes.
	}
		else {

			// Routine 2 - Scanning and Parsing
			// Backdoor Scan 
			backdoor_scan($file, $contents, $fSHA256, $avDir, $keyFile);
			
			// Routine 3 - Virus Scanning
			// Virus Scan
			sha256_scan($file, $fSHA256, $avDir, $shaFile);
		}
	
																//echo substr($filename, 27); // just the file name, no path
}
// *****************************************************************************************

// Backdoor Scan
// This function checks the key database 
// against each file's contents for known keywords, strings, 
// and source code that are backdoor and trojan related. 
// If a key string of a known backdoor is detected, you will be notified.

function backdoor_scan($file, $contents, $fSHA256, $avDir, $keyFile) {
	
	$database = $avDir.$keyFile;
	$keys = file_get_contents($database);	
	
	$key = explode (',', $keys);
	foreach($key as $ke =>$k){ 
	
	/* The \b in the pattern indicates a word boundary, so only the distinct
	 * word "web" is matched, and not a word partial like "webbing" or "cobweb" */
		if (preg_match("/\b".$k."\b/i", $contents)) {
			// Announce if found
			printnow('<div style="color:#C5DE27;"><strong>A match was found!</strong><br /></div>', false);
			printnow('<div style="color:#C5DE27;"><strong>Possible Infected File</strong></div>'.$file.'<br />', false);
			printnow('<div style="color:#C5DE27;"><strong>SHA256</strong></div>'.$fSHA256.'<br />', false);
			printnow('<div style="color:#C5DE27;"><strong>Matched Source</strong></div>'.$k.'<br /><br /><br />', false);
		} else {
				//echo "A match was not found.".'<br /><br />';
		}
	}
//	if (preg_match("/\bob\b/i", $contents)) {
//		echo "A match was found!<br />";
//		echo 'File	: '.$file.'<br />';
//		echo 'SHA256: '.$SHA256.'<br />';	} else {
//			//echo "A match was not found.".'<br /><br />';
//	}
}
// *****************************************************************************************
	
// sha256_scan() function

// This function checks the sha256 virus database 
// against each files sha256 hash. If a hash of a known infected file
// is detected, you will be notified.

function sha256_Scan($file, $fSHA256, $avDir, $shaFile) {

	$database = $avDir.$shaFile;
	$hashes = file_get_contents($database);	
	
	$sha = explode (',', $hashes);
	
	foreach($sha as $sh =>$s){ 
		if(strpos($s, $fSHA256) !== false){
		printnow('<div style="color:#C5DE27;"><strong>WARNING: Infected File Found!!</strong><br /></div>', false);
		printnow('<div style="color:#C5DE27;"><strong>Possible Infected File</strong></div>'.$file.'<br />', false);
		printnow('<div style="color:#C5DE27;"><strong>SHA256</strong></div>'.$fSHA256.'<br /><br />', false); 
		}  
	}
}


// *****************************************************************************************
	
// Attempting to get real time print out of messages
// Does not seem to work though.
function printnow($str, $bbreak=true){
    print "$str";
    if($bbreak){
    	print "<br />";
    }
    ob_flush(); flush();
}

?>