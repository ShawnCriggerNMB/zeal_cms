<?php	
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Class      : AW-Stats Display Module
// Written By : Shawn Crigger of EWD
// Written On : Oct 4, 2010
// Updated By : Cory Shaw of EWD
// Updated On : May 3, 2014
// Copyright Zeal Technologies & S-Vizion Software
//***************************************************/

 
function execute()
{
  global $site_base_url;
  
  $base_path       = str_replace('/home/', '', dirname(__FILE__) );
  
  $base_path       = substr ( $base_path, 0, 8);
  
	// Required settings
  $Domain          = str_replace('http://', '', $site_base_url);
  $Domain          = str_replace('www.', '', $Domain);
	$Domain					 = strtolower($Domain);
    		
  $UserId          = $base_path;       
  //$Secret          = base64_decode( awstat ); // IF PASSWORD IS ENCODED
	$Secret					 = awstat_pass; // IF PASSWORD IS RAW
  $ImageDir        = $site_base_url . '/' . admin_dir . '/images/awicon'; 
   
  // Optional settings
  $DefaultLanguage = 'en';
  $HideLanguages   = true;
  $HideAwstatsLogo = false;
  $CustomLogo      = $ImageDir . '/other/awstats_logo1.png';
  $CustomAltTitle  = '';
  $CustomUrl       = '';
   
  // ------------------------------------------------------
   
//  $SayDomain=(isset($_GET['config'])) ?  $_GET['config'] : $Domain;
  
  if(1==count($_GET))
  {   
    $qs="tool=awstats&config=$Domain&lang=$DefaultLanguage&framename=mainright";
  } else {
    $qs = 'tool=awstats&'; 
    foreach ($_GET as $key=>$value) 
    {
      $value = urlencode(stripslashes($value));
      $qs   .="$key=$value&";
    }
    $qs=substr($qs,0,-1);
  }

  $Secret=rawurlencode($Secret);
  $Stats = file_get_contents("https://$UserId:$Secret@$Domain:2083/awstats.pl?$qs");
  $Stats=str_replace('<form name="FormDateFilter"', "<center><h2>Web statistics for $Domain</h2></center>\r\n<form name=\"FormDateFilter\"", $Stats);
  $Stats=str_replace('awstats.pl', $_SERVER['PHP_SELF'] , $Stats); //
  if ( substr ( $ImageDir,-1)<>'/' )
    $ImageDir.='/';
  $Stats=str_replace('/images/awstats/', $ImageDir, $Stats);
  $Stats=str_replace('framename=index', 'framename=mainright', $Stats);
  $Stats=str_replace('name="framename" value="index"', 'name="framename" value="mainright"', $Stats);

  $Stats=str_replace('<input type="hidden" name="framename" value="mainright" />', '<input type="hidden" name="framename" value="mainright" /><input type="hidden" name="tool" value="awstats" />', $Stats);
  
  $Stats=str_replace('target="mainright"', '', $Stats);
  if ( $HideLanguages)
    $Stats = preg_replace('/<td align="right" rowspan="2">(<a .*<\/a>).*<br \/><a .*<\/td>/Us', '<td align="right" rowspan="2">$1</td>', $Stats, 1);
  if ( $HideAwstatsLogo or $CustomLogo <>'' )
  {
    $Img   = "<img src=\"$CustomLogo\" border=\"0\" alt=\"$CustomAltTitle\" title=\"$CustomAltTitle\" />";
    $Logo  = (''==$CustomUrl) ? $Img : "<a href=\"$CustomUrl\" target=\"_blank\">$Img</a>";
    if ( '' == $CustomLogo)
      $Logo='&nbsp;';
    $Stats = preg_replace('/<td align="right" rowspan="2">(<a .*<\/a>)/Us', '<td align="right" rowspan="2">'.$Logo, $Stats, 1);
  }
  
  $Stats = str_replace('<b>Advanced Web Statistics 6.9 (build 1.925)</b> - <a href="http://awstats.sourceforge.net" target="awstatshome">Created by awstats (plugins: geoipfree)</a>', '', $Stats);
  
  echo $Stats;

}

?>