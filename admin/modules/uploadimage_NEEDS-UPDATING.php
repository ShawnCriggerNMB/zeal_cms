<?php
//	if (!isset($_SESSION['mem_id'])) die();
//	if ( !defined('BASE') ) die('No Direct Script Access');
	
//	 echo 'vrul: '.$_POST['vrulid'].'<br>'; // for testing
	
//	if ($_POST['vrulid'] == '' || !is_numeric($_POST['vrulid']) ) die('Invalid variables');
	
//	$vrulid = $_POST['vrulid'];
?>	
	<p class="pagename">Upload New Photos</p>
	<?php 
		if(!empty($_POST) && $_POST['dowhat'] == "upload") 
		{
			$error = false;	
			
			// make sure at least one image is being uploaded
			if ($_FILES['imagefile1']['type'] == "" && $_FILES['imagefile2']['type'] == "" && 
					$_FILES['imagefile3']['type'] == "" && $_FILES['imagefile4']['type'] == "" && 
					$_FILES['imagefile5']['type'] == ""){	
				$error = true;
				$errormsg .= "You must browse for at least one photo.<br>";
			}
			
			// make sure weights are numeric
			if (!is_numeric($_POST['weight1']) || !is_numeric($_POST['weight2']) || 
				!is_numeric($_POST['weight3']) || !is_numeric($_POST['weight4']) || 
				!is_numeric($_POST['weight5'])){
				$error = true;
				$errormsg .= "\"Order\" must me a number between 0 and 999.<br>";
			}
			
			// verify images are jpg or gif
			if ($_FILES['imagefile1']['type'] != ""){		
				if ($_FILES['imagefile1']['type'] != "image/gif" && $_FILES['imagefile1']['type'] != "image/jpg" && 
						$_FILES['imagefile1']['type'] != "image/jpeg" && $_FILES['imagefile1']['type'] != "image/pjpeg"){
					$error = true;
					$errormsg .= "Photo 1 must be a jpg or gif.<br>";
				}// end if file type
			}
			if ($_FILES['imagefile2']['type'] != ""){		
				if ($_FILES['imagefile2']['type'] != "image/gif" && $_FILES['imagefile2']['type'] != "image/jpg" && 
						$_FILES['imagefile2']['type'] != "image/jpeg" && $_FILES['imagefile2']['type'] != "image/pjpeg"){
					$error = true;
					$errormsg .= "Photo 2 must be a jpg or gif.<br>";
				}// end if file type
			}
			if ($_FILES['imagefile3']['type'] != ""){		
				if ($_FILES['imagefile3']['type'] != "image/gif" && $_FILES['imagefile3']['type'] != "image/jpg" && 
						$_FILES['imagefile3']['type'] != "image/jpeg" && $_FILES['imagefile3']['type'] != "image/pjpeg"){
					$error = true;
					$errormsg .= "Photo 3 must be a jpg or gif.<br>";
				}// end if file type
			}
			if ($_FILES['imagefile4']['type'] != ""){		
				if ($_FILES['imagefile4']['type'] != "image/gif" && $_FILES['imagefile4']['type'] != "image/jpg" && 
						$_FILES['imagefile4']['type'] != "image/jpeg" && $_FILES['imagefile4']['type'] != "image/pjpeg"){
					$error = true;
					$errormsg .= "Photo 4 must be a jpg or gif.<br>";
				}// end if file type
			}
			if ($_FILES['imagefile5']['type'] != ""){		
				if ($_FILES['imagefile5']['type'] != "image/gif" && $_FILES['imagefile5']['type'] != "image/jpg" && 
						$_FILES['imagefile5']['type'] != "image/jpeg" && $_FILES['imagefile5']['type'] != "image/pjpeg"){
					$error = true;
					$errormsg .= "Photo 5 must be a jpg or gif.<br>";
				}// end if file type
			}
			
			if (!$error) 
			{
				$pwidth  = '500';
				$pheight = '350';
				$prefix  = $vrulid;
				
				if ( !empty ( $_FILES['imagefile1']['name'] ) )
				{
					$_FILES['imagefile'] = $_FILES['imagefile1'];
					$photo1 = image_resize($prop_upl_dir, $pwidth, $pheight, $prefix);
					//echo 'photo1: '.$photo1.'<br>';// for testing
					// insert image to db
					$insertSQL = "INSERT INTO cms_rotating_comm (image, label, weight, date, active) VALUES 
												('".$photo1."', '".stripslashes($_POST['photo1label'])."', 
												'".$_POST['weight1']."', '".date('Y-m-d')."', '1')";
					$Result1 = mysql_query($insertSQL) or die(mysql_error());
				}
				if ( !empty ( $_FILES['imagefile2']['name'] ) )
				{
					$_FILES['imagefile'] = $_FILES['imagefile2'];
					$photo2 = image_resize($prop_upl_dir, $pwidth, $pheight, $prefix);
					//echo 'photo2: '.$photo2.'<br>';// for testing
					// insert image to db
					$insertSQL = "INSERT INTO cms_rotating_comm (image, label, weight, date, active) VALUES 
												('".$photo2."', '".stripslashes($_POST['photo2label'])."', 
												'".$_POST['weight2']."', '".date('Y-m-d')."', '1')";
				$Result1 = mysql_query($insertSQL) or die(mysql_error());
				}
				if ( !empty ( $_FILES['imagefile3']['name'] ) )
				{
					$_FILES['imagefile'] = $_FILES['imagefile3'];
					$photo3 = image_resize($prop_upl_dir, $pwidth, $pheight, $prefix);
					//echo 'photo3: '.$photo3.'<br>';// for testing
					// insert image to db
					$insertSQL = "INSERT INTO cms_rotating_comm (image, label, weight, date, active) VALUES 
												('".$photo3."', '".stripslashes($_POST['photo3label'])."', 
												'".$_POST['weight3']."', '".date('Y-m-d')."', '1')";
				$Result1 = mysql_query($insertSQL) or die(mysql_error());
				}
				if ( !empty ( $_FILES['imagefile4']['name'] ) )
				{
					$_FILES['imagefile'] = $_FILES['imagefile4'];
					$photo4 = image_resize($prop_upl_dir, $pwidth, $pheight, $prefix);
					//echo 'photo4: '.$photo4.'<br>';// for testing
					// insert image to db
					$insertSQL = "INSERT INTO cms_rotating_comm (image, label, weight, date, active) VALUES 
												('".$photo4."', '".stripslashes($_POST['photo4label'])."', 
												'".$_POST['weight4']."', '".date('Y-m-d')."', '1')";
				$Result1 = mysql_query($insertSQL) or die(mysql_error());
				}
				if ( !empty ( $_FILES['imagefile5']['name'] ) )
				{
					$_FILES['imagefile'] = $_FILES['imagefile5'];
					$photo5 = image_resize($prop_upl_dir, $pwidth, $pheight, $prefix);
					//echo 'photo5: '.$photo5.'<br>';// for testing
					// insert image to db
					$insertSQL = "INSERT INTO cms_rotating_comm (image, label, weight, date, active) VALUES 
												('".$photo5."', '".stripslashes($_POST['photo5label'])."', 
												'".$_POST['weight5']."', '".date('Y-m-d')."', '1')";
				$Result1 = mysql_query($insertSQL) or die(mysql_error());
				}

			
				echo "<p align='center'><span class='red'><b>Your photo(s) have been uploaded for ".$idname." # 
					".$vrulid.".</b></span><br><br>
					<a href='loggedin.php?page=images&propid=".$vrulid."'>Click here</a> to view photos for ".$idname." 
					# ".$vrulid."<br>
					OR<br>
					Choose from the navigation area.</p>";
			} // end if !error
		} // end if !empty post 
	
	//	if( (!empty($_POST) && $_POST['dowhat'] != "upload") || $error) 
	//	{ 
	?>
			<p align="center">Browse for the photo(s) and click submit.</p>
			<p class="notes"><b>NOTES:</b><br>
				* You can upload up to 5 photos at a time.<br>
				* Photo type must be a jpg or gif.<br>
				* For best results, photos should be at least 800px wide and landscape (wider than height).<br>
        * "Order" is the order in which you want your photos to display (0 - 999).<br />
				* The larger the photo, the longer the upload will take. Be patient.<br>
        * Do NOT click the Submit button more than once.</p>
				
        <p align="center"><span class="red"><b>You are about to upload photos for for the Communities page.</b></span><br><br>
					If you <u>don't</u> want to upload a photo now,<br />
          <a href="#">click here</a> to return to admin home.</p> 
				
				<?php if($error) echo "<p class='errmsg'><b>ERROR:</b><br>".$errormsg."</p>"; ?>
				
        <form method="POST" enctype="multipart/form-data" name="form1">
          <table width="580" border="0" align="center" cellpadding="3" cellspacing="0" class="formbrdr">
            <tr>
              <td><div align="right" class="formcontent">Photo 1: </div></td>
              <td><div align="left" class="formcontent">
                <input name="imagefile1" type="file" class="formcontent" id="imagefile1" size="40" /></div></td>
            </tr>
            <tr>
              <td><div align="right" class="formcontent">Photo 1 Label: </div></td>
              <td><div align="left">
                <input name="photo1label" type="text" class="formcontent" id="photo1label" 
                  value="<?=stripslashes($_POST['photo1label'])?>" size="40" maxlength="100"></div></td>
            </tr>
            <tr>
              <td><div align="right" class="formcontent">Photo 1 Order: </div></td>
              <td><div align="left">
                <input name="weight1" type="text" class="formcontent" id="weight1" 
                  value="<?php if($_POST['weight1']=='') echo '0'; else echo $_POST['weight1']; ?>" size="5" 
                  maxlength="3"></div></td>
            </tr>
            <tr valign="top">
              <td colspan="2" class="space_bot"><div align="center" class="req"></div></td>
            </tr>
            
            <tr>
              <td><div align="right" class="formcontent">Photo 2: </div></td>
              <td><div align="left" class="formcontent">
                <input name="imagefile2" type="file" class="formcontent" id="imagefile2" size="40" /></div></td>
            </tr>
            <tr>
              <td><div align="right" class="formcontent">Photo 2 Label: </div></td>
              <td><div align="left">
                <input name="photo2label" type="text" class="formcontent" id="photo2label" 
                  value="<?=stripslashes($_POST['photo2label'])?>" size="40" maxlength="100">
              </div></td>
            </tr>
            <tr>
              <td><div align="right" class="formcontent">Photo 2 Order: </div></td>
              <td><div align="left">
                <input name="weight2" type="text" class="formcontent" id="weight2" 
                  value="<?php if($_POST['weight2']=='') echo '0'; else echo $_POST['weight2']; ?>" size="5" 
                  maxlength="3"></div></td>
            </tr>
            <tr valign="top">
            <td colspan="2" class="space_bot"><div align="center" class="req"></div></td>
            </tr>
            
            <tr>
            <td><div align="right" class="formcontent">Photo 3: </div></td>
            <td><div align="left" class="formcontent">
              <input name="imagefile3" type="file" class="formcontent" id="imagefile3" size="40" /></div></td>
            </tr>
            <tr>
              <td><div align="right" class="formcontent">Photo 3 Label: </div></td>
              <td><div align="left">
                <input name="photo3label" type="text" class="formcontent" id="photo3label" 
                  value="<?=stripslashes($_POST['photo3label'])?>" size="40" maxlength="100"></div></td>
            </tr>
            <tr>
              <td><div align="right" class="formcontent">Photo 3 Order: </div></td>
              <td><div align="left">
                <input name="weight3" type="text" class="formcontent" id="weight3" 
                  value="<?php if($_POST['weight3']=='') echo '0'; else echo $_POST['weight3']; ?>" size="5" 
                  maxlength="3"></div></td>
            </tr>
            <tr valign="top">
              <td colspan="2" class="space_bot"><div align="center" class="req"></div></td>
            </tr>
            
            <tr>
              <td><div align="right" class="formcontent">Photo 4: </div></td>
              <td><div align="left" class="formcontent">
                <input name="imagefile4" type="file" class="formcontent" id="imagefile4" size="40" /></div></td>
            </tr>
            <tr>
              <td><div align="right" class="formcontent">Photo 4 Label: </div></td>
              <td><div align="left">
                <input name="photo4label" type="text" class="formcontent" id="photo4label" 
                  value="<?=stripslashes($_POST['photo4label'])?>" size="40" maxlength="100">
              </div></td>
            </tr>
            <tr>
              <td><div align="right" class="formcontent">Photo 4 Order: </div></td>
              <td><div align="left">
                <input name="weight4" type="text" class="formcontent" id="weight4" 
                  value="<?php if($_POST['weight4']=='') echo '0'; else echo $_POST['weight4']; ?>" size="5" 
                  maxlength="3"></div></td>
            </tr>
            <tr valign="top">
              <td colspan="2" class="space_bot"><div align="center" class="req"></div></td>
            </tr>
            
            <tr>
              <td><div align="right" class="formcontent">Photo 5: </div></td>
              <td><div align="left" class="formcontent">
                <input name="imagefile5" type="file" class="formcontent" id="imagefile5" size="40" /></div></td>
            </tr>
            <tr>
              <td><div align="right" class="formcontent">Photo 5 Label: </div></td>
              <td><div align="left">
                <input name="photo5label" type="text" class="formcontent" id="photo5label" 
                  value="<?=stripslashes($_POST['photo5label'])?>" size="40" maxlength="100"></div></td>
            </tr>
            <tr>
              <td><div align="right" class="formcontent">Photo 5 Order: </div></td>
              <td><div align="left">
                <input name="weight5" type="text" class="formcontent" id="weight5" 
                  value="<?php if($_POST['weight5']=='') echo '0'; else echo $_POST['weight5']; ?>" size="5" 
                  maxlength="3"></div></td>
            </tr>
            <tr valign="top">
              <td colspan="2" class="space_bot"><div align="center" class="req"></div></td>
            </tr>
						  
            <tr>
            	<td colspan="2" align="center">
                <input name="dowhat" type="hidden" value="upload">
               <!-- <input name="vrulid" type="hidden" value="< ?=$vrulid?>"> -->
                <input name="Submit" type="submit" value="Submit"><br>
                <span class="red">(do not click more than once)</span>
              </td>
            </tr>
					</table>
        </form>
         
			<?php
   // }
	?>