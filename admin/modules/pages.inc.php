<?php	
if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : Content Pages
// Written By : Shawn Crigger of EWD
// Written On : May 24, 2010
// Updated By : Chuck Bunnell of EWD
// Updated On : June 12, 2014
// Copyright Zeal Technologies
//***************************************************/

// if update make sure this id exists
update_verify();

/**
 *
 *  Setup Arrays for Pages That Cannot Be Deleted, 
 *  Cannot Have Subpages Added,
 *  Cannot have the link name updated
 *  Cannot be turned off
 *
 */

$str_nodels = array('1','2','3','4','5','6','7','8','9','10');
$str_nosubs = array('1','2','3','4','5','6','7','8','9','10');
$str_nolink = array('1','2','3','4','5','6','7','8','9','10');
$str_notoff = array('1','2','3');

$bad_del  = array_merge ( $str_nodels, explode (',', $mod_config['no_delete'] ) );
$bad_subs = array_merge ( $str_nosubs, explode (',', $mod_config['no_subpages'] ) );
$bad_link = array_merge ( $str_nolink, explode (',', $mod_config['no_link'] ) );
$bad_toff = array_merge ( $str_notoff, explode (',', $mod_config['no_deactivate'] ) );

function execute()
{
	switch($_GET['action'])
	{
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}
}


/***************************************************************
 *
 * function manage
 *
 **************************************************************/

function manage()
{   
	global $db, $identifier, $module_name, $dbtbl, $action, $id, $bad_subs, $bad_del, $config, $bad_update;

	// $config to get $use_seo
	$row = fetch_config_data();
	$use_seo = intval($row['use_seo']);
	
	$link = '<a href="./?tool='.$identifier.'&action=add">Add a Website Page</a>';
	//$link = '';
	print_header('Main Website Pages', $link );
	
	$th  = "\n";
	$s   = 0;
	
	if ( is_saint() )
	{
		$th = '<th style="width:60px;"><h3>ID</h3></th>';
		$s  = 3;
	}
	
	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sortable" id="table">
		<thead>
			<tr>
				'.$th.' 
				<th><h3>Navigation Name</h3></th>
				<th><h3>Link to Page</h3></th>
				<th style="width:70px"><h3>Weight</h3></th>
				<th style="width:70px"><h3>Active</h3></th>
				<th class="nosort"><h3>Tools</h3></th>
			</tr>
		</thead>
		<tbody>';
											
		$stmt = $db->prepare("SELECT * FROM ".$dbtbl." WHERE onpage_id = '0' AND type = 'main page' ORDER BY weight");
		$stmt->execute();
		$pages = $stmt->rowCount();
		while ( $row = $stmt->fetch(PDO::FETCH_ASSOC))
		{
			$active = yes_no ( $row['active'] );
			
			$td = '';
			if ( is_saint() )
				$td = '	<td style="font-weight:bold;">'.$row['id'].'</td>';
			
			$link = ( $use_seo == 0 ) ? 'index.php&amp;page=' . $row['link'] : '/' . $row['link'];
			
			echo '<tr align="left" valign="middle">
				'.$td.' 
				<td>'.$row['nav_name'].'</td>
				<td>'.$link.'.html</td>
				<td>'.$row['weight'].'</td>
				<td>'.$active.'</td>
				<td style="padding:0;text-align:center;"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row['id'].'">update</a>&nbsp;|&nbsp;';
					echo ( in_array($row['id'], $bad_del ) ) ? '<span class="notool">remove</span>' : '<a href="./?tool='.$identifier.'&action=remove&id='.$row['id'].'">remove</a>'; 																			
				echo '</td>
			</tr>';
		}
	
		echo '</tbody>
	</table>';
	
	$pages = ( $pages > 20 ) ? true : false;

	echo_js_sorter ( $pages, $s );
		
	echo '<div class="spacer">&nbsp;</div>';
}


/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 *
 **************************************************************/

function add( $errors = '' )
{
	global $db, $identifier, $module_name, $id, $action, $dbtbl, $bad_del, $bad_subs, $bad_link, $bad_toff;
	$t_index = $val_weight = $val_label = null;
	$base_path = dirname(__FILE__);
	
	include ($base_path . '/ckeditor/ckeditor.php');
	$option = '';
	
	if ( $errors )
	{
		echo '<ul class="error_message">
			<strong>Please fill in the required fields.</strong>';
			// set error messages for required fields
			if ( in_array('label', $errors ) )
			{
				echo '<li>A Page Label is required.</li>';
				$val_label = ' class="form_field_error" ';
			}
			
			if ( in_array('used_label', $errors ) )
			{
				echo '<li>This Page Label is already in use.</li>';
				$val_label = ' class="form_field_error" ';
			}
			
			if ( in_array('nav_name', $errors ) )
			{
				echo '<li>A Navigation Name is required, even if you are not going to place it in the navigation areas.</li>';
				$val_nav_name = ' class="form_field_error" ';
			}
			
			if ( in_array('used_nav_name', $errors ) )
			{
				echo '<li>This Navigation Name is already in use.</li>';
				$val_nav_name = ' class="form_field_error" ';
			}
			
			if ( in_array('link', $errors ) )
			{
				echo '<li>A Link Name is required.</li>';
				$val_link = ' class="form_field_error" ';
			}
		
			if ( in_array('used_link', $errors ) )
			{
				echo '<li>This Link Name is already in use.</li>';
				$val_link = ' class="form_field_error" ';
			}
		
		echo '</ul>';
	
	} else {
		
		$c = ( $action == 'update' ) ? 'update this' : 'add a new';
	
		echo  '<ul class="notice_message">';
			// set instructions here
			echo '<strong>To ' . $c . ' page, fill out the form and click submit.</strong>
			<li>Fields marked with a red * are required.</li>
			<li>The `Page Label` is what will appear at the top of the content area for that page. It is an important SEO tool so try to include your main keyword or phrase for this page.</li>
			<li>The `Navigation Name` is the text that will appear in the navigatin menu. You must fill it out even if you are not going to display it in the navigation.</li>
			<li>The `Link Name` is what will appear in the address bar. ONLY use letters and dashes for best results; NO Spaces!</li>';
			if ($action == 'update' )
				echo '<li>We do NOT recommend updating your Link Name. It can cause issues with your SEO.</li>';
			echo '<li>The `Weight` is the order weight in which this page will be displayed in the navigation.</li>
			<li>Set `Active` to "Yes" for the page to be viewable on website.</li>
			<li>In the editor, hold <strong>Shift</strong> and press <strong>Enter</strong> at same time to Start a new paragraph.</li>';
		echo '</ul>';
	}
	
	if ($action == "update")
	{
		// Run the query for the existing data to be displayed.
		$stmt = $db->prepare('SELECT * FROM '.$dbtbl.' WHERE id = ?');
    $stmt->execute(array($id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
	} 
	
	if (!empty ($_POST))
	{
		$row = sanitize_vars ($_POST);
	}


/********************************************************
 * Start Building Form
 *******************************************************/

	$r = required();
	// set the tooltip for link name
	$linktip = ($action == "update") ? tooltip('We do NOT recommend you change your link name. If you do, recommended length is 23 characters or less') : tooltip('Recommend 23 characters or less');
	// if link name is not allowed to be updated, use different tool tip
	$linktip = ($action == "update" && in_array($row['id'], $bad_link)) ? tooltip('This link name cannot be updated') : $linktip;
	// link names that are not allowed to be updated, set to readonly
	$thislink = ($action == "update" && in_array($row['id'], $bad_link)) ? ' readonly' : '';
	
	echo '<form name="form" id="form" method="post" action="#">
		<table>
    	<input type="hidden" name="id" value="' . $id . '" />';
		
    	echo '<tr>
      	<td><label for="label">'.$r.'Page Label</label></td>
        <td><input ' . $val_label . ' type="text" name="label" id="label" value="'. htmlspecialchars($row['label']) .'" size="45" maxlength="150" />
					'.tooltip('Up to 150 characters').'</td>
      </tr>'; // class="labelcell"
	
    	echo '<tr>
      	<td><label for="nav_name">'.$r.'Navigation Name</label></td>
        <td><input ' . $val_nav_name . ' type="text" name="nav_name" id="nav_name" value="'. htmlspecialchars($row['nav_name']) .'" size="45" maxlength="50" />
					'.tooltip('Recommend 23 characters or less').'</td>
      </tr>';
	
		
    	echo '<tr>
      	<td><label for="link">'.$r.'Link Name</label></td>
        <td><input ' . $val_link . ' type="text" name="link" id="link" value="'. htmlspecialchars($row['link']) .'" size="45" maxlength="50" '.$thislink.' />
					'.$linktip.'</td>
      </tr>';
							
			// if POST row['content'] == $row['pageconent'];
			$content = ($_POST) ? $_POST['pagecontent'] : $row['content']; 
			echo '<tr>
				<td colspan="2" style="text-align:center; width:100%">
					<textarea style="min-height:270px; height:270px; width:100%;" name="pagecontent" id="pagecontent">' . $content . '</textarea>
				</td>
			</tr>';

			$ckeditor  = new CKEditor( ) ;
			$ck_config = set_fck_config();
			$ck_events = set_fck_events();
			
			$ck_config['toolbar'] = set_fck_toolbar();
			$ck_config['height']  = 200;
			$ck_config['width']   = '102%';

			// EDIT BELOW LINE TO ADD YOUR WEBSITE STYLESHEET
			// $ck_config['contentsCss'] = $base_site_url . '/styles/main.css';
			$ckeditor->replace("pagecontent", $ck_config, $ck_events);
						
			echo '<tr>
				<td colspan="2"></td>
			</tr>';
	
			echo '<tr>
				<td colspan="2" style="width:100%">
					<p class="center"><strong>SEARCH ENGINE OPTIMIZATION</strong></p>
					<p style="text-align:left;"><strong>Meta Tags:</strong> The keywords and keyphrases need to be in the content of the page and in all three meta tags below. Do NOT use special characters in the text.<br />
						<strong>Title Example:</strong> Cougars in the wild, Cougars in America<br />
						<strong>Keyword Example:</strong> cougars in the wild,cougars in america,cougars,wild,america<br />
						<strong>Description Example:</strong> There are very few cougars in the wild with only a small amount of cougars in America.</p>
					<p><span class="note">NOTE: keywords and phrases are seperated only with a comma. Description should be under 150 characters long.</span></p>
				</td>
			</tr>';

			echo '<tr>
				<td><label for="meta_title">Meta Title: </label></td>
				<td><textarea style="width:80%" name="meta_title" id="meta_title" cols="70" rows="2">' . stripslashes (htmlspecialchars_decode ($row['meta_title'])) . '</textarea></td>
			</tr>';						
	
			echo '<tr>
				<td><label for="meta_kw">Meta Keywords: </label></td>
				<td><textarea style="width:80%" name="meta_kw" id="meta_kw" cols="70" rows="2">' . stripslashes (htmlspecialchars_decode ($row['meta_kw'])) . '</textarea></td>
			</tr>';						
	
			echo '<tr>
				<td><label for="meta_descr">Meta Description: </label></td>
				<td><textarea style="width:80%" name="meta_descr" id="meta_descr" cols="70" rows="2">' . stripslashes (htmlspecialchars_decode ($row['meta_descr'])) . '</textarea></td>
			</tr>';				
				
			echo '<tr>
				<td colspan="2"></td>
			</tr>';
	
			echo '<tr>
				<td><label for="show_in_nav">'.$r.'Show in Main Nav</label></td>
				<td>
				'.create_slist ( $list, 'show_in_nav', $row['show_in_nav'], 1) .
				tooltip('Set to Yes to show this in the Main Navigation').'
				</td>
			</tr>';
			
			echo '<tr>
				<td><label for="show_in_foot_nav">'.$r.'Show in Footer Nav</label></td>
				<td>
				'.create_slist ( $list, 'show_in_foot_nav', $row['show_in_foot_nav'], 1) .
				tooltip('Set to Yes to show this in the Footer Navigation').'
				</td>
			</tr>';
			
			echo '<tr>
				<td><label for="weight">Order: </label></td>
				<td><input ' . $val_weight . ' type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />
					'.tooltip('Recommend increments of 10').'</td>
			</tr>';	
	
			// only display "active" for pages that can be turned off
			// use hidden field for db input for others
			if ($action == 'update' && in_array($row['id'], $bad_toff))
			{
				echo '<input type="hidden" name="active" value="' . $row['active'] . '" />';
			} else {
				echo '<tr>
					<td><label for="active">'.$r.'Active</label></td>
					<td>
					'.create_slist ( $list, 'active', $row['active'], 1) .
					tooltip('Set to Yes to allow Page to be viewable').'
					</td>
				</tr>';
			}
	
			echo '<tr>
				<td colspan="2"></td>
			</tr>';
			
			echo '<tr>
				<td colspan="2" style="padding:3px;"><input type="submit" name="submit" value="Submit" /></td>
			</tr>';

    echo '</table>
	</form>';
	
	echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>';
	
	echo '<div class="spacer">&nbsp;</div>';

}//end function


/***************************************************************
 *
 * function sanitize_vars
 * @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 **************************************************************/


function sanitize_vars( $data )
{
  global $identifier;
    
	$r_data['label']                = $data['label'] ;
	$r_data['nav_name']             = $data['nav_name'] ;
	$r_data['link']                 = rembadchar ( stripslashes ($data['link'] ));
	$r_data['pagecontent']          = stripslashes ( $data['pagecontent'] );
	$r_data['meta_title']           = htmlspecialchars ( $data['meta_title']);
	$r_data['meta_kw']              = htmlspecialchars ( $data['meta_kw']);
	$r_data['meta_descr']           = htmlspecialchars ( $data['meta_descr']);
	$r_data['weight']               = intval ($data['weight']);
	$r_data['show_in_nav']     			= intval ($data['show_in_nav']);
	$r_data['show_in_foot_nav']     = intval ($data['show_in_foot_nav']);
	$r_data['active']               = intval ($data['active']);
	
	return $r_data;
}


/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $db, $identifier, $module_name, $id, $dbtbl, $bad_del;
	
	$stmt = $db->prepare("SELECT id,nav_name FROM ".$dbtbl." WHERE id = ?");
  $stmt->execute(array($id));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$totalRows = $stmt->rowCount();
	
	print_header('Delete a '.$module_name.' - ' . $row['nav_name'] );	
	
	if($totalRows == '0' || empty($id))
		echo '<ul class="error_message"><li>This page does not exist.</li></ul>';
	else if($row['id'] == '1') // home page
		echo '<ul class="error_message"><li>The Home Page cannot be deleted.</li></ul>';
	else if (in_array($row['id'], $bad_del)) // contact page and misc pages
		echo '<ul class="error_message">
			<li>The "'.$row['nav_name'].'" page cannot be deleted.</li>
			<li>To make this page inactive, <a class="tools" href="./?tool='.$identifier.'&action=update&id='.$row['id'].'">click here</a> and set "Active?" to "No."</li>
		</ul>';
	else { 

		if ( !empty($_POST ))
		{ 
			$errno = 0;
			try 
			{
				$stmt = $db->prepare("DELETE FROM ".$dbtbl." WHERE id = ?");
				$stmt->execute(array($id));
			}
			
			catch(PDOException $ex)
			{
				$errno = $ex->getCode();
			}
			
			print_mysql_message ( $errno , $module_name, $id, 2 ) ;
		
		} else {
			
			echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">
				<input type="hidden" name="id" value="' . $id . '">
				<div class="center">Are you sure you want to delete this record?</div>
				<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div>
			</form>';
		}
	}
}	
		

/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of contents
 *
 **************************************************************/

function update()
{
	global $db, $identifier, $module_name, $action, $id, $dbtbl, $bad_del, $bad_subs;
		
	if ( $action == 'update' ) 
	{
		print_header('Update '.$module_name);
		/*
		// make sure this id exists
		$stmt = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE id = ?");
		$stmt->execute(array($id));
			if ( $stmt->rowCount() == 0 || $id == '0' )
			{
				echo '<ul class="error_message"><li>This page does not exist.</li></ul>';
				exit;
			} */
	}
	else
		print_header('Add New '.$module_name);

	if ( array_key_exists ('submit',$_POST))
	{
		require_once("classes/validation.php");

		// set rules for required fields
		$rules = array();
		$rules[] = "required,label,label";
		$rules[] = "required,nav_name,nav_name";
		$rules[] = "required,link,link";
	
		$errors = validateFields($_POST, $rules);
	
		if ( $action != 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE label = ?");
			$stmt->execute(array($data['label']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_label';
		
			$stmt1 = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE nav_name = ?");
			$stmt1->execute(array($data['nav_name']));
			if ( $stmt1->rowCount() != 0 )
				$errors[] = 'used_nav_name';

			$stmt2 = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE link = ?");
			$stmt2->execute(array($data['link']));
			if ( $stmt2->rowCount() != 0 )
				$errors[] = 'used_link';
		}

		if ( $action == 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE label = ? AND id != ".$id);
			$stmt->execute(array($data['label']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_label';

			$stmt1 = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE nav_name = ? AND id != ".$id);
			$stmt1->execute(array($data['nav_name']));
			if ( $stmt1->rowCount() != 0 )
				$errors[] = 'used_nav_name';

			$stmt2 = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE link = ? AND id != ".$id);
			$stmt2->execute(array($data['link']));
			if ( $stmt2->rowCount() != 0 )
				$errors[] = 'used_link';
		}

	}
	
	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$vars = array();
		$data = sanitize_vars( $_POST );
		$type = ( $action == 'update' ) ? 0 : 1;
		$xsql = "";
	
		$data['pagecontent'] = strip_fck_strip_value( $data['pagecontent']) ;
		
		if ( $action == 'update' )
		{
			$sql = 'UPDATE ' . $dbtbl . ' SET label = ?, nav_name = ?, link = ?, 
				content = ?, meta_title = ?, meta_kw = ?, meta_descr = ?, weight = ?, 
				show_in_nav = ?, show_in_foot_nav = ?, active = ? , date = ?';
			array_push($vars, $data['label']);
			array_push($vars, $data['nav_name']);
			array_push($vars, $data['link']);
			array_push($vars, $data['pagecontent']);
			array_push($vars, $data['meta_title']);
			array_push($vars, $data['meta_kw']);
			array_push($vars, $data['meta_descr']);
			array_push($vars, $data['weight']);
			array_push($vars, $data['show_in_nav']);
			array_push($vars, $data['show_in_foot_nav']);
			array_push($vars, $data['active']);
			array_push($vars, date('Y-m-d'));
			
			$sql .= " WHERE id = ?";
			array_push($vars, $id);
			
		} else {

			$sql = 'INSERT INTO ' . $dbtbl . ' (label, nav_name, link, content, 
				meta_title, meta_kw, meta_descr, weight, show_in_nav, show_in_foot_nav,
				active, date)
				VALUES ' . "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			array_push($vars, $data['label']);
			array_push($vars, $data['nav_name']);
			array_push($vars, $data['link']);
			array_push($vars, $data['pagecontent']);
			array_push($vars, $data['meta_title']);
			array_push($vars, $data['meta_kw']);
			array_push($vars, $data['meta_descr']);
			array_push($vars, $data['weight']);
			array_push($vars, $data['show_in_nav']);
			array_push($vars, $data['show_in_foot_nav']);
			array_push($vars, $data['active']);
			array_push($vars, date('Y-m-d'));
		}

		$errno = 0;
		try
		{
			$stmt = $db->prepare($sql);
			$stmt->execute($vars);
		}
			
		catch(PDOException $ex)
		{
			$errno = $ex->getCode();
		}

		if ( ( $errno != 0 ) && ( is_saint() ) )
			print_debug($sql);

		print_mysql_message ( $errno , $module_name, $id, $type ) ;
	
	} else {
		add( $errors );
	}

}//end function
?>