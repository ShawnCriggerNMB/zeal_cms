<?php
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : Brands Module
// Written By : Darrell Bessent of EWD
// Written On : March 27, 2013
// Revision   : 164
// Description: This module is used for viewing
//							tables and fields in the db.
// Copyright Zeal Technologies
//***************************************************/

function execute()
{
	if ( empty ($_POST) )						
		manage();
	else
		switch ( $_POST['full'] )
		{		
			case '1':
				full_list();
				break;
			default:
			case '0':
				list_fields();				
				break;			
		}		
}

function manage()
{
	$tables = get_tables();
	print_header('Select Table to View Fields');
	echo '<form action="#" method="post" id="signup">' . "\n" .
			 '<div class="success_message">' . "\n";
	echo 'Please Select a Table to View the Fields in. <br /> <br />';	
	echo create_slist ( $tables, 'table' );
	echo '<br />' . "\n";
	echo 'Full View or Just Field Names : ' . create_slist ( array(), 'full', '0', 1, 2) . "<br />\n";
	echo '<br /><br /> <input type="submit" value="submit" />';
	echo '</div></form>' . "\n";
	
}

function get_tables()
{
	$tables = mysql_query("show tables") or die(mysql_error());

	if (mysql_num_rows($tables))
	{
		$table_list=Array();

		while ($table_data = mysql_fetch_row($tables))
		{
			$table_list[$table_data[0]] = $table_data[0];
		}
	}
	
	mysql_free_result($tables);

	return $table_list;
}

function list_fields()
{
	$tbl = mysql_real_escape_string ( $_POST['table'] );
	$result = mysql_query("SHOW COLUMNS FROM $tbl");
	
	if (!$result)
	{
    echo 'Could not run query: ' . mysql_error();
    exit;
	}
	
	if (mysql_num_rows($result) > 0)
	{
		echo '<div class="success_message">' . "\n";
    while ($row = mysql_fetch_assoc($result))		
		{
			echo $row['Field'] . "<br />\n";
    }			
		echo '</div>';
	}
	
}

function full_list()
{
	$tbl    = mysql_real_escape_string ( $_POST['table'] );
	$query  = "SELECT * FROM $tbl LIMIT 1";
	$result = mysql_query($query);		
	$fields = mysql_num_fields($result);
	
	for($count=0;$count<$fields;$count++)
	{
		 $field = mysql_fetch_field($result,$count);
		 //print_debug ( $field );
		 echo "<p>$field->name,$field->type</p>";
	}	
}


?>