<?php	
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module: Home Page for Admin Section
// Written By : Darrell Bessent of Zeal Technologies
// Written On : Jun 07, 2013
// Updated On : May 03, 2014
// Updated By : Cory Shaw
// Copyright Zeal Technologies
//***************************************************/


function execute()
{
	echo display_dashboard();
}

function fetch_output()
{
	global $db, $userinfo, $sitename;

	if ($userinfo['seen_msgs'] == 0 )
	{
		$messages = check_fail_attempts().welcome();
		$stmt = $db->prepare("UPDATE " . db_prefix . admin_table." SET `seen_msgs`='1' WHERE `username`=?");
		$stmt->execute(array($userinfo['username']));
	} else {
		$messages = '';
	}
}	
?>