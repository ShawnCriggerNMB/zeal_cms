 <?php	
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : News Module
// Written By : Darrell Bessent of EWD
// Written On : May 04, 2013
// Revision   : 164
// Updated by : Darrell Bessent of EWD
// Updated On : May 04, 2013
// Description: This module is used with websites that 
//							want to post news and events.
// Copyright Zeal Technologies 
//***************************************************/

/***************************************************************
 *
 * Sanitize variables for use in forms and whatnots
 *
 *
 **************************************************************/

$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);
$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);


$mod_config  = get_module_settings ( 'news' );

$upload_file_url  = $site_base_url . '/uploads/news/';
$upload_file_dir  = $site_base_dir . '/uploads/news/';

$upload_image_url  = $site_base_url . '/uploads/news/';
$upload_image_dir  = $site_base_dir . '/uploads/news/';


$width = '198';
$height = '289';

function execute()
{
	switch($_GET['action'])
	{
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}//end switch
}//end function


/***************************************************************
 *
 * function manage
 * Querrys DB and Displays Slider Content
 *
 *
 **************************************************************/

function manage()
{

  global $upload_image_dir,$upload_image_url,$upload_file_dir,$upload_file_url;
  
  $i    = 0;
  $link = '<a href="./?tool=news&action=add">Add A News Post</a>';
  
  print_header('Manage News',$link);
  
  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 
       ' 	  <thead>' . "\n" . 
       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" . 
       ' 			  <th><h3>Title</h3></th>' . "\n" . 	
			 ' 			  <th><h3>News</h3></th>' . "\n" . 
			 ' 			  <th><h3>Date</h3></th>' . "\n" .
			 ' 			  <th><h3>Image</h3></th>' . "\n" . 
       ' 			  <th><h3>File</h3></th>' . "\n" .    
       ' 			  <th style="width:50px; text-align:center;"><h3>Order</h3></th>' . "\n" . 
       ' 			  <th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . 
       ' 			  <th style="width:100px;" class="nosort"><h3>Tools</h3></th>' . "\n" . 
       '		 </tr></thead><tbody>' . "\n";
  
  $results = mysql_query('SELECT * FROM ' . db_prefix . 'news ORDER BY weight,title');
  $pages   = mysql_num_rows($results);
  while ($row = mysql_fetch_array($results))
  {    

    $i++;
    
    $image_link 				 = 'No File Uploaded';
		$file_link 					 = 'No File Uploaded';
    $title							 = $row['title'];
		$news								 = $row['news'];
		$date								 = $row['date'];
		
    $image_filename = $upload_image_dir . $row['image'];
		$file_filename = $upload_file_dir . $row['file'];
    
    $image_link = ( file_exists ( $image_filename ) ) ? '<a href="' . $upload_image_url . $row['image'] . '" target="_blank">' . $row['image'] . '</a>' : $image_link;
    $file_link = ( file_exists ( $file_filename ) ) ? '<a href="' . $upload_file_url . $row['file'] . '" target="_blank">' . $row['file'] . '</a>' : $file_link;
			          
    $act = yes_no ( $row['active'] );
    $news = substr($news, 0, 60) . '...';
		
    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";
    echo '<td style="padding-left:20px;">' . $title . '</td>' . "\n"; 	
		echo '<td style="padding-left:20px;">' . $news . '</td>' . "\n"; 
		echo '<td style="padding-left:20px;">' . $date . '</td>' . "\n";	
		echo '<td style="padding-left:20px;">' . $image_link . '</td>' . "\n"; 	
		echo '<td style="padding-left:20px;">' . $file_link . '</td>' . "\n"; 	
    echo '<td style="width:50px; text-align:center;">' . $row['weight'] . '</td>' . "\n"; 
    echo '<td style="width:50px; text-align:center;">' . $act . '</td>' . "\n"; 
    echo '<td style="width:100px;" valign="middle"><strong><a href="./?tool=news&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=news&action=remove&id='.$row['id'].'">Remove</a></strong></td>';
    echo '</tr>' . "\n";
	  
  }//end while

  echo '</tbody></table>';

  $pages = ( $pages > 20 ) ? true : false;
	
  echo_js_sorter ( $pages );
             
}//end function

/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Banner Form for User Input
 *
 *
 **************************************************************/

function add( $errors = '' )
{
  global $id, $action, $site_base_url, $site_base_dir, $upload_image_url, $upload_image_dir, $upload_file_url, $upload_file_dir, $mod_config, $height, $width;
   
   
  if ( $errors )
  {
   echo '<div class="error_message">';
   if ( in_array('title', $errors ) )
     echo '* You must fill out a title.<br/>';
   echo '</br></div>' . "\n";
  } else {
    $c = ( $action == 'update' ) ? 1 : 0;
    print_notice_message ( $c );
  }
  
  if ($action == "update")
  {
   // Run the query for the existing data to be displayed.
   $results = mysql_query('SELECT * FROM ' . db_prefix . 'news WHERE id = "' . $id . '"');
   $row 				= mysql_fetch_array($results);
  } 
	if (!empty ($_POST))
	{	
   $row     = sanitize_vars ($_POST);
  }//end if
  
	echo '<div class="notice_message">NOTES : <br /> 
					<ul>
						<li>The `TITLE` is the title for the news post.</li>
						<li>The `NEWS` is the actual news post.</li>
						<li>The `IMAGE` is for when you want to display an image with the news.</li>
						<li>The `FILE` may be a *.DOC, *.DOCX or *.PDF.</li>
						<li>The `ORDER` is the order weight in which you wish for the news to be listed.</li>
					<ul>	
				</div>';
	
  echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";			
  echo '<input type="hidden" name="id" value="' . $id . '" />';
  
  $t_index = 1;
  echo '   <tr>' . "\n".
       '      <td > <label for="title">Title</label></td>  ' . "\n" . 
       '      <td > <input type="text" name="title" id="title" value="'. $row['title'] .'" size="45" /></td>  ' . "\n" . 
       '   </tr>' . "\n";

	
	  $t_index++;
		$r_news = str_replace('<br />', '\n', $row['news']);
	echo '   <tr>' . "\n".
       '      <td > <label for="news">News</label></td>  ' . "\n" . 
       '      <td > <textarea name="news" id="news" rows="5" cols="40" tabindex="' . $t_index . '">'.$r_news.'</textarea></td>  ' . "\n" . 
       '   </tr>' . "\n";		 
			 	  
			if ($row['date'] == '') 
			 { 
			 $row['date'] = date("Y-m-d").' '.date("h:i a"); 
			 } 
  echo '   <tr>' . "\n".
       '      <td > <label for="date">Date</label></td>  ' . "\n" . 
       '      <td > <input type="text" name="date" id="ate" value="'. $row['date'] .'" size="45" /></td>  ' . "\n" . 
       '   </tr>' . "\n";		  	 


  $t_index++;
  echo '   <tr>' . "\n".
       '      <td > <label for="weight">Order</label></td>  ' . "\n" . 
       '      <td > <input type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />' . "\n" .
       ' 						<span class="note" style="padding-left:5px;"> ( Recommend increments of 10 ) </span>' . "\n" .
       '      </td>' . "\n" .
       '   </tr>' . "\n";
   
  $t_index++;
		echo '   <tr>' . "\n" .
							' 						<td ><label for="active">Active</label></td>  ' . "\n" . 
							' 						<td >' . "\n" . 
							' 						<select id="active" name="active">' . "\n" . 
							' 									<option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 
							' 									<option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 
							' 						</select>' . "\n" . 
						 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to Display on Website ) </span>' . "\n" . 
       ' 						</td>  ' . "\n" . 
							'				</tr>  '. "\n";
  
  /****************************
  * File Uploading Section
  ***************************/
  
	// image upload handler
  if ( $row['image'] != '' )
  {
    
   $image_filename      = $upload_image_dir . $row['image'];
  
   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		
   echo '<tr>' . "\n";
   echo '<td ><label for="current_image">Current Image</label></td>' . "\n";
   echo '<td ><a href="' . $upload_image_url . $row['image'] . '">' . $row['image'] . '</a></td>' . "\n";
   echo '<input type="hidden" name="current_image" value="' . $row['image'] . '" />' . "\n";
   echo '</tr>' . "\n";
  }
  
  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		
  echo '<tr>' . "\n";
  echo '<td ><label for="image">Upload Image</label></td>' ."\n";
  echo '<td ><input name="image" type="file" id="image" size="15">' . "\n";
  echo '<span class="note">( File must be a *.JPG, *.PNG, or *.GIF )</span></td>' . "\n";
  echo '</tr>' . "\n";
 
 	// file upload handler
   if ( $row['file'] != '' )
  {
    
   $file_filename      = $upload_file_dir . $row['file'];
  
   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		
   echo '<tr>' . "\n";
   echo '<td ><label for="current_file">Current File</label></td>' . "\n";
   echo '<td ><a href="' . $upload_file_url . $row['file'] . '">' . $row['file'] . '</a></td>' . "\n";
   echo '<input type="hidden" name="current_file" value="' . $row['file'] . '" />' . "\n";
   echo '</tr>' . "\n";
  }
  
  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		
  echo '<tr>' . "\n";
  echo '<td ><label for="file">Upload File</label></td>' ."\n";
  echo '<td ><input name="file" type="file" id="file" size="15">' . "\n";
  echo '<span class="note">( File must be a *.DOC, *DOCX, *.PDF or *.RTF )</span></td>' . "\n";
  echo '</tr>' . "\n";
	
 // This is our submit
  $t_index++;
  echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";  
  echo '</table></form>' . "\n";
  echo '<script type="text/javascript">document.getElementById(\'title\').focus();</script>' . "\n";

}


/***************************************************************
 *
 * function sanitize_vars
	* @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 *
 **************************************************************/


function sanitize_vars( $data )
{
  global $upload_file_dir, $upload_image_dir, $mod_config, $known_photo_types, $known_file_types;
  
  
  $r_data['title'] 	 = mysql_real_escape_string ( $data['title'] );
	$r_data['news']  	 = mysql_real_escape_string ( $data['news'] );
	$r_data['date']  	 = mysql_real_escape_string ( $data['date'] );
  $r_data['image']   = $data['current_image'];
	$r_data['file']  	 = $data['current_file'];
	$r_data['weight']  = mysql_real_escape_string ( $data['weight'] );
	$r_data['active']  = mysql_real_escape_string ( $data['active'] );
			
  $tid = ( intval ( $data['id'] ) == 0 ) ? fetch_lastid ('news') : $data['id'];
      		 
				 
  if ( !empty ( $_FILES['file']['name'] ) )
  {
            
    $old_file        = $r_data['file'];
    $file            = $_FILES['file'];
		
		$r_data['file'] = process_upload_file ( $tid , $file, $known_file_types, $upload_file_dir);
						
    if ( ( $old_file != $r_data['file'] ) && ( file_exists ( $upload_file_dir . $old_file ) ) && ( $old_file != '' ))
		{
			unlink ( $upload_file_dir . $old_file );
		}
  
	}
	
  if ( !empty ( $_FILES['image']['name'] ) )
  {
            
    $old_image        = $r_data['image'];
    $image            = $_FILES['image'];
    
		$r_data['image'] = process_upload ( $tid , '', $image, $known_photo_types, $upload_image_dir, 1, $width, $height );
			
    if ( ( $old_image != $r_data['image'] ) && ( file_exists ( $upload_image_dir . $old_image ) ) && ( $old_image != '' ))
		{
			unlink ( $upload_image_dir . $old_image );
		}
  
	}
			 
  if ( $data['active'] == 0 && empty($_POST['active']) )
  {
    $r_data['active'] = 1;
    $r_data['weight']  = fetch_weight('news', 'weight' );
  }
  
  return $r_data;

}

/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 * Unlinks Existing Photo
 *
 **************************************************************/

function remove()
{

	global $id, $upload_file_dir, $upload_image_dir, $module_name;
	
	$result = mysql_query("SELECT * FROM " . db_prefix . "news WHERE id = '$id'");
	$row    = mysql_fetch_array($result);
	
	print_header('Delete News Post - ' . $row['title']);
	
	if ( !empty($_POST ))
	{		
					
		if ( file_exists ( $upload_file_dir . $row['file'] ))
		{
				@unlink( $upload_file_dir . $row['file'] );
				
				//$sufx = substr($row['file'], -4);
				
		}
		
		if ( file_exists ( $upload_image_dir . $row['image'] ))
		{
				@unlink( $upload_image_dir . $row['image'] );
				
				//$sufx = substr($row['file'], -4);
				
		}
		
		
		$result = mysql_query('DELETE FROM ' . db_prefix . "news WHERE id = '$id'");
		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;
			
	} else {
	
		echo '<form action="./?tool=news&action=remove" method="post" name="form">' . "\n" .
							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .
							'<tr> <td><div align="center">Are you sure you want to delete this news post?</div></td></tr>' . "\n" .
							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .
							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=news\'"></div></td></tr>' . "\n" .
							'</table></form>';
	}//end if
}//end function


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of page contents
 *
 **************************************************************/

function update()
{
		global $action, $id, $module_name;
		
		if ( $action == 'update' )
				print_header('Update News Post');
		else
				print_header('Add News Post');
 
		if ( array_key_exists ('submit',$_POST))
		{				
    require_once("classes/validation.php");
  
    $rules = array();    
    
    $rules[] = "required,title,title";
		$rules[] = "required,news,news";
    
    $errors = validateFields($_POST, $rules);
    
		}
		
		if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	 {
		
				$data  = sanitize_vars( $_POST );			
		 	  $type  = ( $action == 'update' ) ? 0 : 1;
				$news = str_replace('\n', '<br />', $data['news']);
				
				if ( $action == 'update' )
						$sql = "UPDATE " . db_prefix . "news SET title = '" . $data['title'] . "', news = '" . $news . "', weight = '" . $data['weight'] . "', active = '" . $data['active'] . "', file = '" . $data['file'] . "', image = '" . $data['image'] . "', date = '" . $data['date'] . "'	WHERE id = '$id'";
 		 else
					 $sql = 'INSERT INTO ' . db_prefix . 'news (title, news, date, image, file, weight, active) VALUES ' .
												 "('" . $data['title'] ."', '" . $news . "', '" . $data['date'] . "', '" . $data['image'] . "', '" . $data['file'] ."', '" . $data['weight'] . "', '" . $data['active'] . "')";
											
//    echo $sql;
    
				mysql_query ( $sql );
    
    if ( is_saint() && mysql_errno != 0 )
      echo '<div class="error_message"><pre>' . $sql . '</pre></div>';
   			
				print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;   
		
  } else {
		add( $errors );
	}//end if	
}//end function



?>