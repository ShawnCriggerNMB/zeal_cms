<?php
if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : States Settings / Programmer ++ Level
// Written By : Cory Shaw of EWD
// Written On : May 9, 2014
// Updated By : Chuck Bunnell of EWD
// Updated On : May 27, 2014
// Copyright Zeal Technologies
//***************************************************/

// if update make sure this id exists
update_verify();

function execute()
{
	switch($_GET['action'])
	{
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}
}


/***************************************************************
 *
 * function manage
 * Querrs DB and Displays Content
 *
 **************************************************************/

function manage()
{
	global $db, $identifier, $module_name, $dbtbl;
	$i    = 0;

	$link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';

	$extra_header = null;

	if (!empty($_GET['state_id'])) 
	{
		// get state info
		$stmt = $db->prepare("SELECT name FROM ".db_prefix."states WHERE id = ?");
		$stmt->execute(array($_GET['state_id']));
		$state = $stmt->fetchColumn();
		$extra_header = " - ".$state;
	}

	print_header('Manage '.$module_name.$extra_header,$link);

	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sortable" id="table">
		<thead>
			<tr align="left" valign="top">
				<th><h3>City Name</h3></th>
				<th style="width:140px"><h3>State</h3></th>
				<th style="width:70px"><h3>Active</h3></th>
				<th class="nosort"><h3>Tools</h3></th>
			</tr>
		</thead>
		<tbody>';

			if(!isset($_GET['state_id']))
			{
				$stmt = $db->prepare('SELECT '.db_prefix.'cities.id, '.db_prefix.'cities.name, '.db_prefix.'cities.active, '.db_prefix.'states.name AS state FROM ' . db_prefix . $identifier . '
					JOIN '.db_prefix.'states
					ON '.db_prefix.'states.id = '.db_prefix.'cities.state_id
					ORDER BY '.db_prefix.'cities.name ASC');
				$stmt->execute();
			
			} else {
				
				$stmt = $db->prepare('SELECT '.db_prefix.'cities.id, '.db_prefix.'cities.name, '.db_prefix.'cities.active, '.db_prefix.'states.name AS state FROM ' . db_prefix . $identifier . '
				JOIN '.db_prefix.'states
				ON '.db_prefix.'states.id = '.db_prefix.'cities.state_id
				WHERE state_id = ?
				ORDER BY '.db_prefix.'cities.name ASC');
				$stmt->execute(array($_GET['state_id']));
			}
	
			$pages = $stmt->rowCount();
	
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$act  = yes_no ( $row['active'] );
	
				echo '<tr align="left" valign="middle">
					<td>' . $row['name'] . '</td>
					<td>' . $row['state'] . '</td>
					<td>' . $act . '</td>
					<td style="padding:0;text-align:center;"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&action=remove&id='.$row['id'].'">Remove</a></strong></td>
				</tr>';
			}
    echo '</tbody>
	</table>';

	$pages = ( $pages > 20 ) ? true : false;

	echo_js_sorter ( $pages );

	echo '<div class="spacer">&nbsp;</div>';
}//end function


/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 * 
 **************************************************************/

function add( $errors = '' )
{
	global $db, $identifier, $module_name, $id, $action, $dbtbl;
	
	if ( $errors )
	{
		echo '<ul class="error_message">
			<strong>Please fill in the required fields.</strong>';
			// set error messages for required fields
			if ( in_array('name', $errors ) )
			{
				echo '<li>You must fill out a name.</li>';
				$val_name = ' class="form_field_error" ';
			}

			if ( in_array('used_name', $errors ) )
			{
				echo '<li> This City name is already being used with this state.</li>';
				$val_name = ' class="form_field_error" ';      
			}
	
			if ( in_array('state_id', $errors ) )
			{
				echo '<li>You must select a state.</li>';
				$val_stateid = ' class="form_field_error" ';
			}

			if ( in_array('active', $errors ) )
			{
				echo '<li>You must select from active.</li>';
				$val_active = ' class="form_field_error" ';
			}

		echo '</ul>';

	} else {

		$c = ( $action == 'update' ) ? 'update this' : 'add a new';

		echo  '<ul class="notice_message"><strong>To ' . $c . ' this record, fill out the form and click submit.</strong>
			<li>All fields are required.</li>
		</ul>';
  }

	if ($action == "update")
	{
		$stmt = $db->prepare('SELECT * FROM '.$dbtbl.' WHERE id = ?');
		$stmt->execute(array($id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
	}

	if (!empty ($_POST))
	{
		$row = sanitize_vars ($_POST);
	}

/********************************************************
 * Start Building Form
 *******************************************************/

	$r = required();

  echo '<form name="form" id="form" method="post" action="#">
		<table>
    	<input type="hidden" name="id" value="' . $id . '" />';
			echo '<tr>
				<td> <label for="name">'.$r.'City Name</label></td>
				<td> <input ' . $val_name . ' type="text" name="name" id="name" value="'. htmlspecialchars($row['name']) .'" size="45" /></td>
			</tr>';

			echo '<tr>
				<td> <label for="state_id">'.$r.'State</label></td>
				<td> <select ' . $val_stateid . ' name="state_id" id="state_id">
						<option value="">--SELECT ONE--</option>';
							
						$stmt1 = $db->prepare("SELECT * FROM ".db_prefix."states WHERE active = ? ORDER BY name ASC");
						$stmt1->execute(array(1));
						while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC))
						{
							echo '<option value="'.$row1['id'].'"';
							if($row['state_id'] == $row1['id'])
								echo ' selected';
							echo '>'.$row1['name'].'</option>';
						}
					echo '</select>
				</td>
			</tr>';

			echo '<tr>    
				<td><label for="active">'.$r.'Active</label></td>  
				<td>
					'.create_slist ( $list, 'active', $row['active'], 1 ) . 
					tooltip('Set to Yes to allow on website').'
				</td>
			</tr>  ';

			echo '<tr>
				<td colspan="2" style="padding:3px;"><input type="submit" name="submit" value="Submit" /></td>
			</tr>';

		echo '</table>
	</form>';

  echo '<script type="text/javascript">document.getElementById(\'name\').focus();</script>';

}//end function


/***************************************************************
 *
 * function sanitize_vars
 * @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 **************************************************************/
function sanitize_vars( $data )
{
	$r_data['name'] = stripslashes ( $data['name'] );
	$r_data['state_id'] = intval ( $data['state_id'] );
	$r_data['active']  = intval ( $data['active'] );

	return $r_data;
}


/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $db, $identifier, $module_name, $id, $dbtbl;

	$stmt = $db->prepare("SELECT * FROM ".$dbtbl." WHERE id = ?");
	$stmt->execute(array($id));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	print_header('Delete '.$module_name.'  - ' . $row['name']);

	if ( !empty($_POST ))
	{
		$errno = 0;

		try 
		{
			$stmt = $db->prepare('DELETE FROM '.$dbtbl.' WHERE id = ?');
			$stmt->execute(array($id));
		}

		catch(PDOException $ex)
		{
			$errno = $ex->getCode();
		}

		print_mysql_message ( $errno , $module_name, $id, 2 ) ;
	
	} else {
		
		echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">
			<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to delete this record?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div>
		</form>';
	}

}//end function


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of contents
 *
 **************************************************************/

function update()
{
	global $db, $identifier, $module_name, $action, $id, $dbtbl;
	
	if ( $action == 'update' )
		print_header('Update '.$module_name);
	else
		print_header('Add New '.$module_name);

	if ( array_key_exists ('submit',$_POST))
	{
		require ("classes/validation.php");
		
		// set rules for required fields
		$rules   = array();
		$rules[] = "required,name,name";
		$rules[] = "required,state_id,state_id";
		$rules[] = "required,active,active";
		$errors = validateFields($_POST, $rules);
	
		if ( $action != 'update' &&  $_POST['state_id'] != ''  )
		{
			$stmt = $db->prepare("SELECT id FROM ".$dbtbl." WHERE name = ? AND state_id = ".$_POST['state_id']);
      $stmt->execute(array($_POST['name']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_name';
		}
		
		if ( $action == 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM ".dbtbl." WHERE name = ? AND state_id = ".$_POST['state_id']." AND id != $id");
      $stmt->execute(array($_POST['name']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_name';
		}
	
	}

	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$vars = array();
		$data = sanitize_vars( $_POST );
		$type = ( $action == 'update' ) ? 0 : 1;
		
		if ( $action == 'update' )
		{
			$sql = "UPDATE ".$dbtbl." SET name = ?, active = ?, state_id = ? WHERE id = ?";
			array_push($vars, $data['name']);
			array_push($vars, $data['active']);
			array_push($vars, $data['state_id']);
			array_push($vars, $id);
		
		} else {

			$sql = 'INSERT INTO '.$dbtbl.' (name, active, state_id) VALUES (?, ?, ?)';
			array_push($vars, $data['name']);
			array_push($vars, $data['active']);
			array_push($vars, $data['state_id']);
		}

		$errono = 0;

		try 
		{
			$stmt = $db->prepare($sql);
			$stmt->execute($vars);
		}

		catch(PDOException $ex)
		{
			$errono = $ex->getCode();
		}

		if ( is_saint() && $errono != 0 )
			echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

		print_mysql_message ( $errono , $module_name, $id, $type ) ;

	} else {
		
		add( $errors );
	}

}//end function

?>