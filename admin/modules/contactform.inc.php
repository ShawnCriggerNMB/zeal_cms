<?php

//****************************************************/

// Module     : Contact Form Setup

// Written By : Darrell Bessent

// Updated On : Jun 09, 2013

// Copyright Zeal Technologies

//***************************************************/



if ( empty ( $config ))
	$config = fetch_config_data();

$dbtbl = db_prefix."contact_form";



function execute()

{

		update();

}





function add ( $errormsg = '' )

{

	global $sitename, $site_base_url, $config, $db, $identifier, $module_name, $dbtbl;

	

	print_header($module_name.' Setup');

	

	if ( $config['display_contact'] == '0' )

		{

			echo '<div class="error_message">`Display Contact Form` is disabled in the `Site Configuration.` Please remember to enable `Display Contact Form` before trying to view any current changes. <br />' . "\n" . '</div>' . "\n";

		}

	 

   if ( $errormsg != '' )

			{

					echo '<div class="error_message"><strong>ERROR:</strong><br />'.$errormsg.'YOUR CHANGES WERE NOT SAVED! Please try again.</div>';

			

			}

    

   echo '<br />'.tooltip('Select the fields you want to use by setting Active to Yes. The form information will be sent to the email address you used on the Setup page.

	 REQUIRED fields are ones that the visitor must fill out to submit the form. ORDER is the order in which the fields will appear in the form. Use 0-999.

	  For the "State" field, use \'2\' in the # CHARS if you only want to allow abbreviations. There are 4 form fields you can customize. When choosing "LABEL", keep it short. Set the # CHARS for the max number of characters your visitor is allowed to type in the form field.

		 You must use be between 1-150.').'    <form action="" name="contact_form" id="contact_form" method="POST">' . "\n" .

        '        <table class="form" style="width:575px;">' . "\n" .

        '          <tr>' . "\n" .

        '            <td class="infocollabel" style="width:59px;">ACTIVE</td>' . "\n" .

        '            <td class="infocollabel" style="width:296px;">LABEL</td>' . "\n" .

        '            <td class="infocollabel" style="width:71px;"># CHARS</td>' . "\n" .

        '            <td class="infocollabel" style="width:85px;">REQUIRED</td>' . "\n" .

        '            <td class="infocollabel" style="width:58px;">ORDER</td>' . "\n" .

        '          </tr>' . "\n";

                  

         



		$stmt = $db->prepare("SELECT * FROM " . $dbtbl . " ORDER BY weight,id"); 

        $stmt->execute();

		$totRows = $stmt->rowCount();

		

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC))

		{

        				//$id[]=$row['id'];

					echo '<input name="id[]" type="hidden" value="'.$row['id'].'" />';

                      

// set up the numchar form field

     if($row['id'] == '11')

					{ // textfield numchars not editable but is unlimited

        $numcharfld = 'unl';

        $numcharfld .=  '<input name="numchar[]" type="hidden" id="numchar[]"  value="'.$row['numchar'].'" />';

     } elseif($row['id'] == '6') { // numchars - state can be 2 characters to 50 characters

        $numcharfld = '<input name="numchar[]" type="text" class="formtext" id="numchar[]" size="3" maxlength="2" value="'.$row['numchar'].'" />';

     } elseif($row['id'] > '11') { // numchars - ID's above #11 can be up to 150 characters

        $numcharfld = '<input name="numchar[]" type="text" class="formtext" id="numchar[]" size="3" maxlength="3" value="'.$row['numchar'].'" />';

     } else { // all others numchars are not editable

        $numcharfld = $row['numchar'];

        $numcharfld .= '<input name="numchar[]" type="hidden" id="numchar[]" value="'.$row['numchar'].'" />';

     } // end if

                        

					echo '<tr><td class="center" style="padding:2px 0;">' . "\n" .

          '<select class="formtext" name="active[]" id="active[]">' . "\n" .

										'									<option value="1" '; if (!(strcmp("1", $row['active']))) echo 'SELECTED'; echo ' >Yes</option>' . "\n" .

										'									<option value="0" '; if (!(strcmp("0", $row['active']))) echo ' SELECTED'; echo ' >No</option>' . "\n" .

										'						</select>' . "\n" .

										'				</td>' . "\n" .

          '    <td class="formrttd">' . "\n";



//echo $row['id']; echo '&nbsp;&nbsp;'; // FOR TESTING - SHOWS ME THE ROW ID's



     if($row['id'] > '10')

					{ // customize labels

          echo '<input name="label[]" type="text" class="formtext" id="label[]" size="20" maxlength="25" value="'.$row['label'].'" />';

          

										if($row['id'] > '11')										

												echo '<span class="attn padattn2">max 150 --&raquo;</span>';

										

     } else { // can not customize labels

          

										echo $row['label'];

          echo '<input name="label[]" type="hidden" id="label[]" value="'.$row['label'].'" />';

          

										if($row['id'] == '6')

												echo '<span class="attn padattn2">Between 2 and 45 only --&raquo;</span>';

     } // end if

                            

					echo '</td>' . "\n";

                            

					echo '<td class="formrttd">'.$numcharfld.'</td>' . "\n" .           

										'<td class="center">' . "\n" .

          '    <select class="formtext" name="req[]" id="req[]">' . "\n" .

          '<option value="1" '; if (!(strcmp("1", $row['req']))) echo 'SELECTED'; echo '>Yes</option>' . "\n" .

										'<option value="0" '; if (!(strcmp("0", $row['req']))) echo 'SELECTED'; echo '>No</option>' . "\n" .

          '</select>' . "\n" . '</td>' . "\n";

                            

				 echo ' <td class="center">' . "\n" .

          '		<input name="weight[]" type="text" class="formtext" id="weight[]" size="3" maxlength="3" value="'.$row['weight'].'">' . "\n" .

          ' </td></tr>' . "\n";

  } // end while

  

		$stmt = $db->prepare("SELECT * FROM " . db_prefix . "messages WHERE name = 'contact_success'");

        $stmt->execute();

		$row    = $stmt->fetch(PDO::FETCH_ASSOC);

		

  echo '    <tr>' . "\n" .

       '       <td colspan="5">' . "\n" .

       '         <p>&nbsp;</p>' . "\n" .

       '           <div style="text-align:left;">' . "\n" .

							$row['label'] . ' <span class="note" style=" color:#B02B2C;font-size:70%">' . "\n" .

       $row['info']. '</span><br />' . "\n";

							

  echo '<textarea name="contact_success" class="" id="contact_success">' . "\n";

  

		$contact_success = $row['text'];

		if ( !empty ($_POST['contact_success'] )) 

					echo stripslashes($_POST['contact_success']);

			else

					echo $contact_success; 

  echo '</textarea>' . "\n";

		

  echo '<p>&nbsp;</p>' . "\n";

		$stmt = $db->prepare("SELECT * FROM " . db_prefix . "messages WHERE name = 'contact_success'");

        $stmt->execute();

		$row    = $stmt->fetch(PDO::FETCH_ASSOC);

		if ( !empty($_POST['aw3']))

				$row['aw3'] = $_POST['aw3'];

		else 

				$row['aw3'] = $config['aw3'];									

		

  echo '<select class="formtext" name="aw3">' . "\n" .

       '  <option value="1" '; if (!(strcmp("1", $row['aw3']))) echo "SELECTED"; echo '>Yes</option>' . "\n" .

						 '  <option value="0" '; if (!(strcmp("0", $row['aw3']))) echo "SELECTED"; echo '>No </option>' . "\n" .

        '		</select>' . "\n" .

        '<span class="note" style=" color:#B02B2C;font-size:73%"> Automatically send a confirmation email to any visitor who fills out the contact form. </span>' . "\n" .

        '<br /><br />' . "\n";

   

		 $stmt = $db->prepare("SELECT * FROM " . db_prefix . "messages WHERE name = 'contact_confirm'");

        $stmt->execute();

		 $row    = $stmt->fetch(PDO::FETCH_ASSOC);



			echo $row['label'] . ' <span class="note" style="font-size:70%;">' . $row['info'] . '</span><br />' . "\n";

                            

			echo 'Hi [First Name], <span class="attn"> (will automatically be shown)</span>' . "\n" .

        '<textarea name="contact_confirm" class="" id="contact_confirm">' . "\n";

   

			if ( !empty($_POST['contact_confirm']))

			{

					echo stripslashes($_POST['contact_confirm']);

			} else {

				 echo $row['text'];

			}

			

			echo '</textarea>' . "\n" .

        $sitename . '<span class="attn"> (will automatically be shown)</span><br />' . "\n" .

        $site_base_url .  '<span class="attn"> (will automatically be shown)</span>' . "\n" .

        '</div>' . "\n" .

        '<p class="center" style="margin-top:15px;"><input name="Submit" type="submit" value="Submit" class="button"></p>' . "\n" .

        '</td></tr></table></form>' . "\n";

		 echo '<div class="spacer">&nbsp;</div>';

			

}





function update()

{

		global $id, $action, $db, $dbtbl, $module_name, $identifier;

	

		$stmt = $db->prepare("SELECT * FROM " . $dbtbl . " ORDER BY weight,id");

    $stmt->execute();

		$totRows = $stmt->rowCount();

		

		if ( !empty($_POST) )

		{

					$error = false;	

					for ($i=0;$i<$totRows;$i++)

					{

							$id = $_POST['id'][$i];

							$numchar = $_POST['numchar'][$i];

							$active = $_POST['active'][$i];

				

							if ($id == "6" && ($numchar < "2" || $numchar > "45"))

							{

									$error = true;

									$errormsg .= "The State # CHARS must be between 2 and 45.<br />";

							}

							if ($id > "11" && $active == "1" && $numchar < "1")

							{

									$error = true;

									$errormsg .= "# CHARS for the customized field(s) must be greater than 1.<br />";

							}

						

							if ($id > "11" && $numchar > "150")

							{

									$error = true;

									$errormsg .= "# CHARS for the customized field(s) must be smaller than 150.<br />";

							}

						} // end for

			}

			

			if ( !$error && !empty($_POST) )

			{

				echo '<div class="success_message">The Contact Form has been updated.<br />'.

				'<a class="tools" href="./?tool='.$identifier.'">Click here to continue managing the contact form.</a></div>';

	

				for ($i=0;$i<$totRows;$i++)

				{

						$id = $_POST['id'][$i];

						$active = $_POST['active'][$i];

						$label = $_POST['label'][$i];

						$numchar = $_POST['numchar'][$i];

						$req = $_POST['req'][$i];

						$weight = $_POST['weight'][$i];

		

						$stmt = $db->prepare("UPDATE " . $dbtbl . "

						    SET active=?,

						    label=?,

						    numchar=?,

						     req=?,

						     weight=?

                         WHERE id = ?");

						$stmt->execute(array($active, $label, $numchar, $req, $weight, $id));

					

	/*				//FOR TESTING ONLY

					echo 'ID: '.$id.'&nbsp;&nbsp;'; 

					echo 'Label: '.$label.'&nbsp;&nbsp;'; 

					echo 'Active: '.$active.'&nbsp;&nbsp;'; 

					echo 'Numchar: '.$numchar.'&nbsp;&nbsp;'; 

					echo 'req: '.$req.'&nbsp;&nbsp;'; 

					echo 'orderby: '.$orderby.'&nbsp;&nbsp;'; 

					echo '<br>'; */

				} // end for

			

				// update if confirmation email is to be sent to visitor

				$stmt = $db->prepare("UPDATE " . db_prefix . "config SET aw3 = ? WHERE id = '1'");

				$stmt->execute(array(intval ( $_POST['aw3'] )));

				

				// update the success message

				$contact_success = stripslashes($_POST['contact_success']);

				$contact_success = strip_tags($_POST['contact_success']);

				

				$stmt = $db->prepare("UPDATE " . db_prefix . "messages SET `text`=? WHERE `name`='contact_success'");

				$stmt->execute(array($contact_success));

	

				// update the confirmation email message to visitor

				$contact_confirm = stripslashes($_POST['contact_confirm']);

				$contact_confirm = strip_tags($_POST['contact_confirm']);

				$stmt = $db->prepare("UPDATE " . db_prefix . "messages SET `text`=? WHERE `name`='contact_confirm'");

				$stmt->execute(array($contact_confirm));

			} else {

				

				add($errormsg);

		}

	

}





?>

