<?php

//****************************************************/

// Module: Listings Management System

// Written By : Cory Shaw of EWD

// Written On : May 12, 2014

// Copyright Zeal Technologies

//***************************************************/



if ( !defined('BASE') ) die('No Direct Script Access');



$upload_url  = $site_base_url . $cms_uploads . 'listings/';

$upload_dir  = $site_base_dir . $cms_uploads . 'listings/';

$height = '';

$width  = '600';

$tn_height = '';

$tn_width = '150';





function execute()

{

    switch($_GET['action'])

    {

        case 'update':

            update();

            break;

        case 'add':

            update();

            break;

        case 'remove':

            remove();

            break;

        default:

            manage();

    }

}





/***************************************************************

 *

 * function manage

 * Querys DB and Displays Resellers

 *

 *

 **************************************************************/



function manage()

{

    global $db, $identifier, $module_name, $dbtbl;



    $i    = 0;

    $link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';



    print_header('Manage '.$module_name,$link);



    $sql = '';



    echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table" >' . "\n" .

        '<thead><tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" .

        '<th><h3>Title</h3></th>' . "\n" .

        '<th><h3>User</h3></th>' . "\n" .

        '<th><h3>Subcategory</h3></th>' . "\n" .

        '<th><h3>City</h3></th>' . "\n" .

        '<th style="width:50px;"><h3>Active</h3></th>' . "\n" .

        '<th class="nosort" style="width:112px;"><h3>Tools</h3></th>' . "\n" .

        '</tr></thead>

        <tbody>';



    $stmt = $db->prepare('SELECT

                                    '.$dbtbl.'.id,

                                    '.$dbtbl.'.title,

                                    '.$dbtbl.'.active,

                                    '.db_prefix.'users.fname,

                                    '.db_prefix.'users.lname,

                                    '.db_prefix.'cities.name AS city,

                                    '.db_prefix.'subcategories.name AS subcategory

                             FROM ' . $dbtbl . '

                             JOIN '.db_prefix.'users

                                ON '.db_prefix.'users.id = '.$dbtbl.'.user_id

                             JOIN '.db_prefix.'cities

                                ON '.db_prefix.'cities.id = '.$dbtbl.'.city_id

                             JOIN '.db_prefix.'subcategories

                                ON '.db_prefix.'subcategories.id = '.$dbtbl.'.subcategory_id

                             ORDER BY title');

    $stmt->execute();

    $pages   = $stmt->rowCount();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))

    {

        $i++;

        $act = yes_no($row['active']);



        echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

        echo '<td>' . $row['title'] . '</td>' . "\n";

        echo '<td>' . $row['fname'] . '&nbsp;' . $row['lname'] . '</td>' . "\n";

        echo '<td>' . $row['subcategory'] . '</td>' . "\n";

        echo '<td>' . $row['city'] . '</td>' . "\n";

        echo '<td>' . $act . '</td>' . "\n";

        echo '<td valign="middle"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&action=remove&id='.$row['id'].'">Remove</a></td>';

        echo '</tr>' . "\n";



    }



    echo '</tbody>

	</table>';



    if ( $pages > 20 )

        $pages = true;

    else

        $pages = false;



    echo_js_sorter ( $pages );



}



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Form for User Input

 *

 *

 **************************************************************/



function add( $errors = '' )

{

    global $db, $identifier, $module_name, $id, $action, $dbtbl, $upload_dir, $upload_url;



    $base_path = dirname(__FILE__);



    include_once $base_path . '/ckeditor/ckeditor.php' ;

    $option = '';



    if ( $errors )

    {

        echo '<div class="error_message">';

        echo 'Please fill in the required fields.';



        if ( in_array('user_id', $errors ) )

        {

            echo '<br />* A user is required.';

            $val_fname = ' class="form_field_error" ';

        }



        if ( in_array('city_id', $errors ) )

        {

            echo '<br />* A city is required.';

            $val_lname = ' class="form_field_error" ';

        }



        if ( in_array('subcategory_id', $errors ) )

        {

            echo '<br />* A subcategory is required.';

            $val_email = ' class="form_field_error" ';

        }



        if ( in_array('title', $errors ) )

        {

            echo '<br />* A title is required.';

            $val_email = ' class="form_field_error" ';

        }



        echo '</div>' . "\n";

    }



    if ($action == "update")

    {

        // Run the query for the existing data to be displayed.

        $stmt = $db->prepare('SELECT * FROM ' . $dbtbl . ' WHERE id = ?');

        $stmt->execute(array($id));

        $row 		 = $stmt->fetch(PDO::FETCH_ASSOC);

    } else {

        $row     = sanitize_vars ( $_POST );

    }



    $req  = '<span class="req">*</span>';

    $preq = ($action == 'add') ? $req : '';



    $cities = explode("|", $row['city_id']);

    /********************************************************

     * Start Building Form

     *******************************************************/



    echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";

    echo '<input type="hidden" name="id" value="' . $id . '" />';



    $passnote = tooltip('Recommended: 8-10 characters using uppercase, lowercase, and numbers.');

    if ($action == 'update')

        $passnote = tooltip('Leave blank to keep current password.<br />

													Recommended: 8-10 characters using uppercase, lowercase, and numbers.');



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="title">Listing Title</label></td>' . "\n" .

        '<td> <input ' . $val_title . ' type="text" name="title" id="title" value="'. htmlspecialchars($row['title']) .'" size="45"

				tabindex="' . $t_index . '" /></td>  ' . "\n" .

        '</tr>' . "\n";



    $t_index++;

    echo  '<tr><td colspan="2" style="width:100%">Description:</td></tr><tr><td colspan="2" style="text-align:center; width:100%">' . "\n" .

        '<textarea style="min-height:270px; height:270px; width:100%;" name="description" id="description">' . $row['description'] . '</textarea>' . "\n" .

        '</td></tr>' . "\n";



    $ckeditor  = new CKEditor( ) ;

    $ck_config = set_fck_config();

    $ck_events = set_fck_events();



    $ck_config['toolbar'] = set_fck_toolbar_pastefromword();

    $ck_config['height']  = 200;

    $ck_config['width']   = '102%';



// EDIT BELOW LINE TO ADD YOUR WEBSITE STYLESHEET

//		$ck_config['contentsCss'] = $base_site_url . '/styles/main.css';

    $ckeditor->replace("description", $ck_config, $ck_events);



    $t_index++;

    echo '<tr>' . "\n" .

        '<td><label for="verified">User</label></td>  ' . "\n" .

        '<td>' . "\n" .

        '<select name="user_id" id="user_id">' .

        '<option value="">--SELECT ONE--</option>';

        $stmt1 = $db->prepare("SELECT * FROM ".db_prefix."users WHERE active = 1 ORDER BY fname");

        $stmt1->execute();



    while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

        echo '<option value ="'.$row1['id'].'"';

        if($row1['id'] == $row['user_id'])

            echo " selected";

        echo '>'.$row1['fname'].' '.$row1['lname'].'</option>';

    }

    echo    '</select>' .

        tooltip('Select which user the listing is connected to') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td><label for="verified">Cities</label></td>  ' . "\n" .

        '<td>' . "\n" .

        '<select name="city_id" id="city_id">';

    echo '<option value="">--SELECT ONE--</option>';

    $stmt1 = $db->prepare("SELECT ".db_prefix."states.name AS state, ".db_prefix."cities.* FROM ".db_prefix."cities

                                        JOIN ".db_prefix."states

                                            ON ".db_prefix."cities.state_id = ".db_prefix."states.id

                                WHERE ".db_prefix."cities.active = 1 AND ".db_prefix."states.active = 1

                                ORDER BY ".db_prefix."cities.name, ".db_prefix."states.name");

    $stmt1->execute();

    while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

        echo '<option value ="'.$row1['id'].'"';

        if($row1['id'] == $row['city_id'])

            echo " selected";

        echo '>'.$row1['name'].', '.$row1['state'].'</option>';

    }

    echo    '</select>' .

        tooltip('Select which city the listing is connected to') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td><label for="verified">Subcategory</label></td>  ' . "\n" .

        '<td>' . "\n" .

        '<select name="subcategory_id" id="subcategory_id">';

    echo '<option value="">--SELECT ONE--</option>';

    $stmt1 = $db->prepare("SELECT * FROM ".db_prefix."categories

                                WHERE active = 1

                                ORDER BY name");

    $stmt1->execute();

    while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

        echo "<optgroup label='".$row1['name']."'>";

        $stmt2 = $db->prepare("SELECT * FROM ".db_prefix."subcategories

                                WHERE active = 1 AND category_id = ?

                                ORDER BY name");

        $stmt2->execute(array($row1['id']));

        while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)){

            echo '<option value ="'.$row2['id'].'"';

            if($row2['id'] == $row['subcategory_id'])

                echo " selected";

            echo '>'.$row2['name'].'</option>';

        }

        echo "</optgroup>";

    }



    echo    '</select>' .

        tooltip('Select which subcategory the listing is connected to') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="active">Active</label></td>  ' . "\n" .

        '<td>' . "\n" .

        create_slist ( $list, 'active', $row['active'], 1, $t_index ) .

        tooltip('Set to Yes to Allow Login') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="listing_images">Listing Images</label></td>  ' . "\n" .

        '<td>' . "\n";

    $stmt1 = $db->prepare("SELECT * FROM ".db_prefix."listing_images WHERE listing_id = ?");

    $stmt1->execute(array($id));

    $current_images = $stmt1->rowCount();

    $config = fetch_config_data();

    while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

        $file = $upload_url.$row1['filename'];

        echo '<div class="image" style="width:300px"><a href="'.$file.'" class="lightbox">'.$row1['filename'].'</a>&nbsp;<a style="float:right" href="javascript:deleteListingImage(\''.$row1['filename'].'\');">Delete</a></div>';

    }

    echo "<br/>";

    for($i = $current_images; $i < ($config['listing_images']); $i++){

        echo "<input type='file' name='listing_images[]' /><br/>";

    }

    echo "<br/>";

    echo '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr><td colspan="2" style="text-align:center; margin:0 auto;" ><input type="submit" name="submit" 

			value="Submit" /></td></tr>' . "\n";

    echo '</table></form>' . "\n";

    echo '<script type="text/javascript">document.getElementById(\'title\').focus();</script>' . "\n";



}







/***************************************************************

 *

 * function sanitize_vars

 * @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

    global $id, $known_photo_types, $upload_dir, $width, $height;



    $config = fetch_config_data();



    $r_data['title']            = $data['title'];

    $r_data['description']      = $data['description'];

    $r_data['user_id']          = intval ( $data['user_id'] );

    $r_data['city_id']          = intval ( $data['city_id'] );

    $r_data['subcategory_id']   = intval ( $data['subcategory_id'] );

    $r_data['active']           = intval ( $data['active'] );



    $r_data['listing_images'] = array();

    for($i = 0; $i < $config['listing_images']; $i++){

        if(!empty ( $_FILES['listing_images']['name'][$i] )){

            array_push($r_data['listing_images'], process_upload ( $id , '', $_FILES['listing_images'], $known_photo_types, $upload_dir, 1, $width, $height, $i ));

        }

    }



    return $r_data;

}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 *

 **************************************************************/



function remove()

{

    global $db, $identifier, $module_name, $id, $module_name, $userinfo, $config, $dbtbl, $upload_dir;



    $stmt = $db->prepare("SELECT * FROM " . $dbtbl . " WHERE id = ?");

    $stmt->execute(array($id));

    $row    = $stmt->fetch(PDO::FETCH_ASSOC);



    print_header('Delete '.$module_name.'  - ' . $row['title']);



    if ( !empty($_POST ))

    {

        $errno = 0;

        try{

            //Delete listing

            $stmt = $db->prepare("DELETE FROM " . $dbtbl . " WHERE id = ?");

            $stmt->execute(array($id));



            //Loop and delete all files

            $stmt = $db->prepare("SELECT * FROM ". db_prefix . "listing_images WHERE listing_id = ?");

            $stmt->execute(array($id));

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){

                if ( file_exists ( $upload_dir . $row['image'] ))

                {

                    @unlink( $upload_dir . $row['filename'] );

                }

            }



            //Delete records from db

            $stmt = $db->prepare("DELETE FROM " . db_prefix . "listing_images WHERE listing_id = ?");

            $stmt->execute(array($id));

        }

        catch(PDOException $ex){

            $errno = $ex->getCode();

        }







        print_mysql_message ( $errno , $module_name, $id, '2' ) ;

    } else {



        echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">' . "\n" .

            '<input type="hidden" name="id" value="' . $id . '">' . "\n" .

            '<table width="100%" border="0">' . "\n" .

            '<tr> <td><div align="center">Are you sure you want to delete this listing?</div></td></tr>' . "\n" .

            '<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;

                <input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div></td>

            </tr>' . "\n" .

            '</table>

        </form>';

    }

}





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute function show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

    global $db, $identifier, $module_name, $action, $id, $module_name, $dbtbl;



    if ( $action == 'update' )

        print_header('Update '.$module_name);

    else

        print_header('Add New '.$module_name);



    if ( array_key_exists ('submit',$_POST))

    {

        require_once("classes/validation.php");



        $rules = array();



        $rules[] = "required,title,title";

		$rules[] = "required,user_id,user_id";

        $rules[] = "required,city_id,city_id";

        $rules[] = "required,subcategory_id,subcategory_id";



        $errors = validateFields($_POST, $rules);



    }



    if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

    {

        $data  = sanitize_vars( $_POST );

        $vars = array();

        $xsql = "";



        if ( $action == 'update' )

        {

            $sql = 'UPDATE ' . $dbtbl . ' SET

                title = ?,

                description = ?,

                user_id = ?,

                city_id = ?,

                subcategory_id = ?,

                active = ?

                ';

            array_push($vars, $data['title']);

            array_push($vars, $data['description']);

            array_push($vars, $data['user_id']);

            array_push($vars, $data['city_id']);

            array_push($vars, $data['subcategory_id']);

            array_push($vars, $data['active']);



            $sql .= " WHERE id = ?";

            array_push($vars, $id);

            $type = 0;

        } else {

            $data = sanitize_vars( $_POST );



            $sql = 'INSERT INTO ' . $dbtbl . ' (title, description, user_id, city_id, subcategory_id, active)

							VALUES ' . "(

							?,

							?,

                            ?,

							?,

							?,

							?

							)";

            array_push($vars, $data['title']);

            array_push($vars, $data['description']);

            array_push($vars, $data['user_id']);

            array_push($vars, $data['city_id']);

            array_push($vars, $data['subcategory_id']);

            array_push($vars, $data['active']);





            $type = 1;

        }



        $errno = 0;

        try{

            $stmt = $db->prepare($sql);

            $stmt->execute($vars);

            if(!empty($data['listing_images'])){



                if ( $action != 'update' )

                    $id = $db->lastInsertId(); //get new insert id



                foreach($data['listing_images'] AS $image){

                    $stmt = $db->prepare("INSERT INTO ".db_prefix."listing_images (filename, active, listing_id) VALUES (?, ?, ?)");

                    $stmt->execute(array($image, 1, $id));

                }



            }

        }

        catch(PDOException $ex){

            $errno = $ex->getCode();

        }





        if ( ( $errno != 0 ) && ( is_saint() ) )

            print_debug($sql);



        print_mysql_message ( $errno , $module_name, $id, $type ) ;



    } else {

        add( $errors );

    }//end if	

}//end function

