<?php	
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module: Changelog for Admin Section
// Written By : Darrell Bessent of EWD
// Written On : Jun 07, 2013
// Copyright Zeal Technologies
//***************************************************/

	function execute()
	{
		echo fetch_output();
	}//end function

 	function fetch_output()
	{
		global $userinfo, $sitename;

		$html = $curr_version = file_get_contents('http://www.zealtechnologies.com/changelog.txt');	

		return $html;
	}

?>