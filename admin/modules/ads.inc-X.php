<?php

//****************************************************/

// Module: Listings Management System

// Written By : Cory Shaw of EWD

// Written On : May 12, 2014

// Copyright Zeal Technologies

//***************************************************/



if ( !defined('BASE') ) die('No Direct Script Access');



$upload_url  = $site_base_url . $cms_uploads . 'ads/';

$upload_dir  = $site_base_dir . $cms_uploads . 'ads/';

$height = '';

$width  = '600';

$tn_height = '';

$tn_width = '150';





function execute()

{

    switch($_GET['action'])

    {

        case 'update':

            update();

            break;

        case 'add':

            update();

            break;

        case 'remove':

            remove();

            break;

        default:

            manage();

    }

}





/***************************************************************

 *

 * function manage

 * Querys DB and Displays Resellers

 *

 *

 **************************************************************/



function manage()

{

    global $db, $identifier, $module_name, $dbtbl;



    $i    = 0;

    $link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';



    print_header('Manage '.$module_name,$link);



    $sql = '';



    echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table" >' . "\n" .

        '<thead><tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" .

        '<th><h3>User</h3></th>' . "\n" .

        '<th><h3>Reseller</h3></th>' . "\n" .

        '<th><h3>Type</h3></th>' . "\n" .

        '<th><h3>Homepage</h3></th>' . "\n" .

        '<th><h3>Subcategory</h3></th>' . "\n" .

        '<th><h3>City</h3></th>' . "\n" .

        '<th><h3>State</h3></th>' . "\n" .

        '<th><h3>Impressions Left</h3></th>' . "\n" .

        '<th><h3>Running</h3></th>' . "\n" .

        '<th style="width:50px;"><h3>Active</h3></th>' . "\n" .

        '<th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" .

        '</tr></thead>

        <tbody>';



    $stmt = $db->prepare('SELECT

                                    '.$dbtbl.'.id,

                                    '.$dbtbl.'.home,

                                    '.$dbtbl.'.running,

                                    '.$dbtbl.'.active,

                                    '.db_prefix.'users.fname AS user_first,

                                    '.db_prefix.'users.lname AS user_last,

                                    '.db_prefix.'resellers.fname AS reseller_first,

                                    '.db_prefix.'resellers.lname AS reseller_last,

                                    '.db_prefix.'ad_types.size AS type,

                                    '.db_prefix.'cities.name AS city,

                                    '.db_prefix.'states.name AS state,

                                    '.db_prefix.'subcategories.name AS subcategory,

                                    '.db_prefix.'categories.name AS category

                             FROM  ' . $dbtbl . '

                             JOIN '.db_prefix.'resellers

                                ON '.db_prefix.'resellers.id = '.$dbtbl.'.reseller_id

                             JOIN '.db_prefix.'users

                                ON '.db_prefix.'users.id = '.$dbtbl.'.user_id

                             JOIN '.db_prefix.'ad_types

                                ON '.db_prefix.'ad_types.id = '.$dbtbl.'.ad_type_id

                             LEFT JOIN '.db_prefix.'cities

                                ON '.db_prefix.'cities.id = '.$dbtbl.'.city_id

                             LEFT JOIN '.db_prefix.'states

                                ON '.db_prefix.'states.id = '.$dbtbl.'.state_id

                             LEFT JOIN '.db_prefix.'subcategories

                                ON '.db_prefix.'subcategories.id = '.$dbtbl.'.subcategory_id

                             LEFT JOIN '.db_prefix.'categories

                                ON '.db_prefix.'categories.id = '.db_prefix.'subcategories.category_id

                                ');

    $stmt->execute();

    $pages   = $stmt->rowCount();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))

    {

        $i++;

        $home = yes_no($row['home']);

        $running = yes_no($row['running']);

        $act = yes_no($row['active']);



        echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

        echo '<td>' . $row['user_first'] . '&nbsp;' . $row['user_last'] . '</td>' . "\n";

        echo '<td>' . $row['reseller_first'] . '&nbsp;' . $row['reseller_last'] . '</td>' . "\n";

        echo '<td>' . $row['type'] . '</td>' . "\n";

        echo '<td>' . $home . '</td>' . "\n";

        echo '<td>' . $row['subcategory'] . '</td>' . "\n";

        echo '<td>' . $row['city'] . '</td>' . "\n";

        echo '<td>' . $row['state'] . '</td>' . "\n";

        echo '<td>' . intval($row['impressions']) . '</td>' . "\n";

        echo '<td>' . $running . '</td>' . "\n";

        echo '<td>' . $act . '</td>' . "\n";

        echo '<td valign="middle"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&action=remove&id='.$row['id'].'">Remove</a></td>';

        echo '</tr>' . "\n";



    }



    echo '</tbody>

	</table>';



    if ( $pages > 20 )

        $pages = true;

    else

        $pages = false;



    echo_js_sorter ( $pages );



}



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Form for User Input

 *

 *

 **************************************************************/



function add( $errors = '' )

{

    global $db, $identifier, $module_name, $id, $action, $dbtbl, $upload_dir, $upload_url, $width, $height;



    $base_path = dirname(__FILE__);



    include_once $base_path . '/ckeditor/ckeditor.php' ;

    $option = '';



    if ( $errors )

    {

        echo '<div class="error_message">';

        echo 'Please fill in the required fields.';



        if ( in_array('user_id', $errors ) )

        {

            echo '<br />* A user is required.';

            $val_fname = ' class="form_field_error" ';

        }



        if ( in_array('reseller_id', $errors ) )

        {

            echo '<br />* A reseller is required.';

            $val_reseller = ' class="form_field_error" ';

        }



        if ( in_array('ad_type_id', $errors ) )

        {

            echo '<br />* An ad type is required.';

            $val_lname = ' class="form_field_error" ';

        }



        if ( in_array('subcategory_id', $errors ) )

        {

            echo '<br />* A subcategory is required.';

            $val_email = ' class="form_field_error" ';

        }



        echo '</div>' . "\n";

    }



    if ($action == "update")

    {

        // Run the query for the existing data to be displayed.

        $stmt = $db->prepare('SELECT * FROM ' . $dbtbl . ' WHERE id = ?');

        $stmt->execute(array($id));

        $row 		 = $stmt->fetch(PDO::FETCH_ASSOC);

    } else {

        $row     = sanitize_vars ( $_POST );

    }



    $req  = '<span class="req">*</span>';

    $preq = ($action == 'add') ? $req : '';

    /********************************************************

     * Start Building Form

     *******************************************************/



    echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";

    echo '<input type="hidden" name="id" value="' . $id . '" />';



    $passnote = tooltip('Recommended: 8-10 characters using uppercase, lowercase, and numbers.');

    if ($action == 'update')

        $passnote = tooltip('Leave blank to keep current password.<br />

													Recommended: 8-10 characters using uppercase, lowercase, and numbers.');



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="verified">User</label></td>  ' . "\n" .

        '<td>' . "\n" .

        '<select name="user_id" id="user_id">' .

        '<option value="">--SELECT ONE--</option>';

    $stmt1 = $db->prepare("SELECT * FROM ".db_prefix."users WHERE active = 1 ORDER BY fname");

    $stmt1->execute();



    while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

        echo '<option value ="'.$row1['id'].'"';

        if($row1['id'] == $row['user_id'])

            echo " selected";

        echo '>'.$row1['fname'].' '.$row1['lname'].'</option>';

    }

    echo    '</select>' .

        tooltip('Select which user the ad is connected to') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="verified">Reseller</label></td>  ' . "\n" .

        '<td>' . "\n" .

        '<select name="reseller_id" id="reseller_id">' .

        '<option value="">--SELECT ONE--</option>';

    $stmt1 = $db->prepare("SELECT * FROM ".db_prefix."resellers WHERE active = 1 ORDER BY fname");

    $stmt1->execute();



    while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

        echo '<option value ="'.$row1['id'].'"';

        if($row1['id'] == $row['reseller_id'])

            echo " selected";

        echo '>'.$row1['fname'].' '.$row1['lname'].'</option>';

    }

    echo    '</select>' .

        tooltip('Select which reseller the ad is connected to') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="verified">Ad Type</label></td>  ' . "\n" .

        '<td>' . "\n" .

        '<select name="ad_type_id" id="ad_type_id">' .

        '<option value="">--SELECT ONE--</option>';

    $stmt1 = $db->prepare("SELECT * FROM ".db_prefix."ad_types ORDER BY size");

    $stmt1->execute();



    while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

        echo '<option value ="'.$row1['id'].'"';

        if($row1['id'] == $row['ad_type_id'])

            echo " selected";

        echo '>'.$row1['size'].'</option>';

    }

    echo    '</select>' .

        tooltip('Select which type the ad is') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="verified">Subcategory</label></td>  ' . "\n" .

        '<td>' . "\n" .

        '<select name="subcategory_id" id="subcategory_id">';

    echo '<option value="">--SELECT ONE--</option>';

    $stmt1 = $db->prepare("SELECT * FROM ".db_prefix."categories

                                WHERE active = 1

                                ORDER BY name");

    $stmt1->execute();

    while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

        echo "<optgroup label='".$row1['name']."'>";

        $stmt2 = $db->prepare("SELECT * FROM ".db_prefix."subcategories

                                WHERE active = 1 AND category_id = ?

                                ORDER BY name");

        $stmt2->execute(array($row1['id']));

        while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)){

            echo '<option value ="'.$row2['id'].'"';

            if($row2['id'] == $row['subcategory_id'])

                echo " selected";

            echo '>'.$row2['name'].'</option>';

        }

        echo "</optgroup>";

    }



    echo    '</select>' .

        tooltip('Select which subcategory the ad is connected to') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td><label for="verified">Cities</label></td>  ' . "\n" .

        '<td>' . "\n" .

        '<select name="city_id" id="city_id">';

    echo '<option value="">--SELECT ONE--</option>';

    $stmt1 = $db->prepare("SELECT ".db_prefix."states.name AS state, ".db_prefix."cities.* FROM ".db_prefix."cities

                                        JOIN ".db_prefix."states

                                            ON ".db_prefix."cities.state_id = ".db_prefix."states.id

                                WHERE ".db_prefix."cities.active = 1 AND ".db_prefix."states.active = 1

                                ORDER BY ".db_prefix."cities.name, ".db_prefix."states.name");

    $stmt1->execute();

    while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

        echo '<option value ="'.$row1['id'].'"';

        if($row1['id'] == $row['city_id'])

            echo " selected";

        echo '>'.$row1['name'].', '.$row1['state'].'</option>';

    }

    echo    '</select>' .

        tooltip('Select which city the ad is connected to') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td><label for="verified">State</label></td>  ' . "\n" .

        '<td>' . "\n" .

        '<select name="state_id" id="state_id">';

    echo '<option value="">--SELECT ONE--</option>';

    $stmt1 = $db->prepare("SELECT * FROM ".db_prefix."states

                                WHERE active = 1

                                ORDER BY name");

    $stmt1->execute();

    while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

        echo '<option value ="'.$row1['id'].'"';

        if($row1['id'] == $row['state_id'])

            echo " selected";

        echo '>'.$row1['name'].'</option>';

    }

    echo    '</select>' .

        tooltip('Select which state the ad is connected to') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="running">Running</label></td>  ' . "\n" .

        '<td>' . "\n" .

        create_slist ( $list, 'running', $row['running'], 1, $t_index ) .

        tooltip('Set to Yes to Allow Ad to Display') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="home">Display on Homepage</label></td>  ' . "\n" .

        '<td>' . "\n" .

        create_slist ( $list, 'home', $row['home'], 1, $t_index ) .

        tooltip('Set to Yes to Allow Ad to Display on Homepage') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="active">Active</label></td>  ' . "\n" .

        '<td>' . "\n" .

        create_slist ( $list, 'active', $row['active'], 1, $t_index ) .

        tooltip('Set to Yes to Allow Login') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' Impressions Left</td>  ' . "\n" .

        '<td>' . "\n" . intval($row['impressions']) . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    /****************************

     * Image Uploading Section

     ***************************/



    if ( $row['image'] != '' )

    {



        $filename      = $upload_dir . $row['image'];

        list ( $width, $height ) = img_size ( $filename );



        echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";

        echo '<tr>' . "\n";

        echo '<td ><label for="current_file">Current Image</label></td>' . "\n";

        echo '<td ><a href="' . $upload_url . $row['image'] . '" class="lightbox" >' . $row['image'] . '</a></td>' . "\n";

        echo '<input type="hidden" name="current_file" value="' . $row['image'] . '" />' . "\n";

        echo '</tr>' . "\n";

    }



    echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";

    echo '<tr>' . "\n";

    echo '<td ><label for="image">Upload Image</label></td>' ."\n";

    echo '<td ><input name="image[]" type="file" id="image" size="15">' . "\n";

    echo '<span class="note">( Image will be auto-magicly resized to '.$width.'w x '.$height.'h pixels `Notice height is proportioned to the width` )</span></td>' . "\n";

    echo '</tr>' . "\n";



    $t_index++;

    echo '<tr><td colspan="2" style="text-align:center; margin:0 auto;" ><input type="submit" name="submit" 

			value="Submit" /></td></tr>' . "\n";

    echo '</table></form>' . "\n";

    echo '<script type="text/javascript">document.getElementById(\'title\').focus();</script>' . "\n";



}







/***************************************************************

 *

 * function sanitize_vars

 * @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

    global $id, $known_photo_types, $upload_dir, $width, $height;



    $r_data['ad_type_id']      =  intval ( $data['ad_type_id'] );

    $r_data['user_id']          = intval ( $data['user_id'] );

    $r_data['reseller_id']      = intval ( $data['reseller_id'] );

    $r_data['state_id']         = intval ( $data['state_id'] );

    $r_data['city_id']          = intval ( $data['city_id'] );

    $r_data['subcategory_id']   = intval ( $data['subcategory_id'] );

    $r_data['home']             = intval ( $data['home'] );

    $r_data['running']          = intval ( $data['running'] );

    $r_data['active']           = intval ( $data['active'] );



    $r_data['image']  = $data['current_file'];

    $tid = ( intval ( $data['id'] ) == 0 ) ? fetch_lastid ('ads') : $data['id'];



    if ( !empty ( $_FILES['image']['name'][0] ) )

    {



        $old_file        = $r_data['image'];

        $image           = $_FILES['image'];





        $r_data['image'] = process_upload ( $tid , '', $image, $known_photo_types, $upload_dir, 1, $width, $height, 0 );



        if ( ( $old_file != $r_data['image'] ) && ( file_exists ( $upload_dir . $old_file ) ) && ( $old_file != '' ))

        {

            unlink ( $upload_dir . $old_file );

        }



    }



    return $r_data;

}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 *

 **************************************************************/



function remove()

{

    global $db, $identifier, $module_name, $id, $module_name, $userinfo, $config, $dbtbl, $upload_dir;



    $stmt = $db->prepare("SELECT * FROM " . $dbtbl . " WHERE id = ?");

    $stmt->execute(array($id));

    $row    = $stmt->fetch(PDO::FETCH_ASSOC);



    print_header('Delete '.$module_name.'  - ' . $row['title']);



    if ( !empty($_POST ))

    {

        //delete old image if there

        if ( file_exists ( $upload_dir . $row['image'] ))

        {

            @unlink( $upload_dir . $row['image'] );

        }



        $errno = 0;

        try{

            //Delete ad

            $stmt = $db->prepare("DELETE FROM " . $dbtbl . " WHERE id = ?");

            $stmt->execute(array($id));

        }

        catch(PDOException $ex){

            $errno = $ex->getCode();

        }







        print_mysql_message ( $errno , $module_name, $id, '2' ) ;

    } else {



        echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">' . "\n" .

            '<input type="hidden" name="id" value="' . $id . '">' . "\n" .

            '<table width="100%" border="0">' . "\n" .

            '<tr> <td><div align="center">Are you sure you want to delete this listing?</div></td></tr>' . "\n" .

            '<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;

                <input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div></td>

            </tr>' . "\n" .

            '</table>

        </form>';

    }

}





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute function show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

    global $db, $identifier, $module_name, $action, $id, $module_name, $dbtbl;



    if ( $action == 'update' )

        print_header('Update '.$module_name);

    else

        print_header('Add New '.$module_name);



    if ( array_key_exists ('submit',$_POST))

    {

        require_once("classes/validation.php");



        $rules = array();



        $rules[] = "required,user_id,user_id";

        $rules[] = "required,reseller_id,reseller_id";

        $rules[] = "required,ad_type_id,ad_type_id";

        $rules[] = "required,subcategory_id,subcategory_id";



        $errors = validateFields($_POST, $rules);



    }



    if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

    {

        $data  = sanitize_vars( $_POST );

        $vars = array();

        $xsql = "";



        if ( $action == 'update' )

        {

            $sql = 'UPDATE ' . $dbtbl . ' SET

                image = ?,

                ad_type_id = ?,

                user_id = ?,

                reseller_id = ?,

                state_id = ?,

                city_id = ?,

                subcategory_id = ?,

                home = ?,

                running = ?,

                active = ?

                ';

            array_push($vars, $data['image']);

            array_push($vars, $data['ad_type_id']);

            array_push($vars, $data['user_id']);

            array_push($vars, $data['reseller_id']);

            array_push($vars, $data['state_id']);

            array_push($vars, $data['city_id']);

            array_push($vars, $data['subcategory_id']);

            array_push($vars, $data['home']);

            array_push($vars, $data['running']);

            array_push($vars, $data['active']);



            $sql .= " WHERE id = ?";

            array_push($vars, $id);

            $type = 0;

        } else {

            $data = sanitize_vars( $_POST );



            $sql = 'INSERT INTO ' . $dbtbl . ' (image, ad_type_id, user_id, reseller_id, state_id, city_id, subcategory_id, home, running, active)

							VALUES ' . "(

							?,

							?,

                            ?,

							?,

							?,

							?,

							?,

							?,

							?,

							?

							)";

            array_push($vars, $data['image']);

            array_push($vars, $data['ad_type_id']);

            array_push($vars, $data['user_id']);

            array_push($vars, $data['reseller_id']);

            array_push($vars, $data['state_id']);

            array_push($vars, $data['city_id']);

            array_push($vars, $data['subcategory_id']);

            array_push($vars, $data['home']);

            array_push($vars, $data['running']);

            array_push($vars, $data['active']);





            $type = 1;

        }



        $errno = 0;

        try{

            $stmt = $db->prepare($sql);

            $stmt->execute($vars);

        }

        catch(PDOException $ex){

            $errno = $ex->getCode();

        }





        if ( ( $errno != 0 ) && ( is_saint() ) )

            print_debug($sql);



        print_mysql_message ( $errno , $module_name, $id, $type ) ;



    } else {

        add( $errors );

    }//end if	

}//end function

