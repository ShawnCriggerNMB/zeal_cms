<?php

//****************************************************/

// Module: File Manager

// Written By : Shawn Crigger of EWD

// Written On : May 2, 2010

// Copyright Zeal Technologies

//

//   _____      __      __ _       _               

//  / ____|     \ \    / /(_)     (_)              

// | (___  ______\ \  / /  _  ____ _   ___   _ __  

//  \___ \|______|\ \/ /  | ||_  /| | / _ \ | '_ \ 

//  ____) |        \  /   | | / / | || (_) || | | |

// |_____/          \/    |_|/___||_| \___/ |_| |_|

//

//***************************************************/





/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/

$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);



function execute()

{

	switch($_GET['action'])

 {

		case 'view':

			view();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Offical Pages

 *

 *

 **************************************************************/



function manage()
{
 

 $i    = 0;

	

	print_header('Current Offical Pages');

	

 echo '

	<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">

	 <thead><tr align="left" valign="top" bgcolor="#ebebeb"> 

			<th><h3>Label</h3></th>

			<th style="width:150px;"><h3>Date Updated</h3></th>

			<th style="width:100px;" class="nosort"><h3>Tools</h3></th>

		 </tr></thead><tbody>' . "\n";



		$results = mysql_query("SELECT * FROM files WHERE active='1' ORDER BY date DESC, label");

		$pages   = mysql_num_rows($results);

		while ($row = mysql_fetch_array($results))

		{    

    									

     echo '<tr align="left" valign="top" '.$bgcolor.'>' . "\n";

    	echo '	<td>' . $row['label'] . '</td>' . "\n";

					echo '	<td style="width:150px;">' . date("m/d/Y, g:i a", $row['date'] ) . '</td>' . "\n";

    	echo '	<td style="width:50px; text-align:center;" valign="middle"><strong><a href="./?tool=files&action=view&id='.$row['id'].'">View</a></strong></td>';

  			echo '</tr>' . "\n";

     

		}//end while

  echo '</tbody></table>';



		if ( $pages > 11 )

			$pages = true;

		else

			$pages = false;

		

		echo_js_sorter ( $pages );

           

}//end function





/***************************************************************

 *

 * function view

 * Querrys DB and Displays Offical Page by ID

 *

 *

 **************************************************************/

function view()

{

		global $id;

		

		$sql = mysql_query ("SELECT * FROM files WHERE id='$id'");

		$row = mysql_fetch_assoc($sql);

		$content = $row['content'];

		echo $content;

		

}







?>

