<?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Communities Module

// About			: The Communities Module is intended for use with

//							companies that deal with communities. For example.

//							A construction company that builds and sales homes 

//							in many different communities.



// Written By : Darrell Bessent of EWD

// Written On : May 3, 2013

// Revision   : 163

// Updated by : Darrell Bessent of EWD

// Updated On : May. 3, 2013



// Copyright Zeal Technologies

//***************************************************/



/***************************************************************

 *

 * Check for and assign our id and action varibles

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);





$mod_config  = get_module_settings ( 'communities' );



$upload_logo_url  = $site_base_url . $cms_uploads . 'communities/logos/';

$upload_logo_dir  = $site_base_dir . $cms_uploads . 'communities/logos/';



$upload_sitemap_url  = $site_base_url . $cms_uploads . 'communities/sitemap/';

$upload_sitemap_dir  = $site_base_dir . $cms_uploads . 'communities/sitemap/';



$logo_width  = '';

$logo_height = '100';

$logo_tn_height = '180';

$logo_tn_width = '150';



$sitemap_width  = '680';

$sitemap_height = '';

$sitemap_tn_height = '180';

$sitemap_tn_width = '150';   



function execute()

{

	switch($_GET['action'])

	{

		case 'update':

			update();

			break;

		case 'add':

			update();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Community Content

 *

 *

 **************************************************************/



function manage()

{



  global $upload_logo_dir,$upload_logo_url, $upload_sitemap_dir,$upload_sitemap_url;

  

  $i    = 0;

  $link = '<a href="./?tool=communities&action=add">Add A Community</a>';

  

  print_header('Manage Communities',$link);

  

  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 

       ' 	  <thead>' . "\n" . 

       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" . 

       ' 			  <th><h3>Community</h3></th>' . "\n" . 

       ' 			  <th><h3>Description</h3></th>' . "\n" . 

			 ' 			  <th><h3>City</h3></th>' . "\n" . 

			 ' 			  <th><h3>State</h3></th>' . "\n" .

       //' 			  <th style="width:50px; text-align:center;"><h3>Map ID</h3></th>' . "\n" . 

       ' 			  <th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . 

       ' 			  <th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" . 

       '		 </tr></thead><tbody>' . "\n";

  

  $results = mysql_query('SELECT * FROM ' . db_prefix . 'communities ORDER BY label');

  $pages   = mysql_num_rows($results);

  while ($row = mysql_fetch_array($results))

  {    



    $i++;

    

   // $link  = 'No Image Uploaded';

    $label = stripslashes ( $row['label'] );

		$descr = stripslashes ( substr($row['descr'], 0, 96) ); $descr .= '...';

		$city  = stripslashes ( $row['city'] );

		$state = stripslashes ( $row['state'] );

	 //	$alttag = stripslashes ( $row['alttag'] );

		

   // $filename = $upload_dir . $row['image'];

    

   // $link = ( file_exists ( $filename ) ) ? '<a href="' . $upload_url . $row['image'] . '" alt="' . $alttag . '" title="' . $label . '" class="lightbox" >' . $row['image'] . '</a>' : $link;

                

    $act = yes_no ( $row['active'] );

    

    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

    echo '<td style="padding-left:20px;">' . $label . '</td>' . "\n";

    echo '<td style="padding-left:20px;">' . $descr . '</td>' . "\n";

		echo '<td style="padding-left:20px;">' . $city . '</td>' . "\n";

		echo '<td style="padding-left:20px;">' . $state . '</td>' . "\n";

   // echo '<td style="width:50px; text-align:center;">' . $row['weight'] . '</td>' . "\n";

    echo '<td text-align:center;">' . $act . '</td>' . "\n";

    echo '<td valign="middle"><strong><a href="./?tool=communities&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=communities&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

    echo '</tr>' . "\n";

	  

  }//end while



  echo '</tbody></table>';



  $pages = ( $pages > 20 ) ? true : false;

	

  echo_js_sorter ( $pages );

             

}//end function



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Banner Form for User Input

 *

 *

 **************************************************************/



function add( $errors = '' )

{

  global $id, $action, $site_base_url, $upload_logo_url, $upload_logo_dir, $upload_sitemap_url, $upload_sitemap_dir, $mod_config, $logo_width, $logo_height, $sitemap_width, $sitemap_height;

   

   

  if ( $errors )

  {

   echo '<div class="error_message">';

   if ( in_array('label', $errors ) )

     echo '* You must fill out a label name.<br/>';

   echo '</br></div>' . "\n";

  } else {

    $c = ( $action == 'update' ) ? 1 : 0;

    print_notice_message ( $c );

  }

  

  if ($action == "update")

  {

   // Run the query for the existing data to be displayed.

   $results = mysql_query('SELECT * FROM ' . db_prefix . 'communities WHERE id = "' . $id . '"');

   $row 				= mysql_fetch_array($results);

  } 

	if (!empty ($_POST))

	{	

   $row     = sanitize_vars ($_POST);

  }//end if

  

	echo '<div class="notice_message">NOTES : <br /> 

					<ul>

						<li>The `COMMUNITY` is the title of the community you are adding.</li>

						<li>The `DESCRIPTION` is the description you would like to appear in the information box for the related community.</li>

						<li>The `INFORMATION` is a the price range short text that appears in the information box. Example `Homes from $159,900`</li>

						<li>The `COORDINATES FROM TOP` is the coordinates from the top of the community map where this community should appear.</li>

						<li>The `COORDINATES FROM LEFT` is the coordinates from the left of the community map where this community should appear.</li>

						<li>The `ORDER` controls the display order on the communities page.</li>

						<li>The `ACTIVE` is to set the community post visability to on or off.</li>

						<li>The `LOGO` must be 100 pixels in height. The width will be adjusted auto-magicly.</li>

						<li>The `SITEMAP` must be 680 pixels in width. The height will be adjusted auto-magicly.</li>

					<ul>	

				</div>';

	

  echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

  $t_index = 1;

  echo '   <tr>' . "\n".

       '      <td > <label for="label">Community</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="label" id="label" value="'. $row['label'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";



  $t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="descr">Description</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="descr" id="descr" value="'. $row['descr'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="availlots">Available Lots</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="availlots" id="availlots" value="'. $row['availlots'] .'" size="45" tabindex="' . $t_index . '" maxlength="5" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="city">City</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="city" id="city" value="'. $row['city'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="state">State</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="state" id="state" value="'. $row['state'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

			 

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="info">Information</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="info" id="info" value="'. $row['info'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";		 

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="phone">Phone</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="phone" id="phone" value="'. $row['phone'] .'" size="45" tabindex="' . $t_index . '" maxlength="12" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

			 

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="map">Map</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="map" id="map" value="'. $row['map'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";	

			 

	//$t_index++;

	//echo '   <tr>' . "\n".

  //     '      <td > <label for="under_const">Under Construction</label></td>  ' . "\n" . 

  //     '      <td > <input type="text" name="under_const" id="under_const" value="'. $row['under_const'] .'" size="45" /></td>  ' . "\n" . 

  //     '   </tr>' . "\n";			 	 

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="coords_top">Coordinates From Top</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="coords_top" id="coords_top" value="'. $row['coords_top'] .'" size="45" tabindex="' . $t_index . '" maxlength="4" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="coords_left">Coordinates From Left</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="coords_left" id="coords_left" value="'. $row['coords_left'] .'" size="45" tabindex="' . $t_index . '" maxlength="4" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

	

	$t_index++;

		echo '   <tr>' . "\n" .

							' 						<td ><label for="hasfloorplans">Floor Plans</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="hasfloorplans" name="hasfloorplans">' . "\n" . 

							' 									<option value="1" '; if ( $row['hasfloorplans'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 

							' 									<option value="0" '; if ( $row['hasfloorplans'] == '0' ) echo 'SELECTED'; echo ' >No</option>' . "\n" . 

							' 						</select>' . "\n" . 

       				' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";

	

	$t_index++;

		echo '   <tr>' . "\n" .

							' 						<td ><label for="soldout">Sold Out</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="soldout" name="soldout">' . "\n" . 

							' 									<option value="1" '; if ( $row['soldout'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 

							' 									<option value="0" '; if ( $row['soldout'] == '0' ) echo 'SELECTED'; echo ' >No</option>' . "\n" . 

							' 						</select>' . "\n" . 

       				' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";



 // $t_index++;

 // echo '   <tr>' . "\n".

 //      '      <td > <label for="weight">Map ID</label></td>  ' . "\n" . 

 //      '      <td > <input type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />' . "\n" .

 //      ' 						<span class="note" style="padding-left:5px;"> ( Recommend increments of 10 ) </span>' . "\n" .

 //      '      </td>' . "\n" .

 //      '   </tr>' . "\n";

   

  $t_index++;

		echo '   <tr>' . "\n" .

							' 						<td ><label for="active">Active</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="active" name="active">' . "\n" . 

							' 									<option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 

							' 									<option value="0" '; if ( $row['active'] == '0' ) echo 'SELECTED'; echo ' >No</option>' . "\n" . 

							' 						</select>' . "\n" . 

						 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to Display on Website ) </span>' . "\n" . 

       ' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";

  

  /****************************

  * Image Uploading Section

  ***************************/



  if ( $row['logo'] != '' )

  {

    

   $logo_filename      = $upload_logo_dir . $row['logo'];

   list ( $w, $h ) = img_size ( $logo_filename );

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="logo_file">Current Logo</label></td>' . "\n";

   echo '<td ><a href="' . $upload_logo_url . $row['logo'] . '" alt="' . $row['label'] . '" title="' . $row['label'] . '" target="_blank" class="lightbox" >' . $row['logo'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="logo_file" value="' . $row['logo'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="logo">Upload Logo</label></td>' ."\n";

  echo '<td ><input name="logo" type="file" id="logo" size="15">' . "\n";

  echo '<span class="note">( Image will be auto-magicly resized to '.$logo_width.'w x '.$logo_height.'h pixels )</span></td>' . "\n";

  echo '</tr>' . "\n";





  if ( $row['sitemap'] != '' )

  {

    

   $sitemap_filename      = $upload_sitemap_dir . $row['sitemap'];

   list ( $w, $h ) = img_size ( $sitemap_filename );

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="sitemap_file">Current Sitemap</label></td>' . "\n";

   echo '<td ><a href="' . $upload_sitemap_url . $row['sitemap'] . '" alt="' . $row['label'] . '" title="' . $row['label'] . '" target="_blank" class="lightbox" >' . $row['sitemap'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="sitemap_file" value="' . $row['sitemap'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="sitemap">Upload Sitemap</label></td>' ."\n";

  echo '<td ><input name="sitemap" type="file" id="sitemap" size="15">' . "\n";

  echo '<span class="note">( Image will be auto-magicly resized to '.$sitemap_width.'w x '.$sitemap_height.'h pixels )</span></td>' . "\n";

  echo '</tr>' . "\n"; 





	// This is our submit button

  $t_index++;

  echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";  

  echo '</table></form>' . "\n";

  echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>' . "\n";



}





/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

  global $upload_logo_dir, $upload_sitemap_dir, $mod_config, $known_photo_types, $logo_width, $logo_height, $sitemap_width, $sitemap_height;

  

  

  $r_data['label']  = mysql_real_escape_string ( $data['label'] );

	$r_data['descr']  = mysql_real_escape_string ( $data['descr'] );

	$r_data['availlots']  = mysql_real_escape_string ( $data['availlots'] );

	$r_data['city']  = mysql_real_escape_string ( $data['city'] );

	$r_data['state']  = mysql_real_escape_string ( $data['state'] );

	$r_data['info']  = mysql_real_escape_string ( $data['info'] );

	$r_data['phone']  = mysql_real_escape_string ( $data['phone'] );

	$r_data['map']  = htmlspecialchars($data['map'], ENT_QUOTES); 

	$r_data['under_const']  = mysql_real_escape_string ( $data['under_const'] );

	$r_data['coords_top']  = intval ( $data['coords_top'] );

	$r_data['coords_left']  = intval ( $data['coords_left'] );

	$r_data['hasfloorplans']  = intval ( $data['hasfloorplans'] );

	$r_data['soldout']  = intval ( $data['soldout'] );

 // $r_data['weight']  = intval ( $data['weight'] );

  $r_data['active'] = intval ( $data['active'] );

  $r_data['logo']  = $data['logo_file'];

	$r_data['sitemap']  = $data['sitemap_file'];

		

  $tid = ( intval ( $data['id'] ) == 0 ) ? fetch_lastid ('communities') : $data['id'];

   

	// Logo image upload handler    		 

  if ( !empty ( $_FILES['logo']['name'] ) )

  {

            

    $old_logo_file        = $r_data['logo'];

    $logo          			  = $_FILES['logo'];

    

		

		$r_data['logo'] = process_upload ( $tid , '', $logo, $known_photo_types, $upload_logo_dir, 1, $logo_width, $logo_height );

						

    if ( ( $old_logo_file != $r_data['logo'] ) && ( file_exists ( $upload_logo_dir . $old_logo_file ) ) && ( $old_logo_file != '' ))

		{

			unlink ( $upload_logo_dir . $old_logo_file );

		}

  

	} // End Logo image upload handler

	

	// Sitemap image upload handler

	if ( !empty ( $_FILES['sitemap']['name'] ) )

  {

            

    $old_sitemap_file        = $r_data['sitemap'];

    $sitemap         			   = $_FILES['sitemap'];

    

		

		$r_data['sitemap'] = process_upload ( $tid , '', $sitemap, $known_photo_types, $upload_sitemap_dir, 1, $sitemap_width, $sitemap_height );

						

    if ( ( $old_sitemap_file != $r_data['sitemap'] ) && ( file_exists ( $upload_sitemap_dir . $old_sitemap_file ) ) && ( $old_sitemap_file != '' ))

		{

			unlink ( $upload_sitemap_dir . $old_sitemap_file );

		}

  

	} // End Sitemap image upload handler

	

			 

  if ( empty($_POST['weight']) )

  {

 //    $r_data['active'] = 1;

     $r_data['weight']  = fetch_weight('communities', 'weight' );

  }

  

  return $r_data;



}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 * Unlinks Existing Photo

 *

 **************************************************************/



function remove()

{



	global $id, $upload_logo_dir, $upload_sitemap_dir, $module_name;

	

	$result = mysql_query("SELECT * FROM " . db_prefix . "communities WHERE id = '$id'");

	$row    = mysql_fetch_array($result);

	

	print_header('Delete Community - ' . $row['label']);

	

	if ( !empty($_POST ))

	{		

					

		if ( file_exists ( $upload_logo_dir . $row['logo'] ))

		{

				@unlink( $upload_logo_dir . $row['logo'] );

		}

		

		if ( file_exists ( $upload_sitemap_dir . $row['sitemap'] ))

		{

				@unlink( $upload_sitemap_dir . $row['sitemap'] );

		}

		

		

		$result = mysql_query('DELETE FROM ' . db_prefix . "communities WHERE id = '$id'");

		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

			

	} else {

	

		echo '<form action="./?tool=communities&action=remove" method="post" name="form">' . "\n" .

							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

							'<tr> <td><div align="center">Are you sure you want to delete this community?</div></td></tr>' . "\n" .

							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=communities\'"></div></td></tr>' . "\n" .

							'</table></form>';

	}//end if

}//end function





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

		global $action, $id, $module_name;

		

		if ( $action == 'update' )

				print_header('Update Community');

		else

				print_header('Add New Community');

 

		if ( array_key_exists ('submit',$_POST))

		{				

    require_once("classes/validation.php");

  

    $rules = array();    

    

    $rules[] = "required,label,label";

    

    $errors = validateFields($_POST, $rules);

    

		}

		

		if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

	 {

		

				$data  = sanitize_vars( $_POST );			

		 	  $type  = ( $action == 'update' ) ? 0 : 1;

				

				if ( $action == 'update' )

						$sql = "UPDATE " . db_prefix . "communities SET label = '" . $data['label'] . "', active = '" . $data['active'] . "', logo = '" . $data['logo'] . "', sitemap = '" . $data['sitemap'] . "',

						  descr = '" . $data['descr'] . "', availlots = '" . $data['availlots'] . "', city = '" . $data['city'] . "',

							 state = '" . $data['state'] . "', info = '" . $data['info'] . "', phone = '" . $data['phone'] . "',

							  map = '" . $data['map'] . "', under_const = '" . $data['under_const'] . "', coords_top = '" . $data['coords_top'] . "',

								 coords_left = '" . $data['coords_left'] . "', hasfloorplans = '" . $data['hasfloorplans'] . "',

								  soldout = '" . $data['soldout'] . "'	WHERE id = '$id'";

 		 else

					 $sql = 'INSERT INTO ' . db_prefix . 'communities (label, active, logo, sitemap, descr, availlots, city, state, info, phone, map, under_const, coords_top, coords_left, hasfloorplans, soldout) 

					 VALUES ' . "('" . $data['label'] ."', '" . $data['active'] . "', '" . $data['logo'] . "', '" . $data['sitemap'] . "', '" . $data['decr'] . "', '" . $data['availlots'] .

					  "', '" . $data['city'] . "', '" . $data['state'] . "', '" . $data['info'] . "', '" . $data['phone'] . "', '" . $data['map'] .

						 "', '" . $data['under_const'] . "', '" . $data['coords_top'] . "', '" . $data['coords_left'] . "', '" . $data['hasfloorplans'] .

						  "', '" . $data['soldout'] . "')";

											

   //echo $sql;

    

				mysql_query ( $sql );

    

    if ( is_saint() && mysql_errno != 0 )

      echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

   			

				print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;   

		

  } else {

		add( $errors );

	}//end if	

}//end function







?>