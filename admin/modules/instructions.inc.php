<?php	
if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module: Home Page for Admin Section
// Written By : Shawn Crigger of EWD
// Written On : May 2, 2010
// Updated By : Chuck Bunnell of EWD
// Updated On : May 22, 2014
// Copyright Zeal Technologies
//***************************************************/


function execute()
{
	echo fetch_output();
}

function fetch_output()
{
	global $db, $userinfo, $sitename;
	
	$html  = '<div class="instruction"> ';
		$html .= 'Welcome to <strong>' . $sitename . '</strong>. You are currently logged in as <strong>' .$userinfo['name'] . '</strong>.<br/>
			Please use the left hand navigation menu to maintain the website.<br/>
			You can come back to this page by clicking the Instructions link in the navigation menu.';
	$html .= '</div>';
	
	$lvl  = $userinfo['level'];
	
	$stmt = $db->prepare("SELECT name, description FROM " . db_prefix . "modules WHERE active = ? AND installed = ? AND weight < ? AND level <= ? ORDER BY weight,identifier");
	
	$stmt->execute(array('1', '1', '999', $lvl));
	
	while ( $row = $stmt->fetch(PDO::FETCH_ASSOC) )
	{
		$html .= '<h2>' . $row['name'] . '</h2>' .
			'<div class="instruction"> ' . $row['description'] . '</div>';												
	}
	
	$html .= '<div class="spacer">&nbsp;</div>';
	
	return $html;
}
?>