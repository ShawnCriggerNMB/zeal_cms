<?php	
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module: Zeal Content Management System Updater
// Written By : Darrell Bessent of Zeal Technologies
// Written On : Oct 31, 2013
// Updated On : Oct 31, 2013
// Copyright Zeal Technologies
//***************************************************/


$id          = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);
$action      = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);
$module_name = get_module_name();



function execute()
{

	switch($_GET['action'])
	{
		case 'update':
			update();
			break;
		default:
			manage();
	}//end switch
	
}//end function
	
/***************************************************************
 *
 * function manage
 * Querrys DB and Displays Slider Content
 *
 *
 **************************************************************/

function manage()
{
	echo '<div>Welcome to the Zeal Technologies Content Management System update interface.<br />
	Remeber, you should only perform an automatic update if you have not made customization changes to any of the deafault installation core files.<br />
	To begin click the Update button bellow. </div><br />';
	main();
}	

// *****************************************************************************************

// main() function

function main() {
	
	// This is the welcoming banner
	//echo '<img src="/admin/modules/phpav/logo.png" /><br /><br />';
	echo '<form method="post" action="index.php?tool=update"><input style="width: 400px;" name="cmd" type="submit" value="Update" /></form><br />';


	// Now initialize our scan
	if ($_POST['cmd'] == 'Update') { init_update(); }
}
// *****************************************************************************************

// init_scan() function
function init_update()
{
	
	echo '<div style="width: 900px;">';
	update_zcms();
	echo '</div>';
}
// *****************************************************************************************
?>