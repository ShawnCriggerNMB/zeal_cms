<?php
if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module: Client Management System
// Written By : Cory Shaw of EWD
// Written On : May 03, 2014
// Updated By : Chuck Bunnell of EWD
// Updated On : July 19, 2014
// Copyright Zeal Technologies
//***************************************************/

// if update make sure this id exists
update_verify();
$sitedir = $config['site_base_dir'];

require( $sitedir . '/lib/cms-load.php');
global $wpdb;
$GLOBALS['cms_users_object'] = new CMS_Users( $wpdb );
function execute()
{
	switch($_GET['action'])
	{
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		case 'archive':
			archive();
			break;
		default:
			manage();
	}
}

// ------------------------------------------------------------------------

function get_mods( $og_mod_id = 0 )
{
	global $db, $dbtbl;
	$user_meta_table = db_prefix . 'usermeta';
	$caps = array( 'moderator', 'administrator' );
	$q = $db->prepare("SELECT u.id,
							first_name.meta_value as first_name,
							last_name.meta_value as last_name,
							cms_capabilities.meta_value as cms_capabilities
							FROM {$dbtbl} AS u
			 			LEFT JOIN {$user_meta_table} AS first_name ON first_name.user_id=u.ID
			 				AND first_name.meta_key='first_name'
			 			LEFT JOIN {$user_meta_table} AS last_name ON last_name.user_id=u.ID
			 				AND last_name.meta_key='last_name'
			 			LEFT JOIN {$user_meta_table} AS cms_capabilities ON cms_capabilities.user_id=u.ID
			 				AND cms_capabilities.meta_key='cms_capabilities'
			 			WHERE user_status != 3 ORDER BY first_name");
	$q->execute();
	$q = $q->fetchAll(PDO::FETCH_OBJ);
	foreach ($q as $user) {
		$user->cms_capabilities = unserialize( $user->cms_capabilities );
		$is_mod = FALSE;
		foreach ($user->cms_capabilities as $cap => $value) {

			$is_mod = ( in_array( $cap, $caps )	) ? TRUE : FALSE;
		}
		if ( FALSE === $is_mod ) continue;

		$sel     = ( $og_mod_id === $user->ID ) ? ' selected ' : NULL;
		$output .= "\t<option value=\"{$user->ID}\" {$sel}>{$user->first_name} {$user->last_name}</option>\n";
	}

	return $output;
}

/***************************************************************
 *
 * function manage
 * Querys DB and Displays Content
 *
 **************************************************************/

function manage()
{
	global $db, $identifier, $module_name, $userinfo, $dbtbl;

	$link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';

	print_header('Manage '.$module_name,$link);

	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sortable" id="table">
		<thead>
			<tr>
				<th><h3>Name</h3></th>
				<th><h3>Email</h3></th>
				<th style="width:80px;"><h3>Moderator</h3></th>
				<th style="width:70px;"><h3>Tier</h3></th>
				<th style="width:80px;"><h3>Verified</h3></th>
				<th style="width:70px;"><h3>Active</h3></th>
				<th class="nosort"><h3>Tools</h3></th>
			</tr>
		</thead>
		<tbody>';


	$user_meta_table = db_prefix . 'usermeta';
	$cap = db_prefix . 'capabilities';

	$sql = "SELECT u.*,
				fname.meta_value AS fname,
				lname.meta_value  AS lname,
				tier.meta_value AS tier,
				verified.meta_value AS verified,
				capabilities.meta_value AS capabilities
			FROM {$dbtbl} AS u
			 	LEFT JOIN {$user_meta_table} AS fname ON fname.user_id=u.ID
			 		AND fname.meta_key='first_name'
			 	LEFT JOIN {$user_meta_table} AS lname ON lname.user_id=u.ID
			 		AND lname.meta_key='last_name'
			 	LEFT JOIN {$user_meta_table} AS tier ON tier.user_id=u.ID
			 		AND tier.meta_key='tier'
			 	LEFT JOIN {$user_meta_table} AS verified ON verified.user_id=u.ID
			 		AND verified.meta_key='verified'
			 	LEFT JOIN {$user_meta_table} AS capabilities ON capabilities.user_id=u.ID
			 		AND capabilities.meta_key='{$cap}'

			WHERE u.user_status != '3'
				ORDER BY fname";

    $stmt = $db->prepare( $sql );
    $stmt->execute();
    $pages   = $stmt->rowCount();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
    {
    		$is_mod = ( strpos( $row['capabilities'], 'moderator' ) > 0 ) ? 'Yes' : 'No';
			//$ismod = yes_no($row['is_mod']);
			$verified = yes_no($row['verified']);
			$act = yes_no($row['user_status']);

			echo '<tr align="left" valign="middle">
				<td>' . $row['fname'] . '&nbsp;' . $row['lname'] . '</td>
				<td>' . $row['user_email'] . '</td>
				<td>' . $is_mod . '</td>
				<td>' . $row['tier'] . '</td>
				<td>' . $verified . '</td>
				<td>' . $act . '</td>
				<td style="text-align:center;"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row['ID'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&action=archive&id='.$row['ID'].'">Archive</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&action=remove&id='.$row['ID'].'">Remove</a></td>
			</tr>';
    }

    echo '</tbody>
	</table>';

  $pages = ( $pages > 20 ) ? true : false;

	echo_js_sorter ( $pages );

	echo '<div class="spacer">&nbsp;</div>';
}//end function


/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 *
 **************************************************************/

function add( $errors = '' )
{
	global $db, $identifier, $module_name, $id, $action, $dbtbl;

	if ( $errors )
	{
		echo '<ul class="error_message">
			<strong>Please fill in the required fields.</strong>';
			// set error messages for required fields
			if ( in_array('first_name', $errors ) )
			{
				echo '<li>A First Name is required.</li>';
				$val_fname = ' class="form_field_error" ';
			}

			if ( in_array('last_name', $errors ) )
			{
				echo '<li>A Last Name is required.</li>';
				$val_lname = ' class="form_field_error" ';
			}

			if ( in_array('user_email', $errors ) )
			{
				echo '<li>A valid Email Address is required for login and verification.</li>';
				$val_email = ' class="form_field_error" ';
			}

			if ( in_array('used_email', $errors ) )
			{
				echo '<li>This email is already in use.</li>';
				$val_email = ' class="form_field_error" ';
			}

			if ( in_array('user_pass', $errors ) )
			{
				echo '<li>A Password is required for login.</li>';
				$val_pass = ' class="form_field_error" ';
			}

			if ( in_array('badpass', $errors ) )
			{
				echo '<li>Password must be 8 or more characters. It must contain at least 1 uppercase, 1 lowercase, 1 number, and 1 allowed character.<br />Allowed Characters: # $ % & + [ ] ?</li>';
				$val_pass = ' class="form_field_error" ';
			}

			if ( in_array('user_login', $errors ) )
			{
				echo '<li>A User Name is required. (4 - 12 characters)</li>';
				$val_user = ' class="form_field_error" ';
			}

			if ( in_array('used_user', $errors ) )
			{
				echo '<li>This UserName already exists in our database. Please select a different one.';
				$val_user = ' class="form_field_error" ';
			}

			if ( in_array('baduser', $errors ) )
			{
				echo '<li>Your User Name must be 4-12 characters and can contain only letters and numbers.';
				$val_user = ' class="form_field_error" ';
			}

			if ( in_array('tier', $errors ) )
			{
				echo '<li>You must select a Tier for this member.';
				$val_tier = ' class="form_field_error" ';
			}

		echo '</ul>';

	} else {

		$c = ( $action == 'update' ) ? 'update this' : 'add a new';
		$u_r = ( $action == 'update' ) ? ' except the Password' : '';

		echo  '<ul class="notice_message">';
			// set instructions here
			echo '<strong>To ' . $c . ' record, fill out the form and click submit.</strong>
			<li>All fields marked with a red * are required.</li>
			<li>Email: Please use a valid email address. This will be used for their login, and it is the only way this user will be able to reset their password if they lose it.</li>';
			if ($action == 'update' )
				echo '<li>Password: Only type in a password if you want to change the present one.</li>';
			echo '<li>Password: Must be 8 or more characters.<br />It must contain at least 1 uppercase, 1 lowercase, 1 number, and 1 allowed character. Allowed Characters: # $ % & + [ ] ?.</li>
			<li>UserName: Must be 4-15 characters long, using letters and numbers only.<br />It will appear on every Post and Comment you make and CANNOT be changed, so choose wisely.</li>
		</ul>';
	}

	if ($action == "update")
	{
		$row = get_user( $id );
		/*
		// Run the query for the existing data to be displayed.
		$stmt = $db->prepare('SELECT * FROM '.$dbtbl.' WHERE id = ?');
		$stmt->execute(array($id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		*/
	}

	if (!empty ($_POST))
	{
		$row = sanitize_vars ($_POST);
	}


/********************************************************
 * Start Building Form
 *******************************************************/

	$r = required();
	// pass only required for "add" not "update"
	$rp = ($action == 'update') ? '' : required();

	echo '<form name="form" id="form" method="post" action="#">
		<table>
    	<input type="hidden" name="id" value="' . $id . '" />';

    	$passnote = tooltip('Password must be 8 or more characters. It must contain at least 1 uppercase, 1 lowercase, 1 number, and 1 allowed character. Allowed Characters: # $ % & + [ ] ?');

		if ($action == 'update')
        $passnote = tooltip('Leave blank to keep current password.<br />
													Password must be 8 or more characters. It must contain at least 1 uppercase, 1 lowercase, 1 number, and 1 allowed character. Allowed Characters: # $ % & + [ ] ?');

    	echo '<tr>
      	<td><label for="first_name">'.$r.'First Name</label></td>
        <td><input ' . $val_fname . ' type="text" name="first_name" id="first_name" value="'. htmlspecialchars($row->first_name) .'" size="45" /></td>
      </tr>';

   		echo '<tr>
      	<td><label for="last_name">'.$r.'Last Name</label></td>
        <td><input ' . $val_lname . ' type="text" name="last_name" id="last_name" value="'. htmlspecialchars($row->last_name) .'" size="45" /></td>
      </tr>';

			echo '<tr>
				<td><label for="user_email">'.$r.'Email</label></td>
        <td><input ' . $val_email . ' type="text" name="user_email" id="user_email" value="'. htmlspecialchars($row->user_email) .'" size="45" /></td>
      </tr>';

			echo '<tr>
        <td><label for="user_pass">'.$rp.'Password</label></td>
        <td><input ' . $val_pass . ' type="text" name="user_pass" id="user_pass" value="'. $data->user_pass .'" size="45" placeholder="************" />' .$passnote. ' </td>
      </tr>';

			if ($action != 'update')
			{
				echo '<tr>
					<td><label for="user_login">'.$r.'UserName</label></td>
					<td><input ' . $val_user . ' type="text" name="user_login" id="user_login" value="'. htmlspecialchars($row->user_login) .'" size="45" maxlength="15" />'.tooltip('4-15 Characters; letters and numbers only. This will be displayed on members posts.').'</td>
				</tr>';
			}

			echo '<tr>
				<td><label for="tier">'.$r.'Tier</label></td>
				<td><select ' . $val_tier . ' name="tier" id="tier">
						<option value="">-- NONE --</option>';

						$stmt2 = $db->prepare("SELECT * FROM ".db_prefix."tiers WHERE active = ? ORDER BY tier ASC");
						$stmt2->execute(array('1'));
						while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC))
						{
							echo '<option value="'.$row2['id'].'"';
							if( $row->tier == $row2['id'] )
								echo ' selected';
							echo '>Tier '.$row2['tier'].'</option>';
						}
					echo '</select>
				</td>
			</tr>';

			echo '<tr>
        <td><label for="verified">'.$r.'Verified</label></td>
        <td>
        '.create_slist ( $list, 'verified', $row->verified, 1) .
        tooltip('If member has paid, set to `Yes`. Tier 1 members should be set to `No`').'
        </td>
     	</tr>';

    	echo '<tr>
        <td><label for="user_status">'.$r.'Active</label></td>
        <td>
        '.create_slist ( $list, 'user_status', $row->user_status, 1) .
        tooltip('Set to Yes to Allow Login').'
        </td>
      </tr>';

			echo '<tr>
				<td colspan="2"></td>
			</tr>';

    	echo '<tr>
        <td><label for="is_mod">'.$r.'Moderator</label></td>
        <td>
        '.create_slist ( $list, 'is_mod', $row->is_mod, 1) .
        tooltip('Set to Yes if this member is a Moderator').'
        </td>
      </tr>';

			/****************************************/
			/* IF MODERATOR							            */
			/****************************************/
			if ($row->is_mod == '1')
			{
				echo '<tr>
					<td><label for="modstartdate">Moderator Start Date</label></td>
					<td>'.date('M d, Y', strtotime($row->mod_start_date)).'</td>
				</tr>';

				echo '<tr>
					<td><label for="modrefid">Moderator Referral Id</label></td>
					<td>'.htmlspecialchars($row->mod_referral_id).'</td>
				</tr>';
			}
			/****************************************/
			/* END IF MODERATOR					            */
			/****************************************/

			/****************************************/
			/* NON-CHANGEABLE INFO                  */
			/****************************************/
			if ($action == 'update')
			{
				echo '<tr>
					<td colspan="2"></td>
				</tr>';

				echo '<tr>
					<td><label for="signupdate">Sign-up Date</label></td>
					<td>'.date('M d, Y', strtotime($row->user_registered)).'</td>
				</tr>';

				echo '<tr>
					<td><label for="user">UserName</label></td>
					<td>'.htmlspecialchars($row->user_login).'</td>
				</tr>';

				$logdate = ( $row->last_login_ts == '' ) ? 'Never Logged' : date("m/d/Y, g:i a", $row->last_login_ts);
				echo '<tr>
					<td><label for="ip">Last Login Date</label></td>
					<td>'.$logdate.'</td>
				</tr>';

				echo '<tr>
					<td><label for="ip">Last Login IP</label></td>
					<td>'.$row->last_login_ip.'</td>
				</tr>';

				if ( 0 === (int) $row->orig_mod_id )
				{
					$modname = 'none';
					$modtip = '';
				} else {
					$row3 = get_user( $row->orig_mod_id );
					/*
					$stmt3 = $db->prepare("SELECT id,fname,lname FROM ".$dbtbl." WHERE id = ?");
					$stmt3->execute(array($row->orig_mod_id));
					$row3 = $stmt3->fetch(PDO::FETCH_ASSOC);
					*/
					$modname = $row3->first_name.' '.$row3->last_name;
					$modname = '<a href="?tool=users&action=update&id='.$row3->ID.'">'.$modname.'</a>';
					$modtip = tooltip('Click name to update moderator.');
				}
				echo '<tr>
					<td><label for="moderator">Referred By (Moderator)</label></td>
					<td>'. $modname . $modtip.'</td>
				</tr>';

				echo '<tr>
					<td colspan="2"></td>
				</tr>';
			}
			/****************************************/
			/* END NON-CHANGEABLE INFO              */
			/****************************************/


			/****************************************/
			/* IF ADD, GET REFERRER INFO            */
			/****************************************/
			if ($action == 'add')
			{
				echo '<tr>
					<td colspan="2"></td>
				</tr>';

				echo '<tr>
					<td><label for="orig_mod_id">Referring Moderator</label></td>
					<td><select ' . $val_referralid . ' name="orig_mod_id" id="orig_mod_id">
							<option value="0">-- NONE --</option>';
						echo get_mods( $row->orig_mod_id );
						echo '</select>
						'.tooltip('If this user was referred by a moderator, select the referring moderator.').'
					</td>
				</tr>';

				echo '<tr>
					<td colspan="2"></td>
				</tr>';
			}
			/****************************************/
			/* END IF ADD, GET REFERRER INFO            */
			/****************************************/

			echo '<tr>
				<td colspan="2" style="padding:3px;"><input type="submit" name="submit" value="Submit" /></td>
			</tr>';

    echo '</table>
	</form>';

	echo '<script type="text/javascript">document.getElementById(\'first_name\').focus();</script>';

}//end function



/***************************************************************
 *
 * function sanitize_vars
 * @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 **************************************************************/


function sanitize_vars( $data )
{
	$r_data['first_name']   = $data['first_name'];
	$r_data['last_name']    = $data['last_name'];
	$r_data['user_email']   = $data['user_email'];
	$r_data['user_pass']    = $data['user_pass'];
	$r_data['user_login']   = $data['user_login'];
	$r_data['orig_mod_id']	= intval ( $data['orig_mod_id'] );
	$r_data['tier']			= intval ( $data['tier'] );
	$r_data['verified']	    = intval ( $data['verified'] );
	$r_data['user_status']  = (int) $data['user_status'];
	$r_data['is_mod']		= intval ( $data['is_mod'] );

	return $r_data;
}

/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $db, $identifier, $module_name, $id, $dbtbl;

	$row = $GLOBALS['cms_users_object']->get_user( $id );
	print_header('Delete '.$module_name.'  - ' . $row->first_name.' '. $row->last_name);

	if ( !empty($_POST ))
	{
		$errno = 0;

		$r = $GLOBALS['cms_users_object']->delete_user( $id );
		print_mysql_message ( $errno , $module_name, $id, '2' ) ;

	} else {

		echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">
    	<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to delete this record?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div>
    </form>';
  }
}


/***************************************************************
 *
 * function archive
 * Sets an 'archive' date for the user and hides it from view
 * Row from Database == $id
 *
 **************************************************************/

function archive()
{
	global $db, $identifier, $module_name, $id, $dbtbl;

	$row = $GLOBALS['cms_users_object']->get_user( $id );
	print_header('Archive '.$module_name.'  - ' . $row->first_name.' '. $row->last_name);

	if ( !empty($_POST ))
	{
		$errno = 0;
		$x_active = '0';
		$arcdate = date('Y-m-d H:i:s');

		// vars stored in the users table.
		$vars = array(
			'user_status' => 3
		);

		$r = $GLOBALS['cms_users_object']->update_user( $id, $vars );

		update_usermeta( $id, 'archived', $arcdate );

		print_mysql_message ( $errno , $module_name, $id, '3' ) ;

	} else {

		echo '<form action="./?tool='.$identifier.'&action=archive" method="post" name="form">
    	<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to archive this record?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div>
    </form>';
  }
}


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of contents
 *
 **************************************************************/

function update()
{
	global $db, $identifier, $module_name, $action, $id, $dbtbl;

	if ( $action == 'update' )
		print_header('Update '.$module_name);
	else
		print_header('Add New '.$module_name);

	if ( array_key_exists ('submit',$_POST))
	{
		require_once("classes/validation.php");

		// set rules for required fields
		$rules = array();
		$rules[] = "required,first_name,first_name";
		$rules[] = "required,last_name,last_name";
		$rules[] = "required,user_email,user_email";
		$rules[] = "valid_email,user_email,user_email";
		$rules[] = "required,tier,tier";

		if ( $action != 'update' )
		{
				$rules[] = "required,user_pass,user_pass";
				$rules[] = "required,user_login,user_login";
		}

		$errors = validateFields($_POST, $rules);

		// password must contain lower case, upper case, number, and min 8 chars
		if (!empty($_POST['user_pass']))
		{
			if (!preg_match( '~[A-Z]~', $_POST['user_pass']) ||
					!preg_match( '~[a-z]~', $_POST['user_pass']) ||
					!preg_match( '~\d~', $_POST['user_pass']) ||
					!preg_match('/[#$%&+\[\]?]/', $_POST['user_pass']) ||
					strlen($_POST['user_pass']) < 8)
			{
				$errors[] = 'badpass';
			}
		}

		if (!empty($_POST['user_login']))
		{
			if (!ctype_alnum($_POST['user_login']) || strlen($_POST['user_login']) < 4 || strlen($_POST['user_login']) > 15)
				$errors[] = 'baduser';
		}

		if ( $action != 'update' )
		{
			if ( $GLOBALS['cms_users_object']->get_user( $_POST['user_login'], array( 'by' => 'login' ) ) )
			{
				$errors[] = 'used_user';
			}

			if ( $GLOBALS['cms_users_object']->get_user( $_POST['user_email'], array( 'by' => 'email' ) ) )
			{
				$errors[] = 'used_email';
			}

		}

	}

	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$vars = array();
		$data = sanitize_vars( $_POST );
		$type = ( $action == 'update' ) ? 0 : 1;
		$xsql = "";

		$new_mod_start_date = date('Y-m-d H:i:s');

		// if adding user and setting as moderator
		// need mod_start_date and need mod_referral_id
		if ($action != 'update' )
		{
			$mod_start_date = '';
			if ($data['is_mod'] == '1')
				$mod_start_date = $new_mod_start_date;
		}

		// if updating user and setting as moderator for first time
		// need mod_start_date and need mod_referral_id
		if ( $action == 'update')
		{
			// mod_referral_id
			$ref_lname = substr($data['last_name'], 0, 3);
			$ref_fname = substr($data['first_name'], 0, 1);
			$ref_id = intval($id);
			// 5 numbers with leading 0's
			$str = substr('00000' . $ref_id, -5);
			$new_mod_referral_id = $ref_lname . $ref_fname . $str;

			// have to qry user for the existing data
			$u = $GLOBALS['cms_users_object']->get_user( $id );

			$mod_start_date = $u->mod_start_date;
			$mod_referral_id = $u->mod_referral_id;

			// if was not mod, but is now - check/set db fields
			if($u->is_mod == '0' && $data['is_mod'] == '1')
			{
				// if mod_start_date is blank add one, else leave it alone.
				if($u->mod_start_date == '')
					$data['mod_start_date'] = $new_mod_start_date;

				// if mod_referral_id is blank add one, else leave it alone.
				if($u->mod_referral_id == '')
					$data['mod_referral_id'] = $new_mod_referral_id;
			}
		}

		if ( $action == 'update' )
		{

			// vars stored in the users table.
			$vars = array(
				'user_email'  => $data['user_email'],
				'user_status' => $data['user_status']
			);


			$GLOBALS['cms_users_object']->update_user( $id, $vars );
			$newId = $id;


			// these are stored in the usermeta table.
			$keys = array(
				'first_name',
				'last_name',
				'tier',
				'verified',
				'is_mod',
				'mod_referral_id',
				'mod_start_date',
			);

			foreach ($keys as $key) {
				update_usermeta( $id, $key, $data[$key] );
			}
/*
			$sql = 'UPDATE ' . $dbtbl . ' SET fname = ?, lname = ?, email = ?, tier = ?, verified = ?, active = ?, is_mod = ?, mod_referral_id = ?, mod_start_date = ? ';
			array_push($vars, $data['fname']);
			array_push($vars, $data['lname']);
			array_push($vars, $data['email']);
			array_push($vars, $data['tier']);
			array_push($vars, $data['verified']);
			array_push($vars, $data['active']);
			array_push($vars, $data['is_mod']);
			array_push($vars, $mod_referral_id);
			array_push($vars, $mod_start_date);

			if ( $data['pass'] != '' )
			{
				$sql .= ", pass = ? ";
				array_push($vars, md5( $data['pass'] ));
			}

			$sql .= " WHERE id = ?";
			array_push($vars, $id);
*/
		} else {

			$message = null;
			$r = $GLOBALS['cms_users_object']->new_user( array(
				'ID' => FALSE,
				'user_email'  => $data['user_email'],
				'user_login'  => $data['user_login'],
				'user_status' => $data['user_status'],
				'user_pass'   => $data['user_pass']
			));

			if ( is_cms_error($r) ) {
				$message = $r->get_error_message();
				echo var_dump($message);die();
			} else {
				$keys = array(
					'first_name',
					'last_name',
					'tier',
					'verified',
					'is_mod',
					'mod_referral_id',
					'mod_start_date',
				);

				foreach ($keys as $key) {
					update_usermeta( $r['ID'], $key, $data[$key] );
				}
				$newId = $r['ID'];
			}

/*

			$sql = 'INSERT INTO ' . $dbtbl . ' (fname, lname, email, pass, user, tier, verified, active, is_mod, orig_mod_id, mod_start_date)
				VALUES ' . "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			array_push($vars, $data['fname']);
			array_push($vars, $data['lname']);
			array_push($vars, $data['email']);
			array_push($vars, md5( $data['pass'] ));
			array_push($vars, $data['user']);
			array_push($vars, $data['tier']);
			array_push($vars, $data['verified']);
			array_push($vars, $data['active']);
			array_push($vars, $data['is_mod']);
			array_push($vars, $data['orig_mod_id']);
			array_push($vars, $mod_start_date);
*/

		}
/*
		$errno = 0;
		try
		{
			$stmt = $db->prepare($sql);
			$stmt->execute($vars);
		}

		catch(PDOException $ex)
		{
			$errno = $ex->getCode();
		}

		// now get the id of this new user and update db with mod_referral_id
		$newId = $db->lastInsertId();
*/

		$mod_referral_id = '';

		if ($data['is_mod'] == '1' && $newId != 0)
		{
			// mod_referral_id
			$ref_lname = substr($data['last_name'], 0, 3);
			$ref_fname = substr($data['first_name'], 0, 1);
			$ref_id = intval($newId);
			// 5 numbers with leading 0's
			$str = substr('00000' . $ref_id, -5);
			$mod_referral_id = $ref_lname . $ref_fname . $str;

			$user = get_user( $newId );
			$user->set_role( 'moderator');

			update_usermeta( $newId, 'mod_referral_id', $mod_referral_id );

//			$stmt = $db->prepare("UPDATE ".$dbtbl." SET mod_referral_id = ? WHERE id = ?");
//			$stmt->execute(array($mod_referral_id, $newId));
		} elseif ( $data['is_mod'] == '0' && $newId != 0 )
		{
			$user = get_user( $newId );
			$user_id = $user->ID;
			$teams = '`' . db_prefix . 'teams`';
			// verify user is not a mod for another team, if not then remove_cap moderator.  Probably need to remove user from team table also.
			$q = $db->prepare( "SELECT * FROM {$teams} WHERE `moderator_id` = :uid_1 OR `sub-moderator_id` = :uid_2" );
			$q->bindParam( ':uid_1', $user_id, PDO::PARAM_INT );
			$q->bindParam( ':uid_2', $user_id, PDO::PARAM_INT );
			$q->execute();
			$q = $q->FetchAll();
			if ( 0 === count ( $q ) )
			{
				$user->remove_cap( 'moderator' );
			}
		}

		if ( ( $errno != 0 ) && ( is_saint() ) )
			print_debug($sql);

		print_mysql_message ( $errno , $module_name, $id, $type ) ;

	} else {
		add( $errors );
	}

}//end function
?>