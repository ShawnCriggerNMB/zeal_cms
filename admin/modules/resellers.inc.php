<?php

//****************************************************/

// Module: Reseller Management System

// Written By : Cory Shaw of EWD

// Written On : May 09, 2014

// Copyright Zeal Technologies

//***************************************************/



if ( !defined('BASE') ) die('No Direct Script Access');









function execute()

{

    switch($_GET['action'])

    {

        case 'update':

            update();

            break;

        case 'add':

            update();

            break;

        case 'remove':

            remove();

            break;

        default:

            manage();

    }

}





/***************************************************************

 *

 * function manage

 * Querys DB and Displays Resellers

 *

 *

 **************************************************************/



function manage()

{

    global $db, $identifier, $module_name, $userinfo, $dbtbl;



    $i    = 0;

    $link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';



    print_header('Manage '.$module_name,$link);



    $sql = '';



    echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table" >' . "\n" .

        '<thead><tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" .

        '<th><h3>Contact</h3></th>' . "\n" .

        '<th><h3>Email</h3></th>' . "\n" .

        '<th><h3>Main Seller</h3></th>' . "\n" .

        '<th style="width:50px;"><h3>Active</h3></th>' . "\n" .

        '<th class="nosort" style="width:112px;"><h3>Tools</h3></th>' . "\n" .

        '</tr></thead>

        <tbody>';



    $stmt = $db->prepare('SELECT * FROM ' . $dbtbl . ' ORDER BY fname');

    $stmt->execute();

    $pages   = $stmt->rowCount();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))

    {

        $i++;



        $main = yes_no($row['main']);

        if($row['main'] == 0){

            //pull main's info

            $stmt1 = $db->prepare('SELECT * FROM ' . $dbtbl . ' WHERE id = ?');

            $stmt1->execute(array($row['main_seller_id']));

            $row1 = $stmt1->fetch(PDO::FETCH_ASSOC);

            $main .= " - ".$row1['fname']." ".$row1['lname'];

        }

        $act = yes_no($row['active']);



        echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

        echo '<td>' . $row['fname'] . '&nbsp;' . $row['lname'] . '</td>' . "\n";

        echo '<td>' . $row['email'] . '</td>' . "\n";

        echo '<td>' . $main . '</td>' . "\n";

        echo '<td>' . $act . '</td>' . "\n";

        echo '<td valign="middle"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&action=remove&id='.$row['id'].'">Remove</a></td>';

        echo '</tr>' . "\n";



    }



    echo '</tbody>

	</table>';



    if ( $pages > 20 )

        $pages = true;

    else

        $pages = false;



    echo_js_sorter ( $pages );



}



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Form for User Input

 *

 *

 **************************************************************/



function add( $errors = '' )

{

    global $db, $identifier, $module_name, $id, $action, $dbtbl;



    if ( $errors )

    {

        echo '<div class="error_message">';

        echo 'Please fill in the required fields.';



        if ( in_array('fname', $errors ) )

        {

            echo '<br />* A Contact Persons First Name is required.';

            $val_fname = ' class="form_field_error" ';

        }



        if ( in_array('lname', $errors ) )

        {

            echo '<br />* A Contact Persons Last Name is required.';

            $val_lname = ' class="form_field_error" ';

        }



        if ( in_array('email', $errors ) )

        {

            echo '<br />* The Email Address does not appear to be valid.';

            $val_email = ' class="form_field_error" ';

        }



        if ( in_array('used_email', $errors ) )

        {

            echo '<br />* This email is already in use.';

            $val_email = ' class="form_field_error" ';

        }



        if ( in_array('pass', $errors ) )

        {

            echo '<br />* A Password is required for login.';

            $val_pass = ' class="form_field_error" ';

        }



        if ( in_array('main', $errors ) )

        {

            echo '<br />* A Main Seller is required.';

            $val_pass = ' class="form_field_error" ';

        }



        echo '</div>' . "\n";

    }



    if ($action == "update")

    {

        // Run the query for the existing data to be displayed.

        $stmt = $db->prepare('SELECT * FROM ' . $dbtbl . ' WHERE id = ?');

        $stmt->execute(array($id));

        $row 		 = $stmt->fetch(PDO::FETCH_ASSOC);

    } else {

        $row     = sanitize_vars ( $_POST );

    }



    $req  = '<span class="req">*</span>';

    $preq = ($action == 'add') ? $req : '';



    $cities = explode("|", $row['city_id']);

    /********************************************************

     * Start Building Form

     *******************************************************/



    echo '<form name="signup" id="signup" method="post" action="#" ><table>' . "\n";

    echo '<input type="hidden" name="id" value="' . $id . '" />';



    $passnote = tooltip('Recommended: 8-10 characters using uppercase, lowercase, and numbers.');

    if ($action == 'update')

        $passnote = tooltip('Leave blank to keep current password.<br />

													Recommended: 8-10 characters using uppercase, lowercase, and numbers.');



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="fname">Contact First Name</label></td>' . "\n" .

        '<td> <input ' . $val_fname . ' type="text" name="fname" id="fname" value="'. htmlspecialchars($row['fname']) .'" size="45"

				tabindex="' . $t_index . '" /></td>  ' . "\n" .

        '</tr>' . "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> <label for="lname">Contact Last Name</label></td>' . "\n" .

        '<td> <input ' . $val_lname . ' type="text" name="lname" id="lname" value="'. htmlspecialchars($row['lname']) .'" size="45"

				tabindex="' . $t_index . '" /></td>  ' . "\n" .

        '</tr>' . "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="email">Email</label></td>' . "\n" .

        '<td> <input ' . $val_email . ' type="text" name="email" id="email" value="'. htmlspecialchars($row['email']) .'" size="45"

				tabindex="' . $t_index . '" /></td>  ' . "\n" .

        '</tr>' . "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$preq. ' <label for="pass">Password</label></td>' . "\n" .

        '<td> <input ' . $val_pass . ' type="text" name="pass" id="pass" value="" size="45" 

				tabindex="' . $t_index . '" />

				' .$passnote. ' </td>' . "\n" .

        '</tr>' . "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="verified">Main Seller</label></td>  ' . "\n" .

        '<td>' . "\n" .

        create_slist ( $list, 'main', $row['main'], 1, $t_index ) .

        tooltip('Set to Yes to mark this seller as a "main seller"') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td><label for="verified">Their Main Seller (if not one already)</label></td>  ' . "\n" .

        '<td>' . "\n" .

            '<select name="main_seller_id" id="main_seller_id">' .

                '<option value="">--SELECT ONE--</option>';

                if($id != ''){

                    $stmt1 = $db->prepare("SELECT * FROM ".$dbtbl." WHERE main = 1 AND active = 1 AND id <> ? ORDER BY fname");

                    $stmt1->execute(array($row['id']));

                }

                else{

                    $stmt1 = $db->prepare("SELECT * FROM ".$dbtbl." WHERE main = 1 AND active = 1 ORDER BY fname");

                    $stmt1->execute();

                }



                while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

                    echo '<option value ="'.$row1['id'].'"';

                    if($row1['id'] == $row['main_seller_id'])

                        echo " selected";

                    echo '>'.$row1['fname'].' '.$row1['lname'].'</option>';

                }

    echo    '</select>' .

        tooltip('If this is not a main seller, please specify who they are selling for') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td><label for="verified">Cities</label></td>  ' . "\n" .

        '<td>' . "\n" .

            '<select name="city_id[]" id="city_id" multiple>';

                $stmt1 = $db->prepare("SELECT ".db_prefix."states.name AS state, ".db_prefix."cities.* FROM ".db_prefix."cities

                                        JOIN ".db_prefix."states

                                            ON ".db_prefix."cities.state_id = ".db_prefix."states.id

                                WHERE ".db_prefix."cities.active = 1 AND ".db_prefix."states.active = 1

                                ORDER BY ".db_prefix."cities.name, ".db_prefix."states.name");

                $stmt1->execute();

                while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

                    echo '<option value ="'.$row1['id'].'"';

                    if(in_array($row1['id'], $cities))

                        echo " selected";

                    echo '>'.$row1['name'].', '.$row1['state'].'</option>';

                }

    echo    '</select>' .

        tooltip('If this is not a main seller, please specify who they are selling for. Ctrl/Cmd + Click to select multiple') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr>' . "\n" .

        '<td> ' .$req. ' <label for="active">Active</label></td>  ' . "\n" .

        '<td>' . "\n" .

        create_slist ( $list, 'active', $row['active'], 1, $t_index ) .

        tooltip('Set to Yes to Allow Login') . "\n" .

        '</td>  ' . "\n" .

        '</tr>  '. "\n";



    $t_index++;

    echo '<tr><td colspan="2" style="text-align:center; margin:0 auto;" ><input type="submit" name="submit" 

			value="Submit" /></td></tr>' . "\n";

    echo '</table></form>' . "\n";

    echo '<script type="text/javascript">document.getElementById(\'fname\').focus();</script>' . "\n";



}







/***************************************************************

 *

 * function sanitize_vars

 * @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{



    $r_data['fname']    = $data['fname'];

    $r_data['lname']    = $data['lname'];

    $r_data['email']    = $data['email'];

    $r_data['pass']     = $data['pass'];

    $r_data['main']		= intval ( $data['main'] );

    $r_data['main_seller_id']		= intval ( $data['main_seller_id'] );

    $r_data['city_id']		= "|".implode("|",$data['city_id'])."|";

    $r_data['active']   = intval ( $data['active'] );



    return $r_data;

}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 *

 **************************************************************/



function remove()

{

    global $db, $identifier, $module_name, $id, $module_name, $userinfo, $config, $dbtbl;



    $stmt = $db->prepare("SELECT * FROM " . $dbtbl . " WHERE id = ?");

    $stmt->execute(array($id));

    $row    = $stmt->fetch(PDO::FETCH_ASSOC);



    print_header('Delete '.$module_name.'  - ' . $row['fname'].' '. $row['lname']);



    if ( !empty($_POST ))

    {

        $errno = 0;

        try{

            $stmt = $db->prepare("DELETE FROM " . $dbtbl . " WHERE id = ?");

            $stmt->execute(array($id));

        }

        catch(PDOException $ex){

            $errno = $ex->getCode();

        }







        print_mysql_message ( $errno , $module_name, $id, '2' ) ;

    } else {



        echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">' . "\n" .

            '<input type="hidden" name="id" value="' . $id . '">' . "\n" .

            '<table width="100%" border="0">' . "\n" .

            '<tr> <td><div align="center">Are you sure you want to delete this record?</div></td></tr>' . "\n" .

            '<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;

                <input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div></td>

            </tr>' . "\n" .

            '</table>

        </form>';

    }

}





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

    global $db, $identifier, $module_name, $action, $id, $module_name, $dbtbl;



    if ( $action == 'update' )

        print_header('Update '.$module_name);

    else

        print_header('Add New '.$module_name);



    if ( array_key_exists ('submit',$_POST))

    {

        require_once("classes/validation.php");



        $rules = array();



        $rules[] = "required,fname,fname";

//		$rules[] = "required,lname,lname";

        $rules[] = "required,email,email";

        $rules[] = "valid_email,email,email";

        $rules[] = "required,main,main";



        if ( $action != 'update' )

            $rules[] = "required,pass,pass";



        $errors = validateFields($_POST, $rules);



        if ( $action != 'update' )

        {

            $stmt = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE email = ?");

            $stmt->execute(array($_POST['email']));

            if ( $stmt->rowCount() != 0 )

                $errors[] = 'used_email';

        }



    }



    if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

    {

        $data  = sanitize_vars( $_POST );

        $vars = array();

        $xsql = "";



        if ( $action == 'update' )

        {

            $sql = 'UPDATE ' . $dbtbl . ' SET

                fname = ?,

                lname = ?,

                email = ?,

                main = ?,

                active = ?

                ';

            array_push($vars, $data['fname']);

            array_push($vars, $data['lname']);

            array_push($vars, $data['email']);

            array_push($vars, $data['main']);

            array_push($vars, $data['active']);



            if ( $data['pass'] != '' ){

                $sql .= ", pass = ? ";

                array_push($vars, md5( $data['pass'] ));

            }



            if ( $data['main_seller_id'] != '' ){

                $sql .= ", main_seller_id = ? ";

                array_push($vars, $data['main_seller_id']);

            }



            if ( $data['city_id'] != '' ){

                $sql .= ", city_id = ? ";

                array_push($vars, $data['city_id']);

            }



            $sql .= " WHERE id = ?";

            array_push($vars, $id);

            $type = 0;

        } else {

            $data = sanitize_vars( $_POST );



            $sql = 'INSERT INTO ' . $dbtbl . ' (fname, lname, email, pass, main, main_seller_id, active)

							VALUES ' . "(

							?,

							?,

                            ?,

							?,

							?,

							?,

							?

							)";

            array_push($vars, $data['fname']);

            array_push($vars, $data['lname']);

            array_push($vars, $data['email']);

            array_push($vars, md5( $data['pass'] ));

            array_push($vars, $data['main']);

            array_push($vars, intval($data['main_seller_id']));

            array_push($vars, $data['active']);





            $type = 1;

        }



        $errno = 0;

        try{

            $stmt = $db->prepare($sql);

            $stmt->execute($vars);

        }

        catch(PDOException $ex){

            $errno = $ex->getCode();

        }





        if ( ( $errno != 0 ) && ( is_saint() ) )

            print_debug($sql);



        print_mysql_message ( $errno , $module_name, $id, $type ) ;



    } else {

        add( $errors );

    }//end if	

}//end function

