<?php
if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : Teams Settings / Programmer ++ Level
// Written By : Chuck Bunnell of EWD
// Written On : May 29, 2014
// Updated On : Aug 11, 2014
// Copyright Zeal Technologies
//***************************************************/

// if update make sure this id exists
update_verify();

$sitedir = $config['site_base_dir'];

require( $sitedir . '/lib/cms-load.php');
global $wpdb;
$GLOBALS['cms_users_object'] = new CMS_Users( $wpdb );

// for icon uploading
$upload_url  = $site_base_url . $cms_uploads . 'teams/';
$upload_dir  = $site_base_dir . $cms_uploads . 'teams/';
$height = '300';
$width  = '960';
//$tn_height = '';
//$tn_width = '48';
$logo_width  = '125';
$logo_height = '125';

function execute()
{
	switch($_GET['action'])
	{
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}
}


function get_mods( $og_mod_id = 0 )
{
	global $db, $dbtbl;
	$user_meta_table = db_prefix . 'usermeta';
	$user = db_prefix . 'users';
	$caps = array( 'moderator', 'administrator' );
	$q = $db->prepare("SELECT u.ID,
							first_name.meta_value as first_name,
							last_name.meta_value as last_name,
							cms_capabilities.meta_value as cms_capabilities
							FROM {$user} AS u
			 			LEFT JOIN {$user_meta_table} AS first_name ON first_name.user_id=u.ID
			 				AND first_name.meta_key='first_name'
			 			LEFT JOIN {$user_meta_table} AS last_name ON last_name.user_id=u.ID
			 				AND last_name.meta_key='last_name'
			 			LEFT JOIN {$user_meta_table} AS cms_capabilities ON cms_capabilities.user_id=u.ID
			 				AND cms_capabilities.meta_key='cms_capabilities'
			 			WHERE user_status != 3 ORDER BY first_name");
	$q->execute();
	$q = $q->fetchAll(PDO::FETCH_OBJ);
	foreach ($q as $user) {
		$user->cms_capabilities = unserialize( $user->cms_capabilities );
		$is_mod = FALSE;
		foreach ($user->cms_capabilities as $cap => $value) {

			$is_mod = ( in_array( $cap, $caps )	) ? TRUE : FALSE;
		}
		if ( FALSE === $is_mod ) continue;

		$sel     = ( $og_mod_id === $user->ID ) ? ' selected ' : NULL;
		$output .= "\t<option value=\"{$user->ID}\" {$sel}>{$user->first_name} {$user->last_name}</option>\n";
	}

	return $output;
}


/***************************************************************
 *
 * function manage
 * Querrs DB and Displays Content
 *
 **************************************************************/

function manage()
{
	global $db, $identifier, $module_name, $dbtbl, $upload_url;

	$link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';

	$extra_header = null;

	if (!empty($_GET['state_id']))
	{
		// get state info
		$stmt = $db->prepare("SELECT name FROM ".db_prefix."sportscategories WHERE id = ?");
		$stmt->execute(array($_GET['sportscat_id']));
		$team = $stmt->fetchColumn();
		$extra_header = " - ".$team;
	}

	print_header('Manage '.$module_name.$extra_header,$link);

	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sortable" id="table">
		<thead>
			<tr align="left" valign="top">
				<th><h3>Team</h3></th>
				<th style="width:200px"><h3>Sports Category</h3></th>
				<th><h3>Moderator</h3></th>
				<th style="width:70px"><h3>Posts</h3></th>
				<th style="width:70px"><h3>Image</h3></th>
				<th style="width:70px"><h3>Logo</h3></th>
				<th style="width:70px"><h3>Active</h3></th>
				<th class="nosort"><h3>Tools</h3></th>
			</tr>
		</thead>
		<tbody>';

			if(!isset($_GET['sportscat_id']))
			{
				$stmt = $db->prepare('SELECT '.db_prefix.'teams.id, '.db_prefix.'teams.name, '.db_prefix.'teams.image, '.db_prefix.'teams.logo, '.db_prefix.'teams.active, '.db_prefix.'teams.moderator_id, '.db_prefix.'sportscategories.name AS sportscat FROM ' . db_prefix . $identifier . '
					JOIN '.db_prefix.'sportscategories
					ON '.db_prefix.'sportscategories.id = '.db_prefix.'teams.sportscat_id
					ORDER BY '.db_prefix.'teams.name ASC');
				$stmt->execute();

			} else {

				$stmt = $db->prepare('SELECT '.db_prefix.'teams.id, '.db_prefix.'teams.name, '.db_prefix.'teams.image, '.db_prefix.'teams.logo, '.db_prefix.'teams.active, '.db_prefix.'teams.moderator_id, '.db_prefix.'sportscategories.name AS sportscat FROM ' . db_prefix . $identifier . '
				JOIN '.db_prefix.'sportscategories
				ON '.db_prefix.'sportscategories.id = '.db_prefix.'teams.sportscat_id
				WHERE sportscat_id = ?
				ORDER BY '.db_prefix.'teams.name ASC');
				$stmt->execute(array($_GET['sportscat_id']));
			}

			$pages = $stmt->rowCount();

			while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$act  = yes_no ( $row['active'] );
        $image = ($row['image'] == '') ? 'No' : '<a href="'.$upload_url . $row['image'].'" class="lightbox" style="color:#060;">Yes</a>';
        $logo = ($row['logo'] == '') ? 'No' : '<a href="'.$upload_url . $row['logo'].'" class="lightbox" style="color:#060;">Yes</a>';


				// qry moderators name from users db
        		$row2 = $GLOBALS['cms_users_object']->get_user( $row['moderator_id'] );
/*
				$stmt2 = $db->prepare("SELECT id,fname,lname FROM ".db_prefix."users WHERE id = ?");
				$stmt2->execute(array($row['moderator_id']));
				$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
*/
				// qry num posts from posts db
				$stmt3 = $db->prepare("SELECT id,sportscat_id,team_id FROM ".db_prefix."posts WHERE team_id = ?");
				$stmt3->execute(array($row['id']));
				$tRows3 = $stmt3->rowCount();
				$row3 = $stmt3->fetch(PDO::FETCH_ASSOC);

				//$posts = ($stmt3->rowCount() > 0) ? '<a href="?tool=posts&team='.$row['id'].'">'.$tRows3.'</a>' : '0';
				$posts = '<a href="?tool=posts&team='.$row['id'].'">'.$tRows3.'</a>';

				echo '<tr align="left" valign="middle">
					<td>' . $row['name'] . '</td>
					<td>' . $row['sportscat'] . '</td>
					<td><a href="?tool=users&action=update&id='.$row2->ID.'">' . $row2->first_name . '&nbsp;' . $row2->last_name . '</a></td>
					<td>'.$posts.'</td>
					<td>' . $image . '</td>
					<td>' . $logo . '</td>
					<td>' . $act . '</td>
					<td style="padding:0;text-align:center;"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&action=remove&id='.$row['id'].'">Remove</a></strong></td>
				</tr>';
			}
    echo '</tbody>
	</table>';

	$pages = ( $pages > 20 ) ? true : false;

	echo_js_sorter ( $pages );

	echo '<div class="spacer">&nbsp;</div>';
}//end function


/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 *
 **************************************************************/

function add( $errors = '' )
{
	global $identifier, $module_name, $id, $action,$db, $site_base_url, $upload_url, $upload_dir, $width, $height, $mod_config, $logo_width, $logo_height;

	if ( $errors )
	{
		echo '<ul class="error_message">
			<strong>Please fill in the required fields.</strong>';
			// set error messages for required fields
			if ( in_array('name', $errors ) )
			{
				echo '<li>You must fill out a name.</li>';
				$val_name = ' class="form_field_error" ';
			}

			if ( in_array('used_name', $errors ) )
			{
				echo '<li> This team name is already being used with this sports category.</li>';
				$val_name = ' class="form_field_error" ';
			}

			if ( in_array('sportscat_id', $errors ) )
			{
				echo '<li>You must select a sports category.</li>';
				$val_sportscatid = ' class="form_field_error" ';
			}

			if ( in_array('active', $errors ) )
			{
				echo '<li>You must select from active.</li>';
				$val_active = ' class="form_field_error" ';
			}

		echo '</ul>';

	} else {

		$c = ( $action == 'update' ) ? 'update this' : 'add a new';

		echo  '<ul class="notice_message"><strong>To ' . $c . ' record, fill out the form and click submit.</strong>
			<li>Image needs to be '.$width.'px wide and '.$height.'px high. If you are not uploading the image to this size, it may look distorted.</li>
			<li>Logo needs to be square and at least '.$logo_width.'px wide and high.</li>
			<li>Required fields have a red "*".</li>
		</ul>';
  }

	if ($action == "update")
	{
		$stmt = $db->prepare('SELECT * FROM ' . db_prefix . $identifier.' WHERE id = ?');
		$stmt->execute(array($id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
	}

	if (!empty ($_POST))
	{
		$row = sanitize_vars ($_POST);
	}

/********************************************************
 * Start Building Form
 *******************************************************/

	$r = required();

  echo '<form name="form" id="form" method="post"  action="#"enctype="multipart/form-data">
		<table>
    	<input type="hidden" name="id" value="' . $id . '" />';
			echo '<tr>
				<td> <label for="name">'.$r.'Team Name</label></td>
				<td> <input ' . $val_name . ' type="text" name="name" id="name" value="'. htmlspecialchars($row['name']) .'" size="45" /></td>
			</tr>';

			echo '<tr>
				<td> <label for="sportscat_id">'.$r.'Sports Category</label></td>
				<td> <select ' . $val_sportscatid . ' name="sportscat_id" id="sportscat_id">
						<option value="">--SELECT ONE--</option>';

						$stmt1 = $db->prepare("SELECT * FROM ".db_prefix."sportscategories WHERE active = 1 ORDER BY name ASC");
						$stmt1->execute();
						while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC))
						{
							echo '<option value="'.$row1['id'].'"';
							if($row['sportscat_id'] == $row1['id'])
								echo ' selected';
							echo '>'.$row1['name'].'</option>';
						}
					echo '</select>
				</td>
			</tr>';

			echo '<tr>
				<td> <label for="moderator_id">Moderator</label></td>
				<td> <select ' . $val_moderatorid . ' name="moderator_id" id="moderator_id">
						<option value="0">-- NONE --</option>';
				echo get_mods( $row['moderator_id'] );
/*
						$stmt2 = $db->prepare("SELECT * FROM ".db_prefix."users WHERE active = ? AND is_mod = ?  AND archived = '' ORDER BY fname ASC");
						$stmt2->execute(array(1,1));
						while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC))
						{
							echo '<option value="'.$row2['id'].'"';
							if($row['moderator_id'] == $row2['id'])
								echo ' selected';
							echo '>'.$row2['fname'].' '.$row2['lname'].'</option>';
						}
*/
					echo '</select>
				</td>
			</tr>';

			echo '<tr>
				<td colspan="2">&nbsp;</td>
			</tr>';

			/****************************
			 * Image Uploading Section
			 ***************************/
			if ( $row['image'] != '' )
			{
        $filename = $upload_dir . $row['image'];
       // list ( $width, $height ) = img_size ( $filename );

        echo '<tr>
					<td><label for="current_file">Current Image</label></td>
        	<td><a href="' . $upload_url . $row['image'] . '" class="lightbox">' . $row['image'] . '</a></td>
					<input type="hidden" name="current_file" value="' . $row['image'] . '" />
        </tr>';
    	}

    	echo '<tr>
				<td><label for="image">Upload Image</label></td>
				<td><input name="image[]" type="file" id="image" size="15" />
					'.tooltip('Image will be resized to '.$width.'px wide and '.$height.'px high. If you are not uploading the image to this size, it may look distorted.').'</td>
    	</tr>';

    	echo '<tr>
				<td colspan="2">&nbsp;</td>
			</tr>';

			/****************************
			 * Logo Uploading Section
			 ***************************/
			if ( $row['logo'] != '' )
			{
        $logofilename = $upload_dir . $row['logo'];
        //list ( $logo_width, $logo_height ) = img_size ( $logofilename );

        echo '<tr>
					<td><label for="current_logo">Current Logo</label></td>
        	<td><a href="' . $upload_url . $row['logo'] . '" class="lightbox">' . $row['logo'] . '</a></td>
					<input type="hidden" name="current_logo" value="' . $row['logo'] . '" />
        </tr>';
    	}

    	echo '<tr>
				<td><label for="logo">Upload Logo</label></td>
				<td><input name="logo[]" type="file" id="logo" size="15" />
					'.tooltip('Logo will be resized to '.$logo_width.'px wide and '.$logo_height.'px high. If you are not uploading the image square, it may look distorted.').'</td>
    	</tr>';

    	echo '<tr>
				<td colspan="2">&nbsp;</td>
			</tr>';

			echo '<tr>
				<td> <label for="color_sb">Sidebar Color</label></td>
				<td> <input ' . $val_colorsb . ' type="text" name="color_sb" id="color_sb" value="'. htmlspecialchars($row['color_sb']) .'" size="10" /></td>
			</tr>';

			echo '<tr>
				<td> <label for="color_bg">Background Color</label></td>
				<td> <input ' . $val_colorbg . ' type="text" name="color_bg" id="color_bg" value="'. htmlspecialchars($row['color_bg']) .'" size="10" /></td>
			</tr>';

			echo '
<script type="text/javascript">
	$(document).ready(function(){
		$("#color_bg, #color_sb").spectrum({
	    	showInput: true,
	    	allowEmpty:true,
	    	showAlpha: false,
	    	showInitial: true,
	    	preferredFormat: "hex"
		});
	});
</script>
';

			echo '<tr>
				<td><label for="active">'.$r.'Active</label></td>
				<td>
					'.create_slist ( $list, 'active', $row['active'], 1 ) .
					tooltip('Set to Yes to allow on website').'
				</td>
			</tr>  ';

			echo '<tr>
				<td colspan="2" style="padding:3px;"><input type="submit" name="submit" value="Submit" /></td>
			</tr>';

		echo '</table>
	</form>';

  echo '<script type="text/javascript">document.getElementById(\'name\').focus();</script>';

}//end function


/***************************************************************
 *
 * function sanitize_vars
 * @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 **************************************************************/
function sanitize_vars( $data )
{
  global $id, $known_photo_types, $upload_dir, $width, $height, $logo_width, $logo_height;

	$r_data['name'] = stripslashes ( $data['name'] );
	$r_data['sportscat_id'] = intval ( $data['sportscat_id'] );
	$r_data['moderator_id'] = intval ( $data['moderator_id'] );
	$r_data['image'] = $data['current_file'];
	$r_data['logo'] = $data['current_logo'];
	$r_data['color_sb'] = $data['color_sb'];
	$r_data['color_bg'] = $data['color_bg'];
	$r_data['active'] = intval ( $data['active'] );

	if ( !empty ( $_FILES['image']['name'][0] ) )
	{
		$old_file = $r_data['image'];
		$image    = $_FILES['image'];

		$r_data['image'] = process_upload ( $id , '', $image, $known_photo_types, $upload_dir, 1, $width, $height, 0, 0 );

		if ( ( $old_file != $r_data['image'] ) && ( file_exists ( $upload_dir . $old_file ) ) && ( $old_file != '' ))
		{
			unlink ( $upload_dir . $old_file );
		}
	}

	if ( !empty ( $_FILES['logo']['name'][0] ) )
	{
		$old_logo = $r_data['logo'];
		$logo     = $_FILES['logo'];

		$r_data['logo'] = process_upload ( $id , 'logo', $logo, $known_photo_types, $upload_dir, 1, $logo_width, $logo_height, 0, 0 );

		if ( ( $old_logo != $r_data['logo'] ) && ( file_exists ( $upload_dir . $old_logo ) ) && ( $old_logo != '' ))
		{
			unlink ( $upload_dir . $old_logo );
		}
	}

	return $r_data;
}


/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $db, $identifier, $module_name, $id, $dbtbl, $upload_dir;

	$stmt = $db->prepare("SELECT * FROM ".$dbtbl." WHERE id = ?");
	$stmt->execute(array($id));
	$row    = $stmt->fetch(PDO::FETCH_ASSOC);

	print_header('Delete '.$module_name.'  - ' . $row['name']);

	if ( !empty($_POST ))
	{
		//delete old image if there
		if ( file_exists ( $upload_dir . $row['image'] ))
		{
			@unlink( $upload_dir . $row['image'] );
		}

		//delete old logo if there
		if ( file_exists ( $upload_dir . $row['logo'] ))
		{
			@unlink( $upload_dir . $row['logo'] );
		}

		$errno = 0;

		try
		{
			$stmt = $db->prepare("DELETE FROM ".$dbtbl." WHERE id = ?");
			$stmt->execute(array($id));
		}

		catch(PDOException $ex)
		{
			$errno = $ex->getCode();
		}

		print_mysql_message ( $errno , $module_name, $id, 2 ) ;

	} else {

		echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">
			<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to delete this record?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div>
		</form>';
	}

}//end function


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of contents
 *
 **************************************************************/

function update()
{
	global $db, $identifier, $module_name, $action, $id, $dbtbl;

	if ( $action == 'update' )
		print_header('Update '.$module_name);
	else
		print_header('Add New '.$module_name);

	if ( array_key_exists ('submit',$_POST))
	{
		require ("classes/validation.php");

		// set rules for required fields
		$rules   = array();
		$rules[] = "required,name,name";
		$rules[] = "required,sportscat_id,sportscat_id";
		$rules[] = "required,active,active";
		$errors = validateFields($_POST, $rules);

		if ( $action != 'update' &&  $_POST['sportscat_id'] != ''  )
		{
			$stmt = $db->prepare("SELECT id FROM ".$dbtbl." WHERE name = ? AND sportscat_id = ".$_POST['sportscat_id']);
      $stmt->execute(array($_POST['name']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_name';
		}

		if ( $action == 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM ".$dbtbl." WHERE name = ? AND sportscat_id = ".$_POST['sportscat_id']." AND id != $id");
      $stmt->execute(array($_POST['name']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_name';
		}

	}

	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$vars = array();
		$data = sanitize_vars( $_POST );
		$type = ( $action == 'update' ) ? 0 : 1;
		$color_sb = ($data['color_sb'] == '') ? '#FFCA27' : $data['color_sb'];
		$color_bg = ($data['color_bg'] == '') ? '#333333' : $data['color_bg'];

		if ( $action == 'update' )
		{
			$sql = "UPDATE ".$dbtbl." SET name = ?, image = ?, logo = ?, sportscat_id = ?, moderator_id = ?, color_sb = ?, color_bg = ?, active = ? WHERE id = ?";
			array_push($vars, $data['name']);
			array_push($vars, $data['image']);
			array_push($vars, $data['logo']);
			array_push($vars, $data['sportscat_id']);
			array_push($vars, $data['moderator_id']);
			array_push($vars, $color_sb);
			array_push($vars, $color_bg);
			array_push($vars, $data['active']);
			array_push($vars, $id);

		} else {

			// do not add image yet - need to get the ID first, then update image
			$sql = 'INSERT INTO '.$dbtbl.' (name, image, logo, sportscat_id, moderator_id, color_sb, color_bg, active) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
			array_push($vars, $data['name']);
			array_push($vars, $data['image']);
			array_push($vars, $data['logo']);
			array_push($vars, $data['sportscat_id']);
			array_push($vars, $data['moderator_id']);
			array_push($vars, $color_sb);
			array_push($vars, $color_bg);
			array_push($vars, $data['active']);
		}

		$errono = 0;

		try
		{
			$stmt = $db->prepare($sql);
			$stmt->execute($vars);
		}

		catch(PDOException $ex)
		{
			$errono = $ex->getCode();
		}

		if ( is_saint() && $errono != 0 )
			echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

		print_mysql_message ( $errono , $module_name, $id, $type ) ;

	} else {

		add( $errors );
	}

}//end function

?>