<?php	
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : Website Configuration
// Written By : Shawn Crigger of EWD
// Written On : May 27, 2010
// Updated By : Cory Shaw of EWD
// Updated On : May 12, 2014
// Updated By : Chuck Bunnell of EWD
// Updated On : May 30, 2014
// Copyright Zeal Technologies
//***************************************************/

function execute()
{
	switch($_GET['action'])
 {
		default:
			update();
	}
}//end function


/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 * 
 **************************************************************/

function add( $errors = '' )
{
	global $db, $identifier, $module_name, $id, $action, $userinfo, $config;
	
	if ( $errors )
	{
		echo '<ul class="error_message">
			<strong>Please fill in the required fields.</strong>';
		// set error messages for required fields
			if ( in_array('site_name', $errors ) )
			{
				echo '<li>The Website Name is required.</li>';
				$val_site_name = ' class="form_field_error" ';
			}
			
			if ( in_array('c_email', $errors ) )
			{
				echo '<li>The Contact Email Address does not appear to be formatted correctly.</li>';
				$val_c_email = ' class="form_field_error" ';
			}
		
		echo '</ul>';
	
	} else {
		
		echo  '<ul class="notice_message">';
			// set instructions here
			echo '<strong>To update this record, fill out the form and click submit.</strong>
			<li>This information is mostly for your contact page, but will also set variables for the rest of the website.</li>
			<li>Your Website Name and a Contact Email are required.</li>
		</ul>';
  
	}
   
	$lvl = $userinfo['level'];

	if ( empty( $config ) )
	{
		$row = fetch_config_data();
	} else {
		$row = $config;
	} 
	
	if ( ( isset ( $_POST )) && ( isset ( $errors ) ) )
		$row = sanitize_vars ( $_POST );
	
	print_notice_message ( 1 );
		
/********************************************************
 * Start Building Form
 *******************************************************/
		
  $r = required();
	echo '<form name="form" id="form" method="post" action="#">
		<table>';			
		
			if ( is_saint() )
			{
				echo '<tr>
					<td><label for="site_base_dir">Absolute Path</label></td>  
					<td><input ' . $val_site_base_dir . ' type="text" name="site_base_dir" id="site_base_dir" value="'. $row['site_base_dir'] .'" size="45" />
							' . tooltip('No Trailing Slashes') . '
					</td>  
				</tr>';
		
				echo '<tr>
					<td><label for="site_base_url">Base URL</label></td>  
					<td><input ' . $val_site_base_url . ' type="text" name="site_base_url" id="site_base_url" value="'. $row['site_base_url'] .'" size="45" />
							' . tooltip('No Trailing Slashes') . '
					</td>  
				</tr>';
										
			} else {
				
				echo '<input type="hidden" id="site_base_dir" name="site_base_dir" value="' . $config['site_base_dir'] . '" />';
				echo '<input type="hidden" id="site_base_url" name="site_base_url" value="' . $config['site_base_url'] . '" />';					
			}				
				
			echo '<tr>
				<td><label for="site_name">'.$r.'Website Name</label></td>  
				<td><input ' . $val_site_name . ' type="text" name="site_name" id="site_name" value="'. stripslashes ( $row['site_name'] ) .'" size="45" /></td>  
			</tr>';

			echo '<tr>
				<td><label for="c_name">Contact Name</label></td>  
				<td><input ' . $val_c_name . ' type="text" name="c_name" id="c_name" value="'. stripslashes ( $row['c_name'] ) .'" size="45" /></td>  
			</tr>';

			echo '<tr>
				<td><label for="c_name_type">Contact Type</label></td>  
				<td><input ' . $val_c_name_type . ' type="text" name="c_name_type" id="c_name_type" value="'. stripslashes ( $row['c_name_type'] ) .'" size="45" /></td>  
			</tr>';

			echo '<tr>
				<td><label for="c_name_show">Show Contact Name on contact page</label></td>
				<td>
					'.create_slist ( $list, 'c_name_show', $row['c_name_show'], 1 ).'
				</td>
			</tr>';
	
			echo '<tr>
				<td><label for="c_email">'.$r.'Contact Email</label></td>  
				<td><input ' . $val_c_email . ' type="text" name="c_email" id="c_email" 
					value="'. stripslashes ( $row['c_email'] ) .'" size="45" />
					' . tooltip('This is the main contact email address. It will be displayed in your contact info.') . '
				</td>  
			</tr>';
	
			echo '<tr>
				<td><label for="c_email_show">Show Email on contact page</label></td>
				<td>
					'.create_slist ( $list, 'c_email_show', $row['c_email_show'], 1 ).'
				</td>
			</tr>';
	
			echo '<tr>
				<td><label for="form_email">Contact Form Email</label></td>  
				<td><input ' . $val_form_email . ' type="text" name="form_email" id="form_email" value="'. stripslashes ( $row['form_email'] ) .'" size="45" />
					' . tooltip('This is the email address your contact form will send to. It is never displayed.<br />
											 You can add multiple email addresses by seperating them with a comma and a space.') . '     
				</td>  
			</tr>';
	
			// PHONE START HERE											
			echo '<tr>
				<td><label for="phone_1">Phone #(1)</label></td>  
				<td><input ' . $val_phone_1 . ' type="text" name="phone_1" id="phone_1" 
					value="'. stripslashes ( $row['phone_1'] ) .'" size="45" /></td>  
			</tr>';
	
			echo '<tr>
				<td><label for="phone_1_type">Phone Type</label></td>  
				<td><input ' . $val_phone_1_type . ' type="text" name="phone_1_type" id="phone_1_type" 
					value="'. stripslashes ( $row['phone_1_type'] ) .'" size="45" />
					' . tooltip('Phone, Cel Phone, Toll Free, Fax, etc.') . '
				</td>  
			</tr>';
			
			echo '<tr>
				<td><label for="phone_1_show">Show Phone #(1) on contact page</label></td>
				<td>
					'.create_slist ( $list, 'phone_1_show', $row['phone_1_show'], 1 ).'
				</td>
			</tr>';
			// PHONE STOP HERE											
			
			// PHONE START HERE											
			echo '<tr>
				<td> <label for="phone_2">Phone #(2)</label></td>  
				<td> <input ' . $val_phone_2 . ' type="text" name="phone_2" id="phone_2" 
					value="'. stripslashes ( $row['phone_2'] ) .'" size="45" /></td>  
			</tr>' . "\n";
	
			echo '<tr>
				<td><label for="phone_2_type">Phone Type</label></td>  
				<td><input ' . $val_phone_2_type . ' type="text" name="phone_2_type" id="phone_2_type" value="'. stripslashes ( $row['phone_2_type'] ) .'" size="45" />
					' . tooltip('Phone, Cel Phone, Toll Free, Fax, etc.') . '
				</td>  
			</tr>';
			
			echo '<tr>
				<td><label for="phone_2_show">Show Phone #(2) on contact page</label></td>
				<td>
					'.create_slist ( $list, 'phone_2_show', $row['phone_2_show'], 1 ).'
				</td>
			</tr>';
			// PHONE STOP HERE											
			
			// PHONE START HERE											
			echo '<tr>
				<td><label for="phone_3">Phone #(3)</label></td>  
				<td><input ' . $val_phone_3 . ' type="text" name="phone_3" id="phone_3" 
					value="'. stripslashes ( $row['phone_3'] ) .'" size="45" /></td>  
			</tr>';
	
			echo '<tr>
				<td><label for="phone_3_type">Phone Type</label></td>  
				<td><input ' . $val_phone_3_type . ' type="text" name="phone_3_type" id="phone_3_type" value="'. stripslashes ( $row['phone_3_type'] ) .'" size="45" />
					' . tooltip('Phone, Cel Phone, Toll Free, Fax, etc.') . '
				</td>  
			</tr>';
			
			echo '<tr>
				<td><label for="phone_3_show">Show Phone #(3) on contact page</label></td>
				<td>
					'.create_slist ( $list, 'phone_3_show', $row['phone_3_show'], 1 ).'
				</td>
			</tr>';
			// PHONE STOP HERE											
	
			echo '<tr>
				<td> <label for="c_address">Address</label></td>  
				<td> <input ' . $val_c_address . ' type="text" name="c_address" id="c_address" value="'. stripslashes ( $row['c_address'] ) .'" size="45" /></td>  
			</tr>';
	
			echo '<tr>
				<td> <label for="c_city">City</label></td>  
				<td> <input ' . $val_c_city . ' type="text" name="c_city" id="c_city" 
					value="'. stripslashes ( $row['c_city'] ) .'" size="45" /></td>  
			</tr>';
	
			echo '<tr>
				<td> <label for="c_state">State</label></td>  
				<td> <input ' . $val_c_state . ' type="text" name="c_state" id="c_state" 
					value="'. stripslashes ( $row['c_state'] ) .'" size="45" /></td>  
			</tr>';
	
			echo '<tr>
				<td> <label for="c_zip">Zip Code</label></td>  
				<td> <input ' . $val_c_zip . ' type="text" name="c_zip" id="c_zip" 
					value="'. stripslashes ( $row['c_zip'] ) .'" size="45" /></td>  
			</tr>';
	
			echo '<tr>
				<td> <label for="c_address_type">Address Type</label></td>  
				<td> <input ' . $val_c_address_type . ' type="text" name="c_address_type" id="c_address_type" value="'. stripslashes ( $row['c_address_type'] ) .'" size="45" /></td>  
			</tr>';
			
			echo '<tr>
				<td><label for="c_address_show">Display Address Information</label></td>
				<td>
					'.create_slist ( $list, 'c_address_show', $row['c_address_show'], 1 ).'
				</td>
			</tr>';
	
			echo '<tr>
				<td><label for="bus_hours">Business Hours</label></td>  
				<td><textarea id="bus_hours" name="bus_hours" cols="33" rows="3">'. str_replace('<br />', '', $row['bus_hours']) .'</textarea>											
					' . tooltip('Setup your business hours.') . '
				</td>  
			</tr>';
	
			echo '<tr>
				<td><label for="show_bus_hours">Display Business Hours</label></td>
				<td>
					'.create_slist ( $list, 'show_bus_hours', $row['show_bus_hours'], 1 ).'
				</td>
			</tr>';
	
			echo '<tr>
				<td><label for="show_contact">Display All Contact Information</label></td>
				<td>
					'.create_slist ( $list, 'show_contact', $row['show_contact'], 1 ).'
				</td>
			</tr>';
	
			echo '<tr>
				<td><label for="display_contact">Display Contact Form</label></td>
				<td>
					'.create_slist ( $list, 'display_contact', $row['display_contact'], 1 ).'
				</td>
			</tr>';
	
			echo '<tr>
				<td colspan="2">&nbsp;</td>  
			</tr>';
			
			if ( is_saint() )
			{
				echo '<tr>
					<td colspan="2">&nbsp;</td>  
				</tr>';
			
				echo '<tr>
					<td><label for="live">Make Site Live</label></td>
					<td>
						'.create_slist ( $list, 'live', $row['live'], 1 ) .
						tooltip('Do not change this unless told to do so. \'No\' will cause site to be unviewable.').'
					</td>
				</tr>';
	
				echo '<tr>
					<td><label for="use_access">Restrict Access Control</label></td>
					<td>
						'.create_slist ( $list, 'use_access', $row['use_access'], 1 ) . 
						tooltip('This option allows you to set levels for administrators to access modules.').'
					</td>
				</tr>';
	
				echo '<tr>
					<td><label for="use_seo">Use SEO Friendly Page URLs</label></td>
					<td>
						'.create_slist ( $list, 'use_seo', $row['use_seo'], 1 ) . 
						tooltip('Please only change this option if you are told to or your site will not work.').'
					</td>
				</tr>';
	
			} else {
				
				echo '<input type="hidden" id="live" name="live" value="' . $config['live'] . '" />';
				echo '<input type="hidden" id="use_access" name="use_access" value="' . $config['use_access'] . '" />';
				echo '<input type="hidden" id="use_seo" name="use_seo" value="' . $config['use_seo'] . '" />';																
			}
		
			echo '<tr>
				<td colspan="2" style="margin:0 auto; text-align:center; padding:3px;"><input type="submit" name="submit" value="Submit" /></td>
			</tr>';
		
		echo '</table>
	</form>';
  
	echo '<div style="height:10px; min-height:10px;">&nbsp;</div>';
	
	echo '<script type="text/javascript">document.getElementById(\'site_name\').focus();</script>' . "\n";

}


/***************************************************************
 *
 * function sanitize_vars
 * @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 **************************************************************/


function sanitize_vars( $data )
{
	$r_data['site_base_dir']      = stripslashes ( $data['site_base_dir']  ) ;
	$r_data['site_base_url']      = stripslashes ( $data['site_base_url']  ) ;
	$r_data['site_name']          = stripslashes ( $data['site_name']  ) ;
	$r_data['c_name']             = stripslashes ( $data['c_name']  ) ;
	$r_data['c_name_type']        = intval ( $data['c_name_type']  );
	$r_data['c_name_show']        = intval ( $data['c_name_show']  );
	$r_data['c_email']            = stripslashes ( $data['c_email']  ) ;
	$r_data['c_email_show']       = intval ( $data['c_email_show']  );
	$r_data['form_email']         = stripslashes ( $data['form_email']  ) ;
	$r_data['phone_1']            = stripslashes ( $data['phone_1']  ) ;		
	$r_data['phone_1_type']       = stripslashes ( $data['phone_1_type']  ) ;
	$r_data['phone_1_show']       = intval ( $data['phone_1_show']  );
	$r_data['phone_2']            = stripslashes ( $data['phone_2']  ) ;
	$r_data['phone_2_type']       = stripslashes ( $data['phone_2_type']  ) ;
	$r_data['phone_2_show']       = intval ( $data['phone_2_show']  ) ;
	$r_data['phone_3']            = stripslashes ( $data['phone_3']  ) ;
	$r_data['phone_3_type']       = stripslashes ( $data['phone_3_type']  ) ;
	$r_data['phone_3_show']       = intval ( $data['phone_3_show']  ) ;
	$r_data['c_address']          = stripslashes ( $data['c_address']  ) ;
	$r_data['c_city']             = stripslashes ( $data['c_city']  ) ;
	$r_data['c_state']            = stripslashes ( $data['c_state']  ) ;
	$r_data['c_zip']              = stripslashes ( $data['c_zip']  ) ;
	$r_data['c_address_type']     = stripslashes ( $data['c_address_type']  ) ;
	$r_data['c_address_show']     = intval ( $data['c_address_show'] );
	$r_data['show_contact']       = intval ( $data['show_contact']  );
	$r_data['display_contact']    = intval ( $data['display_contact']  );
	$r_data['bus_hours']          = stripslashes ( $data['bus_hours']  ) ;
	$r_data['show_bus_hours']     = intval ( $data['show_bus_hours']  ) ;
	$r_data['aw3']        				= intval ( $data['aw3']  );	
	$r_data['live']        				= intval ( $data['live']  );	
	$r_data['use_access']         = intval ( $data['use_access']  );	
	$r_data['use_seo']            = intval ( $data['use_seo'] );

	$r_data['site_base_dir']      = ( substr( $r_data['site_base_dir'], -1 ) != '/' ) ? $r_data['site_base_dir'] : substr( $r_data['site_base_dir'], -1 );
	$r_data['site_base_url']      = ( substr( $r_data['site_base_url'], -1 ) != '/' ) ? $r_data['site_base_url'] : substr( $r_data['site_base_url'], -1 );

	return $r_data;
}

/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of contents
 *
 **************************************************************/

function update()
{
	global $db, $identifier, $module_name, $action, $id, $dbtbl;
	
	print_header('Update '.$module_name);
	$type = 0;
	
	if ( array_key_exists ('submit',$_POST))
	{
		require_once("classes/validation.php");
			
		$rules   = array();        
	
		$rules[] = "required,site_name,site_name";
		$rules[] = "valid_email,c_email,c_email";		
	
		$errors = validateFields($_POST, $rules);								
	}
		
	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$data  = sanitize_vars( $_POST );
		$errno = 0;

		try 
		{
			$stmt = $db->prepare("UPDATE ".$dbtbl." SET
			site_base_dir = ?,
			site_base_url = ?,
			site_name = ?,
			c_name = ?,
			c_name_type = ?,
			c_name_show = ?,
			c_email = ?,
			c_email_show = ?,
			form_email = ?,
			phone_1 = ?,
			phone_1_type = ?,
			phone_1_show = ?,
			phone_2 = ?,
			phone_2_type = ?,
			phone_2_show = ?,
			phone_3 = ?,
			phone_3_type = ?,
			phone_3_show = ?,
			c_address = ?,
			c_city = ?,
			c_state = ?,
			c_zip = ?,
			c_address_type = ?,
			c_address_show = ?,
			show_contact = ?,
			display_contact = ?,
			bus_hours = ?,
			show_bus_hours = ?,
			aw3 = ?,
			live = ?,
			use_access = ?,
			use_seo = ? 
			WHERE id = ?");

			$stmt->execute(array(
				$data['site_base_dir'],
				$data['site_base_url'],
				$data['site_name'],
				$data['c_name'],
				$data['c_name_type'],
				$data['c_name_show'],
				$data['c_email'],
				$data['c_email_show'],
				$data['form_email'],
				$data['phone_1'],
				$data['phone_1_type'],
				$data['phone_1_show'],
				$data['phone_2'],
				$data['phone_2_type'],
				$data['phone_2_show'],
				$data['phone_3'],
				$data['phone_3_type'],
				$data['phone_3_show'],
				$data['c_address'],
				$data['c_city'],
				$data['c_state'],
				$data['c_zip'],
				$data['c_address_type'],
				$data['c_address_show'],
				$data['show_contact'],
				$data['display_contact'],
				str_replace('\n', '<br />', $data['bus_hours']),
				$data['show_bus_hours'],
				$data['aw3'],
				$data['live'],
				$data['use_access'],
				$data['use_seo'],
				1
			));
		}
		catch(PDOException $ex){
			$errno = $ex->getCode();
		}

		print_mysql_message ( $errno , $module_name, $id, $type ) ;

  } else {
		add( $errors );
	}//end if	

}//end function
?>