<?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Content Pages

// Written By : Shawn Crigger of EWD

// Updated On : May 24, 2010

// Copyright Zeal Technologies

//

//   _____      __      __ _       _               

//  / ____|     \ \    / /(_)     (_)              

// | (___  ______\ \  / /  _  ____ _   ___   _ __  

//  \___ \|______|\ \/ /  | ||_  /| | / _ \ | '_ \ 

//  ____) |        \  /   | | / / | || (_) || | | |

// |_____/          \/    |_|/___||_| \___/ |_| |_|

//

//***************************************************/





/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);



$module_name = get_module_name();



/**

 *

 *  Setup Arrays for Pages That Can Not Be Deleted, or Have Subpages Added

 *

 */

 

$bad_subs = array('2','3','4','41','45');

$bad_del  = array('1','2','3','4','5','6','41','45');





function execute()

{

	switch($_GET['action'])

 {

		case 'update':

			update();

			break;

		case 'add':

			update();

			break;

		case 'addsub':

			update();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 *

 **************************************************************/



function manage()
{
   

   global $action, $id, $bad_subs, $bad_del;

		

			$link = '<a href="./?tool=pages&action=add">Add a Website Page</a>';

			print_header('Main Website Pages', $link );



			$th = "\n";

			$s  = 0;

		 if ( is_saint() )

			{

					$th = '<th style="width:50px; text-align:center; font-weight:bold;"><h3>ID</h3></th>' . "\n";

					$s  = 3;

			}

		

		 echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" .

								'<thead>' . "\n" .

								'	<tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" .

								$th . 

								'		<th><h3>Navigation Name</h3></th>' . "\n" .

								'		<th style="width:50px"><h3>Order</h3></th>' . "\n" .

								'		<th style="width:50px"><h3>Active</h3></th>' . "\n" .						

								'		<th style="width:150px" class="nosort" colspan="3" ><h3>Tools</h3></th>'.  "\n" .

								'	</tr></thead><tbody>' . "\n";

																

				$query = mysql_query("SELECT * FROM " . db_prefix . "pages WHERE onpage_id = '0' AND type = 'main page' ORDER BY weight");

				$pages = mysql_num_rows ( $query );

				while ( $row = mysql_fetch_assoc ( $query ))

				{

						$active = yes_no ( $row['active'] );

		

						$td = '';

						if ( is_saint() )

								$td = '  <td style="width:50px; text-align:center; font-weight:bold;">'.$row['id'].'</td>' ."\n";

								

						echo '<tr>' ."\n" . $td . 

											'  <td >'.$row['nav_name'].'</td>' ."\n" . 

											'  <td style="width:50px; text-align:center;">'.$row['weight'].'</td>' ."\n" . 

											'  <td style="width:50px; text-align:center;">'.$active.'</td>' ."\n" . 

											'  <td style="width:50px; text-align:center;"><a href="./?tool=pages&action=update&id='.$row['id'].'">update</a></td>' ."\n" . 

											'  <td style="width:50px; text-align:center;">';

													

      echo ( in_array($row['id'], $bad_subs ) ) ? '<span class="notool">add subpage</span>' : '<a href="./?tool=pages&action=addsub&id='.$row['id'].'">add subpage</a>';					

						

						echo '</td> ' ."\n" . 

											'<td style="width:50px; text-align:center;">' ."\n" ;

																										

						echo ( in_array($row['id'], $bad_del ) ) ? '<span class="notool">remove</span>' : '<a href="./?tool=pages&action=remove&id='.$row['id'].'">remove</a>'; 																			

						

						echo '</td></tr>' ."\n";

				} // end while

				

				echo '</tbody></table>';

		

				if ( $pages > 20 )

					$pages = true;

				else

					$pages = false;

				

				echo_js_sorter ( $pages, $s );

								

				print_header('Website Sub-Pages');



				$th = "\n";

				$s  = 0;

			 if ( is_saint() )

				{

						$th = '<th style="width:50px; text-align:center; font-weight:bold;"><h3>ID</h3></th>' . "\n";

						$s++;

				}

				

				echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table1">' . "\n" .

									'<thead>' . "\n" .

									'	<tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" . $th .  

									'		<th><h3>Navigation Name</h3></th>' . "\n" .

									'		<th><h3>Under Navigation</h3></th>' . "\n" .

         '		<th style="width:50px"><h3>Order</h3></th>' . "\n" .

									'		<th style="width:50px; text-align:center;" ><h3>Active</h3></th>' . "\n" .						

									'		<th style="width:10px; text-align:center;" class="nosort" colspan="2" ><h3>Tools</h3></th>'.  "\n" .

									'	</tr></thead><tbody>' . "\n";

																						

				$query2   = mysql_query("SELECT * FROM " . db_prefix . "pages WHERE type = 'subpage' ORDER BY onpage_id,label");

				$totRows2 = mysql_num_rows($query2);

				if($totRows2 == '0')

				{

						echo '<tr><td colspan="5" class="info"><strong>No Subpages Found</strong></td></tr>';

				} else { // if there are subpages

						

						while($row2 = mysql_fetch_assoc($query2))

						{

						

								$active = yes_no ( $row2['active'] );

		

		// find main pages for the subpages

								$query3 = mysql_query("SELECT * FROM " . db_prefix . "pages WHERE id = '".$row2['onpage_id']."'");

								$row3   = mysql_fetch_assoc($query3);



								$td = '';

								if ( is_saint() )

										$td = '  <td style="width:50px; text-align:center; font-weight:bold;">'.$row2['id'].'</td>' ."\n";



																						

								echo '<tr>' . "\n" . $td . 

													'			<td >'.$row2['nav_name'].'</td>' . "\n" .

													'   <td >'.$row3['nav_name'].'</td>' . "\n" .

             '   <td style="width:50px; text-align:center;">'.$row3['weight'].'</td>' ."\n" . 

													'   <td style="width:50px; text-align:center;">'.$active.'</td>' . "\n" .

													'   <td style="width:50px; text-align:center;"><a class="tools" href="./?tool=pages&action=update&id='.$row2['id'].'">update</a></td>' . "\n" .

													'			<td style="width:50px; text-align:center;">';	

									

        echo ( in_array($row2['id'], $bad_del ) ) ? '<span class="notool">remove</span>' : '<a href="./?tool=pages&action=remove&id='.$row2['id'].'">remove</a>';

								

								echo '</td></tr>' . "\n";

						} // end while

				} // end if totRows2

		

				echo '</tbody></table>';

				

				if ( $totRows2 > 11 )

					$pages = true;

				else

					$pages = false;

				

				echo_js_sorter ( $pages, $s, 'table1', 1 );

		

				echo '<div class="spacer">&nbsp;</div>' . "\n";

 

}





/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 *

 **************************************************************/



function remove()

{

		global $id, $module_name;

		

		$query = mysql_query("SELECT id,nav_name FROM " . db_prefix . "pages WHERE id = '".$id."'");

		$row = mysql_fetch_assoc($query);

		$totalRows = mysql_num_rows($query);

	

	

		print_header('Delete a Website Page - ' . $row['nav_name'] );	

		// check if subpages are attached to the page being deleted

		$query2 = mysql_query("SELECT id FROM " . db_prefix . "pages WHERE onpage_id = '".$row['id']."'");

		$row2 = mysql_fetch_assoc($query2);

		$totalRows2 = mysql_num_rows($query2);



		if($totalRows == '0' || empty($id))

		{

				echo '<p class="error_message">This page does not exist.</p>';

		} else if($row['id'] == '1' || $row['id'] == '3') { // home page

				echo '<p class="error_message">The "'.$row['nav_name'].'" page can NOT be deleted.</p>';

		} else if($row['id'] == '2' || $row['id'] == '4') { // contact page

				echo '<div class="error_message">The "'.$row['nav_name'].'" page can NOT be deleted.<br />

										To make this page inactive, <a class="tools" href="./?tool=pages&action=update&id='.$row['id'].'">click here</a> and set "Active?" to "No."</div>';

		} else if($totalRows2 > '0') {

				echo '<div class="error_message">WARNING: You have '.$totalRows2.' subpages attached to this page.<br />You have to move or remove the subpages first.<br/>' . "\n" .

									'Click <a class="tools" href="./?tool=pages">here</a> to manage pages</div>'; 

		} else { 



		if(!empty($_POST))

		{ 

  			

				$deleteSQL = mysql_query("DELETE FROM " . db_prefix . "pages WHERE id = '".$row['id']."'");

			

				print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

		

		} else {

				echo '<form action="./?tool=pages&action=remove" method="post" name="form">' . "\n" .								 

									'<input type="hidden" name="id" value="' . $id . '"><table width="80%" border="0">' . "\n" .

									'<tr> <td><p style="text-align:center"><strong>You are about to Delete the "'.$row['nav_name'].'" page.</strong></p></td></tr>' . 		

									'<tr> <td><div align="center">Are you sure you want to delete this record?</div></td></tr>' . "\n" .

									'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

									'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=pages\'"></div></td></tr>' . "\n" .

									'</table></form>';

				

				

		} // end if POST

	} // end if totrows

}	

		



/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

		$r_data['label']       = mysql_real_escape_string ( $data['label'] );

		$r_data['nav_name']    = mysql_real_escape_string ( $data['nav_name'] );

		$r_data['pagecontent'] = mysql_real_escape_string ( stripslashes ( $data['pagecontent'] ));

		$r_data['type']        = mysql_real_escape_string ( $data['type']);

		$r_data['onpage_id']   = mysql_real_escape_string ( $data['onpage_id'] );

		$r_data['meta_title']  = mysql_real_escape_string ( $data['meta_title'] );

		$r_data['meta_kw']     = mysql_real_escape_string ( $data['meta_kw'] );

		$r_data['meta_descr']  = mysql_real_escape_string ( $data['meta_descr'] );



		$r_data['show_in_nav'] = intval ( $data['show_in_nav'] );

		

		$r_data['weight'] = intval ( $data['weight'] );		

		$r_data['active'] = intval ( $data['active'] ) ;

	

		return $r_data;

}







function add( $errors )

{

		global $action, $id, $bad_subs;

		

		$base_path = dirname(__FILE__);

		

		include_once $base_path . '/ckeditor/ckeditor.php' ;

		$option = '';

		

		if ($action == "update")

		{

				// Run the query for the existing data to be displayed.

				$results = mysql_query('SELECT * FROM ' . db_prefix . 'pages WHERE id = "' . $id . '"');

				$row 				= mysql_fetch_array($results);

				print_header ('Update A Website Page');

		} elseif ( $action == 'addsub' ) {

				$row     = sanitize_vars ( $_POST );

				

				$results = mysql_query("SELECT id,type,active,nav_name FROM " . db_prefix . "pages WHERE type = 'main page' and id = '" . $id . "'");

				$parent 	= mysql_fetch_array($results);

    

				$row['onpage_id'] = $parent['id'];

				$row['type']      = 'subpage';



    if ( !array_key_exists ('submit',$_POST))

      $row['active'] = 1;

								

				$option           = '						<option value="'.$id.'" SELECTED>' . $row['nav_name'] . '</option>' . "\n";

								

				print_header ('Add A Website Sub-Page');

		

		} else {

    $row     = sanitize_vars ( $_POST );

    if ( !array_key_exists ('submit',$_POST))

    {

      $row['active']      = 1;

      $row['show_in_nav'] = 1;

    

    }

    /*

    if ( intval ( $row['weight'] )  == 0 )

      $row['weight'] = fetch_weight ( 'pages');    

    */

    

				print_header ('Add A Website Page');

  }//end if



			if ( $errors != '' )

			{						

					//$errormsg = implode(' ', $errors);

					echo '<div class="error_message"><strong>ERROR:</strong><br />'.$errors.'</div>';

			} else {

					echo '<div class="notice_message">To edit this page, fill out the form and click submit.<br />' . "\n" .

												'<ul><strong>NOTES:</strong>' . "\n" .

												'<li> The Page Label is what will appear at the top of the content area for that page.</li>' . "\n" .

												'<li> The Navigation Name is what will appear in the navigation area.</li>' . "\n" .

            '<li> Hold <strong>Shift</strong> and press <strong>Enter</strong> at same time to Start a new paragraph.</li>' . "\n" .

												'<li> Order is the order it will appear in the navigation bar. Use 0 - 999. </li> </div>' . "\n";

			}

			

			

		$r = required();

		

		echo '<form name="signup" id="signup" method="post" action="#" ><table>' . "\n";			

		echo '<input type="hidden" name="id"   value="' . $id . '" />';



		echo '   <tr>' . "\n" .

							'					<td  class="labelcell"> <label for="label">'.$r.' Page Label: </label></td>' . "\n" .

							'				 <td > <input ' . $val_label . ' type="text" name="label" id="label" value="'. $row['label'] .'" size="45" maxlength="100" tabindex="1" />' . "\n" .

							' <span class="note">(Up to 100 characters)</span></td>' . "\n" .

							'			 </tr>' . "\n";




		echo '   <tr>' . "\n" .

							'					<td  class="labelcell"> <label for="nav_name">'.$r.' Navigation Name: </label></td>' . "\n" .

							'				 <td > <input ' . $val_label . ' type="text" name="nav_name" id="nav_name" value="'. $row['nav_name'] .'" size="45" maxlength="50" />' . "\n" .

							' <span class="note">(Recommend 23 characters or less)</span></td>' . "\n" .

							'			 </tr>' . "\n";						

		

  if ( !in_array($row['id'], $bad_subs ) )  							

		{

			
					echo '   <tr>' . "\n" .    

										'					<td  class="labelcell"><label for="type">' . $r . ' Page Type :</label></td>  ' . "\n" .

										'						<td >' . "\n" .

										'						<select id="type" name="type">' . "\n" .							

										'									<option value="main page" '; if ( $row['type'] == 'main page' ) echo ' SELECTED'; echo ' >Main Page</option>' . "\n" .

										'									<option value="subpage" '; if ( $row['type'] == 'subpage' ) echo ' SELECTED'; echo ' >Subpage</option>' . "\n" .							

										'						</select>' . "\n" .

										'						</td>' . "\n" .

										'				</tr>  '. "\n";

	

					echo '<tr>' . "\n" .										

										'<td colspan="2" style="width:100%" ><span class="note">If this page is a subpage, you can choose below which page it will appear under.<br />' . "\n";

			

					echo '(';

					if ( ($row['type'] == 'main page') || ( $action='add' ))

					{

								echo 'This page is presently not a subpage.';

					} else {

							$query2 = mysql_query("SELECT id,nav_name FROM pages WHERE id = '".$row['onpage_id']."'");

							$row2   = mysql_fetch_assoc($query2);											



							echo 'This page is presently a subpage under the "'.$row2['nav_name'].'" page.';

					}

					echo ')</span>' . "\n" .

										'</td></tr>' . "\n";



			
					echo '   <tr>' . "\n" .    

										'					<td  class="labelcell"><label for="onpage_id">' . $r . ' Main Page :</label></td>  ' . "\n" .

										'						<td >' . "\n" .

										'						<select id="onpage_id" name="onpage_id">' . "\n" .	$option .						

										'									<option value="0" '; if ( $row['onpage_id'] == '0' ) echo ' SELECTED'; echo ' >Not a Subpage</option>' . "\n";

				

				

					$query3 = mysql_query("SELECT id,type,active,nav_name FROM " . db_prefix . "pages WHERE type = 'main page' AND active = '1' ORDER by nav_name");

					while($row3 = mysql_fetch_assoc($query3))

					{

								echo '									<option value="'.$row3['id'].'" '; if ( $row['onpage_id'] == $row3['id'] ) echo ' SELECTED'; echo ' >' . $row3['nav_name'] . '</option>' . "\n";				

					} 

																																					

					echo '						</select>' . "\n" .

										'						</td>' . "\n" .

		

  								'				</tr>  '. "\n";		

  } else {

    $row['type'] = 'main page';

  } // end if id ! 1 or 3 

		

		echo '<tr><td colspan="2" style="width:100%">&nbsp;</td></tr>' . "\n";



		echo '<tr><td colspan="2" style="text-align:center; width:100%">' . "\n" .

							'<textarea style="min-height:270px; height:270px; width:100%;" name="pagecontent" id="pagecontent">' . $row['content'] . '</textarea>' . "\n" .

							'</td></tr>' . "\n";



		$ckeditor  = new CKEditor( ) ;

		$ck_config = set_fck_config();

  $ck_events = set_fck_events();

  

		$ck_config['toolbar'] = set_fck_toolbar();

		$ck_config['height']  = 200;

		$ck_config['width']   = '113%';



// EDIT BELOW LINE TO ADD YOUR WEBSITE STYLESHEET

//		$ck_config['contentsCss'] = $base_site_url . '/styles/main.css';

		$ckeditor->replace("pagecontent", $ck_config, $ck_events);

							



	 echo '<tr><td colspan="2" style="width:100%">&nbsp;</td></tr>' . "\n";



		echo '<tr><td colspan="2" style="width:100%"><p class="center"><strong>SEARCH ENGINE OPTIMIZATION</strong></p>' . "\n" .

							'<p style="text-align:left;"><strong>Meta Tags:</strong> The keywords and keyphrases need ' . "\n" .

							'to be in the content of the page and in all three meta tags below. Do NOT use special ' . "\n" .

							'characters in the text.<br />' . "\n" .

							'<strong>Title Example:</strong> Cougars in the wild, Cougars in America<br />' . "\n" .

							'<strong>Keyword Example:</strong> cougars in the wild,cougars in ' . "\n" .

							'america,cougars,wild,america<br />' . "\n" .

							'<strong>Description Example:</strong> There are very few cougars in the wild with ' . "\n" .

							'only a small amount of cougars in America.</p>' . "\n" .

							'<p><span class="note">NOTE: keywords and phrases are seperated only with a comma. ' . "\n" .

							'Description should be under 150 characters long.</span></p>' . "\n" .

						 '</td></tr>' . "\n";






		echo '   <tr>' . "\n" .

							'					<td  class="labelcell"> <label for="meta_title">Meta Title: </label></td>' . "\n" .

							'				 <td >' . "\n" . 

							'							<textarea style="width:80%" name="meta_title" id="meta_title" cols="70" rows="2" tabindex="' . $t_index . '">' . $row['meta_title'] . '</textarea>' . "\n" .							

							'						</td>' . "\n" .

							'			 </tr>' . "\n";						




		echo '   <tr>' . "\n" .

							'					<td  class="labelcell"> <label for="meta_kw">Meta Keywords: </label></td>' . "\n" .

							'				 <td >' . "\n" . 

							'							<textarea style="width:80%" name="meta_kw" id="meta_kw" cols="70" rows="2" tabindex="' . $t_index . '">' . $row['meta_kw'] . '</textarea>' . "\n" .							

							'						</td>' . "\n" .

							'			 </tr>' . "\n";						




		echo '   <tr>' . "\n" .

							'					<td  class="labelcell"> <label for="meta_descr">Meta Description: </label></td>' . "\n" .

							'				 <td >' . "\n" . 

							'							<textarea style="width:80%" name="meta_descr" id="meta_descr" cols="70" rows="2" tabindex="' . $t_index . '">' . $row['meta_descr'] . '</textarea>' . "\n" .							

							'						</td>' . "\n" .

							'			 </tr>' . "\n";						



		echo '<tr><td colspan="2" style="width:100%">&nbsp;</td></tr>' . "\n";

		

		if($row['id'] != "3")

		{



		
				echo '   <tr>' . "\n" .

									'					<td  class="labelcell"> <label for="weight">Order: </label></td>' . "\n" .

									'				 <td > <input ' . $val_weight . ' type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />' . "\n" .

									'<span class="note" style="padding-left:5px;"> ( Recommend increments of 10 ) </span></td>' . "\n" .									

									'			 </tr>' . "\n";						





			
					echo '   <tr>' . "\n" .    

										'					<td  class="labelcell"><label for="show_in_nav">Show In Navigation?</label></td>  ' . "\n" .

										'						<td >' . "\n" .

										'						<select id="show_in_nav" name="show_in_nav">' . "\n" .							

										'									<option value="1" '; if ( $row['show_in_nav'] == '1' ) echo ' SELECTED'; echo ' >Yes</option>' . "\n" .

										'									<option value="0" '; if ( $row['show_in_nav'] == '0' ) echo ' SELECTED'; echo ' >No</option>' . "\n" .							

										'						</select>' . "\n" .

										'<span class="note">("Yes" will show link in navigation area - "No" allows it to be hidden but available)</span>' . "\n" .

										'						</td>' . "\n" .

										'				</tr>  '. "\n";

																												

		} // end if id != 3 



		if ( !in_array($row['id'], $bad_subs ) )

		{ 

			
					echo '   <tr>' . "\n" .    

										'					<td  class="labelcell"><label for="active">Active?</label></td>  ' . "\n" .

										'						<td >' . "\n" .

										'						<select id="active" name="active">' . "\n" .							

										'									<option value="1" '; if ( $row['active'] == '1' ) echo ' SELECTED'; echo ' >Yes</option>' . "\n" .

										'									<option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>' . "\n" .							

										'						</select>' . "\n" .

										'<span class="note">("No" will cause the page to be unviewable)</span>' . "\n" .

										'						</td>' . "\n" .

										'				</tr>  '. "\n";

  } else {

    $row['active'] = 1;

  } // end if id != 1 or 3 





		echo '<tr><td colspan="2" style="width:100%">&nbsp;</td></tr>'. "\n";

		echo '<tr><td colspan="2" style="width:100%; text-align:center; margin:0 auto; padding:3px; "><input name="Submit" type="submit" value="Submit" class="button"> </td></tr>'. "\n";

		echo '</table></form>'. "\n";

		echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>' . "\n";

		echo '<div class="spacer">&nbsp;</div>';





}//end if empty get id





function update()

{

		global $action, $id, $module_name, $bad_del, $bad_subs;

		

		if ( !empty($_POST ) )

		{

				$query = mysql_query("SELECT * FROM " . db_prefix . "pages WHERE id = '$id'");

				$row   = mysql_fetch_assoc($query);

				$totRows = mysql_num_rows($query);

	

				$data = sanitize_vars ( $_POST );

		

				if ( ( $id == '0' || $totRows == '0') && ( $action == 'update' ))

				{

					echo '<div class="error_message">This page does not exist.</div>';

					exit;

				} 

 

		}

		

		if ( ( $action == 'update' ) && ( !empty($_POST) ) )

		{

				$error = false;

				if (($data['label'] == ""))

				{

						$error = true;

						$errormsg .= "The Page Label is required.<br />";

					}//end if

					// Query to search for duplicate label

					$results = mysql_query("SELECT id FROM " . db_prefix . "pages WHERE label = '".$data['label']."' AND id != '".$row['id']."'");

					if (mysql_num_rows($results) > 0)

					{

						$error = true;

						$errormsg .= "This Page Label is already in use.<br />";

					}//end if

					if ($data['nav_name'] == "")

					{

							$error = true;

							$errormsg .= "The Navigation Name is required.<br />";

					}//end if

					// Query to search for duplicate nav name

					$results = mysql_query("SELECT id FROM " . db_prefix . "pages WHERE nav_name = '".$data['nav_name']."' AND id != '".$row['id']."'");

					if (mysql_num_rows($results) > 0)

					{

							$error = true;

							$errormsg .= "This Navigation Name is already in use.<br />";

					}	//end if

					// Query: if this is a main page that is being changed to subpage, make sure no subpages are attached

					if ($data['type'] == "subpage")

					{

						$results = mysql_query("SELECT id FROM " . db_prefix . "pages WHERE onpage_id = '".$row['id']."'");

						if (mysql_num_rows($results) > 0)

						{

							$error = true;

							$errormsg .= "This page must remain a Main Page while it has subpages under it.<br />";

						}//end if

					}//end if subpage

					if ($data['type'] == "subpage" && $data['onpage_id'] == $row['id'])

					{

						$error = true;

						$errormsg .= "You can not place a subpage under itself.<br />";

					}//end if

					if ($data['type'] == "main page" && $data['onpage_id'] != '0' && $row['id'] != '1')

					{

						$error = true;

						$errormsg .= "Only a subpage can be listed under another page's navigation.<br />";

					}//end if

					if ($data['type'] == "subpage" && $data['onpage_id'] == '0' && $row['id'] != '1')

					{

						$error = true;

						$errormsg .= "A subpage must be listed under another page's navigation.<br />";

					}//end if

					if ($row['id'] != '3')

					{

							if($data['weight'] < "0" || !is_numeric($data['weight']))

							{

									$error = true;

									$errormsg .= "The Order must be between 0 and 999.<br />";

							} // end if

					}// end if

			

				if (!$error)

				{

						if($row['id'] == "1")

						{

							$onpage_id = $row['onpage_id'];

							$type = $row['type'];

							$link = $row['link'];

							$active = $row['active'];

							$weight = $data['weight'];

						} else 

						if($row['id'] == "3")

						{

							$onpage_id = $row['onpage_id'];

							$type = $row['type'];

							$link = $row['link'];

							$active = $row['active'];

							$weight = $row['weight'];

						} else

      if(in_array($row['id'], $bad_del) || in_array($row['id'], $bad_subs) )

      {

        $onpage_id = 0;

        $type      = 'main page';

        $link      = $row['link'];

        $active    = '1';

        $weight    = $row['weight'];

      } else

      { // all other pages

							if($_POST['type'] == 'main page') {

								$onpage_id  = '0';

						} else {

								$onpage_id  = $data['onpage_id'];

						} // end if main page

							$type = $data['type'];

							//make page link

							$makelink = $data['nav_name'];

							//remove bad characters function from includes/secure.inc.php

							$link = rembadchar($makelink);

							$active = $data['active'];

							$weight = $data['weight'];

				} // end if id

				

				$data['pagecontent'] = strip_fck_strip_value( $data['pagecontent']) ;

				

				$updateSQL = "UPDATE " . db_prefix . "pages SET onpage_id = '".$onpage_id."', type = '".$type."', 

					label = '".$data['label']."', nav_name = '".$data['nav_name']."', link = '".$link."', 

					content = '".$data['pagecontent']."', meta_title = '".$data['meta_title']."', 

					meta_kw = '".$data['meta_kw']."', meta_descr = '".$data['meta_descr']."', 

					weight = '".$weight."', show_in_nav = '". intval ( $_POST['show_in_nav'] ) ."', 

					active = '".$active."', date = '".date('Y-m-d')."' WHERE id = ".$row['id'];

				

				mysql_query($updateSQL);

						

				$msg = 'The page "'.$data['nav_name'].'" has been updated.';

					

				print_mysql_message ( mysql_errno() , $module_name, $id, 0 , $msg) ;   	

					

			}//end if (!$error)

		} elseif ( ( $action == 'add' || $action == 'addsub' ) && ( !empty($_POST) ) )

		{

		

				if ($id != '0' )

				{ // if this is a subpage

						$query_get = mysql_query("SELECT id,type,nav_name FROM pages WHERE id = '".$id."'");

						$row_get = mysql_fetch_assoc($query_get);

				} // end if

				if ($row_get['type'] == 'subpage' || $row_get['id'] == '3' || $row_get['id'] == '4')

				{ // can't add subpage

						echo '<p class="error_message">You can not add a subpage to the "'.$row_get['nav_name'].'" page.</p>';

						exit;

				} 

				

					$error = false;

					if ($data['label'] == "")

					{

							$error = true;

							$errormsg .= "The Page Label is required.<br />";

					}//end if

			// Query to search for duplicate label

			

					$results = mysql_query("SELECT id FROM " . db_prefix . "pages WHERE label = '".$data['label']."'");

					if (mysql_num_rows($results) > 0)

					{

							$error = true;

							$errormsg .= "This Page Label is already in use.<br />";

					}//end if

					if ($data['nav_name'] == "")

					{

							$error = true;

							$errormsg .= "The Navigation Name is required.<br />";

					}//end if

					// Query to search for duplicate nav name

					$results = mysql_query("SELECT id FROM " . db_prefix . "pages WHERE nav_name = '".$data['nav_name']."'");

					if (mysql_num_rows($results) > 0)

					{

							$error = true;

							$errormsg .= "This Navigation Name is already in use.<br />";

					}//end if

					if ($data['weight'] < "0" || !is_numeric($data['weight']))

					{

							$error = true;

							$errormsg .= "The Order must be between 0 and 999.<br />";

					}//end if

			

					if (!$error)

					{

							

							$makelink = $data['nav_name'];					

							$link = rembadchar($makelink);

				

							if (!empty($id))

							{

									$onpage_id = $row_get['id'];

									$type = 'subpage';

							} else {

								$onpage_id = '0';

								$type = 'main page';

							} // end if

					

				$data['pagecontent'] = strip_fck_strip_value( $data['pagecontent']) ;

				

				$insertSQL = "INSERT INTO " . db_prefix . "pages (onpage_id, type, label, nav_name, link, content, meta_title, 

					meta_kw, meta_descr, weight, show_in_nav, active, date) VALUES ('".$onpage_id."', '".$type."', 

					'".$data['label']."', '".$data['nav_name']."', '".$link."', '".$data['pagecontent']."', 

					'".$data['meta_title']."', '".$data['meta_kw']."', '".$data['meta_descr']."', 

					'".$data['weight']."', '".$data['show_in_nav']."', '".$data['active']."', 

					'".date('Y-m-d')."')";

		

				mysql_query($insertSQL);

			  

				$msg = 'The page "'.$data['nav_name'].'" has been added.';

				print_mysql_message ( mysql_errno() , $module_name, $id, 1, $msg ) ;

				

			}// end if !$error

		} else {

			

			add($errormsg);

		}

}

		

		

?>