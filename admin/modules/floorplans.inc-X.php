<?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Floor Plans Module

// About			: The Floor Plans Module is intended for use with

//							the communities module and companies that deal with communities. 

//							For example: A construction company that builds and sales homes 

//							in many different communities.



// Written By : Darrell Bessent of EWD

// Written On : May 3, 2013

// Revision   : 163

// Updated by : Darrell Bessent of EWD

// Updated On : May. 3, 2013



// Copyright Zeal Technologies

//***************************************************/



/***************************************************************

 *

 * Check for and assign our id and action varibles

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);





$mod_config  = get_module_settings ( 'floorplans' );



$upload_floorplan_url  = $site_base_url . $cms_uploads . 'floorplans/floorplan/';

$upload_floorplan_dir  = $site_base_dir . $cms_uploads . 'floorplans/floorplan/';



$upload_options_url  = $site_base_url . $cms_uploads . 'floorplans/options/';

$upload_options_dir  = $site_base_dir . $cms_uploads . 'floorplans/options/';



$upload_image_url  = $site_base_url . $cms_uploads . 'floorplans/image/';

$upload_image_dir  = $site_base_dir . $cms_uploads . 'floorplans/image/';



$image_width  = '200';

$image_height = '93';   



// Dont forget to set up the actual sizes

$floorplan_width  = '700';

$floorplan_height = '800'; 



function execute()

{

	switch($_GET['action'])

	{

		case 'update':

			update();

			break;

		case 'add':

			update();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Slider Content

 *

 *

 **************************************************************/



function manage()

{



  global $upload_floorplan_url,$upload_floorplan_dir ;

  

  $i    = 0;

  $link = '<a href="./?tool=floorplans&action=add">Add A Floor Plan</a>';

  

  print_header('Manage Floor Plans',$link);

  

  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 

       ' 	  <thead>' . "\n" . 

       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" . 

       ' 			  <th><h3>Plan</h3></th>' . "\n" . 

			 ' 			  <th><h3>Floor Plan</h3></th>' . "\n" . 

       ' 			  <th style="width:50px; text-align:center;"><h3>Order</h3></th>' . "\n" . 

       ' 			  <th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . 

       ' 			  <th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" . 

       '		 </tr></thead><tbody>' . "\n";

  

  $results = mysql_query('SELECT * FROM ' . db_prefix . 'floorplans ORDER BY weight,plan');

  $pages   = mysql_num_rows($results);

  while ($row = mysql_fetch_array($results))

  {    



    $i++;

    

    $link  = 'No Image Uploaded';

    $plan  = stripslashes ( $row['plan'] );

		

    $floorplan_filename = $upload_floorplan_dir . $row['floorplan'];

    

    $link = ( file_exists ( $floorplan_filename ) ) ? '<a href="' . $upload_floorplan_url . $row['floorplan'] . '" alt="' . $plan . '" title="' . $plan . '" class="lightbox" >' . $row['floorplan'] . '</a>' : $link;

                

    $act = yes_no ( $row['active'] );

    

    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

    echo '<td style="padding-left:20px;">' . $plan . '</td>' . "\n";

		echo '<td style="padding-left:20px;">' . $link . '</td>' . "\n";

    echo '<td text-align:center;">' . $row['weight'] . '</td>' . "\n";

    echo '<td text-align:center;">' . $act . '</td>' . "\n";

    echo '<td valign="middle"><strong><a href="./?tool=floorplans&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=floorplans&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

    echo '</tr>' . "\n";

	  

  }//end while



  echo '</tbody></table>';



  $pages = ( $pages > 20 ) ? true : false;

	

  echo_js_sorter ( $pages );

             

}//end function



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Banner Form for User Input

 *

 *

 **************************************************************/

// START HERE ON MONDAY

function add( $errors = '' )

{

  global $id, $action, $site_base_url, $upload_floorplan_url, $upload_floorplan_dir, $upload_options_url, $upload_options_dir, $upload_image_url, $upload_image_dir, $mod_config, $image_width, $image_height, $floorplan_width, $floorplan_height;

   

   

  if ( $errors )

  {

   echo '<div class="error_message">';

   if ( in_array('plan', $errors ) )

     echo '* You must fill out a plan name.<br/>';

   echo '</br></div>' . "\n";

  } else {

    $c = ( $action == 'update' ) ? 1 : 0;

    print_notice_message ( $c );

  }

  

  if ($action == "update")

  {

   // Run the query for the existing data to be displayed.

   $results = mysql_query('SELECT * FROM ' . db_prefix . 'floorplans WHERE id = "' . $id . '"');

   $row 		= mysql_fetch_array($results);

  } 

	if (!empty ($_POST))

	{	

   $row     = sanitize_vars ($_POST);

  }//end if

  

	echo '<div class="notice_message">NOTES : <br /> 

					<ul>

						<li>The `PLAN TYPE` is type of plan the floor plan belongs too.</li>

						<li>The `FLOOR PLAN LABEL` is title of the floor plan you are adding.</li>

						<li>The `COMMUNITY` is community that offers the floor plan you are adding.</li>

						<li>The `ORDER` controls the display order on the floor plans page.</li>

						<li>The `ACTIVE` is to set the floor plan post visability to on or off.</li>

					<ul>	

				</div>';

	

  echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

  $t_index = 1;

	// Run the query for the existing data to be displayed.

   $pt_results = mysql_query('SELECT * FROM ' . db_prefix . 'plantypes');



		echo '   <tr>' . "\n" .

							' 						<td ><label for="ptid">Plan Type</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="ptid" name="active">' . "\n";

							

					while ($pt_row 		= mysql_fetch_array($pt_results))

					{

						echo		' 			<option value="'.$pt_row['id'].'" '; if ( $row['ptid'] == $pt_row['id'] ) echo 'SELECTED'; echo ' >'.$pt_row['type'].'</option>' . "\n";

					}

					

			   echo ' 					  </select>' . "\n" . 

						  ' 						<span class="note" style="padding-left:5px;"> ( Please select the new floor plan\'s plan type. ) </span>' . "\n" . 

     				  ' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";



  $t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="plan">Floor Plan Label</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="plan" id="plan" value="'. $row['plan'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

	

	// Build Community Option Boxes

	$qry_com = mysql_query("SELECT id,label FROM cms_communities WHERE `active`='1' ORDER BY weight");

	$totnum = mysql_num_rows($qry_com);

	$com_options = '';

	

	// Check if selected

		$qry2 = mysql_query("SELECT comm_id FROM cms_floorplans WHERE `id`='$id'");

		$row2 = mysql_fetch_array($qry2); // fetch array

		$iscom = explode(',', $row2['comm_id']); // explode it

	while ($row_com = mysql_fetch_assoc($qry_com))

	{



		if (in_array($row_com['id'], $iscom)) //use in_array to check if in array

		{

			$checked = 'checked';

		} else

			{

				$checked = '';

			}

		// End check if selected



		$com_options .= '<input name="comm_id[]" type="checkbox" value="'.$row_com['id'].'" '.$checked.'>&nbsp;<label for="'.$row_com['label'].'">'.$row_com['label'].'</label><br />';

	}

	// End of Build Community Options Boxes

	// Print out the community options boxes

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="comm_id">Community</label></td>  ' . "\n" . 

       '      <td >'.$com_options.'</td>  ' . "\n" . 

       '   </tr>' . "\n";

	// end print out

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="sqft">Square Feet</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="sqft" id="sqft" value="'. $row['sqft'] .'" size="45" tabindex="' . $t_index . '" maxlength="5" /></td>  ' . "\n" .  

       '   </tr>' . "\n";

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="hsqft">Heated Square Feet</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="hsqft" id="hsqft" value="'. $row['hsqft'] .'" size="45" tabindex="' . $t_index . '" maxlength="5" /></td>  ' . "\n" .  

       '   </tr>' . "\n";

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="usqft">Unheated Square Feet</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="usqft" id="usqft" value="'. $row['usqft'] .'" size="45" tabindex="' . $t_index . '" maxlength="5" /></td>  ' . "\n" .  

       '   </tr>' . "\n";

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="baths">Baths</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="baths" id="baths" value="'. $row['baths'] .'" size="45" tabindex="' . $t_index . '" maxlength="5" /></td>  ' . "\n" .  

       '   </tr>' . "\n";

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="beds">Beds</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="beds" id="beds" value="'. $row['beds'] .'" size="45" tabindex="' . $t_index . '" maxlength="2" /></td>  ' . "\n" .  

       '   </tr>' . "\n";

	

  $t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="story">Story</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="story" id="story" value="'. $row['story'] .'" size="45" tabindex="' . $t_index . '" maxlength="1" /></td>  ' . "\n" .  

       '   </tr>' . "\n";

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="garage">Garage</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="garage" id="garage" value="'. $row['garage'] .'" size="45" tabindex="' . $t_index . '" maxlength="1" /></td>  ' . "\n" .  

       '   </tr>' . "\n";

	

	$t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="price">Price</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="price" id="price" value="'. $row['price'] .'" size="45" tabindex="' . $t_index . '" maxlength="10" /></td>  ' . "\n" .  

       '   </tr>' . "\n";

	

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="weight">Order</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />' . "\n" .

       ' 						<span class="note" style="padding-left:5px;"> ( Recommend increments of 10 ) </span>' . "\n" .

       '      </td>' . "\n" .

       '   </tr>' . "\n";

   

  $t_index++;

		echo '   <tr>' . "\n" .

							' 						<td ><label for="active">Active</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="active" name="active">' . "\n" . 

							' 									<option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 

							' 									<option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 

							' 						</select>' . "\n" . 

						 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to Display on Website ) </span>' . "\n" . 

       ' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";

  

  /****************************

  * Image Uploading Section

  ***************************/

  

  if ( $row['image'] != '' )

  {

    

   $image_filename      = $upload_image_dir . $row['image'];

   list ( $imgw, $imgh ) = img_size ( $image_filename );

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="current_image">Current Display Image</label></td>' . "\n";

   echo '<td ><a href="' . $upload_image_url . $row['image'] . '" alt="' . $row['plan'] . '" title="' . $row['plan'] . '" target="_blank" class="lightbox" >' . $row['image'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="current_image" value="' . $row['image'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="image">Upload Floor Plan Display Image</label></td>' ."\n";

  echo '<td ><input name="image" type="file" id="image" size="15">' . "\n";

  echo '<span class="note">( Image will be auto-magicly resized to '.$image_width.' x '.$image_height.' pixels )</span></td>' . "\n";

  echo '</tr>' . "\n";

	

  /****************************

  * Floorplan Uploading Section

  ***************************/

  

  if ( $row['floorplan'] != '' )

  {

    

   $floorplan_filename      = $upload_floorplan_dir . $row['floorplan'];

   list ( $fpw, $fph ) = img_size ( $floorplan_filename );

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="current_floorplan">Current Floor Plan</label></td>' . "\n";

   echo '<td ><a href="' . $upload_floorplan_url . $row['floorplan'] . '" alt="' . $row['plan'] . ' floor plan" title="' . $row['plan'] . ' floor plan" target="_blank" class="lightbox" >' . $row['floorplan'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="current_floorplan" value="' . $row['floorplan'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="floorplan">Upload Floor Plan</label></td>' ."\n";

  echo '<td ><input name="floorplan" type="file" id="floorplan" size="15">' . "\n";

  echo '<span class="note">( Image will be auto-magicly resized to '.$floorplan_width.' x '.$floorplan_height.' pixels )</span></td>' . "\n";

  echo '</tr>' . "\n"; 

	

	/****************************

  * Options PDF Uploading Section

  ***************************/

  

  if ( $row['options'] != '' )

  {

    

   $options_filename      = $upload_options_dir . $row['options'];

   //list ( $fpw, $fph ) = img_size ( $options_filename );

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="current_options">Current Options</label></td>' . "\n";

   echo '<td ><a href="' . $upload_options_url . $row['options'] . '" alt="' . $row['plan'] . ' options" title="' . $row['plan'] . ' options" target="_blank" class="lightbox" >' . $row['options'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="current_options" value="' . $row['options'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="options">Upload Options File</label></td>' ."\n";

  echo '<td ><input name="options" type="file" id="options" size="15">' . "\n";

  echo '</tr>' . "\n"; 



  $t_index++;

  echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";  

  echo '</table></form>' . "\n";

  echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>' . "\n";



}





/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

  global $upload_image_dir, $upload_floorplan_dir, $upload_options_dir, $mod_config, $known_photo_types, $known_file_types, $image_height, $image_width, $floorplan_height, $floorplan_width; //, $tn_height, $tn_width;

  

  $r_data['ptid']  = $data['ptid'];

  $r_data['plan']  = mysql_real_escape_string ( $data['plan'] );

	$r_data['comm_id']  = $data['comm_id'];

	$r_data['sqft']  = $data['sqft'];

	$r_data['hsqft']  = $data['hsqft'];

	$r_data['usqft']  = $data['usqft'];

	$r_data['baths']  = $data['baths'];

	$r_data['beds']  = $data['beds'];

	$r_data['story']  = $data['story'];

	$r_data['garage']  = $data['garage'];

	$r_data['price']  = $data['price'];

	$r_data['image']  = $data['current_image'];

  //$r_data['image_tn']  = $data['current_image'];

	$r_data['floorplan']  = $data['current_floorplan'];

	$r_data['options']  = $data['current_options'];

  $r_data['weight']  = intval ( $data['weight'] );

  $r_data['active'] = intval ( $data['active'] ) ;

		

  $tid = ( intval ( $data['id'] ) == 0 ) ? fetch_lastid ('floorplans') : $data['id'];

  

	// upload image    		 

  if ( !empty ( $_FILES['image']['name'] ) )

  {

            

    $old_file        = $r_data['image'];

    $image           = $_FILES['image'];

    

		

		$r_data['image'] = process_upload ( $tid , '', $image, $known_photo_types, $upload_image_dir, 1, $image_width, $image_height );

		

		//$r_data['image_tn'] = process_upload ( $tid , 'tn', $image, $known_photo_types, $upload_dir, 1, $tn_width, $tn_height );

						

    if ( ( $old_file != $r_data['image'] ) && ( file_exists ( $upload_image_dir . $old_file ) ) && ( $old_file != '' ))

		{

			unlink ( $upload_image_dir . $old_file );

			

			//$sufx = substr($old_file, -4);

			//$timg = substr($old_file,0,-4);

			//$timg = $timg.'_tn'.$sufx;

				

			//unlink( $upload_dir . $timg );

		}

  

	}

		// upload floorplan image

	  if ( !empty ( $_FILES['floorplan']['name'] ) )

  {

            

    $old_file        = $r_data['floorplan'];

    $image           = $_FILES['floorplan'];

    

		

		$r_data['floorplan'] = process_upload ( $tid , '', $image, $known_photo_types, $upload_floorplan_dir, 1, $floorplan_width, $floorplan_height );

						

    if ( ( $old_file != $r_data['floorplan'] ) && ( file_exists ( $upload_floorplan_dir . $old_file ) ) && ( $old_file != '' ))

		{

			unlink ( $upload_floorplan_dir . $old_file );

		}

  

	}

	

		// upload options file

	  if ( !empty ( $_FILES['options']['name'] ) )

  {

            

    $old_file        = $r_data['options'];

    $file            = $_FILES['options'];

		

		$r_data['options'] = process_upload_file ( $tid , $file, $known_file_types, $upload_options_dir);

						

    if ( ( $old_file != $r_data['options'] ) && ( file_exists ( $upload_options_dir . $old_file ) ) && ( $old_file != '' ))

		{

			unlink ( $upload_options_dir . $old_file );

		}

  

	}

			 

  if ( empty($_POST['weight']) )

  {

    $r_data['weight']  = fetch_weight('gallery_imgs', 'weight' );

  }

  

  return $r_data;



}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 * Unlinks Existing Photo

 *

 **************************************************************/



function remove()

{



	global $id, $upload_image_dir, $upload_floorplan_dir, $upload_options_dir, $module_name;

	

	$result = mysql_query("SELECT * FROM " . db_prefix . "floorplans WHERE id = '$id'");

	$row    = mysql_fetch_array($result);

	

	print_header('Delete Floor Plan - ' . $row['plan']);

	

	if ( !empty($_POST ))

	{		

					

		if ( file_exists ( $upload_image_dir . $row['image'] ))

		{

				@unlink( $upload_image_dir . $row['image'] );

				

				//$sufx = substr($row['image'], -4);

				//$timg = substr($row['image'],0,-4);

				//$timg = $timg.'_tn'.$sufx;

				

				//@unlink( $upload_dir . $timg );

		}

		

		if ( file_exists ( $upload_floorplan_dir . $row['floorplan'] ))

		{

				@unlink( $upload_floorplan_dir . $row['floorplan'] );

		}

		

		if ( file_exists ( $upload_options_dir . $row['options'] ))

		{

				@unlink( $upload_options_dir . $row['options'] );

		}

		

		

		$result = mysql_query('DELETE FROM ' . db_prefix . "floorplans WHERE id = '$id'");

		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

			

	} else {

	

		echo '<form action="./?tool=floorplans&action=remove" method="post" name="form">' . "\n" .

							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

							'<tr> <td><div align="center">Are you sure you want to delete this image?</div></td></tr>' . "\n" .

							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=floorplans\'"></div></td></tr>' . "\n" .

							'</table></form>';

	}//end if

}//end function





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

		global $action, $id, $module_name;

		

		if ( $action == 'update' )

				print_header('Update Floor Plan');

		else

				print_header('Add New Floor Plan');

 

		if ( array_key_exists ('submit',$_POST))

		{				

    require_once("classes/validation.php");

  

    $rules = array();    

    

    $rules[] = "required,plan,plan";

    

    $errors = validateFields($_POST, $rules);

    

		}

		

		if(!empty($_POST['comm_id'])) {

			$allcoms = implode(",", $_POST['comm_id']);

		}

		

		if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

	 {

		

				$data  = sanitize_vars( $_POST );			

		 	  $type  = ( $action == 'update' ) ? 0 : 1;

				

				if ( $action == 'update' )

						$sql = "UPDATE " . db_prefix . "floorplans SET ptid = '" . $data['ptid'] . "', plan = '" . $data['plan'] . "', 

						comm_id = '" . $allcoms . "', sqft = '" . $data['sqft'] . "', hsqft = '" . $data['hsqft'] . "', 

						usqft = '" . $data['usqft'] . "', baths = '" . $data['baths'] . "', beds = '" . $data['beds'] . "', 

						story = '" . $data['story'] . "', garage = '" . $data['garage'] . "', price = '" . $data['price'] . "', 

						floorplan = '" . $data['floorplan'] . "', image = '" . $data['image'] . "', options = '" . $data['options'] . "', 

						weight = '" . $data['weight'] . "', active = '" . $data['active'] . "'	WHERE id = '$id'";

 		 else

					 $sql = 'INSERT INTO ' . db_prefix . 'floorplans (ptid, plan, comm_id, sqft, hsqft, usqft, baths, 

					 beds, story, garage, price, floorplan, image, options, weight, active) VALUES ' .

												 "('" . $data['ptid'] . "', '" . $data['plan'] . "', '" . $allcoms . "', '" . 

												 $data['sqft'] . "', '" . $data['hsqft'] . "', '" . $data['usqft'] . "', '" . $data['baths'] . "', '" . 

												 $data['beds'] . "', '" . $data['story'] . "', '" . $data['garage'] . "', '" . $data['price'] . "', '" . 

												 $data['floorplan'] . "', '" . $data['image'] . "', '" . $data['options'] . "', '" . 

												 $data['weight'] . "', '" . $data['active'] . "')";

											

//    echo $sql;

    

				mysql_query ( $sql );

    

    if ( is_saint() && mysql_errno != 0 )

      echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

   			

				print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;   

		

  } else {

		add( $errors );

	}//end if	

}//end function







?>