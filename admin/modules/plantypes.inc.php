<?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module: Plan Types 

// Written By : Darrell Bessent of EWD

// Written On : May 09, 2013

// Updated by : Darrell Bessent of EWD

// Updated On : May 09, 2013

// Copyright Zeal Technologies & S-Vizion Software & BlackSuit IT

//***************************************************/



/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/



$id          = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action      = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);

$module_name = get_module_name();



function execute()

{

	switch($_GET['action'])

 {

		case 'update':

			update();

			break;

		case 'add':

			update();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Testimonial info

 *

 *

 **************************************************************/



function manage()

{

  global $userinfo;

 

  $i    = 0;

	$link = '<a href="./?tool=plantypes&action=add">Add a Plan Type</a>';

	

  $lvl  = $userinfo['level'];



  if ( $lvl == 0 )

  {

			$link = '';

	}

/********************************************************

 * Extra Info 

 *******************************************************/



 

	print_header('Manage Plan Types',$link);

	

 $sql = '';

	

	echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table" >' . "\n" .

						'<thead><tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" . 

						'<th><h3>Plan Type</h3></th>' . "\n" .

						'<th style="width:50px; text-align:center;"><h3>Order</h3></th>' . "\n" . 

      			'<th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . 

      $hdr .      

						'<th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" .

	'</tr></thead><tbody>';



				

		$results = mysql_query('SELECT * FROM ' . db_prefix . 'plantypes ORDER BY weight');

		$pages   = mysql_num_rows($results);

		while ($row = mysql_fetch_array($results))

		{    

   		$i++;

 

 			$act = yes_no ( $row['active'] );

     

		  echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

				echo '<td style="padding-left:20px;">' . $row['type'] . '</td>' . "\n";

				echo '<td style="text-align:center;">' . $row['weight'] . '</td>' . "\n";

				echo '<td style="text-align:center;">' . $act . '</td>' . "\n";

				//echo $tbl;

				echo '<td valign="middle"><strong><a href="./?tool=plantypes&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=plantypes&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

  		echo '</tr>' . "\n";

     

		}//end while

  echo '</tbody></table>';



	$pages = ( $pages > 20 ) ? true : false;

	

	echo_js_sorter ( $pages );

             

}//end function



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Form for User Input

 * 

 *

 **************************************************************/



function add( $errors = '' )

{

		global $id, $action, $site_base_url, $type, $mod_config;

		

	$base_path = dirname(__FILE__);

	

	include_once $base_path . '/ckeditor/ckeditor.php' ;

	$option = '';

				

		if ( $errors )

		{

			echo '<div class="error_message">';

				echo 'Please fill in the required fields.';

				if ( in_array('type', $errors ) )

				{

					echo '<br />* You must fill out a plan type.';

					$val_content = ' class="form_field_error" ';

				}

		

			echo '</div>' . "\n";

		}

			

		if ($action == "update")

		{

			// Run the query for the existing data to be displayed.

			$results = mysql_query('SELECT * FROM ' . db_prefix . 'plantypes WHERE id = "' . $id . '"');

			$row 				= mysql_fetch_array($results);

		} 

		

	if (!empty ($_POST))

	{

   $row     = sanitize_vars ($_POST); 

  }//end if





/********************************************************

 * Start Building the Testimonial Form

 *******************************************************/

	

			echo '<div class="notice_message">NOTES : <br /> 

					<ul>

						<li>The `PLAN TYPE` is the new plan type you wish to add.<br />

						<li>Setting the `ORDER` will tell the plan type where to appear within the list on the page.</li>

						<li>The `ACTIVE` is to set the plan type post visability to on or off.</li>

					<ul>	

				</div>';

		

	echo '<form name="signup" id="signup" method="post" action="#" ><table>' . "\n";			

		echo '<input type="hidden" name="id" value="' . $id . '" />';

		

		


		echo '<tr>' . "\n" .

			'<td> <label for="type">Plan Type</label></td>' . "\n" .

			'<td> <input ' . $val_name . ' type="text" name="type" id="type" value="'. $row['type'] .'" size="45" 

				tabindex="' . $t_index . '" /></td>  ' . "\n" .

		'</tr>' . "\n";






		echo '<tr>' . "\n" .

			'<td> <label for="weight">Order</label></td>' . "\n" .

			'<td> <input ' . $val_weight . ' type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="45" 

				tabindex="' . $t_index . '" />' . "\n" .

				tooltip('Setting the `Order` will tell the testimonial where to appear within the list on the testimonial page.') . "\n" . 

				'</td>' . "\n" .

		'</tr>' . "\n";

									




		echo '   <tr>' . "\n" .

							' 						<td ><label for="active">Active</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="active" name="active">' . "\n" . 

							' 									<option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 

							' 									<option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 

							' 						</select>' . "\n" . 

						 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to Display on Website ) </span>' . "\n" . 

       ' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";

		

			


		echo '<tr><td colspan="2" style="text-align:center; margin:0 auto;" ><input type="submit" name="submit" 

			value="Submit" /></td></tr>' . "\n";  

 	echo '</table></form>' . "\n";

	echo '<script type="text/javascript">document.getElementById(\'type\').focus();</script>' . "\n";



}







/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

	

	$r_data['type'] = mysql_real_escape_string ( $data['type'] );

	$r_data['weight']    = mysql_real_escape_string ( $data['weight'] );

	$r_data['active']		= intval ( $data['active'] );

	

		 

	if ( empty($_POST['weight']) )

  {

    $r_data['weight']  = fetch_weight('plantypes', 'weight' );

  }

 

	return $r_data;

}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 *

 **************************************************************/



function remove()

{

	global $id, $module_name, $userinfo;

		

	$result = mysql_query("SELECT * FROM " . db_prefix . "plantypes WHERE id = '".$id."'");

	$row    = mysql_fetch_array($result);

	

 

	print_header('Delete Plan Type - ' . $row['type']);

	

	if ( !empty($_POST ))

	{		

		$result = mysql_query("DELETE FROM " . db_prefix . "plantypes WHERE id = '$id'");



		print_mysql_message ( mysql_errno() , $module_name, $id, '2' ) ;

	} else {

		echo '<form action="./?tool=plantypes&action=remove" method="post" name="form">' . "\n" .

			'<input type="hidden" name="id" value="' . $id . '">' . "\n" .

			'<div class="center">Are you sure you want to delete this plan type?</div>' . "\n" .

			'<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;

				<input name="No" type="button" value="No" onClick="window.location = \'./?tool=plantypes\'"></div>' . "\n" .

		'</form>';

	}

}





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

	global $action, $id, $module_name;

	

	if ( $action == 'update' )

		print_header('Update Plan Type');

	else

		print_header('Add New Plan Type');

	

	if ( array_key_exists ('submit',$_POST) )

	{				

		require_once("classes/validation.php");

	

		$rules = array();    

		

		$rules[] = "required,type,type";

	

		

		$errors = validateFields($_POST, $rules);

	

		if ( $action != 'update' )

		{

			$results = mysql_query("SELECT id FROM " . db_prefix . "plantypes WHERE 

														 type = '" . mysql_real_escape_string ($_POST['type']) . "'");

			if ( mysql_num_rows($results) != 0 )

				$errors[] = 'used_content';

		}

	

	}

	

	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

	{

		$data  = sanitize_vars( $_POST );			

															

		if ( $action == 'update' ) 

		{

			$sql = 'UPDATE ' . db_prefix . 'plantypes SET ';

		

			$xsql[] .= "type = '" . $data['type'] . "' ";

			$xsql[] .= " weight = '" . $data['weight'] . "' ";

			$xsql[] .= " active = '" . $data['active'] . "' ";

							

			$slen = count ( $xsql );

	 

			for($i=0;$i<$slen;$i++)

			{

				$sql .= $xsql[$i];

				if ( $i != ( $slen - 1 ) )

					$sql .= ', ';      

			}

			

			$sql .= " WHERE id = '$id'";

			$type = 0;

		} else {

			$data = sanitize_vars( $_POST );

					

		 	$sql = 'INSERT INTO ' . db_prefix . 'plantypes (type, weight, active) 

							VALUES ' . "('" . $data['type'] ."', '" .$data['weight'] ."', 

													 '" .$data['active'] ."')";

	

			$type = 1;

		}	

			

		mysql_query ( $sql );

		

		if ( ( mysql_errno() != 0 ) )

			print_debug($sql);

			

		print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;

				

  } else {

		add( $errors );

	}//end if	

}//end function

