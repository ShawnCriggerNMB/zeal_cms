<?php
if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : States Settings / Programmer ++ Level
// Written By : Cory Shaw of EWD
// Written On : May 9, 2014
// Updated By : Chuck Bunnell of EWD
// Updated On : July 31, 2014
// Copyright Zeal Technologies
//***************************************************/

// if update make sure this id exists
update_verify();

function execute()
{
	switch($_GET['action'])
	{
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}
}


/***************************************************************
 *
 * function manage
 * Querrs DB and Displays Content
 *
 **************************************************************/

function manage()
{
	global $identifier, $module_name, $db;

	$i    = 0;
	$link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';

	print_header('Manage '.$module_name,$link);

	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sortable" id="table">
  	<thead>
    	<tr>
				<th><h3>State</h3></th>
				<th style="width:70px"><h3>Short</h3></th>
				<th style="width:70px"><h3>Cities</h3></th>
				<th style="width:70px"><h3>Active</h3></th>
				<th class="nosort"><h3>Tools</h3></th>
			</tr>
		</thead>
		<tbody>';

			$stmt = $db->prepare('SELECT * FROM ' . db_prefix . $identifier);
			$stmt->execute();
			$pages   = $stmt->rowCount();
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
        // qry db for num of cities for each state
				$stmt1 = $db->prepare("SELECT COUNT(*) AS count FROM " . db_prefix . "cities WHERE state_id = ?");
        $stmt1->execute(array($row['id']));
        $row1 = $stmt1->fetch(PDO::FETCH_ASSOC);
        
				if($row1['count'] > 0)
          $cities = '<a href="./?tool=cities&state_id='.$row['id'].'">'.$row1['count'].'</a>';
        else
          $cities = 0;

        $act  = yes_no ( $row['active'] );
        
				echo '<tr align="left" valign="middle">
        	<td>' . $row['name'] . '</td>
        	<td>' . $row['abbrev'] . '</td>
        	<td>' . $cities . '</td>
       		<td>' . $act . '</td>
        	<td style="padding:0;text-align:center;"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&action=remove&id='.$row['id'].'">Remove</a></strong></td>
        </tr>';
    	}//end while

    echo '</tbody>
	</table>';

	$pages = ( $pages > 20 ) ? true : false;

	echo_js_sorter ( $pages );
	echo '<div class="spacer">&nbsp;</div>';

}//end function


/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 *
 **************************************************************/

function add( $errors = '' )
{
  global $identifier, $module_name, $id, $action,$db, $site_base_url, $upload_url, $upload_dir, $mod_config;

	if ( $errors )
	{
		echo '<ul class="error_message">
			<strong>Please fill in the required fields.</strong>';
			// set error messages for required fields
			if ( in_array('name', $errors ) )
			{
				echo '<li>You must fill out a name.</li>';
				$val_name = ' class="form_field_error" ';
			}
	
			if ( in_array('used_name', $errors ) )
			{
				echo '<li> This name is already being used.</li>';
				$val_name = ' class="form_field_error" ';      
			}
		
			if ( in_array('abbrev', $errors ) )
			{
				echo '<li>You must set an abbreviation for this state.</li>';
				$val_abbrev = ' class="form_field_error" ';
			}
	
			if ( in_array('used_abbrev', $errors ) )
			{
				echo '<li> This Abbreviation is already being used.</li>';
				$val_abbrev = ' class="form_field_error" ';      
			}
		
		echo '</ul>' . "\n";
	
	} else {
				
		$c = ( $action == 'update' ) ? 'update this' : 'add a new';
		echo  '<ul class="notice_message"><strong>To ' . $c . ' record, fill out the form and click submit.</strong>
			<li>All fields are required.</li>
		</ul>';
	}
	
	if ($action == "update")
	{
			$stmt = $db->prepare('SELECT * FROM ' . db_prefix . $identifier.' WHERE id = ?');
			$stmt->execute(array($id));
			$row 		= $stmt->fetch(PDO::FETCH_ASSOC);
	}

	if (!empty ($_POST))
	{
			$row     = sanitize_vars ($_POST);
	}//end if

/********************************************************
 * Start Building Form
 *******************************************************/
	$r = required();

	echo '<form name="form" id="form" method="post" action="#">
		<table>
			<input type="hidden" name="id" value="' . $id . '" />';

			echo '<tr>
				<td><label for="name">'.$r.'State Name</label></td>
				<td><input ' . $val_name . ' type="text" name="name" id="name" value="'. htmlspecialchars($row['name']) .'" size="45" /></td>
			</tr>';

			echo '<tr>
				<td><label for="abbrev">'.$r.'Abbreviation</label></td>
				<td><input ' . $val_abbrev . ' type="text" name="abbrev" id="abbrev" value="'. htmlspecialchars($row['abbrev']) .'" size="10" maxlength="2" /></td>
			</tr>';

			echo '<tr>
				<td><label for="active">'.$r.'Active</label></td>
				<td>
					'.create_slist ( $list, 'active', $row['active'], 1 ) . 
					tooltip('Set to Yes to allow on website').'
				</td>
			</tr>';

			echo '<tr>
				<td colspan="2" style="padding:3px;"><input type="submit" name="submit" value="Submit" /></td>
			</tr>';
		
		echo '</table>
	</form>';
	
	echo '<script type="text/javascript">document.getElementById(\'name\').focus();</script>';

}//end function


/***************************************************************
 *
 * function sanitize_vars
 * @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 **************************************************************/

function sanitize_vars( $data )
{
	$r_data['name'] = stripslashes ( $data['name'] );
	$r_data['abbrev'] = stripslashes ( $data['abbrev'] );
	$r_data['active'] = intval ( $data['active'] );

	return $r_data;
}


/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $identifier, $module_name, $id, $db;

	$stmt = $db->prepare("SELECT * FROM " . db_prefix . $identifier." WHERE id = ?");
	$stmt->execute(array($id));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	print_header('Delete '.$module_name.'  - ' . $row['name']);

	if ( !empty($_POST ))
	{
		$errno = 0;
		try
		{
			$stmt = $db->prepare('DELETE FROM ' . db_prefix . $identifier." WHERE id = ?");
      $stmt->execute(array($id));
    }
        
		catch(PDOException $ex)
		{
			$errno = $ex->getCode();
		}
		
		print_mysql_message ( $errno , $module_name, $id, 2 ) ;

  } else {

		echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">
			<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to delete this record?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div>
		</form>';
	}

}//end function


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute function show_form() to
 * allow editing of contents
 *
 **************************************************************/

function update()
{
	global $identifier, $module_name, $action, $id, $module_name, $db;

	if ( $action == 'update' )
		print_header('Update '.$module_name);
	else
		print_header('Add New '.$module_name);

	if ( array_key_exists ('submit',$_POST))
	{
		require ("classes/validation.php");
		
		// set rules for required fields
		$rules   = array();
		$rules[] = "required,name,name";
		$rules[] = "required,abbrev,abbrev";
		$rules[] = "required,active,active";

		$errors = validateFields($_POST, $rules);
	
		if ( $action != 'update' && $_POST['name'] != '')
		{
			$stmt = $db->prepare("SELECT id FROM " . db_prefix . $identifier . " WHERE name = ?");
      $stmt->execute(array($_POST['name']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_name';
		}
		
		if ( $action == 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM " . db_prefix . $identifier . " WHERE name = ? AND id != $id");
      $stmt->execute(array($_POST['name']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_name';
		}
	
		if ( $action != 'update' && $_POST['abbrev'] != '')
		{
			$stmt = $db->prepare("SELECT id FROM " . db_prefix . $identifier . " WHERE abbrev = ?");
      $stmt->execute(array($_POST['abbrev']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_abbrev';
		}
		
		if ( $action == 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM " . db_prefix . $identifier . " WHERE abbrev = ? AND id != $id");
      $stmt->execute(array($_POST['abbrev']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_abbrev';
		}
	
	}

	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$vars = array();
		$data = sanitize_vars( $_POST );
		$type = ( $action == 'update' ) ? 0 : 1;
		
		if ( $action == 'update' )
		{
			$sql = "UPDATE " . db_prefix . $identifier." SET name = ?, abbrev = ?, active = ? WHERE id = ?";
			array_push($vars, $data['name']);
			array_push($vars, $data['abbrev']);
			array_push($vars, $data['active']);
			array_push($vars, $id);
		
		} else {
			
			$sql = 'INSERT INTO ' . db_prefix . $identifier.' (name, abbrev, active) VALUES (?, ?, ?)';
			array_push($vars, $data['name']);
			array_push($vars, $data['abbrev']);
			array_push($vars, $data['active']);
		}

		$errono = 0;
		try
		{
			$stmt = $db->prepare($sql);
			$stmt->execute($vars);
		}
		
		catch(PDOException $ex)
		{
			$errono = $ex->getCode();
		}

		if ( is_saint() && $errono != 0 )
			echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

		print_mysql_message ( $errono , $module_name, $id, $type ) ;

	} else {
			
			add( $errors );
	}
	
}//end function

?>