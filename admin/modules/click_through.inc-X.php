<?php

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Click Through Stats for the Banner Advertisement Module

// Written By : Darrell Bessent

// Written On : May 28, 2013

// Updated by : Darrell Bessent of EWD

// Updated On : May 28, 2013

// Copyright Zeal Technologies & S-Vizion Software

//***************************************************/

//

// NOTES:

// 				Exporting url needs a rewrite rule. CSV script can not be an include. It must be called through a header()

//				or it will include part of the pages HTML in the saved csv file. .htaccess overwrite for folder admin/modules/exporting

//				is not working. pages.php is still catching the page and sending it to 404.





/***************************************************************

 *

 * Start and configuration of module

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) > 0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) > 0 ) ? $_POST['action'] : $_POST['action']);



$mod_config  = get_module_settings ( 'click_through' );



function execute()

{

	switch($_GET['action'])

	{

		case 'view':

			view();

			break;

		case 'flush_padb';

			flush_padb();

			break;	

		case 'flush_whdb';

			flush_whdb();

			break;		

		case 'export_padb';

			export_padb();

			break;		

		case 'export_whdb';

			export_whdb();

			break;		

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 *

 **************************************************************/



function manage()

{

	global $action, $id;



	$link = '<a href="./?tool=click_through&action=flush_whdb">Flush</a>';

	$link2 = '<a href="?tool=click_through&action=export_whdb">Export</a>';

	print_header('Banner Advertisment Stats', $link .'<a href="#"> - </a>'. $link2 );



	$th  = "\n";





	echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" .

			 '<thead>' . "\n" .

			 '	<tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" .

			 $th .

			 '		<th><h3>Banner Ad</h3></th>' . "\n" .

			 '		<th><h3>Page On</h3></th>' . "\n" .

			 '		<th><h3>Click Stats</h3></th>' . "\n" .

			 '		<th style="width:112px" class="nosort" colspan="3" ><h3>Tools</h3></th>'.  "\n" .

			 '	</tr></thead><tbody>' . "\n";



		// We want a DISTINCT ad_label and ad_id but we dont want ad_label to be displayed more than once in our listings, unless...

		// pageon is not DISTINCT...and it won't be so we DISTINCT all 3! :) 

		$query = mysql_query("SELECT DISTINCT ad_label, pageon, ad_id FROM " . db_prefix . "click_through ORDER BY ad_label");

		while ( $row = mysql_fetch_array ( $query ))

		{

			// Start our table row with the banner advertisment label and the page it is on

			echo '<tr>' ."\n" .

				 	 '  <td style="padding-left:20px;" >'.$row['ad_label'].'</td>' ."\n".

					 '  <td style="padding-left:20px;" >'.$row['pageon'].'</td>' ."\n";

			

			// Do our counting : We want to count ad_ids where they match our ad_id AND pageon we are currently on in the DB

			$query2 = mysql_query("SELECT COUNT(ad_id) FROM " . db_prefix . "click_through WHERE `ad_id`='".$row['ad_id']."' AND `pageon`='".$row['pageon']."'");

			while ( $row2 = mysql_fetch_array ( $query2 ))

			{	

				// Print the number of hits for the current banner advertisment we are on by counting ad_id where it matches our qry

				echo '  <td style="padding-left:20px;" >'.$row2['COUNT(ad_id)'].'</td>' ."\n";

			} // end while



			echo '  <td style="width:50px; text-align:center;"><a href="./?tool=click_through&action=view&ad_id='.$row['ad_id'].'&ad_label='.$row['ad_label'].'">View</a></td>' ."\n";

			echo '</tr>' ."\n"; // end the table row

		} // end while

			

	echo '</tbody></table>'; // end the table



	$pages = ( $pages > 20 ) ? true : false;



	echo_js_sorter ( $pages, $s );



	echo '<div class="spacer">&nbsp;</div>' . "\n";



}





/***************************************************************

 *

 * function view

 * View a banner ads statistic page

 *

 **************************************************************/



function view()

{

	global $action, $id;

	

	$link = '<a href="./?tool=click_through&action=flush_padb&ad_id='.$_GET['ad_id'].'&ad_label='.$_GET['ad_label'].'">Flush</a>';

	$link2 = '<a href="./?tool=click_through&action=export_padb&ad_id='.$_GET['ad_id'].'&ad_label='.$_GET['ad_label'].'">Export</a>';	

	

	print_header($_GET['ad_label']. ' - Banner Advertisment Stats', $link .'<a href="#"> - </a>'. $link2 );

	

	$th  = "\n";

	echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" .

			 '<thead>' . "\n" .

			 '	<tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" .

			 $th .

			 '		<th><h3>Page On</h3></th>' . "\n" .

			 '		<th style="width:200px"><h3>IP</h3></th>' . "\n" .

			 '		<th><h3>Date Clicked</h3></th>' . "\n" .

			 '		<th style="width:200px" class="nosort" colspan="3" ><h3>Tools</h3></th>'.  "\n" .

			 '	</tr></thead><tbody>' . "\n";

		

			$query = mysql_query("SELECT * FROM " . db_prefix . "click_through WHERE `ad_id`='".$_GET['ad_id']."' ORDER BY pageon");

			while ( $row = mysql_fetch_array ( $query ))

			{

				// Not in use yet

				echo '<tr>' ."\n" .

				 '  <td style="padding-left:20px;" >'.$row['pageon'].'</td>' ."\n".

				 '  <td style="padding-left:20px;" >'.$row['ip'].'</td>' ."\n".

				 '  <td style="padding-left:20px;" >'.$row['date'].'</td>' ."\n".

				 '  <td style="width:50px; text-align:center;"><a href="http://www.geoiptool.com/en/?IP='.$row['ip'].'" target="_blank">GeoIP</a></td>' ."\n".

				 '</tr>' ."\n"; // end the table row		

			}

	echo '</tbody></table>'; // end the table

	

	$pages = ( $pages > 20 ) ? true : false;



	echo_js_sorter ( $pages, $s );



	echo '<div class="spacer">&nbsp;</div>' . "\n";



}

/***************************************************************

 *

 * Excel export of whole click_through stats database

 *

 *

 **************************************************************/	

function export_whdb()

{

	global $action, $id;

  header('Location: whdb.php');

	//include('modules/exporting/whdb/index.php');

	exit();

}



/***************************************************************

 *

 * Excel export of click_through stats for a single banner ad

 *

 *

 **************************************************************/	

function export_padb()

{

	global $action, $id;

	header('Location: padb.php?ad_id='.$_GET['ad_id'].'&ad_label='.$_GET['ad_label']);

	exit();

}



/***************************************************************

 *

 * Flush the DB of all current banner advertisment stats

 *

 *

 **************************************************************/	

function flush_whdb()

{

	global $action, $id;

	

	$query = mysql_query("SELECT * FROM " . db_prefix . "click_through");

	while ( $row = mysql_fetch_array ( $query ))

	{



		$select = "DELETE FROM " . db_prefix . "click_through WHERE ad_id = '".$row['ad_id']."'";

		mysql_query ( $select ) or die ( "Sql error : " . mysql_error( ) );

		

	}

	

	header('Location: index.php?tool=click_through');

	exit();

}



/***************************************************************

 *

 * Flush the DB of all current banner advertisment stats,

 * for the currently selected banner only

 *

 *

 **************************************************************/	

function flush_padb()

{

	global $action, $id;

	

	$query = mysql_query("SELECT * FROM " . db_prefix . "click_through WHERE `ad_id`='".$_GET['ad_id']."' AND ad_label = '".$_GET['ad_label']."'");

	while ( $row = mysql_fetch_array ( $query ))

	{



		$select = "DELETE FROM " . db_prefix . "click_through WHERE ad_id = '".$row['ad_id']."' AND ad_label = '".$row['ad_label']."'";

		mysql_query ( $select ) or die ( "Sql error : " . mysql_error( ) );

		

	}

	

	header('Location: index.php?tool=click_through');

	exit();

}



?>