<?php	
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module: Admin Management System
// Written By : Shawn Crigger of EWD
// Written On : May 11, 2010
// Updated By : Cory Shaw of EWD
// Updated On : May 3, 2014
// Updated By : Chuck Bunnell of EWD
// Updated On : July 21, 2014
// Copyright Zeal Technologies
//***************************************************/

// if update make sure this id exists
update_verify();

function execute()
{
	switch($_GET['action'])
 {
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}//end switch
}//end function


/***************************************************************
 *
 * function manage
 * Querrys DB and Displays info
 *
 **************************************************************/

function manage()
{
	global $db, $identifier, $module_name, $userinfo, $dbtbl;
 
  $i    = 0;
	$link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';
	
  $lvl  = $userinfo['level'];

  if ( $lvl == 0 )
  {
		$link = '';
	}
/********************************************************
 * Extra Info for Saint Admin Modes
 *******************************************************/
 
 if ( is_saint() )
 {
		$hdr = '<th style="width:130px;"><h3>Last IP</h3></th>
					<th style="width:165px;"><h3>Last Login</h3></th>';
 }
 
	print_header('Manage '.$module_name,$link);
	
 $sql = '';
	
	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sortable" id="table">
		<thead>
			<tr>
				<th><h3>Name</h3></th>
				<th><h3>Email</h3></th>
				<th style="width:110px;"><h3>Level</h3></th>
				<th style="width:70px;"><h3>Active</h3></th>
				'.$hdr.'  
				<th class="nosort"><h3>Tools</h3></th>
			</tr>
		</thead>
		<tbody>';

			if ( $lvl == '0' )
				$sql = " AND id = '" . $userinfo['id'] . "' ";
					
			$stmt = $db->prepare("SELECT * FROM " . $dbtbl . " WHERE level <= ? $sql ORDER BY id");
			$stmt->execute(array($lvl));
			$pages   = $stmt->rowCount();
			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{    
				if ( is_saint() )
				{
					$lt = ( $row['last_login_ts'] == '' ) ? 'Never Logged' : date("m/d/Y, g:i a", strtotime($row['last_login_ts']) );
					
					$tbl = '<td>' . $row['last_login_ip'] . '</td>
								<td>' . $lt . '</td>';					  
				}
	
				$lvl = ( $row['level'] == '1' ) ? 'Super-Admin' : 'Admin';
				if ( $row['level'] >= '2' )
					$lvl = 'Programmer';
			 
				$act  = yes_no ( $row['active'] );
				
				echo '<tr align="left" valign="middle">
					<td>' . $row['fname'] . '&nbsp;' . $row['lname'] . '</td>
					<td>' . $row['email'] . '</td>
					<td>' . $lvl . '</td>
					<td>' . $act . '</td>
					'.$tbl.'
					<td style="padding:0;text-align:center;"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&action=remove&id='.$row['id'].'">Remove</a></strong></td>
				</tr>' . "\n";
			 
			}//end while
  
		echo '</tbody>
	</table>';

	if ( $pages > 20 )
		$pages = true;
	else
		$pages = false;
	
	echo_js_sorter ( $pages );
             
}//end function


/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 * 
 **************************************************************/

function add( $errors = '' )
{
	global $db, $identifier, $module_name, $id, $action, $dbtbl, $userinfo, $config;
	$list = $options = $options2 = null;
				
	if ( $errors )
	{
		echo '<ul class="error_message">
			<strong>Please fill in the required fields.</strong>';
			// set error messages for required fields
			if ( in_array('fname', $errors ) )
			{
				echo '<li> You must fill out a First name.</li>';
				$val_fname = ' class="form_field_error" ';      
			}
					
			if ( in_array('lname', $errors ) )
			{
				echo '<li> You must fill out a Last name.</li>';
				$val_lname = ' class="form_field_error" ';     
			}
	
			if ( in_array('used_email', $errors ) )
			{
				echo '<li> This email address is already being used.</li>';
				$val_email = ' class="form_field_error" ';      
			}
			
			if ( in_array('email', $errors ) )
			{
				echo '<li> You must use a valid email address.</li>';
				$val_email = ' class="form_field_error" ';      
			}
					
			if ( in_array('pass', $errors ) )
			{
				echo '<li> You must fill out a password.</li>';
				$val_pass = ' class="form_field_error" ';      
			}
	
			if ( in_array('badpass', $errors ) )
			{
				echo '<li>Password must be 8 or more characters. It must contain at least 1 uppercase, 1 lowercase, 1 number, and 1 allowed character.<br />Allowed Characters: # $ % & + [ ] ?</li>';
				$val_pass = ' class="form_field_error" ';
			}

		echo '</ul>';
	
	} else {
		
		$c = ( $action == 'update' ) ? 'update this' : 'add a new';
		$u_r = ( $action == 'update' ) ? ' except the Password' : '';
		echo  '<ul class="notice_message">';
			// set instructions here
			echo '<strong>To ' . $c . ' record, fill out the form and click submit.</strong>
			<li>All fields are required'.$u_r.'.</li>
			<li>Email: Please use a valid email address. This will be used for their login, and it is the only way this user will be able to reset their password if they lose it.</li>';
			if ($action == 'update' )
				echo '<li>Password: Only type in a password if you want to change the present one.</li>';
			echo '<li>Password: Must be 8 - 20 characters.<br />It must contain at least 1 uppercase, 1 lowercase, 1 number, and 1 allowed character. Allowed Characters: # $ % & + [ ] ?.</li>
		</ul>';
	}
			
	if ($action == "update")
	{
		// Run the query for the existing data to be displayed.
		$stmt = $db->prepare('SELECT * FROM ' . $dbtbl . ' WHERE id = ?');
		$stmt->execute(array($id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
	} 
	
	if (!empty ($_POST))
	{
	 $row = sanitize_vars ($_POST);
	}
		
	// users admin level
	$lvl = $userinfo['level'];
		
	if ( $row['level'] > $lvl )
	{
		echo '<br/><div class="error_message">You can not edit '.$module_name.' that have a higher level then you.';
		echo '<br/><a href="./?tool='.$identifier.'" >Click here to manage the '.$module_name.' .</a></div>';
		exit;
	}

	if ( ( $lvl == '0' ) && ( $row['id'] != $userinfo['id'] ) )
	{
		echo '<br/><div class="error_message">You can only edit your settings.';
		echo '<br/><a href="./?tool='.$identifier.'" >Click here to manage the '.$module_name.' .</a></div>';
		exit;
	}

/********************************************************
 * Start Building Form
 *******************************************************/
		
  $r = required();
	// pass only required for "add" not "update"
	$rp = ($action == 'update') ? '' : required();
	
	echo '<form name="form" id="form" method="post" action="" >
		<table>
			<input type="hidden" name="id" value="' . $id . '" />';
		
    	$passnote = tooltip('Password must be 8 - 20 characters. It must contain at least 1 uppercase, 1 lowercase, 1 number, and 1 allowed character. Allowed Characters: # $ % & + [ ] ?');
    	
			if ($action == 'update')
        $passnote = tooltip('Leave blank to keep current password.<br />
													Password must be 8 - 20 characters. It must contain at least 1 uppercase, 1 lowercase, 1 number, and 1 allowed character. Allowed Characters: # $ % & + [ ] ?');
		
			echo '<tr>
				<td><label for="fname">'.$r.'First Name</label></td>
				<td><input ' . $val_fname . ' type="text" name="fname" id="fname" value="'. htmlspecialchars($row['fname']) .'" size="45" /></td>
			</tr>';

			echo '<tr>
				<td><label for="lname">'.$r.'Last Name</label></td>
				<td><input ' . $val_lname . ' type="text" name="lname" id="lname" value="'. htmlspecialchars($row['lname']) .'" size="45" /></td>
			</tr>';

			echo '<tr>
				<td><label for="email">'.$r.'Email</label></td>
				<td><input ' . $val_email . ' type="text" name="email" id="email" value="'. htmlspecialchars($row['email']) .'" size="45" /></td>
			</tr>';

			echo '<tr>
				<td> <label for="pass">'.$rp.'Password</label></td>
				<td> <input ' . $val_pass . ' type="text" name="pass" id="pass" value="" size="45" /> ' .$passnote. ' </td>
			</tr>';
									
			if ( $lvl > 0 )
			{
				$opt = '';
				
				if ( is_saint() )
				{
					$s = ( defined ('saint' ) ) ? saint : 2;
					$o   = ( $row['level'] == $s ) ? ' SELECTED' : ' ';				
					$opt = '<option value="' . $s . '" ' . $o . ' >Programmer </option>';																														
				}
		
				echo '<tr>
					<td class="labelcell"><label for="level">'.$r.'Level</label></td>
					<td><select id="level" name="level">
							<option value="0" '; if ( $row['level'] == '0' ) echo ' SELECTED'; echo '>Admin</option>
							<option value="1" '; if ( $row['level'] == '1' ) echo ' SELECTED'; echo '>Super-Admin</option>
							'.$opt.'
						</select>
					</td>
				</tr>';
			}
		
			echo '<tr>
				<td><label for="active">'.$r.'Active</label></td>
				<td>
					'.create_slist ( $list, 'active', $row['active'], 1 ) . 
					tooltip('Set to Yes to Allow Login').'
				</td>
			</tr>';
		
			if ( ( $userinfo['level'] >= 1 ) && ( $config['use_access'] == '1' ) )
			{						
				echo '<tr>
					<td class="biglabelcell" colspan="2"><label for="access">Permissions</label></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<div style="float:left; margin-right:30px;">' .$options. '</div>
						<div style="float:left;">' .$options2. '</div>
					</td>
				</tr>';
			}
			
			echo '<tr>
				<td colspan="2" style="padding:3px;"><input type="submit" name="submit" value="Submit" /></td>
			</tr>
 		</table>
	</form>';
	
	echo '<script type="text/javascript">document.getElementById(\'fname\').focus();</script>';

}


/***************************************************************
 *
 * function sanitize_vars
 * @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 **************************************************************/

function sanitize_vars( $data )
{
	$r_data['fname']  = $data['fname'];
	$r_data['lname']  = $data['lname'];
	$r_data['email']  = $data['email'];
	$r_data['level']  = $data['level'];
	$r_data['pass']   = $data['pass'];
	$r_data['active']	= intval ( $data['active'] );
	
	if ((  $data['access'] != '' ) || is_array($data['access'] ))
		$r_data['access'] = implode ( ',', $data['access']);
  else
    $r_data['access'] = "";
		 
 	if ( !array_key_exists('active', $_POST ) )
		$r_data['active'] = 1;
 
	return $r_data;
}


/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
  global $db, $identifier, $module_name, $id, $dbtbl, $userinfo, $config;
		
	$stmt = $db->prepare("SELECT * FROM " . $dbtbl . " WHERE id = ?");
  $stmt->execute(array($id));
	$row  = $stmt->fetch(PDO::FETCH_ASSOC);
	
  $lvl  = $userinfo['level'];
  if ( $row['level'] > $lvl )
  {
		echo '<br/><div class="error_message">You can not delete '.$module_name.' that have a higher level then you.';
		echo '<br/><a href="./?tool='.$identifier.'" >Click here to manage the '.$module_name.' .</a></div>';
	 	exit;
  }
 
	print_header('Delete '.$module_name.'  - ' . $row['fname'].' '.$row['lname']);
	
	if ( !empty($_POST ))
	{		
		$errno = 0;
        
		try 
		{
			$stmt = $db->prepare("DELETE FROM " . db_prefix . $identifier . " WHERE id = ?");
			$stmt->execute(array($id));
		}
		
		catch(PDOException $ex) 
		{
			$errno = $ex->getCode();
		}
		
		print_mysql_message ( $errno , $module_name, $id, '2' ) ;
	
	} else {
		
		echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">
			<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to delete this record?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div>
		</form>';
	}

}//end function


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute function show_form() to
 * allow editing of contents
 *
 **************************************************************/

function update()
{
	global $db, $identifier, $module_name, $action, $id, $dbtbl;
	
	if ( $action == 'update' )
		print_header('Update '.$module_name);
	else
		print_header('Add New '.$module_name);
	
	if ( array_key_exists ('submit',$_POST) )
	{				
		require ("classes/validation.php");
	
		// set rules for required fields
		$rules = array();    
		$rules[] = "required,fname,fname";
		$rules[] = "required,lname,lname";
		$rules[] = "required,email,email";
		$rules[] = "valid_email,email,email";
	
		if ( $action != 'update' )
			$rules[] = "required,pass,pass";
		
		$errors = validateFields($_POST, $rules);
	
		if ( $action != 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE email = ?");
      $stmt->execute(array($_POST['email']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_email';
		}
		
		if ( $action == 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM " . $dbtbl . " WHERE email = ? AND id != ?");
      $stmt->execute(array($_POST['email'], $id));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_email';
		}
		
		// password must contain lower case, upper case, number, and min 8 chars
		if (!empty($_POST['pass']))
		{
			if (!preg_match( '~[A-Z]~', $_POST['pass']) || 
					!preg_match( '~[a-z]~', $_POST['pass']) || 
					!preg_match( '~\d~', $_POST['pass']) || 
					!preg_match('/[#$%&+\[\]?]/', $_POST['pass']) || 
					strlen($_POST['pass']) < 8 || 
					$numpass > 20)
			{
				$errors[] = 'badpass';
			}
		}
				
	}
	
	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$vars = array();
		$data  = sanitize_vars( $_POST );			
		$type = ( $action == 'update' ) ? 0 : 1;
		$xsql = array();
															
		if ( $action == 'update' ) 
		{
			$sql = 'UPDATE ' . $dbtbl . ' SET username = ?, fname = ?, lname = ?, email = ?, active = ?, level = ?, access = ? ';
			array_push($vars, $data['email']);
			array_push($vars, $data['fname']);
			array_push($vars, $data['lname']);
			array_push($vars, $data['email']);
			array_push($vars, $data['active']);
			array_push($vars, $data['level']);
			array_push($vars, $data['access']);

			if ( $data['pass'] != '' )
			{
				$sql .= ", pass = ? ";
				array_push($vars, md5( $data['pass']));
			}
			
			$sql .= " WHERE id = ?";
      array_push($vars, $id);
		
		} else {
			
		 	$sql = 'INSERT INTO ' . $dbtbl . ' (username, fname, lname, level, access, email, active, pass) VALUES ' . "(?, ?, ?, ?, ?, ?, ?, ?)";
			array_push($vars, $data['email']);
			array_push($vars, $data['fname']);
			array_push($vars, $data['lname']);
			array_push($vars, $data['level']);
			array_push($vars, $data['access']);
			array_push($vars, $data['email']);
			array_push($vars, $data['active']);
			array_push($vars, md5( $data['pass'] ));
		}

		$errno = 0;
		try 
		{
			$stmt = $db->prepare($sql);
			$stmt->execute($vars);
		}
		
		catch(PDOException $ex)
		{
			$errno = $ex->getCode();
		}

		if ( ( $errno != 0 ) && ( is_saint() ) )
			print_debug($sql);
			
		print_mysql_message ( $errno , $module_name, $id, $type ) ;
				
  } else {
		
		add( $errors );
	}//end if	
	
}//end function
?>