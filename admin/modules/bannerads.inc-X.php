 <?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Banner Ad Affiliates Module

// Written By : Darrell Bessent of EWD

// Written On : May 16, 2013

// Revision   : 164

// Updated by : Darrell Bessent of EWD

// Updated On : May 16, 2013

// Description: This module is used with websites that 

//							want to post affliates/banner ads. 

// Copyright Zeal Technologies 

//***************************************************/



/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);





$mod_config  = get_module_settings ( 'bannerads' );



$upload_file_url  = $site_base_url . '/cmsuploads/bannerads/';

$upload_file_dir  = $site_base_dir . '/cmsuploads/bannerads/';



$upload_image_url  = $site_base_url . '/cmsuploads/bannerads/';

$upload_image_dir  = $site_base_dir . '/cmsuploads/bannerads/';



$width = '';

$height = '';



function execute()

{

	switch($_GET['action'])

	{

		case 'update':

			update();

			break;

		case 'add':

			update();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Slider Content

 *

 *

 **************************************************************/



function manage()

{



  global $upload_image_dir,$upload_image_url,$upload_file_dir,$upload_file_url;

  

  $i    = 0;

  $link = '<a href="./?tool=bannerads&action=add">Add A Banner Advertisement</a>';

  

  print_header('Manage Banner Ads',$link);

  

  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 

       ' 	  <thead>' . "\n" . 

       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" .

       ' 			  <th><h3>Label</h3></th>' . "\n" . 	

			// ' 			  <th><h3>Page On</h3></th>' . "\n" . 

			 ' 			  <th><h3>Location</h3></th>' . "\n" . 

			 ' 			  <th><h3>Start Date</h3></th>' . "\n" . 

       ' 			  <th><h3>End Date</h3></th>' . "\n" .

			 ' 			  <th><h3>Image</h3></th>' . "\n" .    

       ' 			  <th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . 

       ' 			  <th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" . 

       '		 </tr></thead><tbody>' . "\n";

  

  $results = mysql_query('SELECT * FROM ' . db_prefix . 'bannerads ORDER BY weight,label');

  $pages   = mysql_num_rows($results);

  while ($row = mysql_fetch_array($results))

  {    



    $i++;

    

    $image_link 				 = 'No File Uploaded';

		$file_link 					 = 'No File Uploaded';

		$pageon_id					 = $row['pageon_id'];

    $label							 = $row['label'];

		$notes							 = $row['notes'];

		$upload_date				 = $row['upload_date'];

		$start_date					 = $row['start_date'];

		$end_date						 = $row['end_date'];

		

    $image_filename = $upload_image_dir . $row['image'];

		$file_filename = $upload_file_dir . $row['file'];

    

    $image_link = ( file_exists ( $image_filename ) ) ? '<a href="' . $upload_image_url . $row['image'] . '" class="lightbox">' . $row['image'] . '</a>' : $image_link;

    $file_link = ( file_exists ( $file_filename ) ) ? '<a href="' . $upload_file_url . $row['file'] . '" target="_blank">' . $row['file'] . '</a>' : $file_link;

			          

    $act = yes_no ( $row['active'] );

		$ad_loc = ($row['ad_loc'] == 'T') ? 'Top' : 'Side';

		

		$page_qry = mysql_query("SELECT label FROM cms_pages WHERE `id`='$pageon_id'");

		$prow = mysql_fetch_assoc($page_qry);

		$pageon_label = $prow['label'];

    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

    echo '<td style="padding-left:20px;">' . $label . '</td>' . "\n"; 	

		//echo '<td style="padding-left:20px;">' . $pageon_label . '</td>' . "\n"; 

		echo '<td style="padding-left:20px;">' . $ad_loc . '</td>' . "\n"; 

		echo '<td style="padding-left:20px;">' . $start_date . '</td>' . "\n";

		echo '<td style="padding-left:20px;">' . $end_date . '</td>' . "\n";

		echo '<td style="padding-left:20px;">' . $image_link . '</td>' . "\n"; 		

    echo '<td style="width:50px; text-align:center;">' . $act . '</td>' . "\n"; 

    echo '<td valign="middle"><strong><a href="./?tool=bannerads&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=bannerads&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

    echo '</tr>' . "\n";

	  

  }//end while



  echo '</tbody></table>';



  $pages = ( $pages > 20 ) ? true : false;

	

  echo_js_sorter ( $pages );

             

}//end function



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Banner Form for User Input

 *

 *

 **************************************************************/



function add( $errors = '' )

{

  global $id, $action, $site_base_url, $site_base_dir, $upload_image_url, $upload_image_dir, $upload_file_url, $upload_file_dir, $mod_config, $height, $width;

   

   

  if ( $errors )

  {

   echo '<div class="error_message">';

   if ( in_array('label', $errors ) )

     echo '* You must fill out a label.<br/>';

   echo '</br></div>' . "\n";

  } else {

    $c = ( $action == 'update' ) ? 1 : 0;

    print_notice_message ( $c );

  }

  

  if ($action == "update")

  {

   // Run the query for the existing data to be displayed.

   $results = mysql_query('SELECT * FROM ' . db_prefix . 'bannerads WHERE id = "' . $id . '"');

   $row 				= mysql_fetch_array($results);

  } 

	if (!empty ($_POST))

	{	

   $row     = sanitize_vars ($_POST);

  }//end if

  

	echo '<div class="notice_message">NOTES : <br /> 

					<ul>

						<li>The `LABEL` is the label for the banner ad post.</li>

						<li>The `ALT TAG` sets the text displayed when visitor browers do not support images. The ALT tag also helps search engines.</li>

						<li>The `TITLE TAG` sets the text that will be displayed when a visior hovers the mouse on the image.</li>

						<li>The `NOTES` are notes you may wish to take while adding the banner ad.</li>

						<li>The `START DATE` is the date you wish for this advertisment to start displaying on the website.</li>

						<li>The `END DATE` is the date you wish for this advertisment to be removed from the website.</li>

						<li>The `PAGE ON` is the page you wish for the banner advertisement to be displayed on.</li>

						<li>The `IMAGE` is the banner image.</li>

						<li>The `USE UPLOADED FILE` will allow you to link to a file you are uploading instead of the business website. (When using/uploading a file, leave the HREF textbox empyt)</li>

						<li>The `FILE` may be a *.DOC, *.DOCX or *.PDF.</li>

						<li>The `NEW WINDOW` will allow the HREF link to open in a new window, or same window.</li>

						<li>The `HREF` is the location you wish for the banner image to link to. (Example: External website/An uploaded file. When using/uploading a file, leave the HREF textbox empty.)</li>

						<li>The `ACTIVE` is used to control an advertisments status or disable an advertisment before the ending date.</li>

					<ul>	

				</div>';

	

  echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

  $t_index = 1;

  echo '   <tr>' . "\n".

       '      <td > <label for="label">Label</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="label" id="label" value="'. $row['label'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

			 

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="alt_tag">ALT Tag</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="alt_tag" id="alt_tag" value="'. $row['alt_tag'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

			 

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="title_tag">Title Tag</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="title_tag" id="title_tag" value="'. $row['title_tag'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";		 		 

	

	$t_index++;

	$r_notes = str_replace('<br />', '\n', $row['notes']);

	echo '   <tr>' . "\n".

       '      <td > <label for="notes">Notes</label></td>  ' . "\n" . 

       '      <td > <textarea name="notes" id="notes" rows="5" cols="34" tabindex="' . $t_index . '">'.$r_notes.'</textarea></td>  ' . "\n" . 

       '   </tr>' . "\n";		 

			 	  

	$t_index++;

   echo '   <tr>' . "\n" .

	 			'      <td > <label for="start_date">Start Date</label></td>  ' . "\n" . 

				'			<td> <input type=text name="start_date" size=20 value="'; if (!empty($row['start_date'])) echo $row['start_date']; echo '">' . "\n" . 

				'			<a href="javascript:show_calendar(\'signup.start_date\',\'YYYY-MM-DD\');" onmouseover="window.status=\'Date Picker\';return true;" onmouseout="window.status=\'\';return true;"><img src="images/show-calendar.gif" width=18 height=18 border=0 align="bottom" style="vertical-align:-4px;" /></a></td>' . "\n" . 

				'   </tr>' . "\n";	

			 

	$t_index++;

   echo '   <tr>' . "\n" .

	 			'      <td > <label for="end_date">End Date</label></td>  ' . "\n" . 

				'			<td> <input type=text name="end_date" size=20 value="'; if (!empty($row['end_date'])) echo $row['end_date']; echo '">' . "\n" . 

				'			<a href="javascript:show_calendar(\'signup.end_date\',\'YYYY-MM-DD\');" onmouseover="window.status=\'Date Picker\';return true;" onmouseout="window.status=\'\';return true;"><img src="images/show-calendar.gif" width=18 height=18 border=0 align="bottom" style="vertical-align:-4px;" /></a></td>' . "\n" . 

				'   </tr>' . "\n";				 



  

	$t_index++;

	echo '   <tr>' . "\n" .

		 	 ' 						<td ><label for="pageon_id">Page On</label></td>  ' . "\n" . 

			 ' 						<td >' . "\n" . 

			 ' 						<select id="pageon_id[]" name="pageon_id[]" tabindex="' . $t_index . '" multiple>' . "\n".

			 '						<option value="0" '; if ( $row['pageon_id'] == $prow['id'] ) echo 'SELECTED'; echo ' >No Selection</option>' . "\n";

				

				$sel_qry = mysql_query("SELECT pageon_id FROM cms_bannerads WHERE `id`='$id' ORDER BY weight");

				$sel_row = mysql_fetch_array($sel_qry); // fetch array

				$is_selected = explode(',', $sel_row['pageon_id']); // explode it

				

				// Query and list main pages

				$page_qry = mysql_query("SELECT id,label FROM cms_pages WHERE `active`='1' ORDER BY weight");

				$ptotnum = mysql_num_rows($page_qry);

				

				for ($i=1; $i<=$ptotnum; $i++)

				{			

							$prow = mysql_fetch_assoc($page_qry); 

							if (in_array($prow['id'], $is_selected)) //use in_array to check if in array

							{

								$selected = 'SELECTED';

							} else

								{

									$selected = '';

								}

							echo ' 									<option value="'.$prow['id'].'" '; echo $selected; echo ' >'.$prow['label'].'</option>' . "\n";

				}

				// Query and list coupon pages

				$page_qry = mysql_query("SELECT id,label FROM ".db_prefix."coupon_cats WHERE `active`='1' ORDER BY weight");

				$ptotnum = mysql_num_rows($page_qry);

				

				for ($i=1; $i<=$ptotnum; $i++)

				{			

							$prow = mysql_fetch_assoc($page_qry); 

							if (in_array($prow['id'], $is_selected)) //use in_array to check if in array

							{

								$selected = 'SELECTED';

							} else

								{

									$selected = '';

								}

							echo ' 									<option value="'.$prow['id'].'" '; echo $selected; echo ' >'.$prow['label'].'</option>' . "\n";

				}				

 echo  ' 						</select>' . "\n" . 

			 ' 						<span class="note" style="padding-left:5px;"> ( Select the page you would like this advertisement to be displayed on. ) </span>' . "\n" . 

			 ' 						</td>  ' . "\n" . 

			 '				</tr>  '. "\n";	



	

	

  /****************************

  * File Uploading Section

  ***************************/

  

	// image upload handler

  if ( $row['image'] != '' )

  {

    

   $image_filename      = $upload_image_dir . $row['image'];

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="current_image">Current Image</label></td>' . "\n";

   echo '<td ><a href="' . $upload_image_url . $row['image'] . '" class="lightbox">' . $row['image'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="current_image" value="' . $row['image'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="image">Upload Image</label></td>' ."\n";

  echo '<td ><input name="image" type="file" id="image" size="15">' . "\n";

  echo '<span class="note">( File must be a *.JPG, *.PNG, or *.GIF. If this is a header image, please make the height no taller than 60px. If this is a sidebar image, please make the width no larger than 230px. Failure to comply to image sizes may result to improper viewing of the website. )</span></td>' . "\n";

  echo '</tr>' . "\n";

 

 

   $t_index++;

		echo '   <tr>' . "\n" .

							' 						<td ><label for="use_file">Use Uploaded File</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="use_file" name="use_file">' . "\n" . 

							' 									<option value="0" '; if ( $row['use_file'] == '0' ) echo 'SELECTED'; echo ' >No</option>' . "\n" . 

							' 									<option value="1" '; if ( $row['use_file'] == '1' ) echo ' SELECTED'; echo ' >Yes</option>																' . "\n" . 

							' 						</select>' . "\n" . 

						 ' 						<span class="note" style="padding-left:5px;"> ( Set option to `Yes` to use the uploaded file. ) </span>' . "\n" . 

       ' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";	

 

 	// file upload handler

   if ( $row['file'] != '' )

  {

    

   $file_filename      = $upload_file_dir . $row['file'];

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="current_file">Current File</label></td>' . "\n";

   echo '<td ><a href="' . $upload_file_url . $row['file'] . '">' . $row['file'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="current_file" value="' . $row['file'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="file">Upload File</label></td>' ."\n";

  echo '<td ><input name="file" type="file" id="file" size="15">' . "\n";

  echo '<span class="note">( File must be a *.DOC, *DOCX, *.PDF or *.RTF )</span></td>' . "\n";

  echo '</tr>' . "\n";

	

	


	echo '   <tr>' . "\n" .

		 	 ' 						<td ><label for="new_window">New Window</label></td>  ' . "\n" . 

			 ' 						<td >' . "\n" . 

			 ' 						<select id="new_window" name="new_window">' . "\n".

			 '						<option value="1" '; if ( $row['new_window'] == "1" ) echo 'SELECTED'; echo ' >Yes</option>' . "\n".

			 ' 						<option value="0" '; if ( $row['new_window'] == "0" ) echo 'SELECTED'; echo ' >No</option>' . "\n".	

			 ' 						</select>' . "\n" . 

			 ' 						<span class="note" style="padding-left:5px;"> ( Select if you would like the href link to open in a new window. ) </span>' . "\n" . 

			 ' 						</td>  ' . "\n" . 

			 '				</tr>  '. "\n";	

	

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="href">HREF</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="href" id="href" value="'. $row['href'] .'" size="45" tabindex="' . $t_index . '" placeholder="http://www.someadvertiserbannersite.com" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

			 

	$t_index++;

	echo '   <tr>' . "\n" .

						' 						<td ><label for="ad_loc">Ad Location</label></td>  ' . "\n" . 

						' 						<td >' . "\n" . 

						' 						<select id="ad_loc" name="ad_loc">' . "\n" .

						' 									<option value="S" '; if ( $row['ad_loc'] == 'S' ) echo ' SELECTED'; echo ' >Sidebar Ad</option>' . "\n" . 

						' 									<option value="T" '; if ( $row['ad_loc'] == 'T' ) echo 'SELECTED'; echo ' >Top Ad</option>' . "\n" .  

						' 						</select>' . "\n" . 

		        ' 						</td>  ' . "\n" . 

						'				</tr>  '. "\n";

	

	

  $t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="weight">Order</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />' . "\n" .

       ' 						<span class="note" style="padding-left:5px;"> ( Recommend increments of 10 ) </span>' . "\n" .

       '      </td>' . "\n" .

       '   </tr>' . "\n";

  

  $t_index++;

		echo '   <tr>' . "\n" .

							' 						<td ><label for="active">Active</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="active" name="active">' . "\n" . 

							' 									<option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 

							' 									<option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 

							' 						</select>' . "\n" . 

						 ' 						<span class="note" style="padding-left:5px;"> ( Set option to `Yes` to display on the website. ) </span>' . "\n" . 

       ' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";		 

	

 // This is our submit

  $t_index++;

  echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";  

  echo '</table></form>' . "\n";

  echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>' . "\n";



}





/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

  global $upload_file_dir, $upload_image_dir, $mod_config, $known_photo_types, $known_file_types;

  

  $r_data['pageon_id'] 	 			= mysql_real_escape_string ( $data['pageon_id'] );

	$r_data['caton_id'] 	 			= mysql_real_escape_string ( $data['caton_id'] );

  $r_data['label'] 	 					= mysql_real_escape_string ( $data['label'] );

	$r_data['alt_tag'] 	 				= mysql_real_escape_string ( $data['alt_tag'] );

	$r_data['title_tag'] 		 	  = mysql_real_escape_string ( $data['title_tag'] );

	$r_data['notes']  	 				= mysql_real_escape_string ( $data['notes'] );

	$r_data['upload_date']  	  = date('Y-m-d');

	$r_data['start_date']  		  = mysql_real_escape_string ( $data['start_date'] );

	$r_data['end_date']  			  = mysql_real_escape_string ( $data['end_date'] );

  $r_data['image']  				  = $data['current_image'];

	$r_data['file']  					  = $data['current_file'];

	$r_data['new_window']  			= mysql_real_escape_string ( $data['new_window'] );

	$r_data['href']  					  = mysql_real_escape_string ( $data['href'] );

	$r_data['ad_loc']  					= mysql_real_escape_string ( $data['ad_loc'] );

	$r_data['use_file']  				= mysql_real_escape_string ( $data['use_file'] );

	$r_data['weight']  					= mysql_real_escape_string ( $data['weight'] );

	$r_data['active'] 				  = mysql_real_escape_string ( $data['active'] );

			

  $tid = ( intval ( $data['id'] ) == 0 ) ? fetch_lastid ('bannerads') : $data['id'];

  

    		 

				 

  if ( !empty ( $_FILES['file']['name'] ) )

  {

            

    $old_file        = $r_data['file'];

    $file            = $_FILES['file'];

		

		$r_data['file'] = process_upload_file ( $tid , $file, $known_file_types, $upload_file_dir);

						

    if ( ( $old_file != $r_data['file'] ) && ( file_exists ( $upload_file_dir . $old_file ) ) && ( $old_file != '' ))

		{

			unlink ( $upload_file_dir . $old_file );

		}

  

	}

	

  if ( !empty ( $_FILES['image']['name'] ) )

  {

            

    $old_image        = $r_data['image'];

    $image            = $_FILES['image'];

    

		$r_data['image'] = process_upload ( $tid , '', $image, $known_photo_types, $upload_image_dir, 1, $width, $height );

			

    if ( ( $old_image != $r_data['image'] ) && ( file_exists ( $upload_image_dir . $old_image ) ) && ( $old_image != '' ))

		{

			unlink ( $upload_image_dir . $old_image );

		}

  

	}

			 

  if ( empty($_POST['weight']) )

  {

    $r_data['weight']  = fetch_weight('bannerads', 'weight' );

  }

  

  return $r_data;



}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 * Unlinks Existing Photo

 *

 **************************************************************/



function remove()

{



	global $id, $upload_file_dir, $upload_image_dir, $module_name;

	

	$result = mysql_query("SELECT * FROM " . db_prefix . "bannerads WHERE id = '$id'");

	$row    = mysql_fetch_array($result);

	

	print_header('Delete Banner Ad - ' . $row['label']);

	

	if ( !empty($_POST ))

	{		

					

		if ( file_exists ( $upload_file_dir . $row['file'] ))

		{

				@unlink( $upload_file_dir . $row['file'] );

		}

		

		if ( file_exists ( $upload_image_dir . $row['image'] ))

		{

				@unlink( $upload_image_dir . $row['image'] );

		}

		

		

		$result = mysql_query('DELETE FROM ' . db_prefix . "bannerads WHERE id = '$id'");

		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

			

	} else {

	

		echo '<form action="./?tool=bannerads&action=remove" method="post" name="form">' . "\n" .

							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

							'<tr> <td><div align="center">Are you sure you want to delete this news post?</div></td></tr>' . "\n" .

							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=bannerads\'"></div></td></tr>' . "\n" .

							'</table></form>';

	}//end if

}//end function





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

		global $action, $id, $module_name;

		

		if ( $action == 'update' )

				print_header('Update Banner Advertisement');

		else

				print_header('Add Banner Advertisement');

 

		if ( array_key_exists ('submit',$_POST))

		{				

    require_once("classes/validation.php");

  

    $rules = array();    

    

    $rules[] = "required,label,label";

		//$rules[] = "required,href,href";

    

    $errors = validateFields($_POST, $rules);

    

		}



		

		if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

	 {

		

				$data  = sanitize_vars( $_POST );		

				

				if ($data['use_file'] == '1')

				{

					$data['href'] = 'http://www.monstercouponbook.com/cmsuploads/bannerads/'.$data['file'];

				}

				

		 	  $type  = ( $action == 'update' ) ? 0 : 1;

				$notes = str_replace('\n', '<br />', $data['notes']);

				

				// impolde our list 

				if(!empty($_POST['pageon_id'])) {

						$allpages = implode(",", $_POST['pageon_id']);

					}

				

				if ( $action == 'update' )

						$sql = "UPDATE " . db_prefix . "bannerads SET pageon_id = '" . $allpages . "', caton_id = '" . $data['caton_id'] . "', label = '" . $data['label'] . "', alt_tag = '" . $data['alt_tag'] . "', title_tag = '" . $data['title_tag'] . "', notes = '" . $notes . "', upload_date = '" . $data['upload_date'] . "', start_date = '" . $data['start_date'] . "', end_date = '" . $data['end_date'] . "', file = '" . $data['file'] . "', image = '" . $data['image'] . "', new_window = '" . $data['new_window'] . "', href = '" . $data['href'] . "', ad_loc = '" . $data['ad_loc'] . "', use_file = '" . $data['use_file'] . "', weight = '" . $data['weight'] . "', active = '" . $data['active'] . "'	WHERE id = '$id'";

 		 else

					 $sql = 'INSERT INTO ' . db_prefix . 'bannerads (pageon_id, caton_id, label, alt_tag, title_tag, notes, upload_date, start_date, end_date, image, file, new_window, href, ad_loc, use_file, weight, active) VALUES ' .

												 "('". $allpages . "', '" . $data['caton_id'] . "', '" . $data['label'] . "', '" . $data['alt_tag'] . "', '" . $data['title_tag'] . "', '" . $notes . "', '" . $data['upload_date'] . "', '" . $data['start_date'] . "', '" . $data['end_date'] . "', '" . $data['image'] . "', '" . $data['file'] . "', '" . $data['new_window'] . "', '" . $data['href'] . "', '" . $data['ad_loc'] . "', '" . $data['use_file'] . "', '" . $data['weight'] . "', '" . $data['active'] . "')";

											

//    echo $sql;

    

				mysql_query ( $sql );

    

    if ( is_saint() && mysql_errno != 0 )

      echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

   			

				print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;   

		

  } else {

		add( $errors );

	}//end if	

}//end function







?>