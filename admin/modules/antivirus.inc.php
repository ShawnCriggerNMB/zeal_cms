<?php	
if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : Security Module v0.0.1
// Written By : Darrell Bessent of EWD
// Written On : May 07, 2013
// Revision   : 002
// Updated by : Cory Shaw of EWD
// Updated On : May 03, 2014
// Description: This module is used to create security
//							policies and configurations within the CMS.
// Copyright Zeal Technologies
//***************************************************/

$mod_config  = get_module_settings ( 'antivirus' );

function execute()
{
	switch($_GET['action'])
	{
		default:
			manage();
	}//end switch
}//end function


/***************************************************************
 *
 * function manage
 * Querrys DB and Displays Slider Content
 *
 **************************************************************/

function manage()
{	
	include ('phpav/anti-virus.php');
}//end function

