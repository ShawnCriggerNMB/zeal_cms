 <?php	
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : System Log Event Viewer Module
// Written By : Darrell Bessent
// Written On : Jun 08, 2013
// Revision   : 165
// Updated by : Cory Shaw
// Updated On : May 03, 2014
// Description: This module is used to view system logging.
// Copyright Zeal Technologies
//***************************************************/

$mod_config  = get_module_settings ( 'log' );

function execute()
{
	switch($_GET['action'])
	{
		case 'flush_log':
			flush_log();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}//end switch
}//end function


/***************************************************************
 *
 * function manage
 * Querrys DB and Displays Requested Content
 *
 **************************************************************/

function manage()
{
  global $db, $identifier, $module_name, $dbtbl;

  $i    = 0;
  $link = '<a href="?tool='.$identifier.'&action=flush_log">Flush</a>';
  print_header('Manage '.$module_name, $link);
  
	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sortable" id="table">
		<thead>
			<tr>
				<th><h3>ID</h3></th>
				<th><h3>Event</h3></th>
				<th><h3>Username</h3></th>
				<th><h3>IP</h3></th>
				<th><h3>Time</h3></th>
				<th><h3>Date</h3></th>
				<th class="nosort"><h3>Tools</h3></th>
			</tr>
		</thead>
		<tbody>';
		
		$stmt = $db->prepare('SELECT * FROM ' . $dbtbl);
		$stmt->execute();
		$pages   = $stmt->rowCount();
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{    

			$dbid			= $row['id'];
			$event	 	= $row['event'];
			$username	= $row['username'];
			$ip				= $row['ip'];
			$time			= $row['time'];
			$date			= $row['date'];
		
			echo '<tr align="left" valign="middle">
				<td>' . $dbid . '</td>
				<td>' . $event . '</td>
				<td>' . $username . '</td>
				<td>' . $ip . '</td>
				<td>' . $time . '</td>
				<td>' . $date . '</td>
				<td style="padding:0;text-align:center;"><strong><a href="./?tool='.$identifier.'&action=remove&id='.$row['id'].'">Remove</a></strong></td>
			</tr>';
			
		}//end while

  	echo '</tbody>
	</table>';

  $pages = ( $pages > 20 ) ? true : false;
	
  echo_js_sorter ( $pages );
             
}//end function


/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $db, $identifier, $module_name, $id, $dbtbl;
	
	$stmt = $db->prepare("SELECT * FROM " . $dbtbl . " WHERE id = ?");
  $stmt->execute(array($id));
	$row  = $stmt->fetch(PDO::FETCH_ASSOC);
	
	print_header('Delete Event - ' . $row['event'] );
	
	if ( !empty($_POST ))
	{
		$errno = 0;
		try
		{
			$stmt = $db->prepare('DELETE FROM ' . $dbtbl . " WHERE id = ?");
			$stmt->execute(array($id));
		}
		
		catch(PDOException $ex){
			$errno = $ex->getCode();
		}

		print_mysql_message ( $errno , $module_name, $id, 2 ) ;
			
	} else {
	
		echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">
    	<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to delete this record?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div>
    </form>';
	}

}//end function



/***************************************************************
 *
 * function flush log
 * Deletes all rows from Database
 *
 **************************************************************/

function flush_log()
{
	global $db, $identifier, $module_name, $id, $dbtbl;

	print_header('Flush System Event Logs' );
	
	if ( !empty($_POST ))
	{
		$stmt1 = $db->prepare('TRUNCATE ' . $dbtbl);
		$stmt1->execute();
		header ('Location: ?tool=log');
		exit();
			
	} else {
	
		echo '<form action="./?tool='.$identifier.'&action=flush_log" method="post" name="form">
    	<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to flush and empty the entire log?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool=log\'"></div>
    </form>';
	}
							
}
?>