<?php
if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : Team Posts / Programmer ++ Level
// Written By : Chuck Bunnell of EWD
// Written On : Aug 13, 2014
// Copyright Zeal Technologies
//***************************************************/

// admin can only come to this module thru the Teams Module
$thisteam = intval($_GET['team']);
if ($thisteam <= 0)
{
	header ("Location: ?tool=teams");
	exit();
}

// now make sure that the team actually exists
$stmt0 = $db->prepare("SELECT COUNT(id) as howmany FROM ".db_prefix."teams WHERE `id` = :id");
$stmt0->execute(array(':id'=>$thisteam));
$row0 = $stmt0->fetch();
$numteams = $row0['howmany'];

$teamexists = TRUE;
if ($numteams == 0)
{
	$teamexists = FALSE;
	echo '<p class="error_message">The team you have requested does not exist.<br />
			Please <a href="?tool=teams">return to the Teams page</a> and select a Team from there.</p>';
	die();
}

// get the sportscategory id for this team
$stmt01 = $db->prepare("SELECT sportscat_id FROM ".db_prefix."teams WHERE `id` = :id");
$stmt01->execute(array(':id'=>$thisteam));
$row01 = $stmt01->fetch(PDO::FETCH_ASSOC);
$sportscatid = $row01['sportscat_id'];



// if update make sure this id exists
update_verify();

// for image uploading
$upload_url  = $site_base_url . $cms_uploads . 'posts/';
$upload_dir  = $site_base_dir . $cms_uploads . 'posts/';
//$height = '300';
$width  = '600';
//$tn_height = '';
//$tn_width = '48';

if ($teamexists === TRUE)
{
	function execute()
	{
		switch($_GET['action'])
		{
			case 'update':
				update();
				break;
			case 'add':
				update();
				break;
			case 'remove':
				remove();
				break;
			default:
				manage();
		}
	}

	$stmt = $db->prepare("SELECT * FROM ".db_prefix."teams WHERE id = ?");
	$stmt->execute(array($thisteam));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$team = $row['name'];
	$catid = $row['sportscat_id'];

	// now get sports category for this team
	$stmt2 = $db->prepare("SELECT name FROM ".db_prefix."sportscategories WHERE id = ?");
	$stmt2->execute(array($catid));
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
	$sportscat = $row2['name'];
		
}


/***************************************************************
 *
 * function manage
 * Querys DB and Displays Content
 *
 **************************************************************/

function manage()
{
	global $db, $identifier, $module_name, $dbtbl, $teamexists, $upload_url, $thisteam, $sportscatid, $team, $sportscat;

	$link = '<a href="./?tool='.$identifier.'&team='.$thisteam.'&action=add">Add '.$module_name.'</a>';

	//$extra_header = null;
	
	$extra_header = ' - '.$sportscat.': '.$team;

	print_header('Manage '.$module_name.$extra_header,$link);

	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sortable" id="table">
		<thead>
			<tr align="left" valign="top">
				<th><h3>Label</h3></th>
				<th style="width:200px"><h3>Author</h3></th>
				<th style="width:170px"><h3>Date</h3></th>
				<th style="width:70px"><h3>Comments</h3></th>
				<th style="width:70px"><h3>Active</h3></th>
				<th class="nosort"><h3>Tools</h3></th>
			</tr>
		</thead>
		<tbody>';

			$stmt3 = $db->prepare('SELECT id,author_fname,author_lname,label,date,active FROM '.$dbtbl.' WHERE team_id = ? ORDER BY date ASC');
			$stmt3->execute(array($thisteam));
			$numposts = $stmt3->rowCount();
	
			while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC))
			{
				$post_id = intval($row3['id']);
				$author = $row3['author_fname'].' '.$row3['author_lname'];
				$label = $row3['label'];
				$date = $row3['date'];
				$act  = yes_no ( $row3['active'] );
				
				// qry comments table for this post
				$stmt4 = $db->prepare("SELECT COUNT(id) as howmany FROM ".db_prefix."comments WHERE `post_id` = :id");
				$stmt4->execute(array(':id'=>$post_id));
				$row4 = $stmt4->fetch();
				$numcomments = $row4['howmany'];
				
				$comments = '<a href="?tool=comments&post='.$post_id.'">'.$numcomments.'</a>';
	
				echo '<tr align="left" valign="middle">
					<td>' . $label . '</td>
					<td>' . $author . '</td>
					<td>' . $date . '</td>
					<td>' . $comments . '</td>
					<td>' . $act . '</td>
					<td style="padding:0;text-align:center;"><strong><a href="./?tool='.$identifier.'&team='.$thisteam.'&action=update&id='.$row3['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&team='.$thisteam.'&action=remove&id='.$row3['id'].'">Remove</a></strong></td>
				</tr>';
			}
		echo '</tbody>
	</table>';

	$pages = ( $pages > 20 ) ? true : false;

	echo_js_sorter ( $pages );

	echo '<div class="spacer">&nbsp;</div>';
}//end function


/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 * 
 **************************************************************/
function add( $errors = '' )
{
	global $db, $identifier, $module_name, $dbtbl, $action, $id, $teamexists, $upload_url, $upload_dir, $width, $height, $thisteam, $sportscatid, $userinfo, $team, $sportscat;
	
	if ( $errors )
	{
		echo '<ul class="error_message">
			<strong>Please fill in the required fields.</strong>';
			// set error messages for required fields
			if ( in_array('label', $errors ) )
			{
				echo '<li>This Post must have a Label.</li>';
				$val_label = ' class="form_field_error" ';
			}

			if ( in_array('used_label', $errors ) )
			{
				echo '<li> This Label is already being used.</li>';
				$val_label = ' class="form_field_error" ';      
			}
	
			if ( in_array('short_descr', $errors ) )
			{
				echo '<li>You must add a Short Description.</li>';
				$val_short_descr = ' class="form_field_error" ';
			}

			if ( in_array('long_descr', $errors ) )
			{
				echo '<li>You must add a Long Description.</li>';
				$val_long_descr = ' class="form_field_error" ';
			}

			if ( in_array('active', $errors ) )
			{
				echo '<li>You must select from active.</li>';
				$val_active = ' class="form_field_error" ';
			}

		echo '</ul>';

	} else {

		$c = ( $action == 'update' ) ? 'update this' : 'add a new';

		echo  '<ul class="notice_message"><strong>To ' . $c . ' record, fill out the form and click submit.</strong>
			<li>Keep the Label short and to the point. It\'s the attention getter.</li>
			<li>The Short Description is what Tier 1 users will see. Keep it short.</li>
			<li>The Long Description is what Tier 2 and up users will see.</li>
			<li>Image should be at around '.$width.'px wide for best results.</li>
			<li>Required fields have a red "*".</li>
		</ul>';
  }

	if ($action == "update")
	{
		$stmt = $db->prepare('SELECT * FROM '.$dbtbl.' WHERE id = ?');
		$stmt->execute(array($id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
	}

	if (!empty ($_POST))
	{
		$row = sanitize_vars ($_POST);
	}

/********************************************************
 * Start Building Form
 *******************************************************/

	$r = required();
	$author_id = ($action == "update") ? $row['author_id'] : $userinfo['id'];
	$author_type = ($action == "update") ? $row['author_type'] : 'a';
	$author_fname = ($action == "update") ? $row['author_fname'] : $userinfo['fname'];
	$author_lname = ($action == "update") ? $row['author_lname'] : $userinfo['lname'];

  echo '<form name="form" id="form" method="post"  action="#"enctype="multipart/form-data">
		<table>
    	<input type="hidden" name="id" value="'.$id.'" />
			<input type="hidden" name="author_id" value="'.$author_id.'" />
    	<input type="hidden" name="author_type" value="'.$author_type.'" />
    	<input type="hidden" name="author_fname" value="'.$author_fname.'" />
    	<input type="hidden" name="author_lname" value="'.$author_lname.'" />
    	<input type="hidden" name="sportscat_id" value="'.$sportscatid.'" />
    	<input type="hidden" name="team_id" value="'.$thisteam.'" />';
			echo '<tr>
				<td><label for="label">'.$r.'Label</label></td>
				<td><input ' . $val_label . ' type="text" name="label" id="label" value="'. htmlspecialchars($row['label']) .'" size="45" maxlength="200" /></td>
			</tr>';

			echo '<tr>
				<td><label for="short_descr">'.$r.'Short Description</label></td>
				<td><textarea ' . $val_short_descr . ' name="short_descr" id="short_descr" cols="34" rows="3">'. htmlspecialchars($row['short_descr']) .'</textarea>'.tooltip('This is what Tier 1 users will see. Keep it short.').'</td>
			</tr>';

			echo '<tr>
				<td><label for="long_descr">'.$r.'Long Description</label></td>
				<td><textarea ' . $val_long_descr . ' name="long_descr" id="long_descr" cols="50" rows="5">'. htmlspecialchars($row['long_descr']) .'</textarea>'.tooltip('This is what Tier 2 and higher users will see.').'</td>
			</tr>';

			echo '<tr>
				<td colspan="2">&nbsp;</td>
			</tr>';

			/****************************
			 * Image Uploading Section
			 ***************************/
			if ( $row['image'] != '' )
			{
        $filename = $upload_dir . $row['image'];
       // list ( $width, $height ) = img_size ( $filename );

        echo '<tr>
					<td><label for="current_file">Current Image</label></td>
        	<td><a href="' . $upload_url . $row['image'] . '" class="lightbox">' . $row['image'] . '</a></td>
					<input type="hidden" name="current_file" value="' . $row['image'] . '" />
        </tr>';
    	}

    	echo '<tr>
				<td><label for="image">Upload Image</label></td>
				<td><input name="image[]" type="file" id="image" size="15" />
					'.tooltip('Image will be resized to '.$width.'px wide. Images should be at least '.$width.'px wide before uploading for best results.').'</td>
    	</tr>';
			
    	echo '<tr>
				<td colspan="2">&nbsp;</td>
			</tr>';

			echo '<tr>    
				<td><label for="active">'.$r.'Active</label></td>  
				<td>
					'.create_slist ( $list, 'active', $row['active'], 1 ) . 
					tooltip('Set to Yes to allow on website').'
				</td>
			</tr>  ';

			echo '<tr>
				<td colspan="2" style="padding:3px;"><input type="submit" name="submit" value="Submit" /></td>
			</tr>';

		echo '</table>
	</form>';

  echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>';

}//end function


/***************************************************************
 *
 * function sanitize_vars
 * @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 **************************************************************/
function sanitize_vars( $data )
{
  global $id, $known_photo_types, $upload_dir, $width, $height, $teamexists, $thisteam;

	$r_data['author_id'] = intval ( $data['author_id'] );
	$r_data['author_type'] = stripslashes ( $data['author_type'] );
	$r_data['author_fname'] = stripslashes ( $data['author_fname'] );
	$r_data['author_lname'] = stripslashes ( $data['author_lname'] );
	$r_data['sportscat_id'] = intval ( $data['sportscat_id'] );
	$r_data['team_id'] = intval ( $data['team_id'] );
	$r_data['label'] = stripslashes ( $data['label'] );
	$r_data['short_descr'] = stripslashes ( $data['short_descr'] );
	$r_data['long_descr'] = stripslashes ( $data['long_descr'] );
	$r_data['image'] = $data['current_file'];
	$r_data['active'] = intval ( $data['active'] );

	if ( !empty ( $_FILES['image']['name'][0] ) )
	{
		$old_file = $r_data['image'];
		$image    = $_FILES['image'];

		$r_data['image'] = process_upload ( $id , '', $image, $known_photo_types, $upload_dir, 1, $width, $height, 0, 0 );

		if ( ( $old_file != $r_data['image'] ) && ( file_exists ( $upload_dir . $old_file ) ) && ( $old_file != '' ))
		{
			unlink ( $upload_dir . $old_file );
		}
	}
	
	return $r_data;
}


/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $db, $identifier, $module_name, $id, $dbtbl, $upload_dir, $teamexists, $thisteam, $team, $sportscat;

	$stmt = $db->prepare("SELECT * FROM ".$dbtbl." WHERE id = ?");
	$stmt->execute(array($id));
	$row    = $stmt->fetch(PDO::FETCH_ASSOC);

	$extra_header = ' - '.substr($row['label'],0,55).' ...';
	print_header('Delete '.$module_name . $extra_header);

	if ( !empty($_POST ))
	{
		//delete old image if there
		if ( file_exists ( $upload_dir . $row['image'] ))
		{
			@unlink( $upload_dir . $row['image'] );
		}

		$errno = 0;

		try 
		{
			$stmt = $db->prepare("DELETE FROM ".$dbtbl." WHERE id = ?");
			$stmt->execute(array($id));
		}

		catch(PDOException $ex)
		{
			$errno = $ex->getCode();
		}

		print_mysql_message ( $errno , $module_name, $id, 2, '', '', '&team='.$thisteam ) ;
	
	} else {
		
		echo '<form action="./?team='.$thisteam.'&tool='.$identifier.'&action=remove" method="post" name="form">
			<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to delete this record?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'&team='.$thisteam.'\'"></div>
		</form>';
	}

}//end function


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of contents
 *
 **************************************************************/

function update()
{
	global $db, $identifier, $module_name, $action, $id, $dbtbl, $thisteam, $team, $sportscat;
	
	$extra_header = ' - '.$sportscat.': '.$team;
	if ( $action == 'update' )
		print_header('Update '.$module_name.$extra_header);
	else
		print_header('Add New '.$module_name.$extra_header);

	if ( array_key_exists ('submit',$_POST))
	{
		require ("classes/validation.php");
		
		// set rules for required fields
		$rules   = array();
		$rules[] = "required,label,label";
		$rules[] = "required,short_descr,short_descr";
		$rules[] = "required,long_descr,long_descr";
		$rules[] = "required,active,active";
		$errors = validateFields($_POST, $rules);
	
		if ( $action != 'update')
		{
			$stmt = $db->prepare("SELECT id FROM ".$dbtbl." WHERE label = ?");
      $stmt->execute(array($_POST['label']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_label';
		}
		
		if ( $action == 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM ".$dbtbl." WHERE label = ? AND id != $id");
      $stmt->execute(array($_POST['label']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_label';
		}
	
	}

	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$vars = array();
		$data = sanitize_vars( $_POST );
		$type = ( $action == 'update' ) ? 0 : 1;
		
		if ( $action == 'update' )
		{
			$sql = "UPDATE ".$dbtbl." SET author_id = ?, author_type = ?, author_fname = ?, author_lname = ?, sportscat_id = ?, team_id = ?, label = ?, short_descr = ?, long_descr = ?, image = ?, active = ? WHERE id = ?";
			array_push($vars, $data['author_id']);
			array_push($vars, $data['author_type']);
			array_push($vars, $data['author_fname']);
			array_push($vars, $data['author_lname']);
			array_push($vars, $data['sportscat_id']);
			array_push($vars, $data['team_id']);
			array_push($vars, $data['label']);
			array_push($vars, $data['short_descr']);
			array_push($vars, $data['long_descr']);
			array_push($vars, $data['image']);
			array_push($vars, $data['active']);
			array_push($vars, $id);
		
		} else {
			
			$sql = 'INSERT INTO '.$dbtbl.' (author_id, author_type, author_fname, author_lname, sportscat_id, team_id, label, short_descr, long_descr, image, active) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
			array_push($vars, $data['author_id']);
			array_push($vars, $data['author_type']);
			array_push($vars, $data['author_fname']);
			array_push($vars, $data['author_lname']);
			array_push($vars, $data['sportscat_id']);
			array_push($vars, $data['team_id']);
			array_push($vars, $data['label']);
			array_push($vars, $data['short_descr']);
			array_push($vars, $data['long_descr']);
			array_push($vars, $data['image']);
			array_push($vars, $data['active']);
		}
		
		$errono = 0;

		try 
		{
			$stmt = $db->prepare($sql);
			$stmt->execute($vars);
		}

		catch(PDOException $ex)
		{
			$errono = $ex->getCode();
		}
		
		if ( is_saint() && $errono != 0 )
			echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

		print_mysql_message ( $errono , $module_name, $id, $type, '', '', '&team='.$thisteam ) ;

	} else {
		
		add( $errors );
	}

}//end function

?>