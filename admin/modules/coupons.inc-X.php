 <?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Coupons Module

// Written By : Darrell Bessent of EWD

// Written On : May 16, 2013

// Revision   : 164

// Updated by : Darrell Bessent of EWD

// Updated On : May 16, 2013

// Description: This module is used with websites that 

//							want to post business coupons 

//						  (such as a coupon directory website). 

// Copyright Zeal Technologies 

//***************************************************/



/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);





$mod_config  = get_module_settings ( 'coupons' );



$upload_file_url  = $site_base_url . '/cmsuploads/coupons/';

$upload_file_dir  = $site_base_dir . '/cmsuploads/coupons/';



$upload_image_url  = $site_base_url . '/cmsuploads/coupons/';

$upload_image_dir  = $site_base_dir . '/cmsuploads/coupons/';



$width = '';

$height = '';



function execute()

{

	switch($_GET['action'])

	{

		case 'update':

			update();

			break;

		case 'add':

			update();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Slider Content

 *

 *

 **************************************************************/



function manage()

{



  global $upload_image_dir,$upload_image_url,$upload_file_dir,$upload_file_url;

  

  $i    = 0;

  $link = '<a href="./?tool=coupons&action=add">Add A Coupon</a>';

  

  print_header('Manage Coupons',$link);

  

  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 

       ' 	  <thead>' . "\n" . 

       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" .

			 ' 			  <th><h3>Page On</h3></th>' . "\n" . 

       ' 			  <th><h3>Label</h3></th>' . "\n" . 	

			 ' 			  <th><h3>Start Date</h3></th>' . "\n" . 

       ' 			  <th><h3>End Date</h3></th>' . "\n" .

			 ' 			  <th><h3>Image</h3></th>' . "\n" .    

       ' 			  <th style="width:50px; text-align:center;"><h3>Order</h3></th>' . "\n" . 

       ' 			  <th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . 

       ' 			  <th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" . 

       '		 </tr></thead><tbody>' . "\n";

  

  $results = mysql_query('SELECT * FROM ' . db_prefix . 'coupons ORDER BY weight,label');

  $pages   = mysql_num_rows($results);

  while ($row = mysql_fetch_array($results))

  {    



    $i++;

    

    $image_link 				 = 'No File Uploaded';

		$file_link 					 = 'No File Uploaded';

		$pageon_id					 = $row['sub_cat_id'];

    $label							 = $row['label'];

		$notes							 = $row['notes'];

		$upload_date				 = $row['upload_date'];

		$start_date					 = $row['start_date'];

		$end_date						 = $row['end_date'];

		

    $image_filename = $upload_image_dir . $row['image'];

		$file_filename = $upload_file_dir . $row['file'];

    

    $image_link = ( file_exists ( $image_filename ) ) ? '<a href="' . $upload_image_url . $row['image'] . '" class="lightbox">' . $row['image'] . '</a>' : $image_link;

    $file_link = ( file_exists ( $file_filename ) ) ? '<a href="' . $upload_file_url . $row['file'] . '" target="_blank">' . $row['file'] . '</a>' : $file_link;

			          

    $act = yes_no ( $row['active'] );

    $notes = substr($notes, 0, 60) . '...';

		

		$page_qry = mysql_query("SELECT label FROM cms_coupon_cats WHERE `id`='$pageon_id'");

		$prow = mysql_fetch_assoc($page_qry);

		$pageon_label = $prow['label'];

    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

		echo '<td style="padding-left:20px;">' . $pageon_label . '</td>' . "\n"; 

    echo '<td style="padding-left:20px;">' . $label . '</td>' . "\n"; 		

		echo '<td style="padding-left:20px;">' . $start_date . '</td>' . "\n";

		echo '<td style="padding-left:20px;">' . $end_date . '</td>' . "\n";

		echo '<td style="padding-left:20px;">' . $image_link . '</td>' . "\n"; 		

    echo '<td text-align:center;">' . $row['weight'] . '</td>' . "\n"; 

    echo '<td text-align:center;">' . $act . '</td>' . "\n"; 

    echo '<td valign="middle"><strong><a href="./?tool=coupons&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=coupons&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

    echo '</tr>' . "\n";

	  

  }//end while



  echo '</tbody></table>';



  $pages = ( $pages > 20 ) ? true : false;

	

  echo_js_sorter ( $pages );

             

}//end function



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Banner Form for User Input

 *

 *

 **************************************************************/



function add( $errors = '' )

{

  global $id, $action, $site_base_url, $site_base_dir, $upload_image_url, $upload_image_dir, $upload_file_url, $upload_file_dir, $mod_config, $height, $width;

   

   

  if ( $errors )

  {

   echo '<div class="error_message">';

   if ( in_array('label', $errors ) )

     echo '* You must fill out a label.<br/>';

   echo '</br></div>' . "\n";

  } else {

    $c = ( $action == 'update' ) ? 1 : 0;

    print_notice_message ( $c );

  }

  

  if ($action == "update")

  {

   // Run the query for the existing data to be displayed.

   $results = mysql_query('SELECT * FROM ' . db_prefix . 'coupons WHERE id = "' . $id . '"');

   $row 				= mysql_fetch_array($results);

  } 

	if (!empty ($_POST))

	{	

   $row     = sanitize_vars ($_POST);

  }//end if

  

	echo '<div class="notice_message">NOTES : <br /> 

					<ul>

						<li>The `LABEL` is the label for the coupon post.</li>

						<li>The `ALT TAG` sets the text displayed when visitor browers do not support images. The ALT tag also helps search engines.</li>

						<li>The `TITLE TAG` sets the text that will be displayed when a visior hovers the mouse on the image.</li>

						<li>The `START DATE` is the date you wish for this coupon to start displaying on the website.</li>

						<li>The `END DATE` is the date you wish for this coupon to be removed from the website.</li>

						<li>The `BELONGS TO` is the business that this coupon belongs to.</li>

						<li>The `PAGE ON` is the page you wish for the coupon to be displayed on.</li>

						<li>The `IMAGE` is the image related to the coupon. (This is the coupon image that is displayed when a user clicks `Get Coupon.`)</li>

						<li>The `FILE` may be a *.DOC, *.DOCX or *.PDF. (This is the printable coupon *.pdf/*.doc file.)</li>

						<li>The `ORDER` is the order weight in which you wish for the coupon to be listed in the manage area.</li>

						<li>The `ACTIVE` is used to control an coupons status or disable an coupon before the ending date.</li>

					<ul>	

				</div>';

	

  echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

  $t_index = 1;

  echo '   <tr>' . "\n".

       '      <td > <label for="label">Label</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="label" id="label" value="'. $row['label'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

			 

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="alt_tag">ALT Tag</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="alt_tag" id="alt_tag" value="'. $row['alt_tag'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

			 

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="title_tag">Title Tag</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="title_tag" id="title_tag" value="'. $row['title_tag'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";		 		 



			 	  

			if ($row['upload_date'] == '') 

			 { 

			 $row['upload_date'] = date("Y-m-d"); 

			 } 

	//$t_index++;

  //echo '   <tr>' . "\n".

  //     '      <td > <label for="upload_date">Upload Date</label></td>  ' . "\n" . 

  //     '      <td > <input type="text" name="upload_date" id="upload_date" value="'. date("Y-m-d") .'" size="45" tabindex="' . $t_index . '" readonly />  ' .

  //     '   <span class="note" style="padding-left:5px;"> ( READ ONLY. ) </span></td>' . "\n" . '</tr>' . "\n";		  	 



	

	$t_index++;

   echo '   <tr>' . "\n" .

	 			'      <td > <label for="start_date">Start Date</label></td>  ' . "\n" . 

				'			<td> <input type=text name="start_date" size=20 value="'; if (!empty($row['start_date'])) echo $row['start_date']; echo '">' . "\n" . 

				'			<a href="javascript:show_calendar(\'signup.start_date\',\'YYYY-MM-DD\');" onmouseover="window.status=\'Date Picker\';return true;" onmouseout="window.status=\'\';return true;"><img src="images/show-calendar.gif" width=18 height=18 border=0 align="bottom" style="vertical-align:-4px;" /></a></td>' . "\n" . 

				'   </tr>' . "\n";	

			 

	$t_index++;

   echo '   <tr>' . "\n" .

	 			'      <td > <label for="end_date">End Date</label></td>  ' . "\n" . 

				'			<td> <input type=text name="end_date" size=20 value="'; if (!empty($row['end_date'])) echo $row['end_date']; echo '">' . "\n" . 

				'			<a href="javascript:show_calendar(\'signup.end_date\',\'YYYY-MM-DD\');" onmouseover="window.status=\'Date Picker\';return true;" onmouseout="window.status=\'\';return true;"><img src="images/show-calendar.gif" width=18 height=18 border=0 align="bottom" style="vertical-align:-4px;" /></a></td>' . "\n" . 

				'   </tr>' . "\n";				 



 	$t_index++;

	echo '   <tr>' . "\n" .

		 	 ' 						<td ><label for="bus_id">Belongs To</label></td>  ' . "\n" . 

			 ' 						<td >' . "\n" . 

			 ' 						<select id="bus_id" name="bus_id">' . "\n";

			

				$page_qry = mysql_query("SELECT id,label FROM cms_businesses WHERE `active`='1' ORDER BY weight");

				$ptotnum = mysql_num_rows($page_qry);

				

				for ($i=1; $i<=$ptotnum; $i++)

				{			

							$prow = mysql_fetch_assoc($page_qry); 

							echo ' 									<option value="'.$prow['id'].'" '; if ( $row['bus_id'] == $prow['id'] ) echo 'SELECTED'; echo ' >'.$prow['label'].'</option>' . "\n";

				}

							

							

 echo  ' 						</select>' . "\n" . 

			 ' 						<span class="note" style="padding-left:5px;"> ( Select the business the coupon belongs to. ) </span>' . "\n" . 

			 ' 						</td>  ' . "\n" . 

			 '				</tr>  '. "\n";	

 

  

	$t_index++;

	echo '   <tr>' . "\n" .

		 	 ' 						<td ><label for="sub_cat_id">Page On</label></td>  ' . "\n" . 

			 ' 						<td >' . "\n" . 

			 ' 						<select id="sub_cat_id" name="sub_cat_id">' . "\n";

			

				$page_qry = mysql_query("SELECT id,label FROM cms_coupon_cats WHERE `active`='1' ORDER BY weight");

				$ptotnum = mysql_num_rows($page_qry);

				

				for ($i=1; $i<=$ptotnum; $i++)

				{			

							$prow = mysql_fetch_assoc($page_qry); 

							echo ' 									<option value="'.$prow['id'].'" '; if ( $row['sub_cat_id'] == $prow['id'] ) echo 'SELECTED'; echo ' >'.$prow['label'].'</option>' . "\n";

				}

							

							

 echo  ' 						</select>' . "\n" . 

			 ' 						<span class="note" style="padding-left:5px;"> ( Select the page you would like this coupon to be displayed on. ) </span>' . "\n" . 

			 ' 						</td>  ' . "\n" . 

			 '				</tr>  '. "\n";	

	

	

	

	

  /****************************

  * File Uploading Section

  ***************************/

  

	// image upload handler

  if ( $row['image'] != '' )

  {

    

   $image_filename      = $upload_image_dir . $row['image'];

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="current_image">Current Image</label></td>' . "\n";

   echo '<td ><a href="' . $upload_image_url . $row['image'] . '" class="lightbox">' . $row['image'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="current_image" value="' . $row['image'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="image">Upload Image</label></td>' ."\n";

  echo '<td ><input name="image" type="file" id="image" size="15">' . "\n";

  echo '<span class="note">( File must be a *.JPG, *.PNG, or *.GIF. Coupon images must not have a width no larger than 300px. )</span></td>' . "\n";

  echo '</tr>' . "\n";

 

 	// file upload handler

   if ( $row['file'] != '' )

  {

    

   $file_filename      = $upload_file_dir . $row['file'];

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="current_file">Current File</label></td>' . "\n";

   echo '<td ><a href="' . $upload_file_url . $row['file'] . '">' . $row['file'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="current_file" value="' . $row['file'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="file">Upload File</label></td>' ."\n";

  echo '<td ><input name="file" type="file" id="file" size="15">' . "\n";

  echo '<span class="note">( File must be a *.DOC, *DOCX, *.PDF or *.RTF )</span></td>' . "\n";

  echo '</tr>' . "\n";



  $t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="weight">Order</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />' . "\n" .

       ' 						<span class="note" style="padding-left:5px;"> ( Recommend increments of 10 ) </span>' . "\n" .

       '      </td>' . "\n" .

       '   </tr>' . "\n";

  

  $t_index++;

		echo '   <tr>' . "\n" .

							' 						<td ><label for="active">Active</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="active" name="active">' . "\n" . 

							' 									<option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 

							' 									<option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 

							' 						</select>' . "\n" . 

						 ' 						<span class="note" style="padding-left:5px;"> ( Set option to `Yes` to display on the website. ) </span>' . "\n" . 

       ' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";		 

	

 // This is our submit

  $t_index++;

  echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";  

  echo '</table></form>' . "\n";

  echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>' . "\n";



}





/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

  global $upload_file_dir, $upload_image_dir, $mod_config, $known_photo_types, $known_file_types;

	

  $r_data['bus_id']  			 	  = mysql_real_escape_string ( $data['bus_id'] );

	$r_data['sub_cat_id']  			= mysql_real_escape_string ( $data['sub_cat_id'] );

  $r_data['label'] 	 					= mysql_real_escape_string ( $data['label'] );

	$r_data['alt_tag'] 	 				= mysql_real_escape_string ( $data['alt_tag'] );

	$r_data['title_tag'] 		 	  = mysql_real_escape_string ( $data['title_tag'] );

	//$r_data['upload_date']  	  = mysql_real_escape_string ( $data['upload_date'] );

	$r_data['start_date']  		  = mysql_real_escape_string ( $data['start_date'] );

	$r_data['end_date']  			  = mysql_real_escape_string ( $data['end_date'] );

  $r_data['image']  				  = $data['current_image'];

	$r_data['file']  					  = $data['current_file'];

	$r_data['weight']  					= mysql_real_escape_string ( $data['weight'] );

	$r_data['active'] 				  = mysql_real_escape_string ( $data['active'] );

			

  $tid = ( intval ( $data['id'] ) == 0 ) ? fetch_lastid ('coupons') : $data['id'];

      		 

				 

  if ( !empty ( $_FILES['file']['name'] ) )

  {

            

    $old_file        = $r_data['file'];

    $file            = $_FILES['file'];

		

		$r_data['file'] = process_upload_file ( $tid , $file, $known_file_types, $upload_file_dir);

						

    if ( ( $old_file != $r_data['file'] ) && ( file_exists ( $upload_file_dir . $old_file ) ) && ( $old_file != '' ))

		{

			unlink ( $upload_file_dir . $old_file );

		}

  

	}

	

  if ( !empty ( $_FILES['image']['name'] ) )

  {

            

    $old_image        = $r_data['image'];

    $image            = $_FILES['image'];

    

		$r_data['image'] = process_upload ( $tid , '', $image, $known_photo_types, $upload_image_dir, 1, $width, $height );

			

    if ( ( $old_image != $r_data['image'] ) && ( file_exists ( $upload_image_dir . $old_image ) ) && ( $old_image != '' ))

		{

			unlink ( $upload_image_dir . $old_image );

		}

  

	}

			 

  if ( empty($_POST['weight']) )

  {

    $r_data['weight']  = fetch_weight('coupons', 'weight' );

  }

  

  return $r_data;



}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 * Unlinks Existing Photo

 *

 **************************************************************/



function remove()

{



	global $id, $upload_file_dir, $upload_image_dir, $module_name;

	

	$result = mysql_query("SELECT * FROM " . db_prefix . "coupons WHERE id = '$id'");

	$row    = mysql_fetch_array($result);

	

	print_header('Delete Banner Ad - ' . $row['label']);

	

	if ( !empty($_POST ))

	{		

					

		if ( file_exists ( $upload_file_dir . $row['file'] ))

		{

				@unlink( $upload_file_dir . $row['file'] );

		}

		

		if ( file_exists ( $upload_image_dir . $row['image'] ))

		{

				@unlink( $upload_image_dir . $row['image'] );

		}

		

		

		$result = mysql_query('DELETE FROM ' . db_prefix . "coupons WHERE id = '$id'");

		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

			

	} else {

	

		echo '<form action="./?tool=coupons&action=remove" method="post" name="form">' . "\n" .

							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

							'<tr> <td><div align="center">Are you sure you want to delete this coupon?</div></td></tr>' . "\n" .

							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=coupons\'"></div></td></tr>' . "\n" .

							'</table></form>';

	}//end if

}//end function





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

		global $action, $id, $module_name;

		

		if ( $action == 'update' )

				print_header('Update Coupon');

		else

				print_header('Add Coupon');

 

		if ( array_key_exists ('submit',$_POST))

		{				

    require_once("classes/validation.php");

  

    $rules = array();    

    

    $rules[] = "required,label,label";

    

    $errors = validateFields($_POST, $rules);

    

		}

		

		if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

	 {

		

				$data  = sanitize_vars( $_POST );			

		 	  $type  = ( $action == 'update' ) ? 0 : 1;

				$notes = str_replace('\n', '<br />', $data['notes']);

				

				$qry_bus_label = mysql_query("SELECT label FROM ".db_prefix."businesses WHERE `id`='".$data['bus_id']."'");

				$row_bus_label = mysql_fetch_assoc($qry_bus_label);

				

				

				if ( $action == 'update' )

						$sql = "UPDATE " . db_prefix . "coupons SET bus_id = '" . $data['bus_id'] . "', bus_label = '" . $row_bus_label['label'] . "', sub_cat_id = '" . $data['sub_cat_id'] . "', label = '" . $data['label'] . "', alt_tag = '" . $data['alt_tag'] . "', title_tag = '" . $data['title_tag'] . "', upload_date = '" . date("Y-m-d") . "', start_date = '" . $data['start_date'] . "', end_date = '" . $data['end_date'] . "', file = '" . $data['file'] . "', image = '" . $data['image'] . "', weight = '" . $data['weight'] . "', active = '" . $data['active'] . "'	WHERE id = '$id'";

 		 else

					 $sql = 'INSERT INTO ' . db_prefix . 'coupons (bus_id, bus_label, sub_cat_id, label, alt_tag, title_tag, upload_date, start_date, end_date, image, file, weight, active) VALUES ' .

												 "('". $data['bus_id'] . "', '" . $row_bus_label['label'] . "', '" . $data['sub_cat_id'] . "', '" . $data['label'] . "', '" . $data['alt_tag'] . "', '" . $data['title_tag'] . "', '" . date("Y-m-d") . "', '" . $data['start_date'] . "', '" . $data['end_date'] . "', '" . $data['image'] . "', '" . $data['file'] . "', '" . $data['weight'] . "', '" . $data['active'] . "')";

											

   // echo $sql;

    

				mysql_query ( $sql );

    

		// if ( is_saint() && mysql_errno != 0 )

    if ( mysql_errno != 0 )

      echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

   			

				print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;   

		

  } else {

		add( $errors );

	}//end if	

}//end function







?>