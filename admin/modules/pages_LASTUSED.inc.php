<?php

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Content Pages

// Written By : Shawn Crigger of EWD

// Written On : May 24, 2010

// Updated by : Chuck Bunnell / Darrell Bessent of EWD

// Updated On : Feb. 14, 2013

// Copyright Zeal Technologies

//***************************************************/





/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);



$module_name = get_module_name();

$mod_config  = get_module_settings ('pages');

$use_seo     = $config['use_seo'];



/**

 *

 *  Setup Arrays for Pages That Can Not Be Deleted, or Have Subpages Added

 *

 */



$str_del  = array('1','2','3','4','5','6','7','8','9','10');



$bad_subs = explode (',', $mod_config['no_subpages'] );

$bad_del  = array_merge ( $str_del, explode (',', $mod_config['no_delete'] ) );



function execute()

{

	switch($_GET['action'])

	{

		case 'update':

			update();

			break;

//		case 'add':

//			update();

//			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 *

 **************************************************************/



function manage()
{
	global $action, $id, $bad_subs, $bad_del, $use_seo;



	//$link = '<a href="./?tool=pages&action=add">Add a Website Page</a>';

	$link = '';

	print_header('Main Website Pages', $link );



	$th  = "\n";

	$s   = 0;

	$seo = $use_seo;



	if ( is_saint() )

	{

		$th = '<th style="width:50px; text-align:center; font-weight:bold;"><h3>ID</h3></th>' . "\n";

		$s  = 3;

	}



	echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" .

			 '<thead>' . "\n" .

			 '	<tr align="left" valign="top" bgcolor="#ebebeb">' . "\n" .

			 $th .

			 '		<th><h3>Navigation Name</h3></th>' . "\n" .

			 '		<th><h3>Link to Page</h3></th>' . "\n" .

			 '		<th style="width:50px"><h3>Order</h3></th>' . "\n" .

			 '		<th style="width:50px"><h3>Active</h3></th>' . "\n" .

			 '		<th style="width:100px" class="nosort" colspan="3" ><h3>Tools</h3></th>'.  "\n" .

			 '	</tr></thead><tbody>' . "\n";



	$query = mysql_query("SELECT * FROM " . db_prefix . "pages WHERE onpage_id = '0' AND type = 'main page' ORDER BY weight");

	$pages = mysql_num_rows ( $query );

	while ( $row = mysql_fetch_array ( $query ))

	{

		$active = yes_no ( $row['active'] );



		$td = '';

		if ( is_saint() )

				$td = '  <td style="width:50px; text-align:center; font-weight:bold;">'.$row['id'].'</td>' ."\n";



		$link = ( $use_seo == 0 ) ? 'index.php&amp;page=' . $row['link'] : '/' . $row['link'];



		echo '<tr>' ."\n" . $td .

							'  <td style="padding-left:20px;" >'.$row['nav_name'].'</td>' ."\n" .

							'  <td style="padding-left:20px;" >'.$link.'</td>' ."\n" .

							'  <td style="width:50px; text-align:center;">'.$row['weight'].'</td>' ."\n" .

							'  <td style="width:50px; text-align:center;">'.$active.'</td>' ."\n" .

							'  <td style="width:50px; text-align:center;"><a href="./?tool=pages&action=update&id='.$row['id'].'">update</a></td>' ."\n" .

							'<td style="width:50px; text-align:center;">' ."\n" ;



		echo ( in_array($row['id'], $bad_del ) ) ? '<span class="notool">remove</span>' : '<a href="./?tool=pages&action=remove&id='.$row['id'].'">remove</a>';



		echo '</td></tr>' ."\n";

	} // end while



	echo '</tbody></table>';



	$pages = ( $pages > 20 ) ? true : false;



	echo_js_sorter ( $pages, $s );



	echo '<div class="spacer">&nbsp;</div>' . "\n";



}





/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 *

 **************************************************************/



function remove()

{

	global $id, $module_name;



	$query = mysql_query("SELECT id,nav_name FROM " . db_prefix . "pages WHERE id = '".$id."'");

	$row = mysql_fetch_assoc($query);

	$totalRows = mysql_num_rows($query);



	print_header('Delete a Website Page - ' . $row['nav_name'] );



	if($totalRows == '0' || empty($id))

	{

		echo '<p class="error_message">This page does not exist.</p>';

	} else if($row['id'] == '1') { // home page

		echo '<p class="error_message">The "'.$row['nav_name'].'" page can NOT be deleted.</p>';

	} else if($row['id'] < '11') { // contact page and misc pages

		echo '<div class="error_message">The "'.$row['nav_name'].'" page can NOT be deleted.<br />' . "\n" .

				 'To make this page inactive, <a class="tools" href="./?tool=pages&action=update&id='.$row['id'].'">click here</a> and set "Active?" to "No."</div>';

	} else {



		if(!empty($_POST))

		{

			$deleteSQL = mysql_query("DELETE FROM " . db_prefix . "pages WHERE id = '".$row['id']."'");

			print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

		} else {

				echo '<form action="./?tool=pages&action=remove" method="post" name="form">' . "\n" .

						 '<input type="hidden" name="id" value="' . $id . '"><table width="80%" border="0">' . "\n" .

						 '<tr> <td><p style="text-align:center"><strong>You are about to Delete the "'.$row['nav_name'].'" page.</strong></p></td></tr>' .

						 '<tr> <td><div align="center">Are you sure you want to delete this record?</div></td></tr>' . "\n" .

						 '<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

						 '<input name="No" type="button" value="No" onClick="window.location = \'./?tool=pages\'"></div></td></tr>' . "\n" .

						 '</table></form>';

		} // end if POST

	} // end if totrows

}





/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

	$r_data['label']       = mysql_real_escape_string ( $data['label'] );

	$r_data['content'] = mysql_real_escape_string ( stripslashes ( $data['content'] ));

	$r_data['meta_title']  = mysql_real_escape_string ( $data['meta_title'] );

	$r_data['meta_kw']     = mysql_real_escape_string ( $data['meta_kw'] );

	$r_data['meta_descr']  = mysql_real_escape_string ( $data['meta_descr'] );



	return $r_data;

}







function add( $errors )

{

	global $action, $id, $bad_subs;



	$base_path = dirname(__FILE__);



	include_once $base_path . '/ckeditor/ckeditor.php' ;

	$option = '';



	if ($action == "update")

	{

		// Run the query for the existing data to be displayed.

		$results = mysql_query('SELECT * FROM ' . db_prefix . 'pages WHERE id = "' . $id . '"');

		$row 				= mysql_fetch_array($results);

		print_header ('Update A Website Page');



	} else {

		print_header ('Add A Website Page');

  }//end if



	if (!empty ($_POST))

	{

   $row     = sanitize_vars ($_POST);

  }//end if



	if ( $errors != '' )

	{

	//$errormsg = implode(' ', $errors);

		echo '<div class="error_message"><strong>ERROR:</strong><br />'.$errors.'</div>';

	} else {

		echo  '<div class="notice_message">To edit this page, fill out the form and click submit.<br />' . "\n" .

					'<ul><strong>NOTES:</strong>' . "\n" .

					'<li> The Page Label is what will appear at the top of the content area for that page.</li>' . "\n" .

					'<li> Hold <strong>Shift</strong> and press <strong>Enter</strong> at same time to Start a new paragraph.</li>' . "\n" .

					'</ul></div>' . "\n";

	}



	$r = required();



	echo '<form name="signup" id="signup" method="post" action="#" ><table>' . "\n";

	echo '<input type="hidden" name="id"   value="' . $id . '" />';



	echo '   <tr>' . "\n" .

						'					<td  class="labelcell"> <label for="label">'.$r.' Page Label: </label></td>' . "\n" .

						'				 <td > <input ' . $val_label . ' type="text" name="label" id="label" value="'. $row['label'] .'" size="45" maxlength="150" tabindex="1" />' . "\n" .

						' <span class="note">(Up to 150 characters)</span></td>' . "\n" .

						'			 </tr>' . "\n";



	echo '<tr><td colspan="2" style="width:100%">&nbsp;</td></tr>' . "\n";



	echo  '<tr><td colspan="2" style="text-align:center; width:100%">' . "\n" .

				'<textarea style="min-height:270px; height:270px; width:100%;" name="content" id="content">' . $row['content'] . '</textarea>' . "\n" .

				'</td></tr>' . "\n";



	$ckeditor  = new CKEditor( ) ;

	$ck_config = set_fck_config();

	$ck_events = set_fck_events();



	$ck_config['toolbar'] = set_fck_toolbar();

	$ck_config['height']  = 200;

	$ck_config['width']   = '102%';



// EDIT BELOW LINE TO ADD YOUR WEBSITE STYLESHEET

//		$ck_config['contentsCss'] = $base_site_url . '/styles/main.css';

	$ckeditor->replace("content", $ck_config, $ck_events);





	echo '<tr><td colspan="2" style="width:100%">&nbsp;</td></tr>' . "\n";



	echo  '<tr><td colspan="2" style="width:100%"><p class="center"><strong>SEARCH ENGINE OPTIMIZATION</strong></p>' . "\n" .

				'<p style="text-align:left;"><strong>Meta Tags:</strong> The keywords and keyphrases need ' . "\n" .

				'to be in the content of the page and in all three meta tags below. Do NOT use special ' . "\n" .

				'characters in the text.<br />' . "\n" .

				'<strong>Title Example:</strong> Cougars in the wild, Cougars in America<br />' . "\n" .

				'<strong>Keyword Example:</strong> cougars in the wild,cougars in ' . "\n" .

				'america,cougars,wild,america<br />' . "\n" .

				'<strong>Description Example:</strong> There are very few cougars in the wild with ' . "\n" .

				'only a small amount of cougars in America.</p>' . "\n" .

				'<p><span class="note">NOTE: keywords and phrases are seperated only with a comma. ' . "\n" .

				'Description should be under 150 characters long.</span></p>' . "\n" .

				'</td></tr>' . "\n";





	$t_index++;

	echo  '   <tr>' . "\n" .

				'					<td  class="labelcell"> <label for="meta_title">Meta Title: </label></td>' . "\n" .

				'				 <td >' . "\n" .

				'							<textarea style="width:80%" name="meta_title" id="meta_title" cols="70" rows="2" tabindex="' . $t_index . '">' . $row['meta_title'] . '</textarea>' . "\n" .

				'						</td>' . "\n" .

				'			 </tr>' . "\n";



	$t_index++;

	echo  '   <tr>' . "\n" .

				'					<td  class="labelcell"> <label for="meta_kw">Meta Keywords: </label></td>' . "\n" .

				'				 <td >' . "\n" .

				'							<textarea style="width:80%" name="meta_kw" id="meta_kw" cols="70" rows="2" tabindex="' . $t_index . '">' . $row['meta_kw'] . '</textarea>' . "\n" .

				'						</td>' . "\n" .

				'			 </tr>' . "\n";



	$t_index++;

	echo  '   <tr>' . "\n" .

				'					<td  class="labelcell"> <label for="meta_descr">Meta Description: </label></td>' . "\n" .

				'				 <td >' . "\n" .

				'							<textarea style="width:80%" name="meta_descr" id="meta_descr" cols="70" rows="2" tabindex="' . $t_index . '">' . $row['meta_descr'] . '</textarea>' . "\n" .

				'						</td>' . "\n" .

				'			 </tr>' . "\n";



	echo '<tr><td colspan="2" style="width:100%">&nbsp;</td></tr>'. "\n";

	echo '<tr><td colspan="2" style="width:100%; text-align:center; margin:0 auto; padding:3px; "><input name="Submit" type="submit" value="Submit" class="button"> </td></tr>'. "\n";

	echo '</table></form>'. "\n";

	echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>' . "\n";

	echo '<div class="spacer">&nbsp;</div>';



}//end if empty get id





function update()

{

	global $action, $id, $module_name, $bad_del, $bad_subs;



	$data = sanitize_vars ( $_POST );

	if ( !empty($_POST ) && $action == 'update' )

	{

		$query   = mysql_query("SELECT * FROM " . db_prefix . "pages WHERE id = '$id'");

		$row     = mysql_fetch_assoc($query);

		$totRows = mysql_num_rows($query);



		if ( ( $id == '0' || $totRows == '0') && ( $action == 'update' ))

		{

			$error = true;

			$errormsg .= 'This page does not exist.';

			add($errormsg);

			exit(0);

		}



	}



	if ( ( $action == 'update' ) && ( !empty($_POST) ) )

	{

		$error = false;

		if ( "" == $data['label'] )

		{

			$error = true;

			$errormsg .= "The Page Label is required.<br />";

		}//end if

			// Query to search for duplicate label

			$results = mysql_query("SELECT id FROM " . db_prefix . "pages WHERE label = '".$data['label']."' AND id != '".$row['id']."'");

			if (mysql_num_rows($results) > 0)

			{

				$error = true;

				$errormsg .= "This Page Label is already in use.<br />";

			}//end if



			if ( false === $error)

			{

				$data['content'] = strip_fck_strip_value( $data['content']) ;



				$updateSQL = "UPDATE " . db_prefix . "pages SET

						label = '".$data['label']."',

						content = '".$data['content']."',

						meta_title = '".$data['meta_title']."',

						meta_kw = '".$data['meta_kw']."',

						meta_descr = '".$data['meta_descr']."',

						date = '".date('Y-m-d')."' WHERE id = ".$row['id'];



				mysql_query($updateSQL);



				$msg = 'The page "'.$data['label'].'" has been updated.';



				print_mysql_message ( mysql_errno() , $module_name, $id, 0 , $msg) ;

			} else {

				add($errormsg);

			}//end if (!$error)



	} elseif ( $action == 'add' && !empty($_POST) )

	{



			$error = false;

			if ($data['label'] == "")

			{

				$error = true;

				$errormsg .= "The Page Label is required.<br />";

			}//end if



			// Query to search for duplicate label

			$results = mysql_query("SELECT id FROM " . db_prefix . "pages WHERE label = '".$data['label']."'");

			if (mysql_num_rows($results) > 0)

			{

				$error = true;

				$errormsg .= "This Page Label is already in use.<br />";

			}//end if



			if ( false == $error )

			{



				$data['content'] = strip_fck_strip_value( $data['content']) ;



				$insertSQL = "INSERT INTO " . db_prefix . "pages (label, content, meta_title,

					meta_kw, meta_descr, date) VALUES ('".$data['label']."',

					'".$data['content']."', '".$data['meta_title']."',

					'".$data['meta_kw']."', '".$data['meta_descr']."', '".date('Y-m-d')."')";



				mysql_query($insertSQL);



				if ( is_saint() && mysql_errno != 0 )

					echo '<pre>' . $insertSQL . '</pre><hr><br><br>' . mysql_error;



				$msg = 'The page "'.$data['label'].'" has been added.';

				print_mysql_message ( mysql_errno() , $module_name, $id, 1, $msg ) ;

			} else {

				add($errormsg);

			}// end if !$error

		} else {



			add($errormsg);

		}

}





?>