<?php

//****************************************************/

// Module     : Module Settings Config / Programmer ++ Level 

// Written By : Shawn Crigger of EWD

// Written On : Jan 5, 2011

// Revision   : 1

// Copyright Zeal Technologies & S-Vizion Software
//***************************************************/



/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);



function execute()

{

	switch($_GET['action'])

	{

		case 'update':

			update();

			break;

		case 'add':

			update();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Slider Content

 *

 *

 **************************************************************/



function manage()

{

  global $db;

	

  $i    = 0;

  $link = '<a href="./?tool=ticker&action=add">Add Ticker Text</a>';

  

  print_header('Manage Ticker Texts',$link);

  

  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 

       ' 	  <thead>' . "\n" . 

       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" . 

       ' 		<th><h3>Label</h3></th>' . "\n" .        

			 ' 		<th style="width:50px;text-align:center;"><h3>Page</h3></th>' . "\n" . 

			 ' 		<th style="width:50px;text-align:center;"><h3>Order</h3></th>' . "\n" . 

			 ' 		<th style="width:50px;text-align:center;"><h3>Active</h3></th>' . "\n" . 

       ' 		<th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" . 

       '</tr></thead><tbody>' . "\n";

  

	$db->Select('ticker' );		



  while ($row = mysql_fetch_array( $db->aResult ))

  {    



    $i++;

    $act  = yes_no ( $row['active'] );

		$page = yes_no ( $row['page'] );

    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

    echo '<td style="padding-left:20px;">' . $row['title'] . '</td>' . "\n";    

		echo '<td style="text-align:center;">' . $act . '</td>' . "\n";

		echo '<td style="text-align:center;">' . $row['weight'] . '</td>' . "\n";

    echo '<td style="text-align:center;">' . $act . '</td>' . "\n";

    echo '<td valign="middle"><strong><a href="./?tool=ticker&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=ticker&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

    echo '</tr>' . "\n";

	  

  }//end while



  echo '</tbody></table>';



  $pages = ( $db->iRecords > 20 ) ? true : false;

	

  echo_js_sorter ( $pages, 2 , 'table', 0 );

  echo '<div class="spacer">&nbsp;</div>';

	

}//end function



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Banner Form for User Input

 *

 *

 **************************************************************/



function add( $errors = '' )

{

  global $id, $action, $db;

   

   

  if ( $errors )

  {

		echo '<div class="error_message"><ul><strong>Your Form Has Errors</strong>';

		

		if ( in_array('title', $errors ) )

		{

			echo '<li>You must fill out text for the ticker to scroll.</li>';

			$val_label = ' class="form_field_error" ';								

		}



		if ( in_array('url', $errors ) )

		{

			echo '<li>You must fill out a valid website address to link to.</li>';

			$val_t = ' class="form_field_error" ';

		}

				

		echo '</ul></div>' . "\n";

  } else {

    $c = ( $action == 'update' ) ? 'update' : 'add';

		echo  '<div class="notice_message">To ' . $c . ' this setting, fill out the form and click submit.<br />' . "\n" .

					'<ul><strong>NOTES:</strong>' . "\n" .

					'<li> <strong>Label</strong> field will be the navigation name in the left side bar. </li>' . "\n" .

					'<li> <strong>Order</strong> is the order they will be displayed in from 0-999 . </li>' . "\n" .

					'<li> <strong>Active</strong> must be set to <strong>Yes</strong> to display on the website. </li>' . "\n" .

					'</ul> </div>';

  }

  

  if ($action == "update" )

  {

    $row = $db->First ('ticker', array('id' => $id) );

  } else {

    if ( $row['active'] == 0 && empty($_POST['active']) )

    {

      $row['active']  = 1;

      $row['weight']  = fetch_weight('ticker');

    }

    $row = sanitize_vars ($_POST);

  }//end if

  

	$r = required();

	

  echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

  $t_index = 1;

  echo '   <tr>' . "\n".

       '      <td > <label for="title">' . $r . 'Ticker Text</label></td>  ' . "\n" . 

       '      <td > <input ' . $val_title . ' type="text" name="title" id="title" value="'. $row['title'] .'" size="80" maxlength="200" /></td>  ' . "\n" . 

       '   </tr>' . "\n";



  $t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="weight">Order</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />' . "\n" .

       ' 						<span class="note" style="padding-left:5px;"> ( Recommend increments of 10 ) </span>' . "\n" .

       '      </td>' . "\n" .

       '   </tr>' . "\n";



  $t_index++;

	echo '   <tr>' . "\n" .

 			 ' 						<td ><label for="page">Location</label></td>  ' . "\n" . 

	  	 ' 						<td >' . "\n" . 

				create_slist ( $list, 'page', $row['page'], 1, $t_index ) . "\n" . 

			 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to Display on Home Page ) </span>' . "\n" . 

			 ' 						</td>  ' . "\n" . 

			 '				</tr>  '. "\n";

			 

  $t_index++;

	echo '   <tr>' . "\n" .

 			 ' 						<td ><label for="active">Active</label></td>  ' . "\n" . 

	  	 ' 						<td >' . "\n" . 

				create_slist ( $list, 'active', $row['active'], 1, $t_index ) . "\n" . 

			 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to Display on Website ) </span>' . "\n" . 

			 ' 						</td>  ' . "\n" . 

			 '				</tr>  '. "\n";



  $t_index++;

  echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";  

  echo '</table></form>' . "\n";

  echo '<script type="text/javascript">document.getElementById(\'title\').focus();</script>' . "\n";



}





/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

  $r_data = $data;

	

	$r_data['page'] 	= intval ( $data['page'] );

	$r_data['active'] = intval ( $data['active'] );

	$r_data['weight'] = intval ( $data['weight'] );

		

  return $r_data;

}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 * Unlinks Existing Photo

 *

 **************************************************************/



function remove()

{

	global $id, $module_name;

	

	$result = mysql_query("SELECT title FROM " . db_prefix . "ticker WHERE id = '$id'");

	$row    = mysql_fetch_array($result);

	

	print_header('Delete Ticker Text - ' . $row['title']);

	

	if ( !empty($_POST ))

	{		

							

		$result = mysql_query('DELETE FROM ' . db_prefix . "ticker WHERE id = '$id'");

		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

			

	} else {

	

		echo '<form action="./?tool=ticker&action=remove" method="post" name="form">' . "\n" .

							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

							'<tr> <td><div align="center">Are you sure you want to delete this record?</div></td></tr>' . "\n" .

							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=ticker\'"></div></td></tr>' . "\n" .

							'</table></form>';

	}//end if

}//end function





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

	global $action, $id, $module_name, $db;

	

	if ( $action == 'update' )

		print_header('Update Ticker Text');

	else

		print_header('Add New Ticker Text');

	

	if ( array_key_exists ('submit',$_POST))

	{				

		require("classes/validation.php");

		$rules   = array();    

		$rules[] = "required,title,title";		

	

		$errors = validateFields($_POST, $rules);	

	}

		

	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

	{

	

		$data  = sanitize_vars( $_POST );

		$type  = ( $action == 'update' ) ? 0 : 1;



		$exclude = array ('submit', 'id');



		if ( $action == 'update' )

			$res = $db->Update( 'ticker' , $data, array('id' => $id ), $exclude );

		else

			$res = $db->Insert( $data, 'ticker', $exclude );

	

		if ( is_saint() && !$res )

			echo '<div class="error_message" style="word-wrap:break-word;">' . $db->sLastError . '<br />' . $db->sLastQuery . '</div>';

   			

		print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;   

		

  } else {

		add( $errors );

	}//end if	

}//end function



?>