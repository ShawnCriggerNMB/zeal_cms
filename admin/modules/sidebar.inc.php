 <?php	
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : Sidebar Module
// Written By : Darrell Bessent of EWD
// Written On : May 04, 2013
// Revision   : 164
// Updated by : Darrell Bessent of EWD
// Updated On : May 04, 2013
// Description: This module is used with websites that 
//							want to post news and downloads in a sidebar
// Copyright Zeal Technologies 
//***************************************************/

/***************************************************************
 *
 * Sanitize variables for use in forms and whatnots
 *
 *
 **************************************************************/

$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);
$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);


$mod_config  = get_module_settings ( 'sidebar' );

$upload_file_url  = $site_base_url . $cms_uploads . 'sidebar/';
$upload_file_dir  = $site_base_dir . $cms_uploads . 'sidebar/';


function execute()
{
	switch($_GET['action'])
	{
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}//end switch
}//end function


/***************************************************************
 *
 * function manage
 * Querrys DB and Displays Slider Content
 *
 *
 **************************************************************/

function manage()
{

  global $upload_file_dir, $upload_file_url;
  
  $i    = 0;
  $link = '<a href="./?tool=sidebar&action=add">Add A Sidebar Post</a>';
  //$link = '';
  print_header('Manage Sidebar',$link);
  
  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 
       ' 	  <thead>' . "\n" . 
       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" . 
       ' 			  <th><h3>Label</h3></th>' . "\n" . 	
			 ' 			  <th><h3>Content</h3></th>' . "\n" . 
			 ' 			  <th><h3>File</h3></th>' . "\n" .   
       ' 			  <th style="width:50px; text-align:center;"><h3>Order</h3></th>' . "\n" . 
       ' 			  <th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . 
       ' 			  <th style="width:100px;" class="nosort"><h3>Tools</h3></th>' . "\n" . 
       '		 </tr></thead><tbody>' . "\n";
  
  $results = mysql_query('SELECT * FROM ' . db_prefix . 'sidebar ORDER BY weight,label');
  $pages   = mysql_num_rows($results);
  while ($row = mysql_fetch_array($results))
  {    

    $i++;
    
		$file_link 					 = 'No File Uploaded';
    $label							 = $row['label'];
		$content						 = $row['content'];
		
		$file_filename = $upload_file_dir . $row['file'];
    
    $file_link = ( file_exists ( $file_filename ) ) ? '<a href="' . $upload_file_url . $row['file'] . '" target="_blank">' . $row['file'] . '</a>' : $file_link;
			          
    $act = yes_no ( $row['active'] );
    $content = substr($content, 0, 60) . '...';
		
    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";
    echo '<td style="padding-left:20px;">' . $label . '</td>' . "\n"; 	
		echo '<td style="padding-left:20px;">' . $content . '</td>' . "\n"; 
		echo '<td style="padding-left:20px;">' . $file_link . '</td>' . "\n";		
    echo '<td style="width:50px; text-align:center;">' . $row['weight'] . '</td>' . "\n"; 
    echo '<td style="width:50px; text-align:center;">' . $act . '</td>' . "\n"; 
    echo '<td style="width:100px;" valign="middle"><strong><a href="./?tool=sidebar&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=sidebar&action=remove&id='.$row['id'].'">Remove</a></strong></td>';
    echo '</tr>' . "\n";
	  
  }//end while

  echo '</tbody></table>';

  $pages = ( $pages > 20 ) ? true : false;
	
  echo_js_sorter ( $pages );
             
}//end function

/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Banner Form for User Input
 *
 *
 **************************************************************/

function add( $errors = '' )
{
  global $id, $action, $site_base_url, $site_base_dir, $upload_file_url, $upload_file_dir, $mod_config;
   
   
  if ( $errors )
  {
   echo '<div class="error_message">';
   if ( in_array('label', $errors ) )
     echo '* You must fill out a label.<br/>';
   echo '</br></div>' . "\n";
  } else {
    $c = ( $action == 'update' ) ? 1 : 0;
    print_notice_message ( $c );
  }
  
  if ($action == "update")
  {
   // Run the query for the existing data to be displayed.
   $results = mysql_query('SELECT * FROM ' . db_prefix . 'sidebar WHERE id = "' . $id . '"');
   $row 		= mysql_fetch_array($results);
  } 
	if (!empty ($_POST))
	{	
   $row     = sanitize_vars ($_POST);
  }//end if
  
	echo '<div class="notice_message">NOTES : <br /> 
					<ul>
						<li>The `LABEL` is the title for the news post.</li>
						<li>The `CONTENT` is the actual news post.</li>
						<li>The `FILE` may be a *.DOC, *.DOCX, *.PDF, *.JPG, *.PNG or *.GIF.</li>
						<li>The `ORDER` is the order weight in which you wish for the news to be listed.</li>
						<li>The `ACTIVE` is to set the sidebar post visability to on or off.</li>
					<ul>	
				</div>';
	
  echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";			
  echo '<input type="hidden" name="id" value="' . $id . '" />';
  
  $t_index = 1;
  echo '   <tr>' . "\n".
       '      <td > <label for="label">Label</label></td>  ' . "\n" . 
       '      <td > <input type="text" name="label" id="label" value="'. $row['label'] .'" size="45" /></td>  ' . "\n" . 
       '   </tr>' . "\n";

	
	  $t_index++;
		$r_content = str_replace('<br />', '\n', $row['content']);
	echo '   <tr>' . "\n".
       '      <td > <label for="content">Content</label></td>  ' . "\n" . 
       '      <td > <textarea name="content" id="content" rows="5" cols="40" tabindex="' . $t_index . '">'.$r_content.'</textarea></td>  ' . "\n" . 
       '   </tr>' . "\n";

	$t_index++;
	echo '   <tr>' . "\n".
       '      <td > <label for="file_link_text">File Link Text</label></td>  ' . "\n" . 
       '      <td > <input type="text" name="file_link_text" id="file_link_text" value="'. $row['file_link_text'] .'" size="45" /></td>  ' . "\n" . 
       '   </tr>' . "\n";
			 
	$t_index++;
	echo '   <tr>' . "\n" .
					' 						<td ><label for="use_file">Use File</label></td>  ' . "\n" . 
					' 						<td >' . "\n" . 
					' 						<select id="use_file" name="use_file">' . "\n" . 
					' 									<option value="1" '; if ( $row['use_file'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 
					' 									<option value="0" '; if ( $row['use_file'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 
					' 						</select>' . "\n" . 
				 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to use the file. ) </span>' . "\n" . 
	 ' 						</td>  ' . "\n" . 
					'				</tr>  '. "\n";
	
		echo '   <tr>' . "\n" .
					' 						<td ><label for="file_link_blank">New Window</label></td>  ' . "\n" . 
					' 						<td >' . "\n" . 
					' 						<select id="file_link_blank" name="file_link_blank">' . "\n" . 
					' 									<option value="1" '; if ( $row['file_link_blank'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 
					' 									<option value="0" '; if ( $row['file_link_blank'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 
					' 						</select>' . "\n" . 
				 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to open link in new window. ) </span>' . "\n" . 
	 ' 						</td>  ' . "\n" . 
					'				</tr>  '. "\n";
	 
			 
	$t_index++;
	echo '   <tr>' . "\n".
       '      <td > <label for="ext_link">External Link</label></td>  ' . "\n" . 
       '      <td > <input type="text" name="ext_link" id="ext_link" value="'. $row['ext_link'] .'" size="45" /></td>  ' . "\n" . 
       '   </tr>' . "\n";
			 
	echo '   <tr>' . "\n".
       '      <td > <label for="ext_link_text">External Link Text</label></td>  ' . "\n" . 
       '      <td > <input type="text" name="ext_link_text" id="ext_link_text" value="'. $row['ext_link_text'] .'" size="45" /></td>  ' . "\n" . 
       '   </tr>' . "\n";		 

	echo '   <tr>' . "\n" .
					' 						<td ><label for="use_ext_link">Use External Link</label></td>  ' . "\n" . 
					' 						<td >' . "\n" . 
					' 						<select id="use_ext_link" name="use_ext_link">' . "\n" . 
					' 									<option value="1" '; if ( $row['use_ext_link'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 
					' 									<option value="0" '; if ( $row['use_ext_link'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 
					' 						</select>' . "\n" . 
				 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to use the external link. ) </span>' . "\n" . 
	 ' 						</td>  ' . "\n" . 
					'				</tr>  '. "\n";

	$t_index++;
	echo '   <tr>' . "\n" .
					' 						<td ><label for="ext_link_blank">New Window</label></td>  ' . "\n" . 
					' 						<td >' . "\n" . 
					' 						<select id="ext_link_blank" name="ext_link_blank">' . "\n" . 
					' 									<option value="1" '; if ( $row['ext_link_blank'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 
					' 									<option value="0" '; if ( $row['ext_link_blank'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 
					' 						</select>' . "\n" . 
				 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to open link in new window. ) </span>' . "\n" . 
	 ' 						</td>  ' . "\n" . 
					'				</tr>  '. "\n";

  $t_index++;
  echo '   <tr>' . "\n".
       '      <td > <label for="weight">Order</label></td>  ' . "\n" . 
       '      <td > <input type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />' . "\n" .
       ' 						<span class="note" style="padding-left:5px;"> ( Recommend increments of 10 ) </span>' . "\n" .
       '      </td>' . "\n" .
       '   </tr>' . "\n";
   
  $t_index++;
		echo '   <tr>' . "\n" .
							' 						<td ><label for="active">Active</label></td>  ' . "\n" . 
							' 						<td >' . "\n" . 
							' 						<select id="active" name="active">' . "\n" . 
							' 									<option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 
							' 									<option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 
							' 						</select>' . "\n" . 
						 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to Display on Website ) </span>' . "\n" . 
       ' 						</td>  ' . "\n" . 
							'				</tr>  '. "\n";
 
 
 	// file upload handler
   if ( $row['file'] != '' )
  {
    
   $file_filename      = $upload_file_dir . $row['file'];
  
   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		
   echo '<tr>' . "\n";
   echo '<td ><label for="current_file">Current File</label></td>' . "\n";
   echo '<td ><a href="' . $upload_file_url . $row['file'] . '">' . $row['file'] . '</a></td>' . "\n";
   echo '<input type="hidden" name="current_file" value="' . $row['file'] . '" />' . "\n";
   echo '</tr>' . "\n";
  }
  
  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		
  echo '<tr>' . "\n";
  echo '<td ><label for="file">Upload File</label></td>' ."\n";
  echo '<td ><input name="file" type="file" id="file" size="15">' . "\n";
  echo '<span class="note">( File must be a *.DOC, *DOCX, *.PDF or *.RTF )</span></td>' . "\n";
  echo '</tr>' . "\n";
	
 // This is our submit
  $t_index++;
  echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";  
  echo '</table></form>' . "\n";
  echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>' . "\n";

}


/***************************************************************
 *
 * function sanitize_vars
	* @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 *
 **************************************************************/


function sanitize_vars( $data )
{
  global $upload_file_dir, $mod_config, $known_file_types;
  
  
  $r_data['label'] 					 = mysql_real_escape_string ( $data['label'] );
	$r_data['content'] 				 = htmlspecialchars ( $data['content'], ENT_QUOTES );
	$r_data['file_link_text']  = mysql_real_escape_string ( $data['file_link_text'] );
	$r_data['use_file'] 			 = $data['use_file'];
	$r_data['ext_link'] 			 = $data['ext_link'];
	$r_data['ext_link_text']   = mysql_real_escape_string ( $data['ext_link_text'] );
	$r_data['use_ext_link'] 	 = $data['use_ext_link'];
	$r_data['file_link_blank'] = $data['file_link_blank'];
	$r_data['ext_link_blank']  = $data['ext_link_blank'];
	$r_data['file']  					 = $data['current_file'];
	$r_data['weight'] 				 = mysql_real_escape_string ( $data['weight'] );
	$r_data['active'] 				 = mysql_real_escape_string ( $data['active'] );
			
  $tid = ( intval ( $data['id'] ) == 0 ) ? fetch_lastid ('sidebar') : $data['id'];
      		 
				 
  if ( !empty ( $_FILES['file']['name'] ) )
  {
            
    $old_file        = $r_data['file'];
    $file            = $_FILES['file'];
		
		$r_data['file'] = process_upload_file ( $tid , $file, $known_file_types, $upload_file_dir);
						
    if ( ( $old_file != $r_data['file'] ) && ( file_exists ( $upload_file_dir . $old_file ) ) && ( $old_file != '' ))
		{
			unlink ( $upload_file_dir . $old_file );
		}
  
	}
	
			 
  if ( empty($_POST['weight']) )
  {
    $r_data['weight']  = fetch_weight('sidebar', 'weight' );
  }
  
  return $r_data;

}

/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 * Unlinks Existing Photo
 *
 **************************************************************/

function remove()
{

	global $id, $upload_file_dir, $upload_image_dir, $module_name;
	
	$result = mysql_query("SELECT * FROM " . db_prefix . "sidebar WHERE id = '$id'");
	$row    = mysql_fetch_array($result);
	
	print_header('Delete Sidebar Post - ' . $row['label']);
	
	if ( !empty($_POST ))
	{		
					
		if ( file_exists ( $upload_file_dir . $row['file'] ))
		{
				@unlink( $upload_file_dir . $row['file'] );	
		}
		
		
		
		$result = mysql_query('DELETE FROM ' . db_prefix . "sidebar WHERE id = '$id'");
		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;
			
	} else {
	
		echo '<form action="./?tool=sidebar&action=remove" method="post" name="form">' . "\n" .
							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .
							'<tr> <td><div align="center">Are you sure you want to delete this news post?</div></td></tr>' . "\n" .
							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .
							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=sidebar\'"></div></td></tr>' . "\n" .
							'</table></form>';
	}//end if
}//end function


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of page contents
 *
 **************************************************************/

function update()
{
		global $action, $id, $module_name;
		
		if ( $action == 'update' )
				print_header('Update Sidebar Post');
		else
				print_header('Add Sidebar Post');
 
		if ( array_key_exists ('submit',$_POST))
		{				
    require_once("classes/validation.php");
  
    $rules = array();    
    
    $rules[] = "required,label,label";
		$rules[] = "required,content,content";
    
    $errors = validateFields($_POST, $rules);
    
		}
		
		if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	 {
		
				$data  = sanitize_vars( $_POST );			
		 	  $type  = ( $action == 'update' ) ? 0 : 1;
				$content = str_replace('\n', '<br />', $data['content']);
				
				if ( $action == 'update' )
						$sql = "UPDATE " . db_prefix . "sidebar SET label = '" . $data['label'] . "', content = '" . $content . "', 
						file = '" . $data['file'] . "', file_link_text = '" . $data['file_link_text'] . "', 
						file_link_blank = '" . $data['file_link_blank'] . "', use_file = '" . $data['use_file'] . "', 
						ext_link = '" . $data['ext_link'] . "', ext_link_text = '" . $data['ext_link_text'] . "', 
						ext_link_blank = '" . $data['ext_link_blank'] . "', use_ext_link = '" . $data['use_ext_link'] . "',
						 weight = '" . $data['weight'] . "', active = '" . $data['active'] . "'	WHERE id = '$id'";
 		 else
					 $sql = 'INSERT INTO ' . db_prefix . 'sidebar (label, content, file, file_link_text, file_link_blank, use_file, 
					 ext_link, ext_link_text, ext_link_blank, use_ext_link, weight, active) VALUES ' .
												 "('" . $data['label'] ."', '" . $content . "', '" . $data['file'] . "', '" . 
												 $data['file_link_text'] . "', '" . $data['file_link_blank'] . "', '" . $data['use_file'] . "', '" . 
												 $data['ext_link'] ."', '" . $data['ext_link_text'] . "', '" . $data['ext_link_blank'] . "', '" . 
												 $data['use_ext_link'] . "', '" .$data['weight'] . "', '" . $data['active'] . "')";
											
  //  echo $sql;
    
				mysql_query ( $sql );
    
    if ( is_saint() && mysql_errno != 0 )
      echo '<div class="error_message"><pre>' . $sql . '</pre></div>';
   			
				print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;   
		
  } else {
		add( $errors );
	}//end if	
}//end function



?>