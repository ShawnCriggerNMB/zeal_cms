<?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Brands > Models Module

// Written By : Darrell Bessent of EWD

// Written On : March 27, 2013

// Revision   : 164

// Updated by : Darrell Bessent of EWD

// Updated On : March 27, 2013

// Description: This module is used with the brands module for managing

//							any type of brands that have models on a website.

// Copyright Zeal Technologies 

//***************************************************/



/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);





$mod_config  = get_module_settings ( 'brands_mods' );



$upload_url  = $site_base_url . '/uploads/models/';

$upload_dir  = $site_base_dir . '/uploads/models/';

$height = '250';

$width  = '250';

$tn_height = '150';

$tn_width = '150';



   



function execute()

{

	switch($_GET['action'])

	{

		case 'update':

			update();

			break;

		case 'add':

			update();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Slider Content

 *

 *

 **************************************************************/



function manage()

{



  global $upload_dir,$upload_url;

  

  $i    = 0;

  $link = '<a href="./?tool=brands_mods&action=add">Add A Brand Model</a>';

  

  print_header('Manage Brand Models',$link);

  

  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 

       ' 	  <thead>' . "\n" . 

       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" . 

       ' 			  <th><h3>Model</h3></th>' . "\n" . 

			 ' 			  <th><h3>Brand</h3></th>' . "\n" . 

			 ' 			  <th><h3>Description</h3></th>' . "\n" . 

       ' 			  <th><h3>Image Name</h3></th>' . "\n" . 

			 ' 			  <th><h3>ALT Tag</h3></th>' . "\n" . 

       ' 			  <th style="width:50px; text-align:center;"><h3>Order</h3></th>' . "\n" . 

       ' 			  <th style="width:50px; text-align:center;"><h3>Active</h3></th>' . "\n" . 

       ' 			  <th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" . 

       '		 </tr></thead><tbody>' . "\n";

  

  $results = mysql_query('SELECT * FROM ' . db_prefix . 'brands_mods ORDER BY weight,label');

  $pages   = mysql_num_rows($results);

  while ($row = mysql_fetch_array($results))

  {    



    $i++;

    

    $link 	 = 'No Image Uploaded';

    $label	 = stripslashes ( $row['label'] );

		$brandid = stripslashes ( $row['brand_id'] );

		$descr	 = stripslashes ( $row['descr'] );

		$alttag	 = stripslashes ( $row['alttag'] );

		

		$brandidqry = mysql_query('SELECT label FROM ' . db_prefix . 'brands WHERE `id`="'.$brandid.'"');

		$brandidrow = mysql_fetch_assoc($brandidqry);

		$brandlabel = $brandidrow['label'];

		

    $filename = $upload_dir . $row['image'];

    

    $link = ( file_exists ( $filename ) ) ? '<a href="' . $upload_url . $row['image'] . '" alt="' . $alttag . '" title="' . $label . '" class="lightbox" >' . $row['image'] . '</a>' : $link;

                

    $act = yes_no ( $row['active'] );

    $descr = substr($descr, 0, 60) . '...';

		

    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

    echo '<td style="padding-left:20px;">' . $label . '</td>' . "\n";

		echo '<td style="padding-left:20px;">' . $brandlabel . '</td>' . "\n";

		echo '<td style="padding-left:20px;">' . $descr . '</td>' . "\n";

    echo '<td style="padding-left:20px;">' . $link . '</td>' . "\n";

		echo '<td style="padding-left:20px;">' . $alttag . '</td>' . "\n";

    echo '<td text-align:center;">' . $row['weight'] . '</td>' . "\n";

    echo '<td text-align:center;">' . $act . '</td>' . "\n";

    echo '<td valign="middle"><strong><a href="./?tool=brands_mods&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=brands_mods&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

    echo '</tr>' . "\n";

	  

  }//end while



  echo '</tbody></table>';



  $pages = ( $pages > 20 ) ? true : false;

	

  echo_js_sorter ( $pages );

             

}//end function



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Banner Form for User Input

 *

 *

 **************************************************************/



function add( $errors = '' )

{

  global $id, $action, $site_base_url, $upload_url, $upload_dir, $mod_config, $height, $width;

   

   

  if ( $errors )

  {

   echo '<div class="error_message">';

   if ( in_array('label', $errors ) )

     echo '* You must fill out a label name.<br/>';

   echo '</br></div>' . "\n";

  } else {

    $c = ( $action == 'update' ) ? 1 : 0;

    print_notice_message ( $c );

  }

  

  if ($action == "update")

  {

   // Run the query for the existing data to be displayed.

   $results = mysql_query('SELECT * FROM ' . db_prefix . 'brands_mods WHERE id = "' . $id . '"');

   $row 				= mysql_fetch_array($results);

  } 

	if (!empty ($_POST))

	{	

   $row     = sanitize_vars ($_POST);

  }//end if

  

	echo '<div class="notice_message">NOTES : <br /> 

					<ul>

						<li>The `MODEL` is the brand model name.</li>

						<li>The `BRAND` is the brand that the model belongs to.</li>

						<li>The `DESCRIPTION` is a short description of the brand.</li>

						<li>The `ALT TAG` is to help you set alternate image tags.</li>

						<li>The `ORDER` controls the image display order on the gallery page.</li>

						<li>Image should be `SQUARE` or as close as possible.</li>

						<li>The `THUMBNAIL` will be resized to 150x150 automatically and may not be in proportion.</li>

					<ul>	

				</div>';

	

  echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

  $t_index = 1;

  echo '   <tr>' . "\n".

       '      <td > <label for="label">Model</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="label" id="label" value="'. $row['label'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";

			 


	echo '   <tr>' . "\n".

       '      <td > <label for="brand_id">Brand</label></td>  ' . "\n" . 

       '      <td > <select name="brand_id" id="brand_id">';

										

				$brandqry = mysql_query('SELECT * FROM ' . db_prefix . 'brands ORDER BY weight,label');

				while ($brandrow = mysql_fetch_array($brandqry))

				{    

					$i++;						

							echo '<option value="'.$brandrow['id'].'">'.$brandrow['label'].'</option>';

				}

											

	echo '						</select></td>  ' . "\n" . 

       '   </tr>' . "\n";		 

	

	  $t_index++;

		$r_descr = str_replace('<br />', '\n', $row['descr']);

	echo '   <tr>' . "\n".

       '      <td > <label for="descr">Description</label></td>  ' . "\n" . 

       '      <td > <textarea name="descr" id="descr" rows="5" cols="40" tabindex="' . $t_index . '">'.$r_descr.'</textarea></td>  ' . "\n" . 

       '   </tr>' . "\n";		 



  $t_index++;

	echo '   <tr>' . "\n".

       '      <td > <label for="alttag">ALT Tag</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="alttag" id="alttag" value="'. $row['alttag'] .'" size="45" /></td>  ' . "\n" . 

       '   </tr>' . "\n";



  $t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="weight">Order</label></td>  ' . "\n" . 

       '      <td > <input type="text" name="weight" id="weight" value="'. $row['weight'] .'" size="3" maxlength="3" />' . "\n" .

       ' 						<span class="note" style="padding-left:5px;"> ( Recommend increments of 10 ) </span>' . "\n" .

       '      </td>' . "\n" .

       '   </tr>' . "\n";

   

  $t_index++;

		echo '   <tr>' . "\n" .

							' 						<td ><label for="active">Active</label></td>  ' . "\n" . 

							' 						<td >' . "\n" . 

							' 						<select id="active" name="active">' . "\n" . 

							' 									<option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>' . "\n" . 

							' 									<option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>																' . "\n" . 

							' 						</select>' . "\n" . 

						 ' 						<span class="note" style="padding-left:5px;"> ( Set to Yes to Display on Website ) </span>' . "\n" . 

       ' 						</td>  ' . "\n" . 

							'				</tr>  '. "\n";

  

  /****************************

  * Image Uploading Section

  ***************************/

  

  if ( $row['image'] != '' )

  {

    

   $filename      = $upload_dir . $row['image'];

   list ( $w, $h ) = img_size ( $filename );

  

   echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

   echo '<tr>' . "\n";

   echo '<td ><label for="current_file">Current Image</label></td>' . "\n";

   echo '<td ><a href="' . $upload_url . $row['image'] . '" alt="' . $row['alttag'] . '" title="' . $row['label'] . '" target="_blank" class="lightbox" >' . $row['image'] . '</a></td>' . "\n";

   echo '<input type="hidden" name="current_file" value="' . $row['image'] . '" />' . "\n";

   echo '</tr>' . "\n";

  }

  

  echo '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";  		

  echo '<tr>' . "\n";

  echo '<td ><label for="image">Upload Image</label></td>' ."\n";

  echo '<td ><input name="image" type="file" id="image" size="15">' . "\n";

  echo '<span class="note">( Image will be auto-magicly resized to '.$width.' x '.$height.' pixels )</span></td>' . "\n";

  echo '</tr>' . "\n";

 



  $t_index++;

  echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";  

  echo '</table></form>' . "\n";

  echo '<script type="text/javascript">document.getElementById(\'label\').focus();</script>' . "\n";



}





/***************************************************************

 *

 * function sanitize_vars

	* @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{

  global $upload_dir, $mod_config, $known_photo_types, $height, $width, $tn_height, $tn_width;

  

  

  $r_data['label']  = mysql_real_escape_string ( $data['label'] );

	$r_data['brand_id']  = mysql_real_escape_string ( $data['brand_id'] );

	$r_data['descr']  = mysql_real_escape_string ( $data['descr'] );

	$r_data['alttag']  = mysql_real_escape_string ( $data['alttag'] );

  $r_data['weight']  = intval ( $data['weight'] );

  $r_data['active'] = intval ( $data['active'] ) ;

  $r_data['image']  = $data['current_file'];

  $r_data['image_tn']  = $data['current_file'];

		

  $tid = ( intval ( $data['id'] ) == 0 ) ? fetch_lastid ('brands_mods') : $data['id'];

      		 

  if ( !empty ( $_FILES['image']['name'] ) )

  {

            

    $old_file        = $r_data['image'];

    $image           = $_FILES['image'];

    

		

		$r_data['image'] = process_upload ( $tid , '', $image, $known_photo_types, $upload_dir, 1, $width, $height );

		

		$r_data['image_tn'] = process_upload ( $tid , 'tn', $image, $known_photo_types, $upload_dir, 1, $tn_width, $tn_height );

						

    if ( ( $old_file != $r_data['image'] ) && ( file_exists ( $upload_dir . $old_file ) ) && ( $old_file != '' ))

		{

			unlink ( $upload_dir . $old_file );

			

			$sufx = substr($old_file, -4);

			$timg = substr($old_file,0,-4);

			$timg = $timg.'_tn'.$sufx;

				

			unlink( $upload_dir . $timg );

		}

  

	}

			 

  if ( $data['active'] == 0 && empty($_POST['active']) )

  {

    $r_data['active'] = 1;

    $r_data['weight']  = fetch_weight('brands_mods', 'weight' );

  }

  

  return $r_data;



}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 * Unlinks Existing Photo

 *

 **************************************************************/



function remove()

{



	global $id, $upload_dir, $module_name;

	

	$result = mysql_query("SELECT * FROM " . db_prefix . "brands_mods WHERE id = '$id'");

	$row    = mysql_fetch_array($result);

	

	print_header('Delete Brand Model - ' . $row['label']);

	

	if ( !empty($_POST ))

	{		

					

		if ( file_exists ( $upload_dir . $row['image'] ))

		{

				@unlink( $upload_dir . $row['image'] );

				

				$sufx = substr($row['image'], -4);

				$timg = substr($row['image'],0,-4);

				$timg = $timg.'_tn'.$sufx;

				

				@unlink( $upload_dir . $timg );

		}

		

		

		$result = mysql_query('DELETE FROM ' . db_prefix . "brands_mods WHERE id = '$id'");

		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

			

	} else {

	

		echo '<form action="./?tool=brands_mods&action=remove" method="post" name="form">' . "\n" .

							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

							'<tr> <td><div align="center">Are you sure you want to delete this brand model?</div></td></tr>' . "\n" .

							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=brands_mods\'"></div></td></tr>' . "\n" .

							'</table></form>';

	}//end if

}//end function





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

		global $action, $id, $module_name;

		

		if ( $action == 'update' )

				print_header('Update Brand Model');

		else

				print_header('Add New Brand Model');

 

		if ( array_key_exists ('submit',$_POST))

		{				

    require_once("classes/validation.php");

  

    $rules = array();    

    

    $rules[] = "required,label,label";

    

    $errors = validateFields($_POST, $rules);

    

		}

		

		if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

	 {

		

				$data  = sanitize_vars( $_POST );			

		 	  $type  = ( $action == 'update' ) ? 0 : 1;

				$descr = str_replace('\n', '<br />', $data['descr']);

				

				if ( $action == 'update' )

						$sql = "UPDATE " . db_prefix . "brands_mods SET label = '" . $data['label'] . "', descr = '" . $descr . "', weight = '" . $data['weight'] . "', brand_id = '" . $data['brand_id'] . "', active = '" . $data['active'] . "', image = '" . $data['image'] . "', alttag = '" . $data['alttag'] . "'	WHERE id = '$id'";

 		 else

					 $sql = 'INSERT INTO ' . db_prefix . 'brands_mods (label, active, weight, image, alttag, descr, brand_id) VALUES ' .

												 "('" . $data['label'] ."', '" . $data['active'] . "', '" . $data['weight'] . "', '" . $data['image'] . "', '" . $data['alttag'] . "', '" . $descr . "', '" . $data['brand_id'] . "')";

											

//    echo $sql;

    

				mysql_query ( $sql );

    

    if ( is_saint() && mysql_errno != 0 )

      echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

   			

				print_mysql_message ( mysql_errno() , $module_name, $id, $type ) ;   

		

  } else {

		add( $errors );

	}//end if	

}//end function







?>