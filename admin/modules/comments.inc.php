<?php
if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : Post Comments / Programmer ++ Level
// Written By : Chuck Bunnell of EWD
// Written On : Aug 15, 2014
// Copyright Zeal Technologies
//***************************************************/

// admin can only come to this module thru the Posts Module
$thispost = intval($_GET['post']);
if ($thispost <= 0)
{
	header ("Location: ?tool=teams");
	exit();
}

// now make sure that the post actually exists
// if it does, get the label for display - on line 45
$stmt0 = $db->prepare('SELECT id,label FROM '.db_prefix.'posts WHERE id = ?');
$stmt0->execute(array($thispost));
$numposts = $stmt0->rowCount();

$postexists = TRUE;
if ($numposts == 0)
{
	$postexists = FALSE;
	echo '<p class="error_message">The Post you have requested does not exist.<br />
			Please <a href="?tool=teams">return to the Teams page</a> and select a Team -> Post -> Comment.</p>';
	die();
}

// if update make sure this id exists
update_verify();

// for image uploading
//$upload_url  = $site_base_url . $cms_uploads . 'comments/';
//$upload_dir  = $site_base_dir . $cms_uploads . 'comments/';
//$height = '300';
//$width  = '600';
//$tn_height = '';
//$tn_width = '48';

if ($postexists === TRUE)
{
	$row0 = $stmt0->fetch(PDO::FETCH_ASSOC);
	$label = $row0['label'];
	
	function execute()
	{
		switch($_GET['action'])
		{
			case 'update':
				update();
				break;
			case 'add':
				update();
				break;
			case 'remove':
				remove();
				break;
			default:
				manage();
		}
	}

}


/***************************************************************
 *
 * function manage
 * Querys DB and Displays Content
 *
 **************************************************************/

function manage()
{
	global $db, $identifier, $module_name, $dbtbl, $upload_url, $thispost, $label;

	$link = '<a href="./?tool='.$identifier.'&post='.$thispost.'&action=add">Add '.$module_name.'</a>';

	$extra_header = '';
	//$extra_header = ' for Post "'.$label.'"';

	print_header('Manage '.$module_name.$extra_header,$link);
	echo '<h2 style="margin:5px 0 10px 10px;text-decoration:none;">Post: '.$label.'</h2>';
	
	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sortable" id="table">
		<thead>
			<tr align="left" valign="top">
				<th style="width:50px"><h3>ID</h3></th>
				<th><h3>Comment</h3></th>
				<th style="width:200px"><h3>Author</h3></th>
				<th style="width:170px"><h3>Date</h3></th>
				<th style="width:70px"><h3>Spam</h3></th>
				<th style="width:70px"><h3>Active</h3></th>
				<th class="nosort"><h3>Tools</h3></th>
			</tr>
		</thead>
		<tbody>';

			$stmt3 = $db->prepare('SELECT * FROM '.$dbtbl.' WHERE post_id = ? ORDER BY date ASC');
			$stmt3->execute(array($thispost));
	
			while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC))
			{
				$comment_id = intval($row3['id']);
				$author_id = intval($row3['author_id']);
				$author = $row3['author_fname'].' '.$row3['author_lname'];
				$author_type = $row3['author_type'];
				$date = date('Y-m-d  H:i:s', $row3['date']);
				$comment = substr($row3['comment'],0, 100);
				$comment .= (strlen($row3['comment']) > 100) ? ' ...' : '';
				$spamcnt = intval($row3['spam_count']);
				$act = yes_no ( $row3['active'] );
				
				// set $tool according to author type (user and moderator == user)
				$tool = ($author_type == 'a') ? 'admin' : 'users';
				// set link to author using above $tool
				$author_link = '<a href="?tool='.$tool.'&action=update&id='.$author_id.'">'.$author.'</a> <span class="fltrt">'.$author_type.'</span>';
				
				echo '<tr align="left" valign="middle">
					<td style="padding-left:5px;">' . $comment_id . '</td>
					<td>' . $comment . '</td>
					<td>' . $author_link . '</td>
					<td>' . $date . '</td>
					<td>' . $spamcnt . '</td>
					<td>' . $act . '</td>
					<td style="padding:0;text-align:center;"><strong><a href="./?tool='.$identifier.'&post='.$thispost.'&action=update&id='.$row3['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&post='.$thispost.'&action=remove&id='.$row3['id'].'">Remove</a></strong></td>
				</tr>';
			}
		echo '</tbody>
	</table>';

	$pages = ( $pages > 20 ) ? true : false;

	echo_js_sorter ( $pages );

	echo '<div class="spacer">&nbsp;</div>';
}//end function


/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 * 
 **************************************************************/
function add( $errors = '' )
{
	global $db, $identifier, $module_name, $dbtbl, $action, $id, $upload_url, $upload_dir, $width, $height, $thispost, $userinfo, $label;
	
	if ( $errors )
	{
		echo '<ul class="error_message">
			<strong>Please fill in the required fields.</strong>';
			// set error messages for required fields
			if ( in_array('comment', $errors ) )
			{
				echo '<li>This Comment must be filled out.</li>';
				$val_label = ' class="form_field_error" ';
			}

		echo '</ul>';

	} else {

		$c = ( $action == 'update' ) ? 'update this' : 'add a new';

		echo  '<ul class="notice_message"><strong>To ' . $c . ' record, fill out the form and click submit.</strong>
			<li>Required fields have a red "*".</li>
		</ul>';
  }

	if ($action == "update")
	{
		$stmt = $db->prepare('SELECT * FROM '.$dbtbl.' WHERE id = ?');
		$stmt->execute(array($id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
	}

	if (!empty ($_POST))
	{
		$row = sanitize_vars ($_POST);
	}

/********************************************************
 * Start Building Form
 *******************************************************/

	$r = required();

	$author_id = ($action == "update") ? $row['author_id'] : $userinfo['id'];
	$author_type = ($action == "update") ? $row['author_type'] : 'a';
	$author_fname = ($action == "update") ? $row['author_fname'] : $userinfo['fname'];
	$author_lname = ($action == "update") ? $row['author_lname'] : $userinfo['lname'];
  
	echo '<form name="form" id="form" method="post"  action="#"enctype="multipart/form-data">
		<table>
    	<input type="hidden" name="id" value="'.$id.'" />
    	<input type="hidden" name="author_id" value="'.$author_id.'" />
    	<input type="hidden" name="author_type" value="'.$author_type.'" />
    	<input type="hidden" name="author_fname" value="'.$author_fname.'" />
    	<input type="hidden" name="author_lname" value="'.$author_lname.'" />
    	<input type="hidden" name="post_id" value="'.$thispost.'" />';
			
			echo '<tr>
				<td><label for="comment">'.$r.'Comment</label></td>
				<td><textarea ' . $val_comment . ' name="comment" id="comment" cols="34" rows="3">'. htmlspecialchars($row['comment']) .'</textarea>'.tooltip('Swear words and html code will be rejected.').'</td>
			</tr>';

    	echo '<tr>
				<td colspan="2">&nbsp;</td>
			</tr>';

			echo '<tr>    
				<td><label for="active">'.$r.'Active</label></td>  
				<td>
					'.create_slist ( $list, 'active', $row['active'], 1 ) . 
					tooltip('Set to Yes to allow on website').'
				</td>
			</tr>  ';

			echo '<tr>
				<td colspan="2" style="padding:3px;"><input type="submit" name="submit" value="Submit" /></td>
			</tr>';

		echo '</table>
	</form>';

  echo '<script type="text/javascript">document.getElementById(\'comment\').focus();</script>';

}//end function


/***************************************************************
 *
 * function sanitize_vars
 * @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 **************************************************************/
function sanitize_vars( $data )
{
  global $id, $known_photo_types, $upload_dir, $width, $height, $thispost;

	$r_data['author_id'] = intval ( $data['author_id'] );
	$r_data['author_type'] = stripslashes ( $data['author_type'] );
	$r_data['author_fname'] = stripslashes ( $data['author_fname'] );
	$r_data['author_lname'] = stripslashes ( $data['author_lname'] );
	$r_data['post_id'] = intval ( $data['post_id'] );
	$r_data['comment'] = stripslashes ( $data['comment'] );
	$r_data['active'] = intval ( $data['active'] );

	return $r_data;
}


/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $db, $identifier, $module_name, $id, $dbtbl, $upload_dir, $thispost;

	$stmt = $db->prepare("SELECT * FROM ".$dbtbl." WHERE id = ?");
	$stmt->execute(array($id));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	$extra_header = ': '.substr($row['comment'], 0, 50).' ...';
	print_header('Delete '.$module_name . $extra_header);

	if ( !empty($_POST ))
	{
		$errno = 0;

		try 
		{
			$stmt = $db->prepare("DELETE FROM ".$dbtbl." WHERE id = ?");
			$stmt->execute(array($id));
		}

		catch(PDOException $ex)
		{
			$errno = $ex->getCode();
		}

		print_mysql_message ( $errno , $module_name, $id, 2, '', '', '&post='.$thispost ) ;
	
	} else {
		
		echo '<form action="./?post='.$thispost.'&tool='.$identifier.'&action=remove" method="post" name="form">
			<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to delete this record?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'&post='.$thispost.'\'"></div>
		</form>';
	}

}//end function


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute functin show_form() to
 * allow editing of contents
 *
 **************************************************************/

function update()
{
	global $db, $identifier, $module_name, $action, $id, $dbtbl, $thispost, $label;
	
	// get the comment for message display
	//$stmt = $db->prepare("SELECT * FROM ".$dbtbl." WHERE id = ?");
	//$stmt->execute(array($id));
	//$row = $stmt->fetch(PDO::FETCH_ASSOC);

	$extra_header = ' for "'.substr($label, 0, 50).' ..."';
	if ( $action == 'update' )
		print_header('Update '.$module_name.$extra_header);
	else
		print_header('Add New '.$module_name.$extra_header);

	if ( array_key_exists ('submit',$_POST))
	{
		require ("classes/validation.php");
		
		// set rules for required fields
		$rules   = array();
		$rules[] = "required,comment,comment";
		$rules[] = "required,active,active";
		$errors = validateFields($_POST, $rules);
	}

	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$vars = array();
		$data = sanitize_vars( $_POST );
		$type = ( $action == 'update' ) ? 0 : 1;
		$date = time();
		
		if ( $action == 'update' )
		{
			$sql = "UPDATE ".$dbtbl." SET author_id = ?, author_type = ?, author_fname = ?, author_lname = ?, post_id = ?, date = ?, comment = ?, active = ? WHERE id = ?";
			array_push($vars, $data['author_id']);
			array_push($vars, $data['author_type']);
			array_push($vars, $data['author_fname']);
			array_push($vars, $data['author_lname']);
			array_push($vars, $data['post_id']);
			array_push($vars, $date);
			array_push($vars, $data['comment']);
			array_push($vars, $data['active']);
			array_push($vars, $id);
		
		} else {
			
			$sql = 'INSERT INTO '.$dbtbl.' (author_id, author_type, author_fname, author_lname, post_id, date, comment, active) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
			array_push($vars, $data['author_id']);
			array_push($vars, $data['author_type']);
			array_push($vars, $data['author_fname']);
			array_push($vars, $data['author_lname']);
			array_push($vars, $data['post_id']);
			array_push($vars, $date);
			array_push($vars, $data['comment']);
			array_push($vars, $data['active']);
		}
		
		$errono = 0;

		try 
		{
			$stmt = $db->prepare($sql);
			$stmt->execute($vars);
		}

		catch(PDOException $ex)
		{
			$errono = $ex->getCode();
		}
		
		if ( is_saint() && $errono != 0 )
			echo '<div class="error_message"><pre>' . $sql . '</pre></div>';

		print_mysql_message ( $errono , $module_name, $id, $type, '', "", '&post='.$thispost ) ;

	} else {
		
		add( $errors );
	}

}//end function

?>