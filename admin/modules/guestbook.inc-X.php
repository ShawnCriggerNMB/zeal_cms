 <?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Guestbook Module

// Written By : Darrell Bessent of EWD

// Written On : May 04, 2013

// Revision   : 164

// Updated by : Darrell Bessent of EWD

// Updated On : April 01, 2013

// Description: This module is used to view and remove

//							guestbook posts.

// Copyright Zeal Technologies 

//***************************************************/



/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);





$mod_config  = get_module_settings ( 'guestbook' );



function execute()

{

	switch($_GET['action'])

	{

		case 'view':

			view();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Slider Content

 *

 *

 **************************************************************/



function manage()

{

  

  $i    = 0;

  

  print_header('Manage Guestbook Posts');

  

  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 

       ' 	  <thead>' . "\n" . 

       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" . 

       ' 			  <th><h3>ID</h3></th>' . "\n" . 		// Display our DB ID title

			 ' 			  <th><h3>IP Address</h3></th>' . "\n" . // Display our IP title

			 ' 			  <th><h3>First Name</h3></th>' . "\n" . // Display our First Name title

			 ' 			  <th><h3>Last Name</h3></th>' . "\n" . // Display our Last Name title

			 ' 			  <th><h3>Comment</h3></th>' . "\n" .  // Display our Comment title

       ' 			  <th style="width:125px;"><h3>Date</h3></th>' . "\n" .    // Display our Date title

       ' 			  <th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" . // Display the title for the tools

       '		 </tr></thead><tbody>' . "\n";

  

  $results = mysql_query('SELECT * FROM ' . db_prefix . 'guestbook');

  $pages   = mysql_num_rows($results);

  while ($row = mysql_fetch_array($results))

  {    



    $i++;

		

		$dbid			= $row['id'];

		$ip		 		= $row['ip'];

    $fname		= $row['fname'];

		$lname		= $row['lname'];

		$comment	= $row['comment'];

		$date	 		= $row['date'];

		

    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

    echo '<td style="padding-left:20px;">' . $dbid . '</td>' . "\n"; 	// Display the db id for this order

		echo '<td style="padding-left:20px;">' . $ip . '</td>' . "\n"; 	// Display the ip

		echo '<td style="padding-left:20px;">' . $fname . '</td>' . "\n"; 	// Display first name

		echo '<td style="padding-left:20px;">' . $lname . '</td>' . "\n"; 	// Display the last name

		echo '<td style="padding-left:20px;">' . $comment . '</td>' . "\n"; 	// Display the comment

		echo '<td style="padding-left:20px;">' . $date . '</td>' . "\n"; 	// Display the date

    echo '<td valign="middle"><strong><a href="./?tool=guestbook&action=view&id='.$row['id'].'">View</a>&nbsp;|&nbsp;<a href="./?tool=guestbook&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

    echo '</tr>' . "\n";

	  

  }//end while



  echo '</tbody></table>';



  $pages = ( $pages > 20 ) ? true : false;

	

  echo_js_sorter ( $pages );

             

}//end function



//***************************************************************

// VIEW FUNCTION

//***************************************************************

function view()

{

	global $id, $action;

	print_header('Guestbook Post');

	// Run the query for the existing data to be displayed.

   $results = mysql_query('SELECT * FROM ' . db_prefix . 'guestbook WHERE id = "' . $id . '"');

   $row 				= mysql_fetch_array($results);

	 

	  echo '<form name="signup" id="signup"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

  $t_index = 1;

  echo '   <tr>' . "\n".

       '      <td width="250"> <label for="id">ID</label></td>  ' . "\n" . 

       '      <td > '.$row['id'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	 

 	

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="ip">IP Address</label></td>  ' . "\n" . 

       '      <td >'. $row['ip'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="fname">First Name</label></td>  ' . "\n" . 

       '      <td >'. $row['fname'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";			  




  echo '   <tr>' . "\n".

       '      <td > <label for="lname">Last Name</label></td>  ' . "\n" . 

       '      <td >'. $row['lname'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

	


  echo '   <tr>' . "\n".

       '      <td > <label for="comment">Comment</label></td>  ' . "\n" . 

       '      <td >'. $row['comment'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="date">Date</label></td>  ' . "\n" . 

       '      <td >'. $row['date'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

			

  $t_index++;

  echo '</table></form>' . "\n";

}

/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 * Unlinks Existing Photo

 *

 **************************************************************/



function remove()

{



	global $id, $upload_dir, $module_name;

	

	$result = mysql_query("SELECT * FROM " . db_prefix . "guestbook WHERE id = '$id'");

	$row    = mysql_fetch_array($result);

	

	print_header('Delete Post From - ' . $row['fname'] . ' ' . $row['lname'] );

	

	if ( !empty($_POST ))

	{			

		$result = mysql_query('DELETE FROM ' . db_prefix . "guestbook WHERE id = '$id'");

		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

			

	} else {

	

		echo '<form action="./?tool=guestbook&action=remove" method="post" name="form">' . "\n" .

							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

							'<tr> <td><div align="center">Are you sure you want to delete this order?</div></td></tr>' . "\n" .

							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=guestbook\'"></div></td></tr>' . "\n" .

							'</table></form>';

	}//end if

}//end function

?>