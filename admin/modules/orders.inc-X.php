 <?php	

	if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Orders Module

// Written By : Darrell Bessent of EWD

// Written On : April 01, 2013

// Revision   : 164

// Updated by : Darrell Bessent of EWD

// Updated On : April 01, 2013

// Description: This module is used with websites that 

//							accept orders using our customized method

//							and/or commercial_specials module. (order.html via MattressDirect)

// Copyright Zeal Technologies 

//***************************************************/



/***************************************************************

 *

 * Sanitize variables for use in forms and whatnots

 *

 *

 **************************************************************/



$id     = (isset($_GET['id']) AND intval($_GET['id']) > 0 ) ? $_GET['id'] : ((isset($_POST['id']) AND intval($_POST['id']) >0 ) ? $_POST['id'] : $_POST['id']);

$action = (isset($_GET['action']) AND strlen($_GET['action']) > 0 ) ? $_GET['action'] : ((isset($_POST['action']) AND strlen($_POST['action']) >0 ) ? $_POST['action'] : $_POST['action']);





$mod_config  = get_module_settings ( 'orders' );



function execute()

{

	switch($_GET['action'])

	{

		case 'view':

			view();

			break;

		case 'remove':

			remove();

			break;

		default:

			manage();

	}//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrys DB and Displays Slider Content

 *

 *

 **************************************************************/



function manage()

{

  

  $i    = 0;

  

  print_header('Manage Orders');

  

  echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" . 

       ' 	  <thead>' . "\n" . 

       ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" . 

       ' 			  <th><h3>ID</h3></th>' . "\n" . 		// Display our DB ID title

			 ' 			  <th><h3>Comm-Spec ID</h3></th>' . "\n" . // Display our Order ID title

			 ' 			  <th><h3>Order ID</h3></th>' . "\n" . // Display our Order ID title

			 ' 			  <th><h3>Name</h3></th>' . "\n" . // Display our Name title

			 ' 			  <th><h3>First Digits</h3></th>' . "\n" .  // Display our First Set title

       ' 			  <th><h3>Third Digits</h3></th>' . "\n" .    // Display our Third Set title

			 ' 			  <th style="width:50px;"><h3>CVV</h3></th>' . "\n" . // Display our CVV title

			 ' 			  <th><h3>IP Address</h3></th>' . "\n" . // Display our IP title

       ' 			  <th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" . // Display the title for the tools

       '		 </tr></thead><tbody>' . "\n";

  

  $results = mysql_query('SELECT * FROM ' . db_prefix . 'orders');

  $pages   = mysql_num_rows($results);

  while ($row = mysql_fetch_array($results))

  {    



    $i++;

		

		$dbid					 = $row['id'];

		$comspecid		 = $row['comspecid'];

    $orderid			 = $row['orderid'];

		$name		 			 = $row['name'];

		$digits1	 		 = $row['digits1'];

		$digits2	 		 = $row['digits2'];

		$cvv					 = $row['cvv'];

		$ip						 = $row['ip'];	

		

    echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

    echo '<td style="padding-left:20px;">' . $dbid . '</td>' . "\n"; 	// Display the db id for this order

		echo '<td style="padding-left:20px;">' . $comspecid . '</td>' . "\n"; 	// Display the commerical id

		echo '<td style="padding-left:20px;">' . $orderid . '</td>' . "\n"; 	// Display the order id

		echo '<td style="padding-left:20px;">' . $name . '</td>' . "\n"; 	// Display the name

		echo '<td style="padding-left:20px;">' . $digits1 . '</td>' . "\n"; 	// Display the 1st set of digits (1st)

		echo '<td style="padding-left:20px;">' . $digits2 . '</td>' . "\n"; 	// Display the 2nd set of didgits (3rd)

    echo '<td style="padding-left:20px;">' . strrev($cvv) . '</td>' . "\n";  	// Display the cvv

		echo '<td style="padding-left:20px;">' . $ip . '</td>' . "\n";  	// Display the ip

    echo '<td valign="middle"><strong><a href="./?tool=orders&action=view&id='.$row['id'].'">View</a>&nbsp;|&nbsp;<a href="./?tool=orders&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

    echo '</tr>' . "\n";

	  

  }//end while



  echo '</tbody></table>';



  $pages = ( $pages > 20 ) ? true : false;

	

  echo_js_sorter ( $pages );

             

}//end function



//***************************************************************

// VIEW FUNCTION

//***************************************************************

function view()

{

	global $id, $action;

	print_header('Order Information');

	// Run the query for the existing data to be displayed.

   $results = mysql_query('SELECT * FROM ' . db_prefix . 'orders WHERE id = "' . $id . '"');

   $row 				= mysql_fetch_array($results);

	 

	  echo '<form name="signup" id="signup"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

  $t_index = 1;

  echo '   <tr>' . "\n".

       '      <td width="250"> <label for="id">ID</label></td>  ' . "\n" . 

       '      <td > '.$row['id'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	 

 	

	$t_index++;

  echo '   <tr>' . "\n".

       '      <td > <label for="orderid">Order ID</label></td>  ' . "\n" . 

       '      <td >'. $row['orderid'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="orderdate">Order Date</label></td>  ' . "\n" . 

       '      <td >'. $row['orderdate'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";			  




  echo '   <tr>' . "\n".

       '      <td > <label for="name">Name</label></td>  ' . "\n" . 

       '      <td >'. $row['name'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

	


  echo '   <tr>' . "\n".

       '      <td > <label for="address">Address</label></td>  ' . "\n" . 

       '      <td >'. $row['address'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="city">City</label></td>  ' . "\n" . 

       '      <td >'. $row['city'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

	

		


  echo '   <tr>' . "\n".

       '      <td > <label for="state">State</label></td>  ' . "\n" . 

       '      <td >'. $row['state'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="zip">Zip</label></td>  ' . "\n" . 

       '      <td >'. $row['zip'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";		

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="phone">phone</label></td>  ' . "\n" . 

       '      <td >'. $row['phone'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";		  

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="email">Email</label></td>  ' . "\n" . 

       '      <td >'. $row['email'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";		 

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="digits1">First Digits</label></td>  ' . "\n" . 

       '      <td >'. $row['digits1'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="digits2">Third Digits</label></td>  ' . "\n" . 

       '      <td >'. $row['digits2'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="cvv">CVV</label></td>  ' . "\n" . 

       '      <td >'. strrev($row['cvv']) .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

			 


  echo '   <tr>' . "\n".

       '      <td > <label for="ip">IP Address</label></td>  ' . "\n" . 

       '      <td >'. $row['ip'] .'</td>  ' . "\n" . 

       '   </tr>' . "\n";	

		

  $t_index++;

  echo '</table></form>' . "\n";

	

	echo '<br />';

	print_header('Commercial Special Information');

			// find and list specials

				$qry2 = mysql_query("SELECT * FROM `cms_commercial_specials` WHERE `id`='".$row['comspecid']."'");

				$row2 = mysql_fetch_assoc($qry2);



						

								echo '<form name="signup" id="signup"><table>' . "\n";			

  echo '<input type="hidden" name="id" value="' . $id . '" />';

  

 								 $t_index = 1;

						echo '   <tr>' . "\n".

								 '      <td width="250"> <label for="s_id">Commercial ID</label></td>  ' . "\n" . 

								 '      <td >'. $row2['id'] .'</td>  ' . "\n" . 

								 '   </tr>' . "\n";	

								 

								 $t_index++;

						echo '   <tr>' . "\n".

								 '      <td > <label for="s_label">Commercial Special</label></td>  ' . "\n" . 

								 '      <td >'. $row2['label'] .'</td>  ' . "\n" . 

								 '   </tr>' . "\n";	

								 

								 $t_index++;

						echo '   <tr>' . "\n".

								 '      <td > <label for="s_descr">Description</label></td>  ' . "\n" . 

								 '      <td >'. $row2['descr'] .'</td>  ' . "\n" . 

								 '   </tr>' . "\n";	

								 

								 $t_index++;

						echo '   <tr>' . "\n".

								 '      <td > <label for="s_price">Price</label></td>  ' . "\n" . 

								 '      <td >'. $row2['price'] .'</td>  ' . "\n" . 

								 '   </tr>' . "\n";	

								 

								 $t_index++;

									echo '</table></form>' . "\n";

}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 * Unlinks Existing Photo

 *

 **************************************************************/



function remove()

{



	global $id, $upload_dir, $module_name;

	

	$result = mysql_query("SELECT * FROM " . db_prefix . "orders WHERE id = '$id'");

	$row    = mysql_fetch_array($result);

	

	print_header('Delete Order - ' . $row['orderid']);

	

	if ( !empty($_POST ))

	{			

		$result = mysql_query('DELETE FROM ' . db_prefix . "orders WHERE id = '$id'");

		print_mysql_message ( mysql_errno() , $module_name, $id, 2 ) ;

			

	} else {

	

		echo '<form action="./?tool=orders&action=remove" method="post" name="form">' . "\n" .

							'<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

							'<tr> <td><div align="center">Are you sure you want to delete this order?</div></td></tr>' . "\n" .

							'<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

							'<input name="No" type="button" value="No" onClick="window.location = \'./?tool=orders\'"></div></td></tr>' . "\n" .

							'</table></form>';

	}//end if

}//end function

?>