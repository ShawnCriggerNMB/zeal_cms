<?php	
	if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module     : Module Settings Config / Programmer ++ Level 
// Written By : Shawn Crigger of EWD
// Written On : Jan 5, 2011
// Updated By : Cory Shaw of EWD
// Updated On : May 3, 2014
// Copyright Zeal Technologies
//***************************************************/

function execute()
{
	switch($_GET['action'])
	{
		case 'update':
			update();
			break;
		case 'add':
			update();
			break;
		case 'remove':
			remove();
			break;
		default:
			manage();
	}
}


/***************************************************************
 *
 * function manage
 * Querrys DB and Displays Content
 *
 **************************************************************/

function manage()
{
	global $db, $identifier, $module_name, $dbtbl;
  
  $i    = 0;
  $link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';
  
  print_header('Manage '.$module_name,$link);

	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sortable" id="table">
  	<thead>
    	<tr>
      	<th><h3>Module</h3></th>
       	<th><h3>Setting</h3></th>
			 	<th><h3>Value</h3></th>
       	<th class="nosort"><h3>Tools</h3></th>
      </tr>
		</thead>
		<tbody>';
  
		$stmt = $db->prepare('SELECT * FROM '.$dbtbl);
		$stmt->execute();
		$pages = $stmt->rowCount();
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{    
    
			echo '<tr align="left" valign="middle">
    		<td>' . $row['module'] . '</td>
    		<td>' . $row['title'] . '</td>
    		<td>' . $row['value'] . '</td>
    		<td style="padding:0;text-align:center;"><strong><a href="./?tool=mod_settings&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool=mod_settings&action=remove&id='.$row['id'].'">Remove</a></strong></td>
    	</tr>';
  	}

  	echo '</tbody>
	</table>';

  $pages = ( $pages > 20 ) ? true : false;
	
  echo_js_sorter ( $pages );
  echo '<div class="spacer">&nbsp;</div>';
	
}//end function


/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 *
 **************************************************************/

function add( $errors = '' )
{
	global $db, $identifier, $module_name, $id, $action, $dbtbl, $site_base_url, $upload_url, $upload_dir, $mod_config;
   
  if ( $errors )
  {
		echo '<ul class="error_message">
			<strong>Please fill in the required fields.</strong>';
			// set error messages for required fields
		
			if ( in_array('module', $errors ) )
			{
				echo '<li>You must fill out a module identifier.</li>';
				$val_m = ' class="form_field_error" ';								
			}
	
			if ( in_array('title', $errors ) )
			{
				echo '<li>You must fill out a title keyword.</li>';
				$val_t = ' class="form_field_error" ';
			}
				
		echo '</ul>';
		
  } else {
    
		$c = ( $action == 'update' ) ? 'update this' : 'add a new';
		
		echo  '<ul class="notice_message">';
			// set instructions here
			echo '<strong>To ' . $c . ' record, fill out the form and click submit.</strong>
			<li>All fields are required'.$u_r.'.</li>
			<li>This is a Programmer\'s Module, If you Don\'t Know What Your Are Doing Please DO NOT TOUCH. You Can Destory Your Website.</li>
		</ul>';
  }
  
  if ($action == "update")
  {
		// Run the query for the existing data to be displayed.
		$stmt = $db->prepare('SELECT * FROM '.$dbtbl.' WHERE id = ?');
    $stmt->execute(array($id));
		$row 	= $stmt->fetch(PDO::FETCH_ASSOC);
  }
	 
	if (!empty ($_POST))
	{
   $row = sanitize_vars ($_POST);
  }
  
/********************************************************
 * Start Building Form
 *******************************************************/

	$r = required();
	
  echo '<form name="form" id="form" method="post" action="#">
		<table>		
  		<input type="hidden" name="id" value="' . $id . '" />';
  
  		echo '<tr>
      	<td><label for="module">'.$r.'Module Identifier</label></td>
      	<td><input ' . $val_m . ' type="text" name="module" id="module" value="'. htmlspecialchars($row['module']) .'" size="45" /></td>
      </tr>';

  		echo '<tr>
      	<td><label for="title">'.$r.'Setting Title</label></td>
      	<td><input ' . $val_t . ' type="text" name="title" id="title" value="'. htmlspecialchars($row['title']) .'" size="45" /></td>
      </tr>';

  		echo '<tr>
      	<td><label for="value">Setting Value</label></td>
      	<td><input type="text" name="value" id="value" value="'. htmlspecialchars($row['value']) .'" size="45" /></td>
      </tr>';

  		echo '<tr>
				<td colspan="2" style="text-align;center; margin:0 auto; padding:3px;">
					<input type="submit" name="submit" value="Submit" /></td>
			</tr>';  
  	
		echo '</table>
	</form>';
 
  echo '<script type="text/javascript">document.getElementById(\'module\').focus();</script>';

}


/***************************************************************
 *
 * function sanitize_vars
 * @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 **************************************************************/

function sanitize_vars( $data )
{
  $r_data['module'] = stripslashes ( $data['module'] );
  $r_data['title']  = stripslashes ( $data['title'] );
  $r_data['value']  = stripslashes ( $data['value'] );
  
  return $r_data;
}


/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $db, $identifier, $module_name, $id, $dbtbl;
	
	$stmt = $db->prepare("SELECT * FROM ".$dbtbl." WHERE id = ?");
  $stmt->execute(array($id));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
  print_header('Delete '.$module_name.'  - ' . $row['title']);
	
	if ( !empty($_POST ))
	{
		$errno = 0;
		try
		{
			$stmt = $db->prepare("DELETE FROM ".$dbtbl." WHERE id = ?");
			$stmt->execute(array($id));
		}
		
		catch(PDOException $ex){
			$errno = $ex->getCode();
		}
		
		print_mysql_message ( $errno , $module_name, $id, 2 ) ;
			
	} else {
	
		echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">
    	<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to delete this record?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div>
    </form>';
	}
}//end function


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute function show_form() to
 * allow editing of contents
 *
 **************************************************************/

function update()
{
	global $db, $identifier, $module_name, $action, $id, $dbtbl;

	if ( $action == 'update' )
		print_header('Update '.$module_name);
	else
		print_header('Add New '.$module_name);
	
	if ( array_key_exists ('submit',$_POST))
	{				
		require("classes/validation.php");
		
		// set rules for required fields
		$rules = array();
		$rules[] = "required,module,module";
		$rules[] = "required,title,title";
	
		$errors = validateFields($_POST, $rules);	
	}
		
	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
	  
		$vars = array();
		$data = sanitize_vars( $_POST );
		$type = ( $action == 'update' ) ? 0 : 1;
		
		if ( $action == 'update' )
		{
    	$sql = "UPDATE ".$dbtbl." SET module = ?, title = ?, value = ? WHERE id = ?";
			array_push($vars, $data['module']);
			array_push($vars, $data['title']);
			array_push($vars, $data['value']);
			array_push($vars, $id);
		
		} else {
      
			$sql = 'INSERT INTO '.$dbtbl.' (module, title, value) VALUES (?, ?, ?)';
				array_push($vars, $data['module']);
				array_push($vars, $data['title']);
				array_push($vars, $data['value']);
		}

		$errono = 0;
		try
		{
			$stmt = $db->prepare($sql);
			$stmt->execute($vars);
		}
		
		catch(PDOException $ex)
		{
			$errono = $ex->getCode();
		}

		if ( is_saint() && $errono != 0 )
			echo '<div class="error_message"><pre>' . $sql . '</pre></div>';
			
		print_mysql_message ( $errono , $module_name, $id, $type ) ;
	
	} else {
		add( $errors );
	}

}//end function
?>