<?php

if ( !defined('BASE') ) die('No Direct Script Access');



//****************************************************/

// Module     : Subcategory Settings / Programmer ++ Level

// Written By : Cory Shaw of EWD

// Written On : May 9, 2014

// Copyright Zeal Technologies

//***************************************************/



function execute()

{

    switch($_GET['action'])

    {

        case 'update':

            update();

            break;

        case 'add':

            update();

            break;

        case 'remove':

            remove();

            break;

        default:

            manage();

    }//end switch

}//end function





/***************************************************************

 *

 * function manage

 * Querrs DB and Displays Slider Content

 *

 *

 **************************************************************/



function manage()

{

    global $identifier, $module_name, $db;



    $i    = 0;

    $link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';



    print_header('Manage '.$module_name,$link);



    echo '<table width="100%" border="0" cellpadding="2" cellspacing="1" class="sortable" id="table">' . "\n" .

        ' 	  <thead>' . "\n" .

        ' 		<tr align="left" valign="top" bgcolor="#ebebeb"> ' . "\n" .

        ' 			  <th><h3>Sub Category</h3></th>' . "\n" .

        ' 			  <th><h3>Main Category</h3></th>' . "\n" .

        ' 			  <th style="width:50px"><h3>Active</h3></th>' . "\n" .

        ' 			  <th style="width:112px;" class="nosort"><h3>Tools</h3></th>' . "\n" .

        '		 </tr></thead><tbody>' . "\n";



    if(!isset($_GET['category_id'])){

        $stmt = $db->prepare('SELECT '.db_prefix.'subcategories.id, '.db_prefix.'subcategories.name, '.db_prefix.'subcategories.active, '.db_prefix.'categories.name AS category FROM ' . db_prefix . $identifier . '

                                JOIN '.db_prefix.'categories

                                    ON '.db_prefix.'categories.id = '.db_prefix.'subcategories.category_id');

        $stmt->execute();

    }

    else{

        $stmt = $db->prepare('SELECT '.db_prefix.'subcategories.id, '.db_prefix.'subcategories.name, '.db_prefix.'subcategories.active, '.db_prefix.'categories.name AS category FROM ' . db_prefix . $identifier . '

                                JOIN '.db_prefix.'categories

                                    ON '.db_prefix.'categories.id = '.db_prefix.'subcategories.category_id

                                WHERE category_id = ?');

        $stmt->execute(array($_GET['category_id']));

    }





    $pages   = $stmt->rowCount();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))

    {

        $i++;

        $act  = yes_no ( $row['active'] );

        echo '<tr align="left" valign="top" style="background: #' . ($i % 2 ? 'ebebeb' : 'fff') . ';">' . "\n";

        echo '<td style="padding-left:20px;">' . $row['name'] . '</td>' . "\n";

        echo '<td style="padding-left:20px;">' . $row['category'] . '</td>' . "\n";

        echo '<td style="padding-left:20px;">' . $act . '</td>' . "\n";

        echo '<td valign="middle"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row['id'].'">Update</a>&nbsp;|&nbsp;<a href="./?tool='.$identifier.'&action=remove&id='.$row['id'].'">Remove</a></strong></td>';

        echo '</tr>' . "\n";



    }//end while



    echo '</tbody></table>';



    $pages = ( $pages > 20 ) ? true : false;



    echo_js_sorter ( $pages );

    echo '<div class="spacer">&nbsp;</div>';



}//end function



/***************************************************************

 *

 * function add

 * @array $errors --> Holds error names to fill in

 * Prints Banner Form for User Input

 *

 *

 **************************************************************/



function add( $errors = '' )

{

    global $identifier, $module_name, $id, $action,$db, $site_base_url, $upload_url, $upload_dir, $mod_config;





    if ( $errors )

    {

        echo '<div class="error_message"><ul><strong>Your Form Has Errors</strong>';



        if ( in_array('name', $errors ) )

        {

            echo '<li>You must fill out a name.</li>';

            $val_n = ' class="form_field_error" ';

        }



        if ( in_array('category_id', $errors ) )

        {

            echo '<li>You must fill out category.</li>';

            $val_a = ' class="form_field_error" ';

        }



        if ( in_array('active', $errors ) )

        {

            echo '<li>You must fill out active.</li>';

            $val_a = ' class="form_field_error" ';

        }



        echo '</ul></div>' . "\n";

    } else {

        $c = ( $action == 'update' ) ? 'update' : 'add';

        echo  '<div class="notice_message">To ' . $c . ' this setting, fill out the form and click submit.<br />' . "\n" .' </div>';

    }



    if ($action == "update")

    {

        $stmt = $db->prepare('SELECT * FROM ' . db_prefix . $identifier.' WHERE id = ?');

        $stmt->execute(array($id));

        $row 		= $stmt->fetch(PDO::FETCH_ASSOC);

    }



    if (!empty ($_POST))

    {

        $row     = sanitize_vars ($_POST);

    }//end if



    $r = required();



    echo '<form name="signup" id="signup" method="post" action="#" enctype="multipart/form-data"><table>' . "\n";

    echo '<input type="hidden" name="id" value="' . $id . '" />';



    $t_index = 1;

    echo '   <tr>' . "\n".

        '      <td > <label for="module">' . $r . 'Sub-Category Name</label></td>  ' . "\n" .

        '      <td > <input ' . $val_n . ' type="text" name="name" id="name" value="'. htmlspecialchars($row['name']) .'" size="45" /></td>  ' . "\n" .

        '   </tr>' . "\n";



    $t_index = 1;

    echo '   <tr>' . "\n".

        '      <td > <label for="module">' . $r . 'Category</label></td>  ' . "\n" .

        '       <td >

                    <select name="category_id" id="category_id">

                        <option value="">--SELECT ONE--</option>';

    $stmt1 = $db->prepare("SELECT * FROM ".db_prefix."categories WHERE active = 1 ORDER BY name ASC");

    $stmt1->execute();

    while($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)){

        echo '<option value="'.$row1['id'].'"';

        if($row['category_id'] == $row1['id'])

            echo ' selected';

        echo '>'.$row1['name'].'</option>';

    }

    echo '          </select>

                </td>  ' . "\n" .

        '   </tr>' . "\n";



    $t_index++;

    echo '   <tr>    

                    <td ><label for="active">Active</label></td>  

                    <td >

                    <select id="active" name="active">

                                <option value="0" '; if ( $row['active'] == '0' ) echo ' SELECTED'; echo ' >No</option>

                                <option value="1" '; if ( $row['active'] == '1' ) echo 'SELECTED'; echo ' >Yes</option>

                    </select>

                    </td>  

            </tr>  '. "\n";



    $t_index++;

    echo '<tr><td colspan="2" style="text-align;center; margin:0 auto; padding:3px;"><input type="submit" name="submit" value="Submit" /></td></tr>' . "\n";

    echo '</table></form>' . "\n";

    echo '<script type="text/javascript">document.getElementById(\'name\').focus();</script>' . "\n";



}





/***************************************************************

 *

 * function sanitize_vars

 * @array $data = Data to be sanitized

 *

 * Returns sanitized variables to be inserted into DB

 *

 *

 **************************************************************/





function sanitize_vars( $data )

{



    $r_data['name'] = stripslashes ( $data['name'] );

    $r_data['category_id'] = stripslashes ( $data['category_id'] );

    $r_data['active']  = stripslashes ( $data['active'] );



    return $r_data;

}



/***************************************************************

 *

 * function remove

 * Deletes Row from Database == $id

 * Unlinks Existing Photo

 *

 **************************************************************/



function remove()

{

    global $identifier, $module_name, $id, $db;



    $stmt = $db->prepare("SELECT * FROM " . db_prefix . $identifier." WHERE id = ?");

    $stmt->execute(array($id));

    $row    = $stmt->fetch(PDO::FETCH_ASSOC);



    print_header('Delete '.$module_name.'  - ' . $row['name']);



    if ( !empty($_POST ))

    {

        $errno = 0;

        try{

            $stmt = $db->prepare('DELETE FROM ' . db_prefix . $identifier." WHERE id = ?");

            $stmt->execute(array($id));

        }

        catch(PDOException $ex){

            $errno = $ex->getCode();

        }

        print_mysql_message ( $errno , $module_name, $id, 2 ) ;



    } else {



        echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">' . "\n" .

            '<input type="hidden" name="id" value="' . $id . '"><table width="100%" border="0">' . "\n" .

            '<tr> <td><div align="center">Are you sure you want to delete this record?</div></td></tr>' . "\n" .

            '<tr> <td><div align="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;' . "\n" .

            '<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div></td></tr>' . "\n" .

            '</table></form>';

    }//end if

}//end function





/***************************************************************

 *

 * function update

 * Updates DB with information stored in $_POST variable

 * if post is empty will execute functin show_form() to

 * allow editing of page contents

 *

 **************************************************************/



function update()

{

    global $identifier, $module_name, $action, $id, $module_name, $db;



    if ( $action == 'update' )

        print_header('Update '.$module_name);

    else

        print_header('Add New '.$module_name);



    if ( array_key_exists ('submit',$_POST))

    {

        require("classes/validation.php");

        $rules   = array();

        $rules[] = "required,name,name";

        $rules[] = "required,category_id,category_id";

        $rules[] = "required,active,active";



        $errors = validateFields($_POST, $rules);

    }



    if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )

    {

        $vars = array();

        $data  = sanitize_vars( $_POST );

        $type  = ( $action == 'update' ) ? 0 : 1;

        if ( $action == 'update' ){

            $sql = "UPDATE " . db_prefix . $identifier." SET name = ?, active = ?, category_id = ? WHERE id = ?";

            array_push($vars, $data['name']);

            array_push($vars, $data['active']);

            array_push($vars, $data['category_id']);

            array_push($vars, $id);

        }

        else{

            $sql = 'INSERT INTO ' . db_prefix . $identifier.' (name, active, category_id) VALUES (?, ?, ?)';

            array_push($vars, $data['name']);

            array_push($vars, $data['active']);

            array_push($vars, $data['category_id']);

        }



        $errono = 0;

        try{

            $stmt = $db->prepare($sql);

            $stmt->execute($vars);

        }

        catch(PDOException $ex){

            $errono = $ex->getCode();

        }





        if ( is_saint() && $errono != 0 )

            echo '<div class="error_message"><pre>' . $sql . '</pre></div>';



        print_mysql_message ( $errono , $module_name, $id, $type ) ;



    } else {

        add( $errors );

    }//end if	

}//end function



?>