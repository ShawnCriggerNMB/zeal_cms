<?php
if ( !defined('BASE') ) die('No Direct Script Access');

//****************************************************/
// Module: Moderators Settings / Programmer ++ Level
// Written By : Chuck Bunnell of EWD
// Written On : June 19, 2014
// Copyright Zeal Technologies
//***************************************************/

/***************************************************************
 *
 * This module displays the list of moderators
 * but pulls the info from the users db table
 * sowe have to reset some variables here
 *
 **************************************************************/
$identifier = 'users';
$dbtbl = db_prefix. 'users';
$sitedir = $config['site_base_dir'];

require( $sitedir . '/lib/cms-load.php');
global $wpdb;
$GLOBALS['cms_users_object'] = new CMS_Users( $wpdb );

// if update make sure this id exists
update_verify();

function execute()
{
	switch($_GET['action'])
 {
		//case 'update':
		//	update();
		//	break;
		//case 'add':
		//	update();
		//	break;
		//case 'remove':
		//	remove();
		//	break;
		default:
			manage();
	}
}


/***************************************************************
 *
 * function manage
 * Querys DB and Displays Content
 *
 **************************************************************/

function manage()
{
	global $db, $identifier, $module_name, $dbtbl;

	//$link = '<a href="./?tool='.$identifier.'&action=add">Add '.$module_name.'</a>';
	$link = '';

	print_header('Manage '.$module_name,$link);

	$sql = '';
	$users = get_users_by_roles( array( 'moderator' ) );

	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sortable" id="table">
		<thead>
			<tr>
				<th><h3>Name</h3></th>
				<th><h3>Email</h3></th>
				<th><h3>Teams</h3></th>
				<th style="width:100px;"><h3>Referral ID</h3></th>
				<th style="width:130px;"><h3>Last IP</h3></th>
    		<th style="width:165px;"><h3>Last Login</h3></th>
				<th style="width:70px;"><h3>Active</h3></th>
				<th class="nosort"><h3>Tools</h3></th>
			</tr>
		</thead>
		<tbody>';

	foreach ( $users as $row )
	{
		$act  = yes_no ( $row->user_status );

		$login_ts = get_user_meta( $row->ID, 'last_login_ts', true );
		$login_ip = get_user_meta( $row->ID, 'last_login_ip', true );

		$lt = ( $login_ts == '' ) ? 'Never Logged' : date("m/d/Y, g:i a", $login_ts );

		// qry teams that moderator moderates
		$stmt2 = $db->prepare("SELECT id,name FROM " . db_prefix . "teams WHERE moderator_id = ? ORDER BY name");
		$stmt2->execute( array($row->ID ) );
		$pages2 = $stmt2->rowCount();

		echo '<tr align="left" valign="middle">
				<td>' . $row->first_name . '&nbsp;' . $row->last_name . '</td>
				<td>' . $row->user_email . '</td>
			<td>';
		if ($pages2 == 0) {
			echo 'none';
		} else {
			while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)){
				echo '<a href="?tool=teams&action=update&id='.$row2['id'].'">'.$row2['name'].'</a><br>';
			}
		}
		echo '</td>
		<td>' . $row->mod_referral_id . '</td>
		<td>' . $login_ip . '</td>
		<td>' . $lt . '</td>
		<td>' . $act . '</td>
		<td style="padding:0;text-align:center;"><strong><a href="./?tool='.$identifier.'&action=update&id='.$row->ID.'">Update</a>&nbsp;|&nbsp;<span class="notool">Remove</span></strong></td>
		</tr>' . "\n";

	}//end foreach

	echo '</tbody></table>';

	$pages = ( $pages > 20 ) ? true : false;

	echo_js_sorter ( $pages );

	echo '<div class="spacer">&nbsp;</div>';
}//end function


/***************************************************************
 *
 * function add
 * @array $errors --> Holds error names to fill in
 * Prints Form for User Input
 *
 **************************************************************/

function add( $errors = '' )
{
	global $db, $identifier, $module_name, $id, $action, $dbtbl, $config;
	$list = $options = $options2 = null;

	if ( $errors )
	{
		echo '<ul class="error_message">
			<strong>Please fill in the required fields.</strong>';
			// set error messages for required fields
			if ( in_array('fname', $errors ) )
			{
				echo '<li>You must fill out a First name.</li>';
				$val_fname = ' class="form_field_error" ';
			}

			if ( in_array('lname', $errors ) )
			{
				echo '<li>You must fill out a Last name.</li>';
				$val_lname = ' class="form_field_error" ';
			}

			if ( in_array('used_email', $errors ) )
			{
				echo '<li>This email address is already being used.</li>';
				$val_email = ' class="form_field_error" ';
			}

			if ( in_array('email', $errors ) )
			{
				echo '<li>You must use a valid email address.</li>';
				$val_email = ' class="form_field_error" ';
			}

			if ( in_array('pass', $errors ) )
			{
				echo '<li>You must fill out a password.</li>';
				$val_pass = ' class="form_field_error" ';
			}

			if ( in_array('shortpass', $errors ) )
			{
				echo '<li> Your password must be 10-20 characters.</li>';
				$val_pass = ' class="form_field_error" ';
			}

		echo '</ul>';

	} else {

		$c = ( $action == 'update' ) ? 'update this' : 'add a new';
		$u_r = ( $action == 'update' ) ? ' except the Password' : '';
		echo  '<ul class="notice_message">';
			// set instructions here
			echo '<strong>To ' . $c . ' record, fill out the form and click submit.</strong>
			<li>All fields are required'.$u_r.'.</li>
			<li>Email: Please use a valid email address. This will be used for their login, and it is the only way this moderator will be able to reset their password if they lose it.</li>';
			if ($action == 'update' )
				echo '<li>Password: Only type in a password if you want to change the present one.</li>';
			echo '<li>Password: Must be 10-20 characters using upper-case, lower-case, and numbers.</li>
		</ul>';
	}

	if ($action == "update")
	{
		// Run the query for the existing data to be displayed.
		$stmt = $db->prepare('SELECT * FROM '.$dbtbl.' WHERE id = ?');
		$stmt->execute(array($id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
	}

	if (!empty ($_POST))
	{
	 $row = sanitize_vars ($_POST);
	}


/********************************************************
 * Start Building Form
 *******************************************************/

  $r = required();
	// pass only required for "add" not "update"
	$rp = ($action == 'update') ? '' : required();
	echo '<form name="form" id="form" method="post" action="" >
		<table>
			<input type="hidden" name="id" value="' . $id . '" />';

			$passnote = tooltip('Recommended: 10-20 characters using uppercase, lowercase, and numbers.');

			if ($action == 'update')
				$passnote = tooltip('Leave blank to keep current password.<br />
													Recommended: 10-20 characters using uppercase, lowercase, and numbers.');

			echo '<tr>
				<td> <label for="fname">'.$r.'First Name</label></td>
				<td> <input ' . $val_fname . ' type="text" name="fname" id="fname" value="'. htmlspecialchars($row['fname']) .'" size="45" /></td>
			</tr>';

			echo '<tr>
				<td> <label for="lname">'.$r.'Last Name</label></td>
				<td> <input ' . $val_lname . ' type="text" name="lname" id="lname" value="'. htmlspecialchars($row['lname']) .'" size="45" /></td>
			</tr>';

			echo '<tr>
				<td> <label for="email">'.$r.'Email</label></td>
				<td> <input ' . $val_email . ' type="text" name="email" id="email" value="'. htmlspecialchars($row['email']) .'" size="45" /></td>
			</tr>';

			echo '<tr>
				<td> <label for="pass">' . $rp . 'Password</label></td>
				<td> <input ' . $val_pass . ' type="text" name="pass" id="pass" value="" size="45" /> ' .$passnote. ' </td>
			</tr>';

			echo '<tr>
				<td><label for="active">'.$r.'Active</label></td>
				<td>
					'.create_slist ( $list, 'active', $row['active'], 1 ) .
					tooltip('Set to Yes to Allow Login').'
				</td>
			</tr>';

			echo '<tr>
				<td colspan="2" style="padding:3px;"><input type="submit" name="submit" value="Submit" /></td>
			</tr>
 		</table>
	</form>';

	echo '<script type="text/javascript">document.getElementById(\'fname\').focus();</script>';

}


/***************************************************************
 *
 * function sanitize_vars
 * @array $data = Data to be sanitized
 *
 * Returns sanitized variables to be inserted into DB
 *
 **************************************************************/

function sanitize_vars( $data )
{
	$r_data['fname']    = $data['fname'];
	$r_data['lname']    = $data['lname'];
	$r_data['email']    = $data['email'];
	$r_data['pass']     = $data['pass'];
	$r_data['active']		= intval ( $data['active'] );

 	if ( !array_key_exists('active', $_POST ) )
		$r_data['active'] = 1;

	return $r_data;
}


/***************************************************************
 *
 * function remove
 * Deletes Row from Database == $id
 *
 **************************************************************/

function remove()
{
	global $db, $identifier, $module_name, $id, $dbtbl;

	$stmt = $db->prepare("SELECT * FROM ".$dbtbl." WHERE id = ?");
  $stmt->execute(array($id));
	$row  = $stmt->fetch(PDO::FETCH_ASSOC);

	print_header('Delete '.$module_name.'  - ' . $row['fname'].' '.$row['lname']);

	if ( !empty($_POST ))
	{
		$errno = 0;

		try
		{
			$stmt = $db->prepare("DELETE FROM ".$dbtbl." WHERE id = ?");
			$stmt->execute(array($id));
		}

		catch(PDOException $ex)
		{
			$errno = $ex->getCode();
		}

		print_mysql_message ( $errno , $module_name, $id, '2' ) ;

	} else {

		echo '<form action="./?tool='.$identifier.'&action=remove" method="post" name="form">
			<input type="hidden" name="id" value="' . $id . '">
			<div class="center">Are you sure you want to delete this record?</div>
			<div class="center"><input name="Submit" type="submit" id="Submit" value="Yes">&nbsp;&nbsp;<input name="No" type="button" value="No" onClick="window.location = \'./?tool='.$identifier.'\'"></div>
		</form>';
	}

}//end function


/***************************************************************
 *
 * function update
 * Updates DB with information stored in $_POST variable
 * if post is empty will execute function show_form() to
 * allow editing of contents
 *
 **************************************************************/

function update()
{
	global $db, $identifier, $module_name, $action, $id, $dbtbl;

	if ( $action == 'update' )
		print_header('Update '.$module_name);
	else
		print_header('Add New '.$module_name);

	if ( array_key_exists ('submit',$_POST) )
	{
		require ("classes/validation.php");

		// set rules for required fields
		$rules = array();
		$rules[] = "required,fname,fname";
		$rules[] = "required,lname,lname";
		$rules[] = "required,email,email";
		$rules[] = "valid_email,email,email";

		if ( $action != 'update' )
			$rules[] = "required,pass,pass";

		$errors = validateFields($_POST, $rules);

		if ( $action != 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM ".$dbtbl." WHERE email = ?");
      $stmt->execute(array($_POST['email']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_email';
		}

		if ( $action == 'update' )
		{
			$stmt = $db->prepare("SELECT id FROM ".$dbtbl." WHERE email = ? AND id != $id");
      $stmt->execute(array($_POST['email']));
			if ( $stmt->rowCount() != 0 )
				$errors[] = 'used_email';
		}

		$numpass = strlen ($_POST['pass']);
		if ($_POST['pass'] != '' && ($numpass < 10 || $numpass > 20))
			$errors[] = 'shortpass';

	}

	if ( ( empty ( $errors )) && ( array_key_exists ('submit',$_POST)) )
	{
		$vars = array();
		$data  = sanitize_vars( $_POST );
		$type = ( $action == 'update' ) ? 0 : 1;
		$xsql = array();

		if ( $action == 'update' )
		{
			$sql = 'UPDATE '.$dbtbl.' SET fname = ?, lname = ?, email = ?, active = ?';
			array_push($vars, $data['fname']);
			array_push($vars, $data['lname']);
			array_push($vars, $data['email']);
			array_push($vars, $data['active']);

			if ( $data['pass'] != '' )
			{
				$xsql[] .= ", pass = ? ";
				array_push($vars, md5( $data['pass']));
			}

			$sql .= " WHERE id = ?";
      array_push($vars, $id);

		} else {

		 	$sql = 'INSERT INTO '.$dbtbl.' (fname, lname, email, pass, date, active) VALUES ' . "(?, ?, ?, ?, ?, ?)";
			array_push($vars, $data['fname']);
			array_push($vars, $data['lname']);
			array_push($vars, $data['email']);
			array_push($vars, md5( $data['pass'] ));
			array_push($vars, $_SERVER['REQUEST_TIME']);
			array_push($vars, $data['active']);
		}

		$errno = 0;
		try
		{
			$stmt = $db->prepare($sql);
			$stmt->execute($vars);
		}

		catch(PDOException $ex)
		{
			$errno = $ex->getCode();
		}

		if ( ( $errno != 0 ) && ( is_saint() ) )
			print_debug($sql);

		print_mysql_message ( $errno , $module_name, $id, $type ) ;

  } else {

		add( $errors );
	}

}//end function
?>