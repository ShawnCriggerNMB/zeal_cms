<?php
ob_start("ob_gzhandler");
session_start();
define( 'BASE', TRUE );
error_reporting(E_ALL);


include("functions.inc.php");

if ( FALSE === is_user_logged_in() )
{
	$link = admin_url( '/login.php' );
	header("Location: {$link}");
	exit;
}

if ( ! is_object( $current_user ) OR ( ! $current_user->has_cap('administrator') && ! $current_user->has_cap( 'superadmin' ) ) )
{
	cms_die('You do not have permission to access this area.');
}
//global $current_user;
//$current_user = (object) array( 'display_name' => 'Shawn Crigger' );

/*
$t = 'a:8:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:77:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:21:"connections_view_menu";b:1;s:26:"connections_view_dashboard";b:1;s:18:"connections_manage";b:1;s:21:"connections_add_entry";b:1;s:31:"connections_add_entry_moderated";b:1;s:22:"connections_edit_entry";b:1;s:32:"connections_edit_entry_moderated";b:1;s:24:"connections_delete_entry";b:1;s:23:"connections_view_public";b:1;s:24:"connections_view_private";b:1;s:25:"connections_view_unlisted";b:1;s:27:"connections_edit_categories";b:1;s:27:"connections_change_settings";b:1;s:27:"connections_manage_template";b:1;s:24:"connections_change_roles";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:49:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:21:"connections_view_menu";b:1;s:26:"connections_view_dashboard";b:1;s:18:"connections_manage";b:1;s:21:"connections_add_entry";b:1;s:31:"connections_add_entry_moderated";b:1;s:22:"connections_edit_entry";b:1;s:32:"connections_edit_entry_moderated";b:1;s:24:"connections_delete_entry";b:1;s:23:"connections_view_public";b:1;s:24:"connections_view_private";b:1;s:25:"connections_view_unlisted";b:1;s:27:"connections_edit_categories";b:1;s:27:"connections_change_settings";b:1;s:27:"connections_manage_template";b:1;s:24:"connections_change_roles";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:25:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;s:21:"connections_view_menu";b:1;s:26:"connections_view_dashboard";b:1;s:18:"connections_manage";b:1;s:21:"connections_add_entry";b:1;s:31:"connections_add_entry_moderated";b:1;s:22:"connections_edit_entry";b:1;s:32:"connections_edit_entry_moderated";b:1;s:24:"connections_delete_entry";b:1;s:23:"connections_view_public";b:1;s:24:"connections_view_private";b:1;s:25:"connections_view_unlisted";b:1;s:27:"connections_edit_categories";b:1;s:27:"connections_change_settings";b:1;s:27:"connections_manage_template";b:1;s:24:"connections_change_roles";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:6:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:23:"connections_view_public";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:3:{s:4:"read";b:1;s:7:"level_0";b:1;s:23:"connections_view_public";b:1;}}s:14:"backwpup_admin";a:2:{s:4:"name";s:14:"BackWPup Admin";s:12:"capabilities";a:11:{s:8:"backwpup";b:1;s:13:"backwpup_jobs";b:1;s:18:"backwpup_jobs_edit";b:1;s:19:"backwpup_jobs_start";b:1;s:16:"backwpup_backups";b:1;s:25:"backwpup_backups_download";b:1;s:23:"backwpup_backups_delete";b:1;s:13:"backwpup_logs";b:1;s:20:"backwpup_logs_delete";b:1;s:17:"backwpup_settings";b:1;s:23:"connections_view_public";b:1;}}s:14:"backwpup_check";a:2:{s:4:"name";s:21:"BackWPup jobs checker";s:12:"capabilities";a:11:{s:8:"backwpup";b:1;s:13:"backwpup_jobs";b:1;s:18:"backwpup_jobs_edit";b:0;s:19:"backwpup_jobs_start";b:0;s:16:"backwpup_backups";b:1;s:25:"backwpup_backups_download";b:0;s:23:"backwpup_backups_delete";b:0;s:13:"backwpup_logs";b:1;s:20:"backwpup_logs_delete";b:0;s:17:"backwpup_settings";b:0;s:23:"connections_view_public";b:1;}}s:15:"backwpup_helper";a:2:{s:4:"name";s:20:"BackWPup jobs helper";s:12:"capabilities";a:11:{s:8:"backwpup";b:1;s:13:"backwpup_jobs";b:1;s:18:"backwpup_jobs_edit";b:0;s:19:"backwpup_jobs_start";b:1;s:16:"backwpup_backups";b:1;s:25:"backwpup_backups_download";b:1;s:23:"backwpup_backups_delete";b:1;s:13:"backwpup_logs";b:1;s:20:"backwpup_logs_delete";b:1;s:17:"backwpup_settings";b:0;s:23:"connections_view_public";b:1;}}}';

$t = unserialize( $t );

dump($t);
die();
$user = new CMS_User( 1 );
$user->set_role('administrator');
*/

$tool = ( isset( $_GET['tool'] ) && ! empty( $_GET['tool'] ) ) ? $_GET['tool'] : 'dashboard';

$action = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] : 'datatable';
create_sidebar();
$hook_suffix  = NULL;
$module_title = $tool;
$hook_suffix = strtolower( $tool ) . '-' . $action;

$module_html = load_module( $tool );
Console::log( 'testing' );
View::set_tags( get_defined_vars() );
View::show( 'layout' );
