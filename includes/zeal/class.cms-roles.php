<?php
/**
 * @link {https://core.trac.wordpress.org/browser/tags/4.0/src/wp-includes/capabilities.php#L0}
 */
class CMS_Roles {

	var $role_objects = array();
	var $role_names = array();

	// ------------------------------------------------------------------------
	public function __construct() {
		do_action_ref_array('init_roles', array(&$this) );
	}

	// ------------------------------------------------------------------------

	public function add_role($role, $display_name, $capabilities = '') {
		if ( isset($this->role_objects[$role]) )
			return;

		$this->role_objects[$role] = new CMS_Role($role, $capabilities, $this);
		$this->role_names[$role] = $display_name;
		return $this->role_objects[$role];
	}

	// ------------------------------------------------------------------------

	public function remove_role($role) {
		if ( ! isset($this->role_objects[$role]) )
			return;

		unset($this->role_objects[$role]);
		unset($this->role_names[$role]);
	}

	// ------------------------------------------------------------------------

	public function add_cap($role, $cap, $grant = true) {
		if ( isset($this->role_objects[$role]) )
			$this->role_objects[$role]->add_cap($cap, $grant);
	}

	// ------------------------------------------------------------------------

	public function remove_cap($role, $cap) {
		if ( isset($this->role_objects[$role]) )
			$this->role_objects[$role]->remove_cap($cap, $grant);
	}

	// ------------------------------------------------------------------------

	public function &get_role($role) {
		if ( isset($this->role_objects[$role]) )
			return $this->role_objects[$role];
		else
			return null;
	}

	// ------------------------------------------------------------------------

	public function get_names() {
		return $this->role_names;
	}

	// ------------------------------------------------------------------------

	public function is_role($role) {
		return isset($this->role_names[$role]);
	}

	// ------------------------------------------------------------------------

	public function map_meta_cap( $cap, $user_id ) {
		$args = array_slice(func_get_args(), 2);
		return apply_filters( 'map_meta_cap', array( $cap ), $cap, $user_id, $args );
	}
}// end class CMS_Roles()

// ------------------------------------------------------------------------

class CMS_Role {
	var $name;
	var $capabilities;

	// ------------------------------------------------------------------------

	public function __construct($role, $capabilities) {
		$this->name = $role;
		$this->capabilities = $capabilities;
	}

	// ------------------------------------------------------------------------

	public function add_cap($cap, $grant = true) {
		$this->capabilities[$cap] = $grant;
	}

	// ------------------------------------------------------------------------
	public function remove_cap($cap) {
		unset($this->capabilities[$cap]);
	}

	// ------------------------------------------------------------------------

	public function has_cap($cap) {
		$capabilities = apply_filters('role_has_cap', $this->capabilities, $cap, $this->name);
		$grant = !empty( $capabilities[$cap] );
		return apply_filters("{$this->name}_has_cap", $grant);
	}

	// ------------------------------------------------------------------------
}// end class CMS_Role()
