<?php


function cms_get_option( $option )
{
	if ( !class_exists('CMS_Options') ) {
		return;
	}

	return CMS_Options::get( $option );
}

// ------------------------------------------------------------------------

function cms_add_option( $option, $value )
{
	if ( !class_exists('CMS_Options') ) {
		return;
	}

	return CMS_Options::add( $option, $value );
}

// ------------------------------------------------------------------------

function cms_update_option( $option, $value )
{
	if ( !class_exists('CMS_Options') ) {
		return;
	}

	return CMS_Options::update( $option, $value );
}

// ------------------------------------------------------------------------

function cms_delete_option( $option )
{
	if ( !class_exists('CMS_Options') ) {
		return;
	}

	return CMS_Options::delete( $option );
}

// ------------------------------------------------------------------------

function cms_get_transient( $transient )
{
	if ( !class_exists('CMS_Transients') ) {
		return;
	}

	return CMS_Transients::get( $transient );
}

// ------------------------------------------------------------------------

function cms_set_transient( $transient, $value, $expiration = 0 )
{
	if ( !class_exists('CMS_Transients') ) {
		return;
	}

	return CMS_Transients::set( $transient, $value, $expiration );
}

// ------------------------------------------------------------------------

function cms_delete_transient( $transient )
{
	if ( !class_exists('CMS_Transients') ) {
		return;
	}

	return CMS_Transients::delete( $transient );
}