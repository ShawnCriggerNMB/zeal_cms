<?php

// if Users
if ( ! array_key_exists( 'cms_users_object', $GLOBALS ) OR ! is_object( $GLOBALS['cms_users_object'] ) )
{
	$GLOBALS['cms_users_object'] = new CMS_Users( $ndb );
}

// Last sync [WP11537]

/**
 * Zeal Tech CMS User class.
 *
 * @since 2.0.0
 * @package BackPress
 * @subpackage User
 */
class CMS_User {
	/**
	 * User data container.
	 *
	 * This will be set as properties of the object.
	 *
	 * @since 2.0.0
	 * @access private
	 * @var array
	 */
	var $data;

	/**
	 * The user's ID.
	 *
	 * @since 2.1.0
	 * @access public
	 * @var int
	 */
	var $ID = 0;

	/**
	 * The deprecated user's ID.
	 *
	 * @since 2.0.0
	 * @access public
	 * @deprecated Use CMS_User::$ID
	 * @see CMS_User::$ID
	 * @var int
	 */
	var $id = 0;

	/**
	 * The individual capabilities the user has been given.
	 *
	 * @since 2.0.0
	 * @access public
	 * @var array
	 */
	var $caps = array();

	/**
	 * User metadata option name.
	 *
	 * @since 2.0.0
	 * @access public
	 * @var string
	 */
	var $cap_key;

	/**
	 * The roles the user is part of.
	 *
	 * @since 2.0.0
	 * @access public
	 * @var array
	 */
	var $roles = array();

	/**
	 * All capabilities the user has, including individual and role based.
	 *
	 * @since 2.0.0
	 * @access public
	 * @var array
	 */
	var $allcaps = array();

	/**
	 * First name of the user.
	 *
	 * Created to prevent notices.
	 *
	 * @since 2.7.0
	 * @access public
	 * @var string
	 */
	var $first_name = '';

	/**
	 * Last name of the user.
	 *
	 * Created to prevent notices.
	 *
	 * @since 2.7.0
	 * @access public
	 * @var string
	 */
	var $last_name = '';

	// ------------------------------------------------------------------------

	/**
	 * Sets up the object properties.
	 *
	 * Retrieves the userdata and then assigns all of the data keys to direct
	 * properties of the object. Calls {@link CMS_User::_init_caps()} after
	 * setting up the object's user data properties.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @param int|string $id User's ID or username
	 * @param int $name Optional. User's username
	 * @return CMS_User
	 */
	public function __construct( $id, $name = '' )
	{
		global $cms_users_object;

		if ( empty( $id ) && empty( $name ) )
			return;

		if ( ! is_numeric( $id ) ) {
			$name = $id;
			$id = 0;
		}

		if ( ! empty( $id ) )
			$this->data = $GLOBALS['cms_users_object']->get_user( $id );
		else
			$this->data = $GLOBALS['cms_users_object']->get_user( $name, array( 'by' => 'login' ) );

		if ( empty( $this->data->ID ) )
			return;

		foreach ( get_object_vars( $this->data ) as $key => $value ) {
			$this->{$key} = $value;
		}

		$this->id = $this->ID;
		$this->_init_caps();
	}

	// ------------------------------------------------------------------------

	/**
	 * Setup capability object properties.
	 *
	 * Will set the value for the 'cap_key' property to current database table
	 * prefix, followed by 'capabilities'. Will then check to see if the
	 * property matching the 'cap_key' exists and is an array. If so, it will be
	 * used.
	 *
	 * @since 2.1.0
	 * @access protected
	 */
	protected function _init_caps() {
		global $cms_users_object;
		$this->cap_key = $GLOBALS['cms_users_object']->db->prefix . 'capabilities';
		$this->caps = &$this->{$this->cap_key};
		if ( ! is_array( $this->caps ) )
			$this->caps = array();
		$this->get_role_caps();
	}

	// ------------------------------------------------------------------------

	/**
	 * Retrieve all of the role capabilities and merge with individual capabilities.
	 *
	 * All of the capabilities of the roles the user belongs to are merged with
	 * the users individual roles. This also means that the user can be denied
	 * specific roles that their role might have, but the specific user isn't
	 * granted permission to.
	 *
	 * @since 2.0.0
	 * @uses $cms_roles
	 * @access public
	 */
	public function get_role_caps() {
		global $cms_roles, $cms_users_object;

		if ( ! isset( $cms_roles ) )
			$GLOBALS['cms_roles'] = new CMS_Roles( $GLOBALS['cms_users_object']->db );

		//Filter out caps that are not role names and assign to $this->roles
		if ( is_array( $this->caps ) )
			$this->roles = array_filter( array_keys( $this->caps ), array( &$cms_roles, 'is_role' ) );

		//Build $allcaps from role caps, overlay user's $caps
		$this->allcaps = array();
		foreach ( (array) $this->roles as $role ) {
			$role = $GLOBALS['cms_roles']->get_role( $role );
			$this->allcaps = array_merge( (array) $this->allcaps, (array) $role->capabilities );
		}
		$this->allcaps = array_merge( (array) $this->allcaps, (array) $this->caps );
	}

	// ------------------------------------------------------------------------

	/**
	 * Add role to user.
	 *
	 * Updates the user's meta data option with capabilities and roles.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @param string $role Role name.
	 */
	public function add_role( $role ) {
		$this->caps[$role] = true;
		$this->update_user();
	}

	/**
	 * Remove role from user.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @param string $role Role name.
	 */
	public function remove_role( $role ) {
		if ( empty( $this->caps[$role] ) || ( count( $this->caps ) <= 1 ) )
			return;
		unset( $this->caps[$role] );
		$this->update_user();
	}

	/**
	 * Set the role of the user.
	 *
	 * This will remove the previous roles of the user and assign the user the
	 * new one. You can set the role to an empty string and it will remove all
	 * of the roles from the user.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @param string $role Role name.
	 */
	public function set_role( $role ) {
		foreach ( (array) $this->roles as $oldrole )
			unset( $this->caps[$oldrole] );
		if ( !empty( $role ) ) {
			$this->caps[$role] = true;
			$this->roles = array( $role => true );
		} else {
			$this->roles = false;
		}
		$this->update_user();
	}

	// ------------------------------------------------------------------------

	public function update_user() {
		$GLOBALS['cms_users_object']->update_meta( array( 'id' => $this->ID, 'meta_key' => $this->cap_key, 'meta_value' => $this->caps ) );
		$this->get_role_caps();
		//$this->update_user_level_from_caps(); // WP
	}

	// ------------------------------------------------------------------------

	/**
	 * Add capability and grant or deny access to capability.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @param string $cap Capability name.
	 * @param bool $grant Whether to grant capability to user.
	 */
	public function add_cap( $cap, $grant = true ) {
		$this->caps[$cap] = $grant;
		$this->update_user();
	}

	// ------------------------------------------------------------------------

	/**
	 * Remove capability from user.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @param string $cap Capability name.
	 */
	public function remove_cap( $cap ) {
		if ( empty( $this->caps[$cap] ) ) return;
		unset( $this->caps[$cap] );
		$this->update_user();
	}

	// ------------------------------------------------------------------------

	/**
	 * Remove all of the capabilities of the user.
	 *
	 * @since 2.1.0
	 * @access public
	 */
	public function remove_all_caps() {
		global $cms_users_object;
		$this->caps = array();
		$GLOBALS['cms_users_object']->delete_meta( $this->ID, $this->cap_key );
		$this->get_role_caps();
	}

	// ------------------------------------------------------------------------

	/**
	 * Whether user has capability or role name.
	 *
	 * This is useful for looking up whether the user has a specific role
	 * assigned to the user. The second optional parameter can also be used to
	 * check for capabilities against a specfic post.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @param string|int $cap Capability or role name to search.
	 * @param int $post_id Optional. Post ID to check capability against specific post.
	 * @return bool True, if user has capability; false, if user does not have capability.
	 */
	public function has_cap( $cap ) {
		global $cms_roles;

		if ( is_numeric( $cap ) )
			$cap = $this->translate_level_to_cap( $cap );

		$args = array_slice( func_get_args(), 1 );
		$args = array_merge( array( $cap, $this->ID ), $args );
		$caps = call_user_func_array( array(&$GLOBALS['cms_roles'], 'map_meta_cap'), $args );
		// Must have ALL requested caps
		$capabilities = apply_filters( 'user_has_cap', $this->allcaps, $caps, $args );
		foreach ( (array) $caps as $cap ) {
			//echo "Checking cap $cap<br />";
			if ( empty( $capabilities[$cap] ) || !$capabilities[$cap] )
				return false;
		}

		return true;
	}

	// ------------------------------------------------------------------------

	/**
	 * Convert numeric level to level capability name.
	 *
	 * Prepends 'level_' to level number.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @param int $level Level number, 1 to 10.
	 * @return string
	 */
	public function translate_level_to_cap( $level ) {
		return 'level_' . $level;
	}

	// ------------------------------------------------------------------------

}// class CMS_User()

// ------------------------------------------------------------------------
// Helper functions
// ------------------------------------------------------------------------

/**
 * Updates user record
 * @param  integer $ID   user id
 * @param  array   $args  array of values to update
 * @return mixed
 */
function update_user( $ID, $args = null )
{
	return $GLOBALS['cms_users_object']->update_user( $ID, $args );
}

function new_user( $args = null )
{
	return $GLOBALS['cms_users_object']->new_user( $args );
}

// ------------------------------------------------------------------------

/**
 * Gets user by ID, user_login or user_email.
 * @param  integer $user_id $user_id can be user ID#, user_login, user_email (by specifying by = email)
 * @param  array   $args    possible values can be array( 'output' => OBJECT, 'by' => false, 'from_cache' => true, 'append_meta' => true );
 *
 * @return mixed
 */
function get_user( $user_id, $args = null )
{
	$user = $GLOBALS['cms_users_object']->get_user( $user_id, $args );
	if ( is_cms_error($user) )
		return false;
	if ( !empty($user->ID) )
		return new CMS_User( $user->ID );
	return false;
}

// ------------------------------------------------------------------------

/**
 * Deletes user and usermeta from database.
 * @param  integer $user_id
 * @return mixed
 */
function delete_user( $user_id )
{
	$user = $GLOBALS['cms_users_object']->delete_user( $user_id );

	if ( is_cms_error($user) ) return $user;
	return true;

}

// ------------------------------------------------------------------------


/**
* Retrieve user meta field for a user.
*
* @uses get_metadata()
* @link http://codex.wordpress.org/Function_Reference/get_user_meta
*
* @param int $user_id User ID.
* @param string $key Optional. The meta key to retrieve. By default, returns data for all keys.
* @param bool $single Whether to return a single value.
* @return mixed Will be an array if $single is false. Will be value of meta data field if $single
*  is true.
*/
function get_user_meta($user_id, $key = '', $single = false) {
	return get_metadata('user', $user_id, $key, $single);
}

// ------------------------------------------------------------------------


/**
 * Update user meta field based on user ID.
 *
 * Use the $prev_value parameter to differentiate between meta fields with the
 * same key and user ID.
 *
 * If the meta field for the user does not exist, it will be added.
 *
 * @since 3.0.0
 * @uses update_metadata
 * @link http://codex.wordpress.org/Function_Reference/update_user_meta
 *
 * @param int $user_id User ID.
 * @param string $meta_key Metadata key.
 * @param mixed $meta_value Metadata value.
 * @param mixed $prev_value Optional. Previous value to check before removing.
 * @return int|bool Meta ID if the key didn't exist, true on successful update, false on failure.
 */
function update_user_meta($user_id, $meta_key, $meta_value, $prev_value = '') {
	return update_metadata('user', $user_id, $meta_key, $meta_value, $prev_value);
}


/**
 * Add meta data field to a user.
 *
 * Post meta data is called "Custom Fields" on the Administration Screens.
 *
 * @since 3.0.0
 * @uses add_metadata()
 * @link http://codex.wordpress.org/Function_Reference/add_user_meta
 *
 * @param int $user_id User ID.
 * @param string $meta_key Metadata name.
 * @param mixed $meta_value Metadata value.
 * @param bool $unique Optional, default is false. Whether the same key should not be added.
 * @return int|bool Meta ID on success, false on failure.
 */
function add_user_meta($user_id, $meta_key, $meta_value, $unique = false) {
	return add_metadata('user', $user_id, $meta_key, $meta_value, $unique);
}

// ------------------------------------------------------------------------

/**
 * Remove metadata matching criteria from a user.
 *
 * You can match based on the key, or key and value. Removing based on key and
 * value, will keep from removing duplicate metadata with the same key. It also
 * allows removing all metadata matching key, if needed.
 *
 * @since 3.0.0
 * @uses delete_metadata()
 * @link http://codex.wordpress.org/Function_Reference/delete_user_meta
 *
 * @param int $user_id user ID
 * @param string $meta_key Metadata name.
 * @param mixed $meta_value Optional. Metadata value.
 * @return bool True on success, false on failure.
 */
function delete_user_meta($user_id, $meta_key, $meta_value = '') {
	return delete_metadata('user', $user_id, $meta_key, $meta_value);
}

// ------------------------------------------------------------------------

/**
 * Inserts or Updates user meta value
 * @deprecated   Use update_user_meta instead.
 * @param  integer $id         user id
 * @param  string  $meta_key   meta key name
 * @param  string  $meta_value value to use for meta
 * @return mixed
 */
function update_usermeta( $id, $meta_key, $meta_value ) {
	$return = $GLOBALS['cms_users_object']->update_meta( compact( 'id', 'meta_key', 'meta_value' ) );
	if ( is_cms_error( $return ) )
		return false;
	return $return;
}

// ------------------------------------------------------------------------

/**
 * Delete user meta value
 * @deprecated
 * @param  integer $id         user id
 * @param  string  $meta_key   meta key name
 * @param  string  $meta_value optional meta value
 * @return mixed
 */
function delete_usermeta( $id, $meta_key, $meta_value = null ) {
	$return = $GLOBALS['cms_users_object']->delete_meta( compact( 'id', 'meta_key', 'meta_value' ) );
	if ( is_cms_error( $return ) )
		return false;
	return $return;
}

// ------------------------------------------------------------------------

if ( ! function_exists( 'get_current_user_id' ) ) :

/**
 * Returns true/false depending on user logged in
 * @return boolean
 */
function get_current_user_id() {
	global $current_user;
	if ( ! isset( $current_user ) OR ! is_object( $current_user ) ) return FALSE;
	if ( 0 == (int) $current_user->ID ) return false;
	return (int) $current_user->ID;
}
endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'is_user_logged_in' ) ) :

/**
 * Returns true/false depending on user logged in
 * @return boolean
 */
function is_user_logged_in() {
	global $current_user;
	if ( ! isset( $current_user->id ) OR 0 === (int) $current_user->ID ) return FALSE;
	return true;
}
endif;


// ------------------------------------------------------------------------

if ( !function_exists('cms_login') ) :
/**
 * Handles verification of login by username and password, sets global error for error messages and returns boolean.
 *
 * @global $db
 * @global $error
 *
 * @param  string  $username    [description
 * @param  string  $password
 * @param  boolean $already_md5
 * @return booleam
 */
function cms_login($username, $password, $already_md5 = false) {
	global $db, $error;

	if ( '' == $username ) {
		$error = __('<strong>Error</strong>: The email field is empty.');
		return false;
	}

	if ( '' == $password ) {
		$error = __('<strong>Error</strong>: The password field is empty.');
		return false;
	}

	$user = new CMS_User( $username );

	if (!$user || !$user->ID) {
		$error = __('<strong>Error</strong>: Wrong email.');
		return false;
	}

	if ( ! CMS_Pass::check_password( $password, $user->data->user_pass, $user->ID ) ) {
		$error = __('<strong>Error</strong>: Incorrect password.');
		$pwd = '';
		return false;
	}


	// check if user is active
	if ( 1 !== (int)$user->user_status )
	{
		$error = __('<strong>Error</strong>: Your account needs to be activated.');
		return false;
	}

/*
	// @todo: remove mod/admin user cap requirement to login
	if ( !$user->has_cap( 'moderator' ) && !$user->has_cap( 'administrator' ) )
	{
		$error = __('<strong>Error</strong>: You currently have to be a moderator or administrator to go any futher.');
		return false;
	}
*/

	return true;
}
endif;

// ------------------------------------------------------------------------

/**
 * Creates roles in database
 * @param  [type] $name         [description]
 * @param  [type] $display_name [description]
 * @param  array  $capabilities [description]
 * @return [type]               [description]
 */
function create_role( $name, $display_name, $capabilities = array() ) {
	global $cms_roles, $wpdb;
	if ( ! isset( $GLOBALS['cms_roles'] ) )
		$GLOBALS['cms_roles'] = new CMS_Roles( $wpdb );

	$GLOBALS['cms_roles']->add_role( $name, $display_name, $capabilities );
}

// ------------------------------------------------------------------------


/**
 * Get users with specified roles
 * @param  array   $roles    array or string of role names
 * @param  string  $orderby  fieldname to order by, defaults to ID
 * @return
 */
function get_users_by_roles( $roles = array(), $orderby = 'ID' )
{
    global $wpdb;

    if ( ! is_array( $roles ) )
    {
        $roles = array_walk( explode( ",", $roles ), 'trim' );
    }

    $sql = "SELECT {$wpdb->users}.*, first_name.meta_value as first_name, last_name.meta_value as last_name, role.meta_value as role
    			FROM {$wpdb->users}
    		INNER JOIN {$wpdb->usermeta}
    			ON {$wpdb->users}.ID ={$wpdb->usermeta}.user_id
		 	LEFT JOIN {$wpdb->usermeta} AS first_name ON first_name.user_id={$wpdb->users}.ID
		 		AND first_name.meta_key='first_name'
		 	LEFT JOIN {$wpdb->usermeta} AS last_name  ON last_name.user_id ={$wpdb->users}.ID
		 		AND last_name.meta_key='last_name'
		 	LEFT JOIN {$wpdb->usermeta} AS role  ON role.user_id ={$wpdb->users}.ID
		 		AND role.meta_key='{$wpdb->prefix}capabilities'
    		WHERE {$wpdb->usermeta}.meta_key='{$wpdb->prefix}capabilities'
    		AND ( ";
    $i = 1;
    foreach ( $roles as $role ) {
        $sql .= " {$wpdb->usermeta}.meta_value LIKE '%{$role}%' ";
        if ( $i < count( $roles ) ) $sql .= ' OR ';
        $i++;
    }
    $sql .= ' ) ';
    $sql .= ' ORDER BY ' . $orderby;

    return  $wpdb->get_results( $sql );
}// get_users_by_roles()

// ------------------------------------------------------------------------









