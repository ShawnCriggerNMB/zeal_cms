<?php
/**
 * Zeal Tech CMS script procedural API.
 *
 * @package Zeal Tech CMS
 * @since r16
 */

/**
 * Prints script tags in document head.
 *
 * Called by admin-header.php and by cms_head hook. Since it is called by cms_head
 * on every page load, the function does not instantiate the CMS_Scripts object
 * unless script names are explicitly passed. Does make use of already
 * instantiated $cms_scripts if present. Use provided cms_print_scripts hook to
 * register/enqueue new scripts.
 *
 * @since r16
 * @see CMS_Dependencies::print_scripts()
 */
function cms_print_scripts( $handles = false ) {
	do_action( 'cms_print_scripts' );
	if ( '' === $handles ) // for cms_head
		$handles = false;

	global $cms_scripts;
	if ( !is_a($cms_scripts, 'CMS_Scripts') ) {
		if ( !$handles )
			return array(); // No need to instantiate if nothing's there.
		else
			$cms_scripts = new CMS_Scripts();
	}

	return $cms_scripts->do_items( $handles );
}

/**
 * Register new JavaScript file.
 *
 * @since r16
 * @param string $handle Script name
 * @param string $src Script url
 * @param array $deps (optional) Array of script names on which this script depends
 * @param string|bool $ver (optional) Script version (used for cache busting), set to NULL to disable
 * @param bool (optional) Wether to enqueue the script before </head> or before </body>
 * @return null
 */
function cms_register_script( $handle, $src, $deps = array(), $ver = false, $in_footer = false ) {
	global $cms_scripts;
	if ( !is_a($cms_scripts, 'CMS_Scripts') )
		$cms_scripts = new CMS_Scripts();

	$cms_scripts->add( $handle, $src, $deps, $ver );
	if ( $in_footer )
		$cms_scripts->add_data( $handle, 'group', 1 );
}

/**
 * Localizes a script.
 *
 * Localizes only if script has already been added.
 *
 * @since r16
 * @see CMS_Scripts::localize()
 */
function cms_localize_script( $handle, $object_name, $l10n ) {
	global $cms_scripts;
	if ( !is_a($cms_scripts, 'CMS_Scripts') )
		return false;

	return $cms_scripts->localize( $handle, $object_name, $l10n );
}

/**
 * Remove a registered script.
 *
 * @since r16
 * @see CMS_Scripts::remove() For parameter information.
 */
function cms_deregister_script( $handle ) {
	global $cms_scripts;
	if ( !is_a($cms_scripts, 'CMS_Scripts') )
		$cms_scripts = new CMS_Scripts();

	$cms_scripts->remove( $handle );
}

/**
 * Enqueues script.
 *
 * Registers the script if src provided (does NOT overwrite) and enqueues.
 *
 * @since r16
 * @see cms_register_script() For parameter information.
 */
function cms_enqueue_script( $handle, $src = false, $deps = array(), $ver = false, $in_footer = false ) {
	global $cms_scripts;
	if ( !is_a($cms_scripts, 'CMS_Scripts') )
		$cms_scripts = new CMS_Scripts();

	if ( $src ) {
		$_handle = explode('?', $handle);
		$cms_scripts->add( $_handle[0], $src, $deps, $ver );
		if ( $in_footer )
			$cms_scripts->add_data( $_handle[0], 'group', 1 );
	}
	$cms_scripts->enqueue( $handle );
}

/**
 * Check whether script has been added to WordPress Scripts.
 *
 * The values for list defaults to 'queue', which is the same as enqueue for
 * scripts.
 *
 * @since WP unknown; BP unknown
 *
 * @param string $handle Handle used to add script.
 * @param string $list Optional, defaults to 'queue'. Others values are 'registered', 'queue', 'done', 'to_do'
 * @return bool
 */
function cms_script_is( $handle, $list = 'queue' ) {
	global $cms_scripts;
	if ( !is_a($cms_scripts, 'CMS_Scripts') )
		$cms_scripts = new CMS_Scripts();

	$query = $cms_scripts->query( $handle, $list );

	if ( is_object( $query ) )
		return true;

	return $query;
}
