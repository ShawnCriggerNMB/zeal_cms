<?php
/**
 * Taxonomy API
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage Taxonomy
 * @category Helper\Functions
 * @since  3.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */

// Last sync [WP11537]

/**
 * Taxonomy based off of WordPress revision 8782.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage Taxonomy
 * @category Helpers
 * @since  3.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */
function get_object_taxonomies($object_type) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->get_object_taxonomies($object_type);
}

function get_taxonomy( $taxonomy ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->get_taxonomy( $taxonomy );
}

function is_taxonomy( $taxonomy ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->is_taxonomy( $taxonomy );
}

function is_taxonomy_hierarchical($taxonomy) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->is_taxonomy_hierarchical($taxonomy);
}

function register_taxonomy( $taxonomy, $object_type, $args = array() ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->register_taxonomy( $taxonomy, $object_type, $args );
}

function get_objects_in_term( $terms, $taxonomies, $args = array() ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->get_objects_in_term( $terms, $taxonomies, $args );
}

function &get_term($term, $taxonomy, $output = OBJECT, $filter = 'raw') {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->get_term($term, $taxonomy, $output, $filter);
}

function get_term_by($field, $value, $taxonomy, $output = OBJECT, $filter = 'raw') {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->get_term_by($field, $value, $taxonomy, $output, $filter);
}

function get_term_children( $term, $taxonomy ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->get_term_children( $term, $taxonomy );
}

function get_term_field( $field, $term, $taxonomy, $context = 'display' ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->get_term_field( $field, $term, $taxonomy, $context );
}

function get_term_to_edit( $id, $taxonomy ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->get_term_to_edit( $id, $taxonomy );
}

function &get_terms($taxonomies, $args = '') {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->get_terms($taxonomies, $args);
}

function is_term($term, $taxonomy = '') {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->is_term($term, $taxonomy );
}

function sanitize_term($term, $taxonomy, $context = 'display') {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->sanitize_term($term, $taxonomy, $context );
}

function sanitize_term_field($field, $value, $term_id, $taxonomy, $context) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->sanitize_term_field($field, $value, $term_id, $taxonomy, $context);
}

function cms_count_terms( $taxonomy, $args = array() ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->count_terms( $taxonomy, $args );
}

function cms_delete_object_term_relationships( $object_id, $taxonomies ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->delete_object_term_relationships( $object_id, $taxonomies );
}

function cms_delete_term( $term, $taxonomy, $args = array() ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->delete_term( $term, $taxonomy, $args );
}

function cms_get_object_terms($object_ids, $taxonomies, $args = array()) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->get_object_terms($object_ids, $taxonomies, $args );
}

function cms_insert_term( $term, $taxonomy, $args = array() ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->insert_term( $term, $taxonomy, $args );
}

function cms_set_object_terms($object_id, $terms, $taxonomy, $append = false) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->set_object_terms($object_id, $terms, $taxonomy, $append);
}

function cms_unique_term_slug($slug, $term) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->unique_term_slug($slug, $term);
}

function cms_update_term( $term, $taxonomy, $args = array() ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->update_term( $term, $taxonomy, $args );
}

function cms_defer_term_counting($defer=NULL) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->defer_term_counting($defer);
}

function cms_update_term_count( $terms, $taxonomy, $do_deferred=false ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->update_term_count( $terms, $taxonomy, $do_deferred );
}

function cms_update_term_count_now( $terms, $taxonomy ) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->update_term_count_now( $terms, $taxonomy );
}

function clean_object_term_cache($object_ids, $object_type) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->clean_object_term_cache($object_ids, $object_type);
}

function clean_term_cache($ids, $taxonomy = '') {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->clean_term_cache($ids, $taxonomy);
}

function &get_object_term_cache($id, $taxonomy) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->get_object_term_cache($id, $taxonomy);
}

function update_object_term_cache($object_ids, $object_type) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->update_object_term_cache($object_ids, $object_type);
}

function update_term_cache($terms, $taxonomy = '') {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->update_term_cache($terms, $taxonomy);
}

function _get_term_hierarchy($taxonomy) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->_get_term_hierarchy($taxonomy);
}

function &_get_term_children($term_id, $terms, $taxonomy) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->_get_term_children($term_id, $terms, $taxonomy);
}

function _pad_term_counts(&$terms, $taxonomy) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->_pad_term_counts($terms, $taxonomy);
}

function is_object_in_term($object_id, $taxonomy, $terms = null) {
	global $cms_taxonomy_object;
	return $cms_taxonomy_object->is_object_in_term($object_id, $taxonomy, $terms);
}
