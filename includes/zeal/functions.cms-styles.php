<?php
/**
 * Zeal Tech CMS styles procedural API.
 *
 * @package Zeal Tech CMS
 * @since r79
 */

/**
 * Display styles that are in the queue or part of $handles.
 *
 * @since r79
 * @uses do_action() Calls 'cms_print_styles' hook.
 * @global object $cms_styles The CMS_Styles object for printing styles.
 *
 * @param array|bool $handles Styles to be printed. An empty array prints the queue,
 *  an array with one string prints that style, and an array of strings prints those styles.
 * @return bool True on success, false on failure.
 */
function cms_print_styles( $handles = false ) {
	do_action( 'cms_print_styles' );
	if ( '' === $handles ) // for cms_head
		$handles = false;

	global $cms_styles;
	if ( !is_a($cms_styles, 'CMS_Styles') ) {
		echo 'is_a failed<br>';dump($handles);
		if ( !$handles )
		{
			echo 'return empty array';
			return array(); // No need to instantiate if nothing's there.
		}
		else
			$cms_styles = new CMS_Styles();
	}

	return $cms_styles->do_items( $handles );
}

/**
 * Register CSS style file.
 *
 * @since r79
 * @see CMS_Styles::add() For additional information.
 * @global object $cms_styles The CMS_Styles object for printing styles.
 * @link http://www.w3.org/TR/CSS2/media.html#media-types List of CSS media types.
 *
 * @param string $handle Name of the stylesheet.
 * @param string|bool $src Path to the stylesheet from the root directory of WordPress. Example: '/css/mystyle.css'.
 * @param array $deps Array of handles of any stylesheet that this stylesheet depends on.
 *  (Stylesheets that must be loaded before this stylesheet.) Pass an empty array if there are no dependencies.
 * @param string|bool $ver String specifying the stylesheet version number. Set to NULL to disable.
 *  Used to ensure that the correct version is sent to the client regardless of caching.
 * @param string $media The media for which this stylesheet has been defined.
 */
function cms_register_style( $handle, $src, $deps = array(), $ver = false, $media = 'all' ) {
	global $cms_styles;
	if ( !is_a($cms_styles, 'CMS_Styles') )
		$cms_styles = new CMS_Styles();

	$cms_styles->add( $handle, $src, $deps, $ver, $media );
}

/**
 * Remove a registered CSS file.
 *
 * @since r79
 * @see CMS_Styles::remove() For additional information.
 * @global object $cms_styles The CMS_Styles object for printing styles.
 *
 * @param string $handle Name of the stylesheet.
 */
function cms_deregister_style( $handle ) {
	global $cms_styles;
	if ( !is_a($cms_styles, 'CMS_Styles') )
		$cms_styles = new CMS_Styles();

	$cms_styles->remove( $handle );
}

/**
 * Enqueue a CSS style file.
 *
 * Registers the style if src provided (does NOT overwrite) and enqueues.
 *
 * @since r79
 * @see CMS_Styles::add(), CMS_Styles::enqueue()
 * @global object $cms_styles The CMS_Styles object for printing styles.
 * @link http://www.w3.org/TR/CSS2/media.html#media-types List of CSS media types.
 *
 * @param string $handle Name of the stylesheet.
 * @param string|bool $src Path to the stylesheet from the root directory of WordPress. Example: '/css/mystyle.css'.
 * @param array $deps Array of handles (names) of any stylesheet that this stylesheet depends on.
 *  (Stylesheets that must be loaded before this stylesheet.) Pass an empty array if there are no dependencies.
 * @param string|bool $ver String specifying the stylesheet version number, if it has one. This parameter
 *  is used to ensure that the correct version is sent to the client regardless of caching, and so should be included
 *  if a version number is available and makes sense for the stylesheet.
 * @param string $media The media for which this stylesheet has been defined.
 */
function cms_enqueue_style( $handle, $src = false, $deps = array(), $ver = false, $media = false ) {
	global $cms_styles;
	if ( !is_a($cms_styles, 'CMS_Styles') )
		$cms_styles = new CMS_Styles();

	if ( $src ) {
		$_handle = explode('?', $handle);
		$cms_styles->add( $_handle[0], $src, $deps, $ver, $media );
	}
	$cms_styles->enqueue( $handle );
}

/**
 * Check whether style has been added to WordPress Styles.
 *
 * The values for list defaults to 'queue', which is the same as cms_enqueue_style().
 *
 * @since WP unknown; BP unknown
 * @global object $cms_styles The CMS_Styles object for printing styles.
 *
 * @param string $handle Name of the stylesheet.
 * @param string $list Values are 'registered', 'done', 'queue' and 'to_do'.
 * @return bool True on success, false on failure.
 */
function cms_style_is( $handle, $list = 'queue' ) {
	global $cms_styles;
	if ( !is_a($cms_styles, 'CMS_Styles') )
		$cms_styles = new CMS_Styles();

	$query = $cms_styles->query( $handle, $list );

	if ( is_object( $query ) )
		return true;

	return $query;
}

