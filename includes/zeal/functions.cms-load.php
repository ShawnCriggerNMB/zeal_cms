<?php
/**
 * These functions are needed to load Zeal Tech CMS, and are used to properly setup the server enviroment.
 *
 * @package Zeal Tech CMS
 */



if ( ! function_exists( 'cms_unregister_GLOBALS' ) ) :
/**
 * Turn register globals off.
 *
 * @access private
 * @since 2.1.0
 * @return null Will return null if register_globals PHP directive was disabled
 */
function cms_unregister_GLOBALS()
{
	if ( !ini_get( 'register_globals' ) ) return;

	if ( isset( $_REQUEST['GLOBALS'] ) ) die( 'GLOBALS overwrite attempt detected' );

	// Variables that shouldn't be unset
	$no_unset = array( 'GLOBALS', '_GET', '_POST', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES', 'table_prefix' );
	$input = array_merge( $_GET, $_POST, $_COOKIE, $_SERVER, $_ENV, $_FILES, isset( $_SESSION ) && is_array( $_SESSION ) ? $_SESSION : array() );
	foreach ( $input as $k => $v )
	{
		if ( !in_array( $k, $no_unset ) && isset( $GLOBALS[$k] ) ) {
			unset( $GLOBALS[$k] );
		}
	}

}

endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'cms_fix_server_vars' ) ) :
/**
 * Fix $_SERVER variables for various setups.
 *
 * @access private
 * @since 3.0.0
 */
function cms_fix_server_vars()
{
	global $PHP_SELF;

	$default_server_values = array(
		'SERVER_SOFTWARE' => '',
		'REQUEST_URI' => '',
	);

	$_SERVER = array_merge( $default_server_values, $_SERVER );

	// Fix for IIS when running with PHP ISAPI
	if ( empty( $_SERVER['REQUEST_URI'] ) || ( php_sapi_name() != 'cgi-fcgi' && preg_match( '/^Microsoft-IIS\//', $_SERVER['SERVER_SOFTWARE'] ) ) ) {

		// IIS Mod-Rewrite
		if ( isset( $_SERVER['HTTP_X_ORIGINAL_URL'] ) ) {
			$_SERVER['REQUEST_URI'] = $_SERVER['HTTP_X_ORIGINAL_URL'];
		}
		// IIS Isapi_Rewrite
		else if ( isset( $_SERVER['HTTP_X_REWRITE_URL'] ) ) {
			$_SERVER['REQUEST_URI'] = $_SERVER['HTTP_X_REWRITE_URL'];
		} else {
			// Use ORIG_PATH_INFO if there is no PATH_INFO
			if ( !isset( $_SERVER['PATH_INFO'] ) && isset( $_SERVER['ORIG_PATH_INFO'] ) )
				$_SERVER['PATH_INFO'] = $_SERVER['ORIG_PATH_INFO'];

			// Some IIS + PHP configurations puts the script-name in the path-info (No need to append it twice)
			if ( isset( $_SERVER['PATH_INFO'] ) ) {
				if ( $_SERVER['PATH_INFO'] == $_SERVER['SCRIPT_NAME'] )
					$_SERVER['REQUEST_URI'] = $_SERVER['PATH_INFO'];
				else
					$_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'] . $_SERVER['PATH_INFO'];
			}

			// Append the query string if it exists and isn't null
			if ( ! empty( $_SERVER['QUERY_STRING'] ) ) {
				$_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
			}
		}
	}

	// Fix for PHP as CGI hosts that set SCRIPT_FILENAME to something ending in php.cgi for all requests
	if ( isset( $_SERVER['SCRIPT_FILENAME'] ) && ( strpos( $_SERVER['SCRIPT_FILENAME'], 'php.cgi' ) == strlen( $_SERVER['SCRIPT_FILENAME'] ) - 7 ) )
		$_SERVER['SCRIPT_FILENAME'] = $_SERVER['PATH_TRANSLATED'];

	// Fix for Dreamhost and other PHP as CGI hosts
	if ( strpos( $_SERVER['SCRIPT_NAME'], 'php.cgi' ) !== false )
		unset( $_SERVER['PATH_INFO'] );

	// Fix empty PHP_SELF
	$PHP_SELF = $_SERVER['PHP_SELF'];
	if ( empty( $PHP_SELF ) )
		$_SERVER['PHP_SELF'] = $PHP_SELF = preg_replace( '/(\?.*)?$/', '', $_SERVER["REQUEST_URI"] );
}

endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'cms_check_php_mysql_versions' ) ) :
	/**
	 * Check for the required PHP version, and the MySQL extension or a database drop-in.
	 *
	 * Dies if requirements are not met.
	 *
	 * @access private
	 * @since 3.0.0
	 */
	function cms_check_php_mysql_versions() {
		global $required_php_version, $wp_version;
		$php_version = phpversion();
		if ( version_compare( $required_php_version, $php_version, '>' ) ) {
			header( 'Content-Type: text/html; charset=utf-8' );
			die( sprintf( __( 'Your server is running PHP version %1$s but Zeal Tech CMS %2$s requires at least %3$s.' ), $php_version, $wp_version, $required_php_version ) );
		}

		if ( ! extension_loaded( 'mysql' ) && ! extension_loaded( 'mysqli' ) && ! file_exists( WP_CONTENT_DIR . '/db.php' ) ) {
			 header( 'Content-Type: text/html; charset=utf-8' );
			die( __( 'Your PHP installation appears to be missing the MySQL extension which is required by WordPress.' ) );
		}
	}

endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'cms_favicon_request') ) :
/**
 * Don't load all of WordPress when handling a favicon.ico request.
 * Instead, send the headers for a zero-length favicon and bail.
 *
 * @since 3.0.0
 */
function cms_favicon_request() {
	if ( '/favicon.ico' == $_SERVER['REQUEST_URI'] ) {
		header('Content-Type: image/vnd.microsoft.icon');
		header('Content-Length: 0');
		exit;
	}
}
endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'cms_maintenance' ) ) :
/**
 * Dies with a maintenance message when conditions are met.
 *
 * Checks for a file in the WordPress root directory named ".maintenance".
 * This file will contain the variable $upgrading, set to the time the file
 * was created. If the file was created less than 10 minutes ago, WordPress
 * enters maintenance mode and displays a message.
 *
 * The default message can be replaced by using a drop-in (maintenance.php in
 * the wp-content directory).
 *
 * @access private
 * @since 3.0.0
 */
function cms_maintenance() {
	if ( !file_exists( ABSPATH . '.maintenance' ) || defined( 'CMS_INSTALLING' ) )
		return;

	global $upgrading;

	include( ABSPATH . '.maintenance' );
	// If the $upgrading timestamp is older than 10 minutes, don't die.
	if ( ( time() - $upgrading ) >= 600 )
		return;

	if ( file_exists( WP_CONTENT_DIR . '/maintenance.php' ) ) {
		require_once( WP_CONTENT_DIR . '/maintenance.php' );
		die();
	}

	$protocol = $_SERVER["SERVER_PROTOCOL"];
	if ( 'HTTP/1.1' != $protocol && 'HTTP/1.0' != $protocol )
		$protocol = 'HTTP/1.0';
	header( "$protocol 503 Service Unavailable", true, 503 );
	header( 'Content-Type: text/html; charset=utf-8' );
	header( 'Retry-After: 600' );
?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml"<?php if ( is_rtl() ) echo ' dir="rtl"'; ?>>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php _e( 'Maintenance' ); ?></title>

	</head>
	<body>
		<h1><?php _e( 'Briefly unavailable for scheduled maintenance. Check back in a minute.' ); ?></h1>
	</body>
	</html>
<?php
	die();
}
endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'timer_start' ) ) :
	/**
	 * PHP 5 standard microtime start capture.
	 *
	 * @access private
	 * @since 0.71
	 * @global float $timestart Seconds from when function is called.
	 * @return bool Always returns true.
	 */
	function timer_start() {
		global $timestart;
		$timestart = microtime( true );
		return true;
	}
endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'timer_stop' ) ) :
/**
 * Retrieve or display the time from the page start to when function is called.
 *
 * @since 0.71
 *
 * @global float $timestart Seconds from when timer_start() is called.
 * @global float $timeend   Seconds from when function is called.
 *
 * @param int $display   Whether to echo or return the results. Accepts 0|false for return,
 *                       1|true for echo. Default 0|false.
 * @param int $precision The number of digits from the right of the decimal to display.
 *                       Default 3.
 * @return string The "second.microsecond" finished time calculation. The number is formatted
 *                for human consumption, both localized and rounded.
 */
function timer_stop( $display = 0, $precision = 3 ) {
	global $timestart, $timeend;
	$timeend = microtime( true );
	$timetotal = $timeend - $timestart;
	$r = ( function_exists( 'number_format_i18n' ) ) ? number_format_i18n( $timetotal, $precision ) : number_format( $timetotal, $precision );
	if ( $display )
		echo $r;
	return $r;
}
endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'cms_debug_mode' ) ) :

/**
 * Sets PHP error handling and handles WordPress debug mode.
 *
 * Uses three constants: WP_DEBUG, WP_DEBUG_DISPLAY, and WP_DEBUG_LOG. All three can be
 * defined in wp-config.php. Example: <code> define( 'WP_DEBUG', true ); </code>
 *
 * WP_DEBUG_DISPLAY and WP_DEBUG_LOG perform no function unless WP_DEBUG is true.
 * WP_DEBUG defaults to false.
 *
 * When WP_DEBUG is true, all PHP notices are reported. WordPress will also display
 * notices, including one when a deprecated WordPress function, function argument,
 * or file is used. Deprecated code may be removed from a later version.
 *
 * It is strongly recommended that plugin and theme developers use WP_DEBUG in their
 * development environments.
 *
 * When WP_DEBUG_DISPLAY is true, WordPress will force errors to be displayed.
 * WP_DEBUG_DISPLAY defaults to true. Defining it as null prevents WordPress from
 * changing the global configuration setting. Defining WP_DEBUG_DISPLAY as false
 * will force errors to be hidden.
 *
 * When WP_DEBUG_LOG is true, errors will be logged to wp-content/debug.log.
 * WP_DEBUG_LOG defaults to false.
 *
 * Errors are never displayed for XML-RPC requests.
 *
 * @access private
 * @since 3.0.0
 */
function cms_debug_mode() {
	if ( CMS_DEBUG )
	{
		error_reporting( E_ALL );

		if ( CMS_DEBUG_DISPLAY )
			ini_set( 'display_errors', 1 );
		elseif ( null !== CMS_DEBUG_DISPLAY )
			ini_set( 'display_errors', 0 );

		if ( CMS_DEBUG_LOG )
		{
			ini_set( 'log_errors', 1 );
			ini_set( 'error_log', CMS_CONTENT_DIR . '/debug.log' );
		}
	} else {
		error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | E_RECOVERABLE_ERROR );
	}
	if ( defined( 'XMLRPC_REQUEST' ) )
		ini_set( 'display_errors', 0 );
}
endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'cms_using_ext_object_cache' ) ) :
/**
 * Access/Modify private global variable $_wp_using_ext_object_cache
 *
 * Toggle $_wp_using_ext_object_cache on and off without directly touching global
 *
 * @since 3.7.0
 *
 * @param bool $using Whether external object cache is being used
 * @return bool The current 'using' setting
 */
function cms_using_ext_object_cache( $using = null ) {
	global $_wp_using_ext_object_cache;
	$current_using = $_wp_using_ext_object_cache;
	if ( null !== $using )
		$_wp_using_ext_object_cache = $using;
	return $current_using;
}
endif;

// ------------------------------------------------------------------------


if ( ! function_exists( 'cms_set_internal_encoding' ) ) :
/**
 * Sets internal encoding using mb_internal_encoding().
 *
 * In most cases the default internal encoding is latin1, which is of no use,
 * since we want to use the mb_ functions for utf-8 strings.
 *
 * @access private
 * @since 3.0.0
 */
function cms_set_internal_encoding() {

	if ( ! function_exists( 'mb_internal_encoding' ) )  return ;

	$charset = get_option( 'blog_charset' );
	if ( ! $charset || ! @mb_internal_encoding( $charset ) )
	{
		mb_internal_encoding( 'UTF-8' );
	}

}

endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'cms_magic_quotes') ) :
	/**
	 * Add magic quotes to $_GET, $_POST, $_COOKIE, and $_SERVER.
	 *
	 * Also forces $_REQUEST to be $_GET + $_POST. If $_SERVER, $_COOKIE,
	 * or $_ENV are needed, use those superglobals directly.
	 *
	 * @access private
	 * @since 3.0.0
	 */
	function cms_magic_quotes() {
		// If already slashed, strip.
		if ( get_magic_quotes_gpc() ) {
			$_GET    = stripslashes_deep( $_GET    );
			$_POST   = stripslashes_deep( $_POST   );
			$_COOKIE = stripslashes_deep( $_COOKIE );
		}

		// Escape with wpdb.
		$_GET    = add_magic_quotes( $_GET    );
		$_POST   = add_magic_quotes( $_POST   );
		$_COOKIE = add_magic_quotes( $_COOKIE );
		$_SERVER = add_magic_quotes( $_SERVER );

		// Force REQUEST to be GET + POST.
		$_REQUEST = array_merge( $_GET, $_POST );
	}
endif;

// ------------------------------------------------------------------------

/**
 * Runs just before PHP shuts down execution.
 *
 * @access private
 * @since 1.2.0
 */
function shutdown_action_hook() {
	/**
	 * Fires just before PHP shuts down execution.
	 *
	 * @since 1.2.0
	 */
	do_action( 'shutdown' );
	cms_cache_close();
}

// ------------------------------------------------------------------------

if ( ! function_exists( 'cms_clone') ) :
	/**
	 * Copy an object.
	 *
	 * @since 2.7.0
	 * @deprecated 3.2
	 *
	 * @param object $object The object to clone
	 * @return object The cloned object
	 */

	function cms_clone( $object ) {
		// Use parens for clone to accommodate PHP 4. See #17880
		return clone( $object );
	}
endif;

// ------------------------------------------------------------------------

/**
 * Retrieve the current blog id
 *
 * @since 3.1.0
 *
 * @return int Blog id
 */
function get_current_blog_id() {
	global $blog_id;
	return absint($blog_id);
}

// ------------------------------------------------------------------------

// ------------------------------------------------------------------------
/**
 * Wraps ticks around tocks and prefixs with db_prefix for db table.
 * @param  string $table table name
 * @return string
 */
function wrap_db_table( $table='' )
{
	return '`'.DB_PREFIX.$table.'`';
}

// ------------------------------------------------------------------------

/**
 * Returns site_dir, can be called from inside of function without having to global $sitedir, appends $path to $sitedir
 * @global $sitedir
 * @param  string $path optional path to append to end of site dir.
 * @return string
 */
function site_dir( $path = '' )
{
	$sitedir = cms_get_option( 'site_dir' );

	if ( ! empty ( $path ) ) {
		$sitedir = rtrim( $sitedir, '/' ) . '/';
		$path = trim( $path, '/' );
	} else {
		$sitedir = rtrim( $sitedir, '/' ) . '/';
	}
	return $sitedir . $path;
}


// ------------------------------------------------------------------------

/**
 * Returns site_url, can be called from inside of function without having to global $siteurl, appends $path to $siteurl
 * @global $siteurl
 * @param  string $path optional path to append to end of site url.
 * @return string
 */
function site_url( $path = '' )
{

	$siteurl = cms_get_option( 'site_url' );

	if ( ! empty ( $path ) ) {
		$siteurl = rtrim( $siteurl, '/' ) . '/';
		$path = trim( $path, '/' );
	} else {
		$siteurl = rtrim( $siteurl, '/' ) . '/';
	}
	return $siteurl .$path;
}

function admin_url( $path  = '' )
{
	$siteurl = cms_get_option( 'site_url' );

	if ( ! empty ( $path ) ) {
		$siteurl = rtrim( $siteurl, '/' ) . '/' . ADMIN_DIR . '/';
		$path = trim( $path, '/' );
	} else {
		$siteurl = rtrim( $siteurl, '/' ) . '/' . ADMIN_DIR . '/';
	}
	return $siteurl . $path;
}

