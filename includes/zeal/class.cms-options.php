<?php


require_once( ZEAL_PATH . '/interface.cms-options.php');

/* a stub options class that provides static default values.  to use it in a simple application, derive a child class named BP_Options:

include_once('class.bp-options.php');
class BP_Options extends BP_Options_Stub {
}

*/


class CMS_Options_Stub implements CMS_Options_Interface {

	protected static $_defaults = array(
		'application_id'     => 1,
		'aplication_uri'     => '',
		'cron_uri'           => '',
		'cron_check'         => '',
		'wp_http_version'    => '1.0',
		'hash_function_name' => 'md5',
		'language_locale'    => '',
		'language_directory' => '',
		'charset'            => 'UTF-8',
		'gmt_offset'         => 0,
		'timezone_string'    => 'GMT',
	);

	public static function init()
	{
/*
		global $db;
		$config_table = 'cms_config';
		//$config = $wpdb->get_row("SELECT * FROM {$wpdb->config} WHERE id=1", ARRAY_A);
		$config = $db->prepare("SELECT * FROM {$config_table} WHERE id=1");
		$config->execute();
		$config = $config->fetch( PDO::FETCH_ASSOC );
		self::$_defaults = array_merge( self::$_defaults, $config );
*/
		global $wpdb;
		$results = $wpdb->get_results("SELECT option_name, option_value FROM {$wpdb->options} WHERE `autoload` = 'yes' ", ARRAY_A);

		foreach ($results as $config) {
			$name  = $config['option_name'];
			$value = $config['option_value'];
			self::$_defaults[$name] = $value;

			//exit();
		}
		//self::$_defaults = array_merge( self::$_defaults, $config );
	}

	public static function prefix() {
		return '';
	}

	public static function get( $o ) {
		if ( array_key_exists( $o, self::$_defaults ) )
			return self::$_defaults[ $o ];
		return false;
	}

	public static function add( $o, $v )
	{
		global $wpdb;
		if ( !array_key_exists( $o, self::$_defaults ) )
		{
			self::$_defaults[ $o ] = $v;
			$wpdb->insert( $wpdb->options, array( 'option_name' => $o, 'option_value' => $key ) );
		}
	}

	public static function update( $o, $v ) {
		global $wpdb;
		self::$_defaults[ $o ] = $v;
		$wpdb->update( $wpdb->options, array( 'option_value' => $v ), array( 'option_name' => $o ) );
	}

	public static function delete( $o ) {
		global $wpdb;
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->options} WHERE `option_name` = %s", $o ) );
		unset( self::$_defaults[ $o ] );
	}
}

// ------------------------------------------------------------------------

class CMS_Options extends CMS_Options_Stub {

	public static function init()
	{
		parent::init();
	}

	public static function get_all()
	{
		return self::$_defaults;
	}

	public static function test()
	{
		echo var_dump( parent::$_defaults );
	}
}


CMS_Options::init();

//CMS_Options::test(); die();
