<?php
/**
 * The ZealTech version string
 *
 * @global string $cms_version
 */
$cms_version = '3.0.0';

/**
 * Holds the WordPress DB revision, increments when changes are made to the WordPress DB schema.
 *
 * @global int $cms_db_version
 */
$cms_db_version = 1;

/**
 * Holds the required PHP version
 *
 * @global string $required_php_version
 */
$required_php_version = '5.3.0';

/**
 * Holds the required MySQL version
 *
 * @global string $required_mysql_version
 */
$required_mysql_version = '5.0';
