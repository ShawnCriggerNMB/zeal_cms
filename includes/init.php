<?php if ( !defined('BASE') ) die('No Direct Script Access');

// default roles
create_role( 'superadmin', 'Super-Administrator', array(
	'update' => TRUE,
	'create' => TRUE,
	'delete' => TRUE,
	'list'   => TRUE,
) );
create_role( 'administrator', 'Administrator', array(
	'update' => TRUE,
	'create' => TRUE,
	'delete' => TRUE,
	'list'   => TRUE,
) );
create_role( 'moderator', 'Moderator', array(
	'update' => TRUE,
	'create' => TRUE,
	'delete' => TRUE,
	'list'   => TRUE,
) );
create_role( 'user', 'User' );

if ( !defined('AUTH_COOKIE') ) {
	define('AUTH_COOKIE', 'sh');
}
if ( !defined('LOGGED_IN_COOKIE') ){
	define('LOGGED_IN_COOKIE', 'sh_logged_in');
}
if ( !defined('COOKIEPATH') ){
	define('COOKIEPATH', '/' );
}
if ( !defined('COOKIE_DOMAIN') ){
	define('COOKIE_DOMAIN', false);

//	$_site_url = ( ! function_exists( 'site_url' ) ) ? $site_url : site_url();
//	define('COOKIE_DOMAIN', preg_replace('|https?://[^/]+|i', '', $_site_url . '/' ) );
}
if ( !defined('SP_VERSION') ) {
	define('SP_VERSION', '1.0.2');
}

if ( !defined( 'WP_AUTH_COOKIE_VERSION' ) ) {
	define( 'WP_AUTH_COOKIE_VERSION', 1 ); // change to 2 for wp 2.8
}




$GLOBALS['cms_users_object'] = new CMS_Users( $ndb );

$cookies['auth'][] = array(
	'domain' => COOKIE_DOMAIN,
	'path' => COOKIEPATH,
	'name' => AUTH_COOKIE
);
$cookies['logged_in'][] = array(
	'domain' => COOKIE_DOMAIN,
	'path' => COOKIEPATH,
	'name' => LOGGED_IN_COOKIE
);


$GLOBALS['cms_auth_object'] = new CMS_Auth( $wpdb, $GLOBALS['cms_users_object'], $cookies );



$action = @$_REQUEST['action'];

if ( 'login' === $action OR 'logout' === $action ) :
	$error = '';
	switch($action) {

		case 'logout':

			$GLOBALS['cms_auth_object']->clear_auth_cookie();
			nocache_headers();

			$redirect_to = $site_url . 'index.php';
			if ( isset($_REQUEST['redirect_to']) )
				$redirect_to = preg_replace('|[^a-z0-9-~+_.?#=&;,/:]|i', '', $_REQUEST['redirect_to']);

			cms_redirect($redirect_to);
			exit();

		break;

		case 'login' :
		default:

			$user_login = '';
			$user_pass = '';
			$using_cookie = false;
			$redirect_to = ( !isset( $_REQUEST['redirect_to'] ) ) ? site_url() : $_REQUEST['redirect_to'];
			$redirect_to = preg_replace('|[^a-z0-9-~+_.?#=&;,/:]|i', '', $redirect_to);

			if ( $_POST ) {
				$user_login = $_POST['username'];
				$user_login = sanitize_user( $user_login );
				$user_pass  = $_POST['password'];
				$rememberme = @$_POST['rememberme'];
				$rememberme = true;
			} elseif ( !empty($_COOKIE) ) {
				if ( !empty($_COOKIE[AUTH_COOKIE]) ) $using_cookie = true;
			}

			if ( $e = cms_login($user_login, $user_pass) ) {

				$user = new CMS_User( $user_login );

				$ip = $_SERVER['REMOTE_ADDR'];
				$GLOBALS['cms_auth_object']->set_auth_cookie( $user->ID, $rememberme ? time() + 1209600 : 0, $rememberme ? time() + 2209600 : 0, 'auth' );
				$GLOBALS['cms_auth_object']->set_auth_cookie( $user->ID, $rememberme ? time() + 1209600 : 0, $rememberme ? time() + 2209600 : 0, 'logged_in' );
				update_usermeta( $user->ID, 'last_login_ts', time() );
				update_usermeta( $user->ID, 'last_login_ip', $ip );

				cms_redirect($redirect_to);
				exit;
			} else {

				//echo var_dump($user_login, $user_pass, wp_login($user_login, $user_pass) );
				cms_die( $error, 'Error Logging in');
			}

			if ( $using_cookie )
			{
				$error = __('Your session has expired.');
			}

	}

endif;

$current_user = $GLOBALS['cms_auth_object']->get_current_user();


//				die();

//if ( is_object( $current_user ) ) {
/*
if ( ! $current_user )
{
	$path = $_SERVER['REQUEST_URI'];
	cms_redirect("$site_url/signup.html?redirect_to=" . urlencode($site_domain . $path ) );
	exit();

}
*/
/*
if ( !@constant( 'SP_IS_LOGIN' ) && ( !$current_user || (!$current_user->has_cap( 'moderator' ) && !$current_user->has_cap('administrator')) ) ) {
	$path = $_SERVER['REQUEST_URI'];
	cms_redirect("$site_url/login.php?redirect_to=" . urlencode($site_domain . $path ) );
	exit();
}

if ( !is_ssl() && substr($site_url, 0, 5) == 'https' ) {
	cms_redirect( $site_url );
	exit;
}
*/

global $screen;
$screen = new stdClass();


// It's safe to show errors now that we're logged in
$ndb->show_errors();

//add_action( 'init', 'set_page_uri' );



/**
 * Does any actions once CMS is loaded and inited
 */
do_action( 'init' );




