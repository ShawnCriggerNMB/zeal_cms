<?php
/**
 * Core functions for Zeal Technologies CMS Admin area only.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage config
 * @category modules
 * @since  1.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */

if ( !defined('BASE') ) die('No Direct Script Access');

// ------------------------------------------------------------------------

class Config_Admin extends Module_Base {


	protected $_human_field_names = array(
		'site_name'    => 'Website Name',
		'site_url'     => 'Website URL',
		'site_dir'     => 'Website file path',
		'site_email'   => 'Website email address',
	);

	protected $_rules = array(
		'site_name'    => 'required|max_len,72|min_len,3',
		'site_url'     => 'required|valid_url',
		'site_dir'     => 'required|valid_path',
		'site_email'   => 'required|valid_email',
	);

	protected $_fields = array();

	// ------------------------------------------------------------------------

	public function __construct( $pdo )
	{
		global $current_user;

		if ( ! $current_user->has_cap( 'superadmin' ) )
		{
			unset( $this->_rules['site_url'], $this->_rules['site_dir'] );
		}

		parent::__construct( $pdo );

		// this is where you can set form validation rules.
		$this->_rules = array();

//		$data = CMS_Options::get_all();
//dump( cms_get_option('site_name') );die();
		$this->_fields = array(
			'site_name' => array(
				'name'     => 'site_name',
				'value'    => cms_get_option( 'site_name' ),
				'label'    => 'Website Name',
				'help_text' => 'The exact name as you want it displayed on your website.',
				//			'required' => $this->required,
			),
			'site_url' => array(
				'name'     => 'site_url',
				'value'    => cms_get_option('site_url' ),
				'label'    => 'Website URL',
				'help_text' => 'Absolute URL to the CMS.',
			),
			'site_dir' => array(
				'name'     => 'site_dir',
				'value'    => cms_get_option( 'site_dir' ),
				'label'    => 'File system path',
				'help_text' => 'Absolute file system path to the CMS.',
			),
			'site_email' => array(
				'name'     => 'site_email',
				'type'     => 'email',
				'value'    => cms_get_option( 'site_email' ),
				'label'    => 'Website Email Address',
				'help_text' => 'Email address that will be used as the from address in emails.',
			),
			'live' => array(
				'name'     => 'live',
				'value'    => cms_get_option( 'live' ),
				'label'    => 'Make Site Live',
//				'required' => required(),
				'fields'   => array(
					array (
						'key' => 0,
						'value'=>'No&nbsp;'
					),
					array (
						'key'  => 1,
						'value'=>'Yes'
					)
				),
				'help_text' => 'Do not change this unless told to do so. \'No\' will cause site to be unviewable.'
			),
			'submit' => array(
				'name'     => 'submit',
				'value'    => 'Save',
			),

		);
		View::set_tags(
			array(
				'fields'      => $this->_fields,
				'form_errors' => $this->_errors,
			)
		);

	}

	// ------------------------------------------------------------------------

	public function handle_datatable()
	{
		return $this->handle_form();
		//View::show( 'admin_datatable' );
		//return $this->handle_form();
	}


	// ------------------------------------------------------------------------


	public function handle_form()
	{
		View::show( 'admin_form' );
	}

	// ------------------------------------------------------------------------


	public function handle_delete()
	{
		return $this->handle_form();
	}


	// ------------------------------------------------------------------------


	public function handle_submit()
	{

		if ( isset( $_POST['site_url'] ) )
		{
			cms_update_option( 'site_url', $_POST['site_url'] );
		}
		if ( isset( $_POST['site_dir'] ) )
		{
			cms_update_option( 'site_dir', $_POST['site_dir'] );
		}

		cms_update_option( 'site_name',  $_POST['site_name'] );
		cms_update_option( 'site_email', $_POST['site_email'] );
		cms_update_option( 'live', (int) $_POST['live'] );

		$_REQUEST['action'] = 'success';

		$msg = <<<EOL
			Your website config has been successfully updated.<br>
EOL;
		Notify::success( $msg );

	}

	// ------------------------------------------------------------------------


	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------

}


add_filter( 'module_header_title', 'change_title' );
add_filter( 'module_header_subtitle', 'change_subtitle' );

function change_title( $default='' )
{
return 'Website Config';
}


function change_subtitle( $default='' )
{
return 'A subtitle can go here';
}


//$modules['config'] = new Config_Admin( $GLOBALS['wpdb'] );

//$modules['config']->handle_form();
