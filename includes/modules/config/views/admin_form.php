<?php
if ( isset( $form_errors ) && ! is_null( $form_errors ) ) :

	Notify::error( $form_errors );

endif;
?>


<form name="form" class="form-horizontal" method="post" action="#">
<!--
	<input type="hidden" id="site_base_dir" name="site_base_dir" value="<?= $config['site_base_dir']?>" />
	<input type="hidden" id="site_base_url" name="site_base_url" value="<?= $config['site_base_url']?>" />
-->

<?php

	Form::input( $fields['site_name'] );

if ( $current_user->has_cap( 'superadmin' ) ) :
	Form::input( $fields['site_url'] );
	Form::input( $fields['site_dir'] );
endif;


	Form::input( $fields['site_email'] );
	Form::select( $fields['live'] );

	Form::submit ( $fields['submit'] );
?>
</form>
<?php
/*


<tr>
<td><label for="site_name"><?= $r ?>Website Name</label></td>
<td><input <?= $val_site_name?> type="text" name="site_name" id="site_name" value="<?=stripslashes ( $row['site_name'] ) ?>" size="45" /></td>
</tr>

<tr>
<td><label for="c_name">Contact Name</label></td>
<td><input <?= $val_c_name?> type="text" name="c_name" id="c_name" value="<?=stripslashes ( $row['c_name'] )?>" size="45" /></td>
			</tr>

<tr>
	<td><label for="c_name_type">Contact Type</label></td>
	<td><input <?php echo $val_c_name_type; ?> type="text" name="c_name_type" id="c_name_type" value="<?= stripslashes ( $row['c_name_type'] )  ?>" size="45" /></td>
</tr>

<tr>
	<td><label for="c_name_show">Show Contact Name on contact page</label></td>
	<td>
<?php echo create_slist ( $list, 'c_name_show', $row['c_name_show'], 1 ); ?>
	</td>
</tr>

<tr>
	<td><label for="c_email"><?= $r ?>Contact Email</label></td>
	<td><input <?= $val_c_email?> type="text" name="c_email" id="c_email"
		value="<?= stripslashes ( $row['c_email'] )  ?>" size="45" />
<?php echo tooltip('This is the main contact email address. It will be displayed in your contact info.') ?>
	</td>
</tr>

			<tr>
				<td><label for="c_email_show">Show Email on contact page</label></td>
				<td>
<?php echo create_slist ( $list, 'c_email_show', $row['c_email_show'], 1 ); ?>
				</td>
			</tr>

			<tr>
				<td><label for="form_email">Contact Form Email</label></td>
				<td><input <?= $val_form_email?> type="text" name="form_email" id="form_email" value="<?= stripslashes ( $row['form_email'] )  ?>" size="45" />
<?php echo tooltip('This is the email address your contact form will send to. It is never displayed.<br />You can add multiple email addresses by seperating them with a comma and a space.'); ?>
				</td>
			</tr>

			// PHONE START HERE
			<tr>
				<td><label for="phone_1">Phone #(1)</label></td>
				<td><input <?= $val_phone_1?> type="text" name="phone_1" id="phone_1"
					value="<?= stripslashes ( $row['phone_1'] )  ?>" size="45" /></td>
			</tr>

			<tr>
				<td><label for="phone_1_type">Phone Type</label></td>
				<td><input <?= $val_phone_1_type?> type="text" name="phone_1_type" id="phone_1_type"
					value="<?= stripslashes ( $row['phone_1_type'] )  ?>" size="45" />
<?php echo tooltip('Phone, Cel Phone, Toll Free, Fax, etc.'); ?>
				</td>
			</tr>

			<tr>
				<td><label for="phone_1_show">Show Phone #(1) on contact page</label></td>
				<td>
<?php echo create_slist ( $list, 'phone_1_show', $row['phone_1_show'], 1 ); ?>
				</td>
			</tr>
			// PHONE STOP HERE

			// PHONE START HERE
			<tr>
				<td> <label for="phone_2">Phone #(2)</label></td>
				<td> <input <?= $val_phone_2?> type="text" name="phone_2" id="phone_2"
					value="<?= stripslashes ( $row['phone_2'] )  ?>" size="45" /></td>
			</tr>

			<tr>
				<td><label for="phone_2_type">Phone Type</label></td>
				<td><input <?= $val_phone_2_type?> type="text" name="phone_2_type" id="phone_2_type" value="<?= stripslashes ( $row['phone_2_type'] )  ?>" size="45" />
<?php echo tooltip('Phone, Cel Phone, Toll Free, Fax, etc.')?>
				</td>
			</tr>

			<tr>
				<td><label for="phone_2_show">Show Phone #(2) on contact page</label></td>
				<td>
<?php echo create_slist ( $list, 'phone_2_show', $row['phone_2_show'], 1 ); ?>
				</td>
			</tr>
			// PHONE STOP HERE

			// PHONE START HERE
			<tr>
				<td><label for="phone_3">Phone #(3)</label></td>
				<td><input <?= $val_phone_3?> type="text" name="phone_3" id="phone_3"
					value="<?= stripslashes ( $row['phone_3'] )  ?>" size="45" /></td>
			</tr>

			<tr>
				<td><label for="phone_3_type">Phone Type</label></td>
				<td><input <?= $val_phone_3_type?> type="text" name="phone_3_type" id="phone_3_type" value="<?= stripslashes ( $row['phone_3_type'] )  ?>" size="45" />
<?php echo tooltip('Phone, Cel Phone, Toll Free, Fax, etc.')?>
				</td>
			</tr>

			<tr>
				<td><label for="phone_3_show">Show Phone #(3) on contact page</label></td>
				<td>
<?php echo create_slist ( $list, 'phone_3_show', $row['phone_3_show'], 1 ); ?>
				</td>
			</tr>
			// PHONE STOP HERE

			<tr>
				<td> <label for="c_address">Address</label></td>
				<td> <input <?= $val_c_address?> type="text" name="c_address" id="c_address" value="<?= stripslashes ( $row['c_address'] )  ?>" size="45" /></td>
			</tr>

			<tr>
				<td> <label for="c_city">City</label></td>
				<td> <input <?= $val_c_city?> type="text" name="c_city" id="c_city"
					value="<?= stripslashes ( $row['c_city'] )  ?>" size="45" /></td>
			</tr>

			<tr>
				<td> <label for="c_state">State</label></td>
				<td> <input <?= $val_c_state?> type="text" name="c_state" id="c_state"
					value="<?= stripslashes ( $row['c_state'] )  ?>" size="45" /></td>
			</tr>

			<tr>
				<td> <label for="c_zip">Zip Code</label></td>
				<td> <input <?= $val_c_zip?> type="text" name="c_zip" id="c_zip"
					value="<?= stripslashes ( $row['c_zip'] )  ?>" size="45" /></td>
			</tr>

			<tr>
				<td> <label for="c_address_type">Address Type</label></td>
				<td> <input <?= $val_c_address_type?> type="text" name="c_address_type" id="c_address_type" value="<?= stripslashes ( $row['c_address_type'] )  ?>" size="45" /></td>
			</tr>

			<tr>
				<td><label for="c_address_show">Display Address Information</label></td>
				<td>
<?php echo create_slist ( $list, 'c_address_show', $row['c_address_show'], 1 ); ?>
				</td>
			</tr>

			<tr>
				<td><label for="bus_hours">Business Hours</label></td>
				<td><textarea id="bus_hours" name="bus_hours" cols="33" rows="3">'. str_replace('<br />', '', $row['bus_hours'] ?></textarea>
<?php echo tooltip('Setup your business hours.')?>
				</td>
			</tr>

			<tr>
				<td><label for="show_bus_hours">Display Business Hours</label></td>
				<td>
<?php echo create_slist ( $list, 'show_bus_hours', $row['show_bus_hours'], 1 ); ?>
				</td>
			</tr>

			<tr>
				<td><label for="show_contact">Display All Contact Information</label></td>
				<td>
<?php echo create_slist ( $list, 'show_contact', $row['show_contact'], 1 ); ?>
				</td>
			</tr>

			<tr>
				<td><label for="display_contact">Display Contact Form</label></td>
				<td>
<?php echo create_slist ( $list, 'display_contact', $row['display_contact'], 1 ); ?>
				</td>
			</tr>

			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>

			if ( is_saint() )
			{
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>

				<tr>
					<td><label for="live">Make Site Live</label></td>
					<td>
<?php echo create_slist ( $list, 'live', $row['live'], 1 );  ?>
<?php echo tooltip('Do not change this unless told to do so. \'No\' will cause site to be unviewable.'); ?>
					</td>
				</tr>

				<tr>
					<td><label for="use_access">Restrict Access Control</label></td>
					<td>
<?php echo create_slist ( $list, 'use_access', $row['use_access'], 1 ); ?>
<?php echo tooltip('This option allows you to set levels for administrators to access modules.'); ?>
					</td>
				</tr>

				<tr>
					<td><label for="use_seo">Use SEO Friendly Page URLs</label></td>
					<td>
<?php echo create_slist ( $list, 'use_seo', $row['use_seo'], 1 ); ?>
<?php echo tooltip('Please only change this option if you are told to or your site will not work.'); ?>
					</td>
				</tr>

			} else {

				<input type="hidden" id="live" name="live" value="<?= $config['live']?>" />
				<input type="hidden" id="use_access" name="use_access" value="<?= $config['use_access']?>" />
				<input type="hidden" id="use_seo" name="use_seo" value="<?= $config['use_seo']?>" />
			}

			<tr>
				<td colspan="2" style="margin:0 auto; text-align:center; padding:3px;"><input type="submit" name="submit" value="Submit" /></td>
			</tr>

		</table>
	</form>

	<div style="height:10px; min-height:10px;">&nbsp;</div>
*/
