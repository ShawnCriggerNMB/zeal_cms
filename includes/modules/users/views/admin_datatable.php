<table id="data-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>ID</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Role</th>
				<th>Active</th>
				<th>Actions</th>
			</tr>
		</thead>

		<tfoot>
			<tr>
				<th>ID</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Role</th>
				<th>Active</th>
				<th>Actions</th>
			</tr>
		</tfoot>

		<tbody>
<?php foreach ( $results as $row ) : ?>
			<tr>
				<td><?=$row->ID?></td>
				<td><?=$row->first_name?></td>
				<td><?=$row->last_name?></td>
				<td><?=$row->user_email?></td>
				<td><?=apply_filters( 'user_datatable_role', $row->role )?></td>
				<td><?=apply_filters( 'datatable_yes_no', $row->user_status ) ?></td>
				<td><?=create_form_actions( $row->ID )?></td>
			</tr>
<?php endforeach; ?>
		</tbody>
</table>
