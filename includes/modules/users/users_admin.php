<?php
/**
 * Core functions for Zeal Technologies CMS Admin area only.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage users
 * @category modules
 * @since  1.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */

if ( !defined('BASE') ) die('No Direct Script Access');

// ------------------------------------------------------------------------

class Users_Admin extends Module_Base {

	protected $_fields = array();


	// ------------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct();
		add_filter( 'user_datatable_role', array(&$this, 'clean_role'), 10, 1 );
		add_action( 'area-top-right', array(&$this, 'add_new_button' ), 10, 1 );

		$row = array(
			'first_name'  => NULL,
			'last_name'   => NULL,
			'user_email'  => NULL,
			'user_login'  => NULL,
			'user_status' => 0,
		);


		if ( is_numeric( $this->_id ) && 'update' === $this->_action )
		{
			$user = (array) get_user( $this->_id );
			$user = $user['data'];
			$user->role = $this->clean_role( $user->cms_capabilities );
			$user->role = strtolower( $user->role );
			$user = (array) $user;
			$row  = array_merge( $row, $user );
			//$rules = json_encode( $this->_rules );
			$rules = NULL;

			add_action( 'admin_enqueue_scripts', function( $rules ) use ( $rules ) {

//				cms_enqueue_style  ( 'admin-fileupload', site_url( '/admin/assets/css/bootstrap-fileupload.css'), array(), '1', 'all' );
//				cms_enqueue_script ( 'admin-fileupload', site_url( '/admin/assets/js/vendor/bootstrap-fileupload.js'), array(), '1', 'all' );
				cms_enqueue_style  ( 'strength-meter', admin_url( '/assets/vendor/strength-meter/css/strength-meter.css'), array(), '1', 'all' );
				cms_enqueue_script ( 'strength-meter', admin_url( '/assets/vendor/strength-meter/js/strength-meter.min.js'), array(), '1', 'all' );
				cms_enqueue_style  ( 'bootstrap-validation', admin_url( '/assets/vendor/bootstrapvalidator/dist/css/bootstrapValidator.min.css'), array(), '1', 'all' );
				cms_enqueue_script ( 'bootstrap-validation', admin_url( '/assets/vendor/bootstrapvalidator/dist/js/bootstrapValidator.min.js'), array(), '1', 'all' );
				cms_enqueue_script ( 'bootstrap-validation-rules', site_url( '/includes/modules/users/assets/js/users_admin.js'), array(), '1', 'all' );
				//cms_localize_script( 'bootstrap-validation', 'validation_rules', $rules );
			});

		}


		$this->_fields = array(
			'first_name' => array(
				'name'     => 'first_name',
				'value'    => $row['first_name'],
				'label'    => 'First Name',
//				'required' => required(),
			),
			'last_name' => array(
				'name'     => 'last_name',
				'value'    => $row['last_name'],
				'label'    => 'Last Name',
//				'required' => required(),
			),
			'user_email' => array(
				'name'     => 'user_email',
				'value'    => $row['user_email'],
				'label'    => 'Email',
				'type'     => 'email',
//				'required' => required(),
			),
			'user_pass' => array(
				'name'     => 'user_pass',
				//'value'    => $row['user_pass'],
				'label'    => 'Password',
				'type'     => 'password',
				'class'    => 'strength',
				'data'     => ' data-toggle-mask="true" ',
//				'required' => $rp,
				'help_text' => '',
			),
			'user_email' => array(
				'name'     => 'user_email',
				'value'    => $row['user_email'],
				'label'    => 'Email',
				'type'     => 'email',
//				'required' => required(),
			),
			'user_login' => array(
				'name'     => 'user_login',
				'value'    => $row['user_login'],
				'label'    => 'User Name',
//				'required' => required(),
				'help_text' => '4-15 Characters; letters and numbers only. This will be displayed on users listings. You can use the users real name here.'
			),
			'user_status' => array(
				'name'     => 'user_status',
				'value'    => $row['user_status'],
				'label'    => 'Active',
//				'required' => required(),
				'fields'   => array(
					array (
						'key' => 0,
						'value'=>'No&nbsp;'
					),
					array (
						'key'  => 1,
						'value'=>'Yes'
					)
				),
				'help_text' => 'Set to Yes to Allow Login',
			),
			'role' => array(
				'name'     => 'role',
				'value'    => isset( $row['role'] ) ? $row['role'] : NULL,
				'label'    => 'User Role',
//				'required' => required(),
				'fields'   => array(
					array (
						'key' => 'user',
						'value'=>'User'
					),
					array (
						'key'  => 'moderator',
						'value'=> 'Moderator'
					),
					array (
						'key'  => 'administrator',
						'value'=> 'Administrator'
					),
					array (
						'key'  => 'superadmin',
						'value'=> 'Super Administrator'
					)

				),
				'help_text' => 'User role allows access to different areas of the website.',
			),
			'submit' => array(
				'name'     => 'submit',
				'value'    => 'Save',
			),

		);

	}

	// ------------------------------------------------------------------------

	public function handle_datatable()
	{

		$roles = array( 'superadmin', 'administrator', 'moderator', 'user' );
		$results = get_users_by_roles( $roles, 'ID' );

		View::show( 'admin_datatable', array( 'results' => $results ) );
	}


	// ------------------------------------------------------------------------

	public function handle_update()
	{
		$this->handle_form();
	}

	public function handle_form()
	{

		$form = $this->_fields;

		$action = admin_url( '?tool=users&action=new' );
		if ( 'update' === $this->_action )
		{
			$action = admin_url( '?tool=users&action=update&id=' . $this->_id );
		}


		if ( isset( $this->_errors ) && ! is_null( $this->_errors ) )
		{
			Notify::error( $this->_errors );
		}

		Form::open( array( 'action' => $action ) );

		Form::input( $form['first_name'] );
		Form::input( $form['last_name'] );
		Form::input( $form['user_email'] );
		Form::input( $form['user_pass'] );

		if ( 'update' !== $this->_action )
		{
			Form::input( $form['user_login'] );
		}

		Form::select( $form['user_status'] );

		Form::select( $form['role'] );
		Form::submit( $form['submit'] );
		Form::close();
	}

	// ------------------------------------------------------------------------


	public function handle_delete()
	{
		return $this->handle_form();
	}


	// ------------------------------------------------------------------------


	public function handle_submit()
	{
		global $wpdb;

		$data = array(
			'user_nicename'     => $_POST['first_name'] . ' ' . $_POST['last_name'],
			'user_email'        => $_POST['user_email'],
			'user_url'          => isset( $_POST['user_url'] ) ? $_POST['user_url'] : '',
			'user_login'        => isset( $_POST['user_login'] ) ? $_POST['user_login'] : $_POST['user_email'],
			'user_pass'         => $_POST['user_pass'],
			'user_registered'   => time(),
			'display_name'      => $_POST['first_name'] . ' ' . $_POST['last_name'],
			'user_status'       => $_POST['user_status'],
			'user_activation_key' => '',
		);

		if ( 'update' === $this->_action )
		{

			if ( empty( $_POST['user_pass'] ) )
			{
				unset( $data['user_pass'] );
			}
			unset( $data['user_registered'] );
			$data['ID'] = (int) $this->_id;
			$new_user = $GLOBALS['cms_users_object']->update_user( $this->_id, $data );
			$action = 'updated.';
		} else {
			unsert( $data['ID'] );
			$data['user_pass'] = $_POST['user_pass'];
//			$new_user = new_user( $data );
			$new_user = $GLOBALS['cms_users_object']->new_user( $data );
			$this->_id = $wpdb->insert_id;
			$action = 'created.';
		}

		if ( is_cms_error ( $new_user ) )
		{
			$this->_errors = $new_user->get_error_message();
			return $this->handle_form();
		}

		$new_user = get_user( $this->_id );

		$user_id = $new_user->ID;
		update_user_meta( $user_id, 'first_name', $_POST['first_name'] );
		update_user_meta( $user_id, 'last_name', $_POST['last_name'] );
		$new_user->set_role( $_POST['role'] );

		$_REQUEST['action'] = 'success';

		$edit_link  = admin_url( '?tool=users&action=update&id=' . $this->_id );
		$table_link = admin_url( '?tool=users' );

		$msg = <<<EOL
			Your page has been successfully {$action}.<br>
			You can re-open the user to edit by clicking <a href="{$edit_link}" >here</a>.<br>
			You can return to the users list by clicking <a href="{$table_link}" >here</a>.<br>
EOL;
		Notify::success( $msg );
	}

	// ------------------------------------------------------------------------

	public function clean_role( $role = NULL )
	{
		$role = maybe_unserialize( $role );
		$role = array_keys( $role );
		$role = array_shift( $role );
		$role = ucfirst( strtolower( $role ) );
		return $role;
	}

	public function add_new_button( $hook_suffix = NULL )
	{

		if ( 'users-datatable' === $hook_suffix )
		{
			$link = admin_url( '/?tool=users&action=new' );
			echo "<div class='header pull-right' style='padding-top:10px'><a href=\"{$link}\" class=\"btn btn-info\">CREATE NEW</a></div>";
		}

		if ( 'users-update' === $hook_suffix OR 'users-new' === $hook_suffix)
		{
			$link = admin_url( '/?tool=users' );
			echo "<div class='header pull-right' style='padding-top:10px'>
			<a href=\"#\" class=\"btn btn-success save-form\">SAVE</a>
			<a href=\"{$link}\" class=\"btn btn-danger\">CANCEL</a></div>";
		}

	}


}


/*
add_filter( 'module_header_title', 'change_title' );
add_filter( 'module_header_subtitle', 'change_subtitle' );
*/

