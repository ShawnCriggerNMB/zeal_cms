$(document).ready(function() {

	$("a.save-form").click(function(e){
		e.preventDefault();
		$('.form-horizontal').submit();

	});


	$('.form-horizontal').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {

			first_name: {
				message: 'The first name is not valid',
				validators: {
					notEmpty: {
						message: 'The first name is required and cannot be empty'
					},
					stringLength: {
						min: 3,
						max: 30,
						message: 'The first name must be more than 3 and less than 30 characters long'
					},
					regexp: {
						regexp: /^[a-zA-Z0-9_]+$/,
						message: 'The first name can only consist of alphabetical, number and underscore'
					}
				}
			},
			last_name: {
				message: 'The last name is not valid',
				validators: {
					notEmpty: {
						message: 'The last name is required and cannot be empty'
					},
					stringLength: {
						min: 3,
						max: 30,
						message: 'The last name must be more than 3 and less than 30 characters long'
					},
					regexp: {
						regexp: /^[a-zA-Z0-9_]+$/,
						message: 'The last name can only consist of alphabetical, number and underscore'
					}
				}
			},
			user_status: {
				message: 'The user status must be set.',
				validators: {
					notEmpty: {
						message: 'The user status is required and cannot be empty'
					},
					numeric: {
						message: 'The user status is required and cannot be empty'
					}
				}
			},
			role: {
				message: 'The user role must be set.',
				validators: {
					notEmpty: {
						message: 'The user role is required and cannot be empty'
					}
				}
			},
			user_login: {
				message: 'The username is not valid',
				validators: {
					notEmpty: {
						message: 'The username is required and cannot be empty'
					},
					stringLength: {
						min: 6,
						max: 30,
						message: 'The username must be more than 6 and less than 30 characters long'
					},
					regexp: {
						regexp: /^[a-zA-Z0-9_]+$/,
						message: 'The username can only consist of alphabetical, number and underscore'
					}
				}
			},
			user_pass: {
				enabled: false,
				validators: {
					notEmpty: {
						message: 'The password is required and cannot be empty'
					},
					stringLength: {
						min: 8,
						max: 30,
						message: 'The password must be more than 8 and less than 30 characters long'
					},
					regexp: {
						regexp: /^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/,
						message: 'The password must contain at least 1 uppercase, 1 lowercase, 1 number, and 1 allowed character. Allowed Characters: # $ % & + [ ] ?'
					}

				}
			},
			user_email: {
				validators: {
					notEmpty: {
						message: 'The email is required and cannot be empty'
					},
					emailAddress: {
						message: 'The input is not a valid email address'
					}
				}
			}
		}
		// Enable the password/confirm password validators if the password is not empty
	}).on('keyup', '[name="user_pass"]', function() {
		var isEmpty = $(this).val() == '';
		$('.form-horizontal').bootstrapValidator('enableFieldValidators', 'user_pass', !isEmpty); //.bootstrapValidator('enableFieldValidators', 'confirm_password', !isEmpty);
            // Revalidate the field when user start typing in the password field
		if ($(this).val().length == 1) {
			$('.form-horizontal').bootstrapValidator('validateField', 'user_pass'); //.bootstrapValidator('validateField', 'confirm_password');
		}
	});


});
