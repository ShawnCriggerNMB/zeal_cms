<?php
/**
 * Core functions for Zeal Technologies CMS Admin area only.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage profile
 * @category modules
 * @since  1.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */

if ( !defined('BASE') ) die('No Direct Script Access');

// ------------------------------------------------------------------------

class User_Profile_Admin extends Module_Base {

	public $errors = array();
	public $success = FALSE;

	// ------------------------------------------------------------------------

	public function __construct()
	{
		add_action( 'admin_enqueue_scripts', function()  {

			cms_enqueue_style  ( 'admin-fileupload', site_url( '/admin/assets/css/bootstrap-fileupload.css'), array(), '1', 'all' );
			cms_enqueue_script ( 'admin-fileupload', site_url( '/admin/assets/js/vendor/bootstrap-fileupload.js'), array(), '1', 'all' );

			cms_enqueue_style  ( 'strength-meter', site_url( '/admin/assets/vendor/strength-meter/css/strength-meter.css'), array(), '1', 'all' );
			cms_enqueue_script ( 'strength-meter', site_url( '/admin/assets/vendor/strength-meter/js/strength-meter.min.js'), array(), '1', 'all' );

		});

		if ( 'POST' === $_SERVER['REQUEST_METHOD'] )
		{
			return $this->handle_submit();
		}

	}

	// ------------------------------------------------------------------------

	public function __call( $method = '', $args = array() )
	{
		if ( is_callable( array( $this, $method ) ) )
		{
			return call_user_method_array( $method, $this, $args );
		}

		return $this->handle_form();
	}

	// ------------------------------------------------------------------------

	public function handle_form()
	{
		global $current_user;

		$user_img = site_url( '/assets/img/default_user_image.jpeg' );

		if ( isset( $current_user->user_image ) && file_exists( site_dir( '/uploads/users/' . $current_user->user_image ) ) )
		{
			$user_img = site_url( '/uploads/users' . $current_user->user_img );
		}

		if ( is_array( $this->errors ) && 0 === count( $this->errors ) )
		{
			$this->errors = FALSE;
		}

		View::show( 'admin_profile_form', array(
			'user' => $current_user,
			'user_image' => $user_img,
			'errors' => $this->errors,
			'success' => $this->success
		));
	}

	// ------------------------------------------------------------------------

	public function handle_submit()
	{
		global $current_user;
		$user_id = get_current_user_id();

		if ( isset( $_POST['first_name'] ) )
		{
			update_user_meta( $user_id, 'first_name', $_POST['first_name'], $current_user->first_name );
		}

		if ( isset( $_POST['last_name'] ) )
		{
			update_user_meta( $user_id, 'last_name', $_POST['last_name'], $current_user->last_name );
		}

		if ( isset( $_POST['last_name'] ) )
		{
			update_user_meta( $user_id, 'last_name', $_POST['last_name'], $current_user->last_name );
		}

		if ( isset( $_POST['about_you'] ) )
		{
			update_user_meta( $user_id, 'about_you', $_POST['about_you'], $current_user->about_you );
		}

		if ( isset( $_POST['use_debug_profiler'] ) && $current_user->has_cap( 'superadmin' ) )
		{
			update_user_meta( $user_id, 'use_debug_profiler', $_POST['use_debug_profiler'], $current_user->use_debug_profiler );
		}

		if ( isset( $_POST['display_name'] ) && isset( $_POST['user_url'] ) )
		{
			$vars = array(
				'ID'           => $user_id,
				'display_name' => $_POST['display_name'],
				'user_url'     => $_POST['user_url'],
			);
			$user = update_user( $user_id, $vars );
			if ( ! is_cms_error( $user ) )
			{
				$this->success = 'Your information has been successfully updated.';
			}
			$current_user = get_currentuserinfo();
			return $user;
		}

		if ( isset( $_POST['old-password'] ) && isset( $_POST['new-password-1'] ) && isset( $_POST['new-password-2'] ) )
		{
			$pass1 = $_POST['new-password-1'];
			$pass2 = $_POST['new-password-2'];
			$opass = $_POST['old-password'];

			if ( empty( $pass1 ) OR empty( $pass2 ) )
			{
				$this->errors[] = 'You must fill out a password.';
				return FALSE;
			}
			if ( $pass1 !== $pass2 )
			{
				$this->errors[] = 'Your new passwords must match.';
				return FALSE;
			}

			$check = CMS_Pass::check_password($opass, $current_user->user_pass, $user_id );
			if ( FALSE === $check )
			{
				$this->errors[] = 'You have entered the wrong password.';
				return FALSE;
			}

			$vars = array(
				'ID'           => $user_id,
				'user_pass'    => $pass1
			);
			$user = update_user( $user_id, $vars );
			if ( ! is_cms_error( $user ) )
			{
				$this->success = 'Your password has been successfully updated.';
			}
			return $user;
		}
	}

	public function handle_delete()
	{
		return $this->handle_form();
	}

	public function handle_update()
	{
		return $this->handle_form();
	}


	public function handle_datatable()
	{
		return $this->handle_form();
	}


}
