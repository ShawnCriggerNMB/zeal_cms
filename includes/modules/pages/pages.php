<?php
/**
 * Core functions for Zeal Technologies CMS Admin area only.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage pages
 * @category modules
 * @since  1.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */

if ( !defined('BASE') ) die('No Direct Script Access');

// ------------------------------------------------------------------------

class Pages {

	public function __construct()
	{

	}

	// ------------------------------------------------------------------------

	public function fetch( $slug = '' )
	{
		global $wpdb;
		if ( is_numeric( $slug ) )
		{
			$slug = (int) $slug;
			$result = $wpdb->get_row("SELECT * FROM {$wpdb->pages} WHERE `id` = '{$slug}' ");
		} elseif ( ! empty( $slug ) )
		{
			$slug   = $wpdb->escape( $slug );
			$result = $wpdb->get_row("SELECT * FROM {$wpdb->pages} WHERE `link` = '{$slug}' ");
		}

		return $result;
	}

}
