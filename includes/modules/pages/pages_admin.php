<?php
/**
 * Core functions for Zeal Technologies CMS Admin area only.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage pages
 * @category modules
 * @since  1.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */

if ( !defined('BASE') ) die('No Direct Script Access');

// ------------------------------------------------------------------------

class Pages_Admin extends Module_Base {

	protected $_fields = array();

	protected $_human_field_names = array(
		'nav_name'    => 'Navigation Name',
		'label'       => 'Page Title',
		'show_in_nav' => 'Show in Navigation',
		'meta_kw'     => 'SEO Meta Keywords',
		'meta_descr'  => 'SEO Meta Description',
		'meta_title'  => 'SEO Title',
		'show_in_nav' => 'Show in Navigation',
		'hidden-meta_kw'   => 'SEO Meta Keywords',
		'show_in_foot_nav' => 'Show in Footer Navigation',
	);

	protected $_rules = array(
		'nav_name'    => 'required|max_len,72|min_len,6',
		'link'        => 'alpha_dash|max_len,72|min_len,2',
		'pagecontent' => 'required',
    	'weight'      => 'required|numeric|max_len,3|min_len,1',
    	'active'      => 'required|exact_len,1|numeric',
    	'show_in_nav' => 'required|exact_len,1|numeric',
    	'meta_descr'  => 'required|max_len,155|min_len,6',
    	'hidden-meta_kw'   => 'required|max_len,255|min_len,6',
    	'meta_title'       => 'required|max_len,55|min_len,6',
    	'show_in_foot_nav' => 'required|exact_len,1|numeric',
	);

	protected $_filter_rules = array(
		'meta_descr'  => 'noise_words|sanitize_string|trim',
		'meta_title'  => 'trim|sanitize_string',
	);

	protected $_keywords = '';

	// ------------------------------------------------------------------------

	public function __construct( $pdo )
	{
		global $wpdb;
		parent::__construct( $pdo );
		// this is where you can set form validation rules.
		$this->_rules = array();
		GUMP::add_filter( 'slugify', function($value, $params = NULL)
		{
		    return sanitize_title_with_dashes( $value, '', 'save');
		});

		$yes_no = array(
			array (
				'key' => 0,
				'value'=>'No&nbsp;'
			),
			array (
				'key'  => 1,
				'value'=>'Yes'
			)
		);

		$defaults = array(
			'link'        => NULL,
			'nav_name'    => NULL,
			'label'       => NULL,
			'pagecontent' => NULL,
			'weight'      => 10,
			'active'      => 1,
			'show_in_nav' => 0,
			'show_in_foot_nav' => 0,
			'meta_descr'   => NULL,
			'meta_kw'      => NULL,
			'meta_title'   => NULL,
		);

		if ( 'update' === $this->_action && is_integer( $this->_id ) )
		{
			$results = $wpdb->get_results( "SELECT * FROM {$wpdb->pages} WHERE id={$this->_id}");
			$results = is_array( $results ) ? array_shift( $results ) : $results;

			$values = array(
				'link'        => $results->link,
				'nav_name'    => $results->nav_name,
				'label'       => $results->label,
				'pagecontent' => $results->content,
				'weight'      => $results->weight,
				'active'      => $results->active,
				'show_in_nav' => $results->show_in_nav,
				'show_in_foot_nav' => $results->show_in_foot_nav,
				'meta_descr'   => $results->meta_descr,
				'meta_kw'      => $results->meta_kw,
				'meta_title'   => $results->meta_title,
			);
			$this->_keywords = $results->meta_kw;
			$defaults = array_merge( $defaults, $values );
		}

		$this->_fields = array(
			'label' => array(
				'name'     => 'label',
				'id'       => 'page-title',
				'value'    => $defaults['label'],
				'label'    => 'Page Name',
				'help_text' => 'The exact name as you want it displayed on your website.',
				//			'required' => $this->required,
			),
			'nav_name' => array(
				'name'     => 'nav_name',
				'value'    => $defaults['nav_name'],
				'label'    => 'Navigation Bar Name',
				'help_text' => 'The name that appears in the navigation bars.',
			),
			'link' => array(
				'name'     => 'link',
				'id'       => 'page-link',
				'value'    => $defaults['link'],
				'label'    => 'URL to access this page',
				'help_text' => 'This will be auto-created for you if left blank.',
			),
			'pagecontent' => array(
				'name'     => 'pagecontent',
				'value'    => $defaults['pagecontent'],
				'label'    => 'Page Content',
			),
			'weight' => array(
				'name'      => 'weight',
				'value'     => $defaults['weight'],
				'label'     => 'Order',
				'size'      => 3,
				'maxlength' => 3,
				'help_text' => 'Order in nav bar, lower numbers will be displayed first.'
			),
			'meta_title' => array(
				'name'     => 'meta_title',
				'id'       => 'meta-title',
				'value'    => $defaults['meta_title'],
				'label'    => 'Meta Page Title',
				'help_text' => 'Main Page title used for SEO, Bookmarking and at the top of the browser while page is active.',
			),
			'meta_kw' => array(
				'name'     => 'meta_kw',
				'value'    => $defaults['meta_kw'],
				'label'    => 'Meta Keywords',
				'help_text' => 'A maxium of 16 tags are allowed. Press tab or enter to add more.',
				'class'     => 'input-medium tm-input tm-input-success',
				'style'     => 'width:50%;'
			),
			'meta_descr' => array(
				'name'     => 'meta_descr',
				'value'    => $defaults['meta_descr'],
				'label'    => 'Meta Description.',
				'help_text' => 'If left blank your default meta description will be used.',
			),
			'active' => array(
				'name'     => 'active',
				'value'    => $defaults['active'],
				'label'    => 'Active.',
//				'required' => required(),
				'fields'   => $yes_no,
				'help_text' => 'You can work with this page as a draft until you turn active to on.'
			),
			'show_in_nav' => array(
				'name'     => 'show_in_nav',
				'value'    => $defaults['show_in_nav'],
				'label'    => 'Show in main navigation.',
//				'required' => required(),
				'fields'   => $yes_no,
				'help_text' => 'You can work with this page as a draft until you turn active to on.'
			),
			'show_in_foot_nav' => array(
				'name'     => 'show_in_foot_nav',
				'value'    => $defaults['show_in_foot_nav'],
				'label'    => 'Show in footer navigation.',
//				'required' => required(),
				'fields'   => $yes_no,
				'help_text' => 'You can work with this page as a draft until you turn active to on.'
			),
			'submit' => array(
				'name'     => 'submit',
				'value'    => 'Save',
			),

		);
		View::set_tags(
			array(
				'fields'      => $this->_fields,
				'form_errors' => $this->_errors,
			)
		);

	}

	// ------------------------------------------------------------------------

	public function handle_datatable()
	{
		global $wpdb;
		$results = $wpdb->get_results( "SELECT id, nav_name, link, weight, active FROM {$wpdb->pages} ORDER BY `weight`");
		View::show( 'admin_datatable', array( 'results' => $results ) );
		//return $this->handle_form();
	}


	// ------------------------------------------------------------------------

	public function handle_update()
	{
		return $this->handle_form();
	}

	public function handle_new()
	{
		return $this->handle_form();
	}


	public function handle_form()
	{
		$this->_add_assets();
		View::show( 'admin_form' );
	}

	// ------------------------------------------------------------------------

	private function _get_tags()
	{
		$tags = array(
			'keywords' => $this->_keywords
		);
		return $tags;
	}

	private function _add_assets()
	{
		$tags = $this->_get_tags();
		// Load Tags Manager for insertting keywords
		// @link http://welldonethings.com/tags/manager/v3
		add_action( 'admin_enqueue_scripts', function( $tags ) use ( $tags ) {

			cms_enqueue_style  ( 'admin-tagmanager', site_url( '/admin/assets/css/tagmanager.css'), array(), '1', 'all' );
			cms_enqueue_script ( 'admin-tagmanager', site_url( '/admin/assets/js/vendor/bootstrap-tagmanager.js'), array(), '1', 'all' );
			cms_localize_script( 'admin-tagmanager', 'tagmanager', $tags );
			cms_enqueue_script ( 'admin-phoneformater', site_url( '/admin/assets/js/vendor/bootstrap-formhelpers-phone.js'), array(), '1', 'all' );
			cms_enqueue_style  ( 'bootstrap-validation', admin_url( '/assets/vendor/bootstrapvalidator/dist/css/bootstrapValidator.min.css'), array(), '1', 'all' );
			cms_enqueue_script ( 'bootstrap-validation', admin_url( '/assets/vendor/bootstrapvalidator/dist/js/bootstrapValidator.min.js'), array(), '1', 'all' );
			cms_enqueue_script ( 'bootstrap-validation-rules', site_url( '/includes/modules/pages/assets/js/pages_admin.js'), array(), '1', 'all' );

		});
		return $this;
	}

	// ------------------------------------------------------------------------


	public function handle_delete()
	{
		return $this->handle_form();
	}


	// ------------------------------------------------------------------------


	public function handle_submit()
	{
		global $wpdb;

		if ( ! isset( $_POST['link'] ) OR empty( $_POST['link'] ) )
		{
			$link = sanitize_title_with_dashes( $_POST['label'], '', 'save');
		} else {
			$link = $_POST['link'];
		}

		$content = $_POST['pagecontent'];
		$content = remove_accents( $content );

		$action  = 'updated';
		$vars    = array(
			'label'      => $_POST['label'],
			'nav_name'   => $_POST['nav_name'],
			'link'       => $link,
			'content'    => $content,
			'meta_title' => $_POST['meta_title'],
			'meta_kw'    => $_POST['hidden-meta_kw'],
			'meta_descr' => $_POST['meta_descr'],
			'weight'     => (int) $_POST['weight'],
			'active'     => (int) $_POST['active'],
			'show_in_nav' => (int) $_POST['show_in_nav'],
			'show_in_foot_nav' => (int) $_POST['show_in_foot_nav'],
		);

		if ( is_numeric( $this->_id ) && is_scalar( $this->_id ) )
		{
			$result = $wpdb->update( $wpdb->pages , $vars, array( 'id' => $this->_id ) );
		} else {
			$action  = 'created';
			$result = $wpdb->insert( $wpdb->pages, $vars );
			$this->_id = $wpdb->insert_id;
		}

		if ( is_cms_error ( $result ) )
		{
			$this->error = $result;
			return $this->handle_form();
		}
		$_REQUEST['action'] = 'success';

		$edit_link  = admin_url( '?tool=pages&action=update&id=' . $this->_id );
		$table_link = admin_url( '?tool=pages' );
		$view_link  = site_url ( "/{$link}.html" );

		$msg = <<<EOL
			Your page has been successfully {$action}.<br>
			You can open your page to edit by clicking <a href="{$edit_link}" >here</a>.<br>
			You can return to the pages list by clicking <a href="{$table_link}" >here</a>.<br>
			You can view your page on the web by clicking <a target="_blank" href="{$view_link}" >here</a>.<br>
EOL;
		Notify::success( $msg );
	}

	// ------------------------------------------------------------------------

	public function ajax_delete()
	{

	}

	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------

}

add_action( 'area-top-right', 'pages_add_new_button', 10, 1 );


add_filter( 'module_header_title', 'change_title' );
add_filter( 'module_header_subtitle', 'change_subtitle' );

add_action( 'area-top-right', 'pages_add_new_button', 10, 1 );

function pages_add_new_button( $hook_suffix = NULL )
{

	if ( 'pages-datatable' === $hook_suffix )
	{
		$link = admin_url( '/?tool=pages&action=new' );
		echo "<div class='header pull-right' style='padding-top:10px'><a href=\"{$link}\" class=\"btn btn-info\">CREATE NEW</a></div>";
	}

	if ( 'pages-update' === $hook_suffix OR 'pages-new' === $hook_suffix)
	{
		$link = admin_url( '/?tool=pages' );
		echo "<div class='header pull-right' style='padding-top:10px'>
		<a href=\"#\" class=\"btn btn-success save-form\">SAVE</a>
		<a href=\"{$link}\" class=\"btn btn-danger\">CANCEL</a></div>";
	}


}

function change_title( $default='' )
{
return 'Pages';
}


function change_subtitle( $default='' )
{
return 'Create or Update your Website Content';
}

add_action( 'cms_ajax_delete-pages-record', array( 'Pages_Admin', 'ajax_delete' ) );



//$modules['config'] = new Config_Admin( $GLOBALS['wpdb'] );

//$modules['config']->handle_form();
