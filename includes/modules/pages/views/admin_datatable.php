<table id="data-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>ID</th>
				<th>Navigation Name</th>
				<th>Link</th>
				<th>Weight</th>
				<th>Active</th>
				<th>Actions</th>
			</tr>
		</thead>

		<tfoot>
			<tr>
				<th>ID</th>
				<th>Navigation Name</th>
				<th>Link</th>
				<th>Weight</th>
				<th>Active</th>
				<th>Actions</th>
			</tr>
		</tfoot>

		<tbody>
<?php foreach ( $results as $row ) : ?>
			<tr>
				<td><?=$row->id?></td>
				<td><?=$row->nav_name?></td>
				<td><?=$row->link?></td>
				<td><?=$row->weight?></td>
				<td><?=$row->active?></td>
				<td><?=create_form_actions( $row->id )?></td>
			</tr>
<?php endforeach; ?>
		</tbody>
</table>
