<?php
if ( isset( $form_errors ) && ! is_null( $form_errors ) ) :

	Notify::error( $form_errors );

endif;
?>

<div class="box dragbox">
	<div class="box-header">
		<i class="fa fa-plus">&nbsp;</i>Add a Page
	</div>
	<div class="box-content">
		<div class="row">
<?php
	Form::open();

	Form::input( $fields['label'] );
	Form::input( $fields['nav_name'] );
	Form::input( $fields['link'] );
	Form::textarea( $fields['pagecontent'] );
	Form::input( $fields['meta_title'] );
	Form::input( $fields['meta_kw'] );
	Form::input( $fields['meta_descr'] );

	Form::input( $fields['weight'] );

	Form::select( $fields['active'] );
	Form::select( $fields['show_in_nav'] );
	Form::select( $fields['show_in_foot_nav'] );

	Form::submit ( $fields['submit'] );
	Form::close();
?>
		</div>
	</div>
</div>
