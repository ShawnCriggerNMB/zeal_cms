function slugify(text)
{
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}

$(document).ready(function() {

	var $title      = $("input#page-title"),
		$link       = $("input#page-link"),
		$meta_title = $("input#meta-title");

	$("a.save-form").click(function(e){
		e.preventDefault();
		$('.form-horizontal').submit();

	});


	$title.on( "focusout", function(e){
		var val = $title.val();
		if ( '' === val ) return FALSE;
		if ( '' === $link.val() )
		{
			$link.val( slugify( val ) );
		}
		if ( '' === $meta_title.val() )
		{
			$meta_title.val( val );
		}

	});

	$('.form-horizontal').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			label: {
				message: 'The page title is not valid',
				validators: {
					notEmpty: {
						message: 'The page title is required and cannot be empty'
					},
					stringLength: {
						min: 3,
						max: 72,
						message: 'The page title must be more than 3 and less than 72 characters long'
					}

				}
			},
			link: {
				message: 'The page link is not valid',
				validators: {
					notEmpty: {
						message: 'The page link is required and cannot be empty'
					},
					stringLength: {
						min: 3,
						max: 72,
						message: 'The page link must be more than 3 and less than 72 characters long'
					},
					regexp: {
						regexp: /^[a-zA-Z0-9_-]+$/,
						message: 'The page link can only consist of alphabetical, number and underscore and dashes'
					}
				}
			},
			nav_name: {
				message: 'The navigation name is not valid',
				validators: {
					notEmpty: {
						message: 'The navigation name is required and cannot be empty'
					},
					stringLength: {
						min: 3,
						max: 72,
						message: 'The navigation name must be more than 3 and less than 72 characters long'
					}
				}
			},
			meta_descr: {
				message: 'The meta description is not valid',
				validators: {
					notEmpty: {
						message: 'The meta description is required and cannot be empty'
					},
					stringLength: {
						min: 3,
						max: 155,
						message: 'The meta description name must be more than 3 and less than 155 characters long'
					}
				}
			},
			weight: {
				message: 'The page order is not valid',
				validators: {
					notEmpty: {
						message: 'The page order is required and cannot be empty'
					},
					stringLength: {
						min: 1,
						max: 3,
						message: 'The page order must be more than 3 and less than 72 characters long'
					}
				}
			},
    		show_in_foot_nav: {
				message: 'The page footer navigation status must be set.',
				validators: {
					notEmpty: {
						message: 'The page footer navigation status is required and cannot be empty'
					},
					numeric: {
						message: 'The page footer navigation status is required and cannot be empty'
					}
				}
			},
			show_in_nav: {
				message: 'The page navigation status must be set.',
				validators: {
					notEmpty: {
						message: 'The page navigation status is required and cannot be empty'
					},
					numeric: {
						message: 'The page navigation status is required and cannot be empty'
					}
				}
			},
			active: {
				message: 'The page status must be set.',
				validators: {
					notEmpty: {
						message: 'The page status is required and cannot be empty'
					},
					numeric: {
						message: 'The page status is required and cannot be empty'
					}
				}
			}
		}
		// Enable the password/confirm password validators if the password is not empty
	});

});
