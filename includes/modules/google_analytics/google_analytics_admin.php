<?php
/**
 * Core functions for Zeal Technologies CMS Admin area only.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage Google_Analytics
 * @category modules
 * @since  1.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */

if ( !defined('BASE') ) die('No Direct Script Access');

// ------------------------------------------------------------------------

class Google_Analytics_Admin extends Module_Base {

	private $_ga_auth = array();
	private $_ga = NULL;

	// ------------------------------------------------------------------------

	public function __construct()
	{

		require( __DIR__ . '/vendor/GoogleAnalyticsAPI.class.php');

		$key = 'notasecret';

//		$client_id     = cms_get_option( 'ga_client_id' );
//		$email_address = cms_get_option( 'ga_email_address' );
//		$private_key   = cms_get_option( 'ga_private_key' );
// AIzaSyBzHMNO50N4LbehEFeefoq0kjF2zHucumU
//		$email_address = 'svizionsoftware@gmail.com';
//

		$client_id     =  '932934623124-pj2a6dibe00fmglu29jgrucep29k83l4.apps.googleusercontent.com';
		$email_address = '932934623124-pj2a6dibe00fmglu29jgrucep29k83l4@developer.gserviceaccount.com';
		$private_key   = __DIR__ . '/vendor/The-National-Trader-88253602dc32.p12';

//notasecret

// '/super/secure/path/to/your/privatekey.p12'

		$this->_ga = new GoogleAnalyticsAPI('service');
		$this->_ga->auth->setClientId( $client_id ); // From the APIs console
		$this->_ga->auth->setEmail( $email_address ); // From the APIs console
		$this->_ga->auth->setPrivateKey( $private_key ); // Path to the .p12 file

		$auth = $this->_ga->auth->getAccessToken();

		// Try to get the AccessToken
		if ( 200 !== $auth['http_code'] )
		{
			$error = new CMS_Error( 'Invalid Google Analytics login information' );
			cms_die( $error );
		}

		$this->_ga_auth = (object) array(
			'access_token'  => $auth['access_token'],
			'token_expires' => $auth['expires_in'],
			'token_created' => time(),
		);

		$a = $this->get_accounts();
		dump($a);
	}

	// ------------------------------------------------------------------------

	public function get_accounts()
	{
		dump($this->_ga_auth );
		// Set the accessToken and Account-Id
		$this->_ga->setAccessToken( $this->_ga_auth->access_token );
		$this->_ga->setAccountId('ga:xxxxxxx');

		// Load profiles
		$profiles = $this->_ga->getProfiles();
		dump($profiles);die();
		$accounts = array();
		foreach ( $profiles['items'] as $item )
		{
		    $id   = "ga:{$item['id']}";
		    $name = $item['name'];
		    $accounts[$id] = $name;
		}

		return $accounts;
	}

	// ------------------------------------------------------------------------

	public function handle_datatable()
	{
		$accounts = $this->get_accounts();
dump($accounts);
		// Set the accessToken and Account-Id
		$this->_ga->setAccessToken($accessToken);
		$this->_ga->setAccountId('ga:xxxxxxx');

		// Set the default params. For example the start/end dates and max-results
		$defaults = array(
		    'start-date' => date('Y-m-d', strtotime('-1 month')),
		    'end-date' => date('Y-m-d'),
		);
		$this->_ga->setDefaultQueryParams($defaults);

		// Example1: Get visits by date
		$params = array(
		    'metrics' => 'ga:visits',
		    'dimensions' => 'ga:date',
		);
		$visits = $this->_ga->query($params);

		// Example2: Get visits by country
		$params = array(
		    'metrics' => 'ga:visits',
		    'dimensions' => 'ga:country',
		    'sort' => '-ga:visits',
		    'max-results' => 30,
		    'start-date' => '2013-01-01' //Overwrite this from the defaultQueryParams
		);
		$visitsByCountry = $this->_ga->query($params);

		// Example3: Same data as Example1 but with the built in method:
		$visits = $this->_ga->getVisitsByDate();

		// Example4: Get visits by Operating Systems and return max. 100 results
		$visitsByOs = $this->_ga->getVisitsBySystemOs(array('max-results' => 100));

		// Example5: Get referral traffic
		$referralTraffic = $this->_ga->getReferralTraffic();

		// Example6: Get visits by languages
		$visitsByLanguages = $this->_ga->getVisitsByLanguages();
		dump( $visits, $visitsByLanguages, $visitsByOs, $visitsByCountry, $getVisitsByDate );
		die();
	}

	// ------------------------------------------------------------------------

	public function handle_update()
	{
		return $this;
	}

	public function handle_form()
	{
		return $this;
	}

	public function handle_delete()
	{
		return $this;
	}

	public function handle_submit()
	{
		return $this;
	}




}
