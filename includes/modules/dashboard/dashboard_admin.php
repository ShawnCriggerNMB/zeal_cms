<?php
/**
 * Dashboard Module for admin area.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage admin
 * @category modules
 * @since  3.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */

if ( !defined('BASE') ) die('No Direct Script Access');


// ------------------------------------------------------------------------

add_action( 'admin_dashboard', 'left_dashboard_box' );

add_action( 'admin_dashboard', 'dashboard_shortcuts' );
add_action( 'admin_dashboard', 'left_dashboard_box_2' );

do_action( 'admin_dashboard' );

Dashboard::render_dashboard();

// ------------------------------------------------------------------------


function left_dashboard_box()
{
	Dashboard::add_dashboard_box( 'Welcome', 'fa fa-tasks', 'Your welcome text will go here.' );
}

function left_dashboard_box_2()
{
	Dashboard::add_dashboard_box( 'Welcome', 'fa fa-tasks', 'Your welcome text will go here.' );
}

// ------------------------------------------------------------------------


function dashboard_shortcuts()
{
	$icons = 'fa fa-bookmark';
	$title = 'Quick Shortcuts';
	$html =<<<EOL
				<div class="shortcuts">
					<a href="javascript:;" class="shortcut">
						<i class="shortcut-icon fa fa-list-alt"></i>
						<span class="shortcut-label">Apps</span>
					</a>

					<a href="javascript:;" class="shortcut">
						<i class="shortcut-icon fa fa-bookmark"></i>
						<span class="shortcut-label">Bookmarks</span>
					</a>

					<a href="javascript:;" class="shortcut">
						<i class="shortcut-icon fa fa-signal"></i>
						<span class="shortcut-label">Reports</span>
					</a>

					<a href="javascript:;" class="shortcut">
						<i class="shortcut-icon fa fa-comment"></i>
						<span class="shortcut-label">Comments</span>
					</a>

					<a href="javascript:;" class="shortcut">
						<i class="shortcut-icon fa fa-user"></i>
						<span class="shortcut-label">Users</span>
					</a>

					<a href="javascript:;" class="shortcut">
						<i class="shortcut-icon fa fa-file"></i>
						<span class="shortcut-label">Notes</span>
					</a>

					<a href="javascript:;" class="shortcut">
						<i class="shortcut-icon fa fa-picture"></i>
						<span class="shortcut-label">Photos</span>
					</a>

					<a href="javascript:;" class="shortcut">
						<i class="shortcut-icon fa fa-tag"></i>
						<span class="shortcut-label">Tags</span>
					</a>
				</div>
EOL;
	Dashboard::add_dashboard_box( $title, $icons, $html );
}

?>

<div class="row">

</div>


<?php


class Dashboard {

	public static  $count = 0;
	private static $_template = NULL;
	private static $_widgets = array();

	// ------------------------------------------------------------------------

	public static function set_template( $template = NULL )
	{
		if ( is_string( $template ) )
		{
			self::$_template = $template;
			return get_called_class();
		}

		self::$_template = <<<EOL

		<div class="" id="dashboard_item_{count}">
			<div class="box dragbox">
				<div class="box-header">
					<h2><i class="{icon}"></i><span class="break"></span>{title}</h2>{actions}
				</div>
				<div class="box-content">
					{dash_content}
				</div>
			</div>
		</div>
EOL;
		return get_called_class();
	}

	// ------------------------------------------------------------------------

	public static function add_dashboard_box()
	{
		if ( is_null( self::$_template ) )
		{
			self::set_template();
		}
		$args    = func_get_args();
		$title   = $args[0];
		$icon    = $args[1];
		$content = do_shortcode( $args[2] );
		$actions = '';
		self::$count++;

		$searchs  = array( '{count}', '{icon}', '{title}', '{actions}', '{dash_content}' );
		$replaces = array( self::$count, $icon, $title, $actions, $content );
		self::$_widgets[] = str_replace( $searchs, $replaces, self::$_template );
		return get_called_class();
	}

	// ------------------------------------------------------------------------


	public static function render_dashboard()
	{

		$total = count( self::$_widgets );
		$half  = ceil( $total / 2 );

		echo "\t\t<div class=\"col-md-6\" id=\"dsahboard-left\">\n\n";
		for ($i=0; $i < $half; $i++)
		{
			echo self::$_widgets[$i];
		}
		echo "\n\t\t</div>\n\t\t<div class=\"col-md-6\" id=\"dsahboard-right\">\n\n";
		for ($i=$half; $i < $total; $i++)
		{
			echo self::$_widgets[$i];
		}
		echo "\t\t</div>\n\n\n";
	}


}
