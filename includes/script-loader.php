<?php
/**
 * ZEAL Tech CMS scripts and styles default loader.
 *
 * ZEAL Tech CMS themes and plugins will only be concerned about the filters and actions set in this
 * file.
 *
 * Several constants are used to manage the loading, concatenating and compression of scripts and CSS:
 * define('SCRIPT_DEBUG', true); loads the development (non-minified) versions of all scripts and CSS, and disables compression and concatenation,
 * define('CONCATENATE_SCRIPTS', false); disables compression and concatenation of scripts and CSS,
 * define('COMPRESS_SCRIPTS', false); disables compression of scripts,
 * define('COMPRESS_CSS', false); disables compression of CSS,
 * define('ENFORCE_GZIP', true); forces gzip for compression (default is deflate).
 *
 * The globals $concatenate_scripts, $compress_scripts and $compress_css can be set by plugins
 * to temporarily override the above settings. Also a compression test is run once and the result is saved
 * as option 'can_compress_scripts' (0/1). The test will run again if that option is deleted.
 *
 * @package ZEAL Tech CMS
 */

/** ZEAL Tech CMS Dependencies Class */
require( ZEAL_PATH . '/class.cms-dependencies.php' );

/** ZEAL Tech CMS Scripts Class */
require( ZEAL_PATH . '/class.cms-scripts.php' );

/** ZEAL Tech CMS Scripts Functions */
require( ZEAL_PATH . '/functions.cms-scripts.php' );

/** ZEAL Tech CMS Styles Class */
require( ZEAL_PATH . '/class.cms-styles.php' );

/** ZEAL Tech CMS Styles Functions */
require( ZEAL_PATH . '/functions.cms-styles.php' );

/**
 * Register all ZEAL Tech CMS scripts.
 *
 * Localizes some of them.
 * args order: $scripts->add( 'handle', 'url', 'dependencies', 'query-string', 1 );
 * when last arg === 1 queues the script for the footer
 *
 * @since 2.6.0
 *
 * @param object $scripts CMS_Scripts object.
 */
function cms_default_scripts( &$scripts )
{
	include ABSPATH . '/includes/version.php'; // include an unmodified $cms_version

//	$develop_src = false !== strpos( $cms_version, '-src' );

	if ( ! defined( 'SCRIPT_DEBUG' ) ) {
		define( 'SCRIPT_DEBUG', false !== strpos( $cms_version, '-src' ) );
	}

	if ( ! defined( 'ADMIN_DIR') )
	{
		define( 'ADMIN_DIR', 'admin');
	}

	$develop_src = FALSE;
/*
	if ( ! $guessurl = site_url() ) {
		$guessed_url = true;
		$guessurl = cms_guess_url();
	}
*/

	$scripts->base_url = site_url();
	$scripts->content_url = defined('CMS_CONTENT_URL')? CMS_CONTENT_URL : '';
	$scripts->default_version = '1'; //get_bloginfo( 'version' );
	$scripts->default_dirs = array('/assets/js/', '/assets/js/vendor/');

	$suffix = SCRIPT_DEBUG ? '' : '.min';
	$dev_suffix = $develop_src ? '' : '.min';

	// what version of jquery are we requesting.
	$jquery_version = '1.11.1';

	// jQuery
//	$scripts->add( 'jquery',"//ajax.googleapis.com/ajax/libs/jquery/{$version}/jquery.min.js",$scripts->registered['jquery']->deps,$version,true);
//	$scripts->add( 'jquery-ui',"//ajax.googleapis.com/ajax/libs/jquery/{$version}/jquery.min.js",$scripts->registered['jquery']->deps,$version,true);

//  $scripts->add( 'jquery-migrate', site_url(). "/assets/js/vendor/jquery-migrate$suffix.js", array(), '1.2.1' );
   //wp_enqueue_script('waypoints', get_template_directory_uri().'/js/waypoints.min.js', array('jquery'), null, true);

	$scripts->add( 'jquery', site_url(). "assets/js/vendor/jquery-{$jquery_version}.min.js", array(), null, true );
	$scripts->add( 'jquery-nivo-slider', site_url(). "assets/js/vendor/jquery.nivo.slider{$suffix}.js", array(), null, true );
	$scripts->add( 'jquery-nivo-lightbox', site_url(). "assets/js/vendor/jquery.nivo.lightbox{$suffix}.js", array(), null, true );
	$scripts->add( 'jquery-validationEngine', site_url(). "assets/js/vendor/jquery.validationEngine{$suffix}.js", array( 'jquery-validationEngine-en'), null, true );
	$scripts->add( 'jquery-validationEngine-en', site_url(). "assets/js/vendor/jquery.validationEngine-en.js", array(), null, true );
	$scripts->add( 'chosen', site_url(). "assets/js/vendor/chosen.jquery.min.js", array(), null, true );
	$scripts->add( 'plugins', site_url(). "assets/js/plugins.min.js", array(), null, true );

	$scripts->add( 'spectrum', site_url(). "assets/js/vendor/spectrum{$suffix}.js", array() , null, true );
	$scripts->add( 'alerts'  , site_url(). "assets/js/vendor/alerts{$suffix}.js", array(), null, true );

	$scripts->add( 'admin-bootstrap'  , site_url(). ADMIN_DIR . "/assets/js/bootstrap{$suffix}.js", array(), null, true );
	$scripts->add( 'tinyMCE-base'  , site_url(). ADMIN_DIR . "/assets/js/tinymce/tinymce.min.js", array(), null, true );
	$scripts->add( 'tinyMCE'  , site_url(). ADMIN_DIR . "/assets/js/tinymce/jquery.tinymce.min.js", array( 'tinyMCE-base' ), null, true );

	// creates a JSON object script with a few useful php vars accessable by JS.
	did_action( 'init' ) && $scripts->localize( 'plugins', 'userSettings', array(
		'url' => (string) SITECOOKIEPATH,
		'uid' => (string) get_current_user_id(),
		'time' => (string) time(),
	) );

	$scripts->add( 'temp-remove-me', site_url(). "assets/js/docreadyfunctions.js", array(), null, true );
	// common bits for both uploaders
	$max_upload_size = ( (int) ( $max_up = @ini_get('upload_max_filesize') ) < (int) ( $max_post = @ini_get('post_max_size') ) ) ? $max_up : $max_post;

	if ( empty($max_upload_size) )
		$max_upload_size = __('not configured');
}

/**
 * Assign default styles to $styles object.
 *
 * Nothing is returned, because the $styles parameter is passed by reference.
 * Meaning that whatever object is passed will be updated without having to
 * reassign the variable that was passed back to the same value. This saves
 * memory.
 *
 * Adding default styles is not the only task, it also assigns the base_url
 * property, the default version, and text direction for the object.
 *
 * @since 2.6.0
 *
 * @param object $styles
 */
function cms_default_styles( &$styles )
{
	include ABSPATH . '/includes/version.php'; // include an unmodified $cms_version

//	$cms_version = NULL;
	if ( ! defined( 'SCRIPT_DEBUG' ) )
		define( 'SCRIPT_DEBUG', false !== strpos( $cms_version, '-src' ) );

	if ( ! defined( 'ADMIN_DIR') )
	{
		define( 'ADMIN_DIR', 'admin');
	}

	$styles->base_url = site_url();
	$styles->content_url = defined('CMS_CONTENT_URL')? CMS_CONTENT_URL : '';
	$styles->default_version = 1; //get_bloginfo( 'version' );
	$styles->text_direction = function_exists( 'is_rtl' ) && is_rtl() ? 'rtl' : 'ltr';
	$styles->default_dirs = array('/assets/css/', '/assets/css/vendor/');
	$styles->do_concat = false;

	$open_sans_font_url = '';

	$suffix = SCRIPT_DEBUG ? '' : '.min';

	// Common dependencies
	$styles->add( 'fontawesome',   site_url() . "assets/css/vendor/font-awesome{$suffix}.css" );
	$styles->add( 'icomoon',       site_url() . "assets/css/vendor/icomoon{$suffix}.css" );
	// Common dependencies
	$styles->add( 'jquery-ui',   site_url() . "assets/css/vendor/jquery-ui{$suffix}.css" );
	$styles->add( 'jquery-ui-structure',   site_url() . "assets/css/vendor/jquery-ui.structure{$suffix}.css", array( 'jquery-ui') );
	$styles->add( 'jquery-ui-theme',   site_url() . "assets/css/vendor/jquery-ui.theme{$suffix}.css", array( 'jquery-ui', 'jquery-ui-structure') );
	$styles->add( 'chosen',  site_url() . "assets/css/vendor/chosen{$suffix}.css" );
	$styles->add( 'custom',  site_url() . "assets/css/custom.css" );
	$styles->add( 'style',   site_url() . "assets/css/style.css" );
	$styles->add( 'jquery-validationEngine', site_url(). "assets/css/vendor/validationEngine.jquery{$suffix}.css", array() );

	$styles->add( 'spectrum', site_url(). "assets/css/vendor/spectrum{$suffix}.css", array() );
	$styles->add( 'alerts'  , site_url(). "assets/css/vendor/alerts{$suffix}.css", array() );

	$styles->add( 'admin-bootstrap'  , site_url(). ADMIN_DIR . "/assets/css/bootstrap{$suffix}.css", array() );
	$styles->add( 'admin-styles'  , site_url(). ADMIN_DIR . "/assets/css/styles{$suffix}.css", array() );
	// Includes CSS
//	$styles->add( 'admin-bar',      "/wp-includes/css/admin-bar$suffix.css", array( 'open-sans', 'dashicons' ) );
}

/**
 * Reorder JavaScript scripts array to place prototype before jQuery.
 *
 * @since 2.3.1
 *
 * @param array $js_array JavaScript scripts array
 * @return array Reordered array, if needed.
 */
function cms_prototype_before_jquery( $js_array ) {
	if ( false === $prototype = array_search( 'prototype', $js_array, true ) )
		return $js_array;

	if ( false === $jquery = array_search( 'jquery', $js_array, true ) )
		return $js_array;

	if ( $prototype < $jquery )
		return $js_array;

	unset($js_array[$prototype]);

	array_splice( $js_array, $jquery, 0, 'prototype' );

	return $js_array;
}

/**
 * Load localized data on print rather than initialization.
 *
 * These localizations require information that may not be loaded even by init.
 *
 * @since 2.5.0
 */
function cms_just_in_time_script_localization() {
/*
	cms_localize_script( 'autosave', 'autosaveL10n', array(
		'autosaveInterval' => AUTOSAVE_INTERVAL,
		'blog_id' => get_current_blog_id(),
	) );
*/
}

/**
 * Administration Screen CSS for changing the styles.
 *
 * If installing the 'wp-admin/' directory will be replaced with './'.
 *
 * The $_cms_admin_css_colors global manages the Administration Screens CSS
 * stylesheet that is loaded. The option that is set is 'admin_color' and is the
 * color and key for the array. The value for the color key is an object with
 * a 'url' parameter that has the URL path to the CSS file.
 *
 * The query from $src parameter will be appended to the URL that is given from
 * the $_cms_admin_css_colors array value URL.
 *
 * @since 2.6.0
 * @uses $_cms_admin_css_colors
 *
 * @param string $src Source URL.
 * @param string $handle Either 'colors' or 'colors-rtl'.
 * @return string URL path to CSS stylesheet for Administration Screens.
 */
function cms_style_loader_src( $src, $handle ) {
	global $_cms_admin_css_colors;

	if ( defined('CMS_INSTALLING') )
		return preg_replace( '#^admin/#', './', $src );

	if ( 'colors' == $handle ) {
		$color = get_user_option('admin_color');

		if ( empty($color) || !isset($_cms_admin_css_colors[$color]) )
			$color = 'fresh';

		$color = $_cms_admin_css_colors[$color];
		$parsed = parse_url( $src );
		$url = $color->url;

		if ( ! $url ) {
			return false;
		}

		if ( isset($parsed['query']) && $parsed['query'] ) {
			cms_parse_str( $parsed['query'], $qv );
			$url = add_query_arg( $qv, $url );
		}

		return $url;
	}

	return $src;
}

/**
 * Prints the script queue in the HTML head on admin pages.
 *
 * Postpones the scripts that were queued for the footer.
 * print_footer_scripts() is called in the footer to print these scripts.
 *
 * @since 2.8.0
 *
 * @see cms_print_scripts()
 */
function print_head_scripts() {
	global $cms_scripts, $concatenate_scripts;

	if ( ! did_action('cms_print_scripts') ) {
		/** This action is documented in wp-includes/functions.wp-scripts.php */
		do_action( 'cms_print_scripts' );
	}

	if ( !is_a($cms_scripts, 'CMS_Scripts') )
		$cms_scripts = new CMS_Scripts();

	script_concat_settings();
	$cms_scripts->do_concat = $concatenate_scripts;
	$cms_scripts->do_head_items();

	/**
	 * Filter whether to print the head scripts.
	 *
	 * @since 2.8.0
	 *
	 * @param bool $print Whether to print the head scripts. Default true.
	 */
	if ( apply_filters( 'print_head_scripts', true ) ) {
		_print_scripts();
	}

	$cms_scripts->reset();
	return $cms_scripts->done;
}

/**
 * Prints the scripts that were queued for the footer or too late for the HTML head.
 *
 * @since 2.8.0
 */
function print_footer_scripts() {
	global $cms_scripts, $concatenate_scripts;

	if ( !is_a($cms_scripts, 'CMS_Scripts') )
		return array(); // No need to run if not instantiated.

	script_concat_settings();
	$cms_scripts->do_concat = $concatenate_scripts;
	$cms_scripts->do_footer_items();

	/**
	 * Filter whether to print the footer scripts.
	 *
	 * @since 2.8.0
	 *
	 * @param bool $print Whether to print the footer scripts. Default true.
	 */
	if ( apply_filters( 'print_footer_scripts', true ) ) {
		_print_scripts();
	}

	$cms_scripts->reset();
	return $cms_scripts->done;
}

/**
 * @internal use
 */
function _print_scripts() {
	global $cms_scripts, $compress_scripts;

	$zip = $compress_scripts ? 1 : 0;
	if ( $zip && defined('ENFORCE_GZIP') && ENFORCE_GZIP )
		$zip = 'gzip';

	if ( $concat = trim( $cms_scripts->concat, ', ' ) ) {

		if ( !empty($cms_scripts->print_code) ) {
			echo "\t<script type=\"text/javascript\">\n";
			echo "/* <![CDATA[ */\n"; // not needed in HTML 5
			echo $cms_scripts->print_code;
			echo "/* ]]> */\n";
			echo "</script>\n";
		}

		$concat = str_split( $concat, 128 );
		$concat = 'load%5B%5D=' . implode( '&load%5B%5D=', $concat );

		$src = $cms_scripts->base_url . "/admin/load-scripts.php?c={$zip}&" . $concat . '&ver=' . $cms_scripts->default_version;
		echo "<script type='text/javascript' src='" . esc_attr($src) . "'></script>\n";
	}

	if ( !empty($cms_scripts->print_html) )
		echo $cms_scripts->print_html;
}

/**
 * Prints the script queue in the HTML head on the front end.
 *
 * Postpones the scripts that were queued for the footer.
 * cms_print_footer_scripts() is called in the footer to print these scripts.
 *
 * @since 2.8.0
 */
function cms_print_head_scripts() {
	if ( ! did_action('cms_print_scripts') ) {
		/** This action is documented in wp-includes/functions.wp-scripts.php */
		do_action( 'cms_print_scripts' );
	}

	global $cms_scripts;

	if ( !is_a($cms_scripts, 'CMS_Scripts') )
		return array(); // no need to run if nothing is queued

	return print_head_scripts();
}

/**
 * Private, for use in *_footer_scripts hooks
 *
 * @since 3.3.0
 */
function _cms_footer_scripts() {
	print_late_styles();
	print_footer_scripts();
}

/**
 * Hooks to print the scripts and styles in the footer.
 *
 * @since 2.8.0
 */
function cms_print_footer_scripts() {
	/**
	 * Fires when footer scripts are printed.
	 *
	 * @since 2.8.0
	 */
	do_action( 'cms_print_footer_scripts' );
}

/**
 * Wrapper for do_action('cms_enqueue_scripts')
 *
 * Allows plugins to queue scripts for the front end using cms_enqueue_script().
 * Runs first in cms_head() where all is_home(), is_page(), etc. functions are available.
 *
 * @since 2.8.0
 */
function cms_enqueue_scripts() {
	/**
	 * Fires when scripts and styles are enqueued.
	 *
	 * @since 2.8.0
	 */
	do_action( 'cms_enqueue_scripts' );
}

/**
 * Prints the styles queue in the HTML head on admin pages.
 *
 * @since 2.8.0
 */
function print_admin_styles() {
	global $cms_styles, $concatenate_scripts, $compress_css;

	if ( !is_a($cms_styles, 'CMS_Styles') )
		$cms_styles = new CMS_Styles();

	script_concat_settings();
	$cms_styles->do_concat = $concatenate_scripts;
	$zip = $compress_css ? 1 : 0;
	if ( $zip && defined('ENFORCE_GZIP') && ENFORCE_GZIP )
		$zip = 'gzip';

	$cms_styles->do_items(false);

	/**
	 * Filter whether to print the admin styles.
	 *
	 * @since 2.8.0
	 *
	 * @param bool $print Whether to print the admin styles. Default true.
	 */
	if ( apply_filters( 'print_admin_styles', true ) ) {
		_print_styles();
	}

	$cms_styles->reset();
	return $cms_styles->done;
}

/**
 * Prints the styles that were queued too late for the HTML head.
 *
 * @since 3.3.0
 */
function print_late_styles() {
	global $cms_styles, $concatenate_scripts;

	if ( !is_a($cms_styles, 'CMS_Styles') )
		return;

	$cms_styles->do_concat = $concatenate_scripts;
	$cms_styles->do_footer_items();

	/**
	 * Filter whether to print the styles queued too late for the HTML head.
	 *
	 * @since 3.3.0
	 *
	 * @param bool $print Whether to print the 'late' styles. Default true.
	 */
	if ( apply_filters( 'print_late_styles', true ) ) {
		_print_styles();
	}

	$cms_styles->reset();
	return $cms_styles->done;
}

/**
 * @internal use
 */
function _print_styles() {
	global $cms_styles, $compress_css;

	$zip = $compress_css ? 1 : 0;
	if ( $zip && defined('ENFORCE_GZIP') && ENFORCE_GZIP )
		$zip = 'gzip';

	if ( !empty($cms_styles->concat) ) {
		$dir = $cms_styles->text_direction;
		$ver = $cms_styles->default_version;
		$href = $cms_styles->base_url . "/admin/load-styles.php?c={$zip}&dir={$dir}&load=" . trim($cms_styles->concat, ', ') . '&ver=' . $ver;
		echo "<link rel='stylesheet' href='" . esc_attr($href) . "' type='text/css' media='all' />\n";

		if ( !empty($cms_styles->print_code) ) {
			echo "<style type='text/css'>\n";
			echo $cms_styles->print_code;
			echo "\n</style>\n";
		}
	}

	if ( !empty($cms_styles->print_html) )
		echo $cms_styles->print_html;
}

/**
 * Determine the concatenation and compression settings for scripts and styles.
 *
 * @since 2.8.0
 */
function script_concat_settings() {
	global $concatenate_scripts, $compress_scripts, $compress_css;

	$compressed_output = ( ini_get('zlib.output_compression') || 'ob_gzhandler' == ini_get('output_handler') );
	$compress_css = $compress_scripts = false;

	if ( ! isset($concatenate_scripts) ) {
		$concatenate_scripts = defined('CONCATENATE_SCRIPTS') ? CONCATENATE_SCRIPTS : true;
		if ( ! is_admin() || ( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ) )
			$concatenate_scripts = false;
	}

	if ( ! isset($compress_scripts) ) {
		$compress_scripts = defined('COMPRESS_SCRIPTS') ? COMPRESS_SCRIPTS : true;
		if ( $compress_scripts && ( ! cms_get_option('can_compress_scripts') || $compressed_output ) )
			$compress_scripts = false;
	}

	if ( ! isset($compress_css) ) {
		$compress_css = defined('COMPRESS_CSS') ? COMPRESS_CSS : true;
		if ( $compress_css && ( ! cms_get_option('can_compress_scripts') || $compressed_output ) )
			$compress_css = false;
	}
}

add_action( 'cms_default_scripts', 'cms_default_scripts' );
add_filter( 'cms_print_scripts', 'cms_just_in_time_script_localization' );
add_filter( 'print_scripts_array', 'cms_prototype_before_jquery' );

add_action( 'cms_default_styles', 'cms_default_styles' );
add_filter( 'style_loader_src', 'cms_style_loader_src', 10, 2 );

