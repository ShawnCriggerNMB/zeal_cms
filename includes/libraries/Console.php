<?php
/**
 *  This class serves as a wrapper around a global php variable, debugger_logs, that we have created.
 *
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage Profiler\Console
 * @category core
 * @since  3.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 *
 * @link  ( Original Base Class, http://particletree.com/features/php-quick-profiler/)
 * @uses  Profiler
 * @uses  ChromePhp
 * @uses  FB
 */

if ( ! class_exists( 'FB' ) ) {
	require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'fb.php';
}

if ( ! class_exists( 'ChromePhp' ) ) {
	require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'ChromeLog.php';
}

class Console {

	public static $log = array(
		'console'      => array(),
		'memory_count' => 0,
		'log_count'    => 0,
		'speed_count'  => 0,
		'error_count'  => 0,
	);

	/*-----------------------------------
	     LOG A VARIABLE TO CONSOLE
	------------------------------------*/

	/**
	 * Log A Variable To Console
	 * @param  mixed  $data
	 * @return Console
	 */
	public static function log( $data )
	{
		if ( 0 !== $data && empty( $data ) )
		{
			$data = 'empty';
		}

		$log_item = array(
			'data' => $data,
			'type' => 'log'
		);

		self::add_to_console( 'log_count', $log_item );
//		self::$log['console'][] = $log_item;
//		self::$log['log_count'] += 1;

		if ( class_exists( 'ChromePhp' ) )
		{
			ChromePhp::log( $data );
		}
		if ( class_exists( 'FB' ) )
		{
			FB::log( $data );
		}

		return get_called_class();
	}

	// ------------------------------------------------------------------------

	/**
	 * Log Memory Usage Of Variable Or Entire Script
	 * @static
	 * @param  boolean $object [description]
	 * @param  string  $name   [description]
	 * @return Console
	 */
	public static function log_memory( $object = false, $name = 'PHP' )
	{
		$memory = memory_get_usage();
		if( FALSE !== $object )
		{
			$memory = strlen( serialize( $object ) );
		}

		$log_item = array(
			'data'     => $memory,
			'type'     => 'memory',
			'name'     => $name,
			'dataType' => gettype( $object )
		);

		self::add_to_console( 'memory_count', $log_item );
//		self::$log['console'][] = $log_item;
//		self::$log['memory_count'] += 1;

		return get_called_class();
	}

	// ------------------------------------------------------------------------

	/**
	 * Log A Php Exception Object
	 * @static
	 * @param  Exception $exception
	 * @param  string    $message
	 * @return Console
	 */
	public static function log_error( $exception, $message )
	{
		$log_item = array(
			'data' => $message,
			'type' => 'error',
			'file' => $exception->getFile(),
			'line' => $exception->getLine()
		);

		self::add_to_console( 'error_count', $log_item );
//		self::$log['console'][] = $log_item;
//		self::$log['error_count'] += 1;

		return get_called_class();
	}

	// ------------------------------------------------------------------------

	/**
	 * Point In Time Speed Snapshot
	 * @static
	 * @param  string $name
	 * @return Console
	 */
	public static function log_speed( $name = 'Point in Time' )
	{
		$log_item = array(
			'data' => Profiler::get_micro_time(),
			'type' => 'speed',
			'name' => $name,
		);

//		self::$log['console'][] = $log_item;
//		self::$log['speed_count'] += 1;

		self::add_to_console( 'speed_count', $log_item );
		return get_called_class();
	}

	// ------------------------------------------------------------------------

	/**
	 * Set Defaults & Return Logs
	 * @static
	 * @return array
	 */
	public static function get_logs()
	{
		return self::$log;
	}

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	private static function add_to_console( $log=NULL, $item=NULL )
	{

		if ( empty( $log ) OR empty( $item ) )
		{
			return;
		}

		self::$log['console'][]  = $item;
		self::$log[$log]        += 1;
	}

	// ------------------------------------------------------------------------

}
