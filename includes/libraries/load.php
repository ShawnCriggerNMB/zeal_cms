<?php
/**
 *  Quick and dirty "class" loader for lack of a better name, basically just a fast way to verify a file exists and load the class.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage Load
 * @category core
 * @since  3.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */

if ( !defined('BASE') ) die('No Direct Script Access');

// ------------------------------------------------------------------------

class Load {

	/**
	 * Template Tags
	 * @access protected
	 * @see View::set_tags()
	 * @var array
	 */
	protected static $tags = array();

	/**
	 * Directory class files are located under.
	 * @access protected
	 * @see View::set_module_directory()
	 * @var string
	 */
	private static $module_path = '/includes/modules/';

	/**
	 * Directory helper files are located under.
	 * @access protected
	 * @var string
	 */
	private static $helper_path = '/includes/helpers/';

	// ------------------------------------------------------------------------

	/**
	 * Sets view file dir
	 * @static
	 * @param string $dir
	 * @return  Load
	 */
	public static function set_module_dir( $dir = NULL )
	{
		self::$module_path = $dir;
		return get_called_class();
	}

	// ------------------------------------------------------------------------

	/**
	 * Sets Default Tags to be merged with any tags sent via View::show
	 * @static
	 * @param array $tags array of tags
	 * @return  Load
	 */
	public static function set_tags( $tags = array() )
	{
		self::$tags = array_merge( self::$tags, $tags );
		return get_called_class();
	}

	// ------------------------------------------------------------------------

	/**
	 * Loads class $file from module directory, and sets the class reference to $class
	 *
	 * Filename is filtered by "load_library_{$file}" and filename can be changed using that hook.
	 *
	 * @static
	 * @param  string  $file  Filename to load
	 * @param  object  $class Object to return the class as, passed by reference
	 * @return Load
	 */
	public static function library( $file = '', &$class, $is_static = FALSE )
	{
		$action = $file;
		$file   = site_dir( self::$module_path . $file . '.php');
		$module_path  = site_dir( self::$module_path . "/{$action}/{$action}.php" );
		$library_path = site_dir( "/includes/libraries/{$action}.php" );

		if ( file_exists( $module_path ) )
		{
			$file = $module_path;
		} elseif ( file_exists( $library_path ) )
		{
			$file = $library_path;
		}

		if ( function_exists( 'apply_filters' ) )
		{
			$file = apply_filters( "load_library_{$action}", $file );
		}

		if ( ! file_exists( $file ) ) cms_die('library not found: ' . $file );

		$verb   = str_replace( '-', '_', $action );
		$verb   = ucfirst( strtolower( $verb ) );
		require( $file );
		if ( FALSE === $is_static )
		{
			$class  = new $verb;
		}
		return get_called_class();
	}

	// ------------------------------------------------------------------------

	/**
	 * Loads helper $file from module or helpers directory
	 *
	 * Filename is filtered by "load_helpers_{$file}" and filename can be changed using that hook.
	 *
	 * @static
	 * @param  string  $file  Filename to load
	 * @return Load
	 */
	public static function helper( $file = '' )
	{
		$action = $file;
		$file   = site_dir( self::$module_path . $file . '.php');

		$helper_path = site_dir( self::$helper_path . "/{$action}_helper.php" );
		$module_path = site_dir( self::$module_path . "/{$action}/{$action}.php" );

		if ( file_exists( $module_path ) )
		{
			$file = $module_path;
		} elseif ( file_exists( $helper_path ) )
		{
			$file = $helper_path;
		}

		if ( function_exists( 'apply_filters' ) )
		{
			$file   = apply_filters( "load_helpers_{$action}", $file );
		}

		if ( ! file_exists( $file ) ) cms_die('view not found: ' . $file );

		require( $file );

		return get_called_class();
	}

	// ------------------------------------------------------------------------


}
