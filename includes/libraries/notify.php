<?php
/**
 * SC-Tools are a few random classes I wrote over the years to make my life easier.  If it makes your life easier consider buying me a beer or a coffee would be better.
 *
 * @author Shawn Crigger
 * @version 0.1.1
 * @copyright February 21, 2013
 * @package SC-Tools
 **/

/**
 * A Simple Class to make Information Messages, currently uses Twitter Bootstraps Alert layout, but you can set any in the set_template method.
 * This is a prime example of class masterbation of using very advanced class tatics to accomplish a very simple goal.
 *
 * @version 0.1.1
 * @package SC-Tools
 * @author Shawn Crigger
 **/
 class Notify {


	/**
	 * @static
	 * @access private
	 * @var  string  Place holder for Message Template
	 */
	private static $template;

	// ------------------------------------------------------------------------

    /**
     * Magic Method to create the error, info, success messages. See below example of usage.
     *
     *<code>
     * // below will make a error message with Message Text as the body and Header Text as the Header.
     * Notify::error( 'Message Text', 'Header Text' );
     * // this will make a info message with same args as first example but will return instead of returning the value.
     * Notify::error( 'Message Text', 'Header Text', true );
     *</code>
     *
     * @param string  $name       Type of Message to display, currently error, info, success are all valid.
     * @param array   $arguments  Message, Header and Whether to Echo or Return value.
     * @return string|stdout
     */
    public static function __callStatic( $name, $arguments )
    {

		switch ( $name )
		{
			case 'error':
				$class = 'danger';
				$headr = 'ERROR:';
				break;
			case 'message':
			case 'info':
				$class = 'info';
				$headr = 'NOTES:';
				break;
			default:
			case 'success':
				$class = 'success';
				$headr = 'Success:';
				break;
		}
		self::set_template();
		$count = count( $arguments );
		$headr = ( isset($arguments[1]) ) ? $arguments[1] : $headr;
		$msg   = self::clean_msg( $arguments[0] );
		$template = str_replace(
			array('{class}', '{header}', '{msg}'),
			array($class, $headr, $msg),
			self::$template
		);

		if ( isset( $arguments[2] ) && is_bool( $arguments[2] ) )
		{
			return $template;
		}

		echo $template;
    }

	// ------------------------------------------------------------------------

	/**
	 * Sets template for notification messages.
	 * @static
	 * @param [string] $temp  Optional to override the default template.
	 * @return void
	 */
	public static function set_template( $temp = null )
	{
		if ( null !== $temp )
		{
			self::$template = $temp;
			return;
		}

		self::$template = "\t\t<div class=\"alert alert-{class} alert-block\" style=\"margin:9px auto\">\n
			\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n
			<h4>{header}</h4>{msg}</div>\n";
	}

	// ------------------------------------------------------------------------

	/**
	 * Takes a array and returns a bulleted list or returns the string.
	 * @static
	 * @access private
	 * @param $msg mixed
	 * @return string
	 */
	private static function clean_msg( $msg = null )
	{
		if ( is_string( $msg ) )
		{
			return $msg;
		}
		$note = NULL;
		if ( is_array( $msg ) )
		{
			$note = '<ul>';
			foreach ($msg as $value) {
				$value = trim( $value );
				if ( empty( $value ) ) continue;
				$note .= "<li>{$value}</li>\n\n";
			}
			$note .= '</ul>';
		}
		return $note;
	}


}//end class


/* End of file notify.php */
/* Location: ./libraries/notify.php/ */