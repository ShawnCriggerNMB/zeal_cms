<?php
/**
 *  This class processes the logs and organizes the data for output to the browser.
 *
 *  Initialize this class with a start time at the beginning of your code, and then call the display method
 *  when your code is terminating.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage Profiler
 * @category core
 * @since  3.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 *
 * @link  ( Original Base Class, http://particletree.com/features/php-quick-profiler/)
 * @uses  Console
 */
class Profiler {

	public $output = array();
	public $start_time = NULL;
	public $config    = array();

	// ------------------------------------------------------------------------

	public function __construct( $start_time = NULL )
	{
		$this->start_time = $start_time;

		if ( ! class_exists( 'Console' ) )
		{
			$null = FALSE;
			Load::library( 'Console', $null );
			//require_once($_SERVER['DOCUMENT_ROOT'].$config.'classes/Console.php');
		}
	}

	//--------------------------------------------------------------------

	/**
	 * Format The Different Types Of Logs
	 * @access protected
	 * @return Profiler
	 */
	protected function _compile_console()
	{
		if ( ! class_exists( 'Console' ) )
		{
			$this->output['console'] = NULL;
			return get_called_class();
		}

		$logs = Console::get_logs();

		if ( isset( $logs['console'] ) && is_array( $logs['console'] ) )
		{
			foreach ( $logs['console'] as $key => $log )
			{
				if ($log['type'] == 'log')
				{
					$logs['console'][$key]['data'] = print_r( $log['data'], true );
				}
				elseif ($log['type'] == 'memory')
				{
					$logs['console'][$key]['data'] = static::get_readable_filesize( $log['data'] );
				}
				elseif($log['type'] == 'speed') {
					$logs['console'][$key]['data'] = static::get_readable_time( ( $log['data'] - $this->start_time ) * 1000 );
				}
			}
		}

		$this->output['console'] = $logs;

		return get_called_class();
	}

	//--------------------------------------------------------------------

	/**
	 * Aggregate Data On The Files Included
	 * @access protected
	 * @return Profiler
	 */
	protected function _compile_file_data()
	{
		$files       = get_included_files();
		$file_list   = array();

		$file_totals = array(
			'count'   => count( $files ),
			'size'    => 0,
			'largest' => 0,
		);

		foreach( $files as $key => $file )
		{
			$size        = filesize( $file );

			$file_list[] = array(
					'name' => $file,
					'size' => static::get_readable_filesize( $size )
			);

			$file_totals['size'] += $size;

			if( $size > $file_totals['largest'] )
			{
				$file_totals['largest'] = $size;
			}

		}

		$file_totals['size']         = static::get_readable_filesize( $file_totals['size'] );
		$file_totals['largest']      = static::get_readable_filesize( $file_totals['largest'] );
		$this->output['files']       = $file_list;
		$this->output['file_totals'] = $file_totals;

		return get_called_class();
	}

	// ------------------------------------------------------------------------

	/**
	 * Memory Usage And Memory Available
	 * @access protected
	 * @return Profiler
	 */
	protected function _compile_memory_data()
	{
		$used = (float) static::get_readable_filesize( memory_get_peak_usage() );
		$used = number_format( $used , 4);
		$memory_totals = array(
			'used'  => $used,
			'total' => ini_get("memory_limit"),
		);


		$this->output['memory_totals'] = $memory_totals;
		$this->output['benchmarks'] = $memory_totals;
		return get_called_class();
	}

	// ------------------------------------------------------------------------

	/**
	 * Query Data -- Database Object With Logging Required
	 * @access protected
	 * @global $wpdb
	 * @uses   BPDB
	 * @return Profiler
	 */
	protected function _compile_query_data()
	{
		global $wpdb;

		$total_queries = $wpdb->queries;

		$queries      = array();
		$query_totals = array(
			'count' => 0,
			'time'  => 0,
		);

		$query_totals['count'] += $wpdb->num_queries;

		// Key words we want bolded
		$highlight = array('SELECT', 'DISTINCT', 'FROM', 'WHERE', 'AND', 'LEFT&nbsp;JOIN', 'ORDER&nbsp;BY', 'GROUP&nbsp;BY', 'LIMIT', 'INSERT', 'INTO', 'VALUES', 'UPDATE', 'OR&nbsp;', 'HAVING', 'OFFSET', 'NOT&nbsp;IN', 'IN', 'LIKE', 'NOT&nbsp;LIKE', 'COUNT', 'MAX', 'MIN', 'ON', 'AS', 'AVG', 'SUM', '(', ')');

		if( is_array( $total_queries ) && count ( $total_queries ) > 0 )
		{

			//$query_totals['count'] += $this->db->num_queries;
			foreach( (array) $total_queries as $q )
			{
				$time = number_format( $q[1], 4 );
				$sql  = $q[0];
				foreach ( $highlight as $bold )
				{
					$sql = str_replace( $bold, '<strong style="color:#FFF">'. $bold .'</strong>', $sql);
				}
				$query = array(
					"$time"    => $sql,
//					'explain' => $sql,
					'caller'  => $q[2],
				);

				$query_totals['time'] += $time;
				$queries[] = $query;
//				$queries[][$time] = $sql;
			}
			$query_totals['time']   = static::get_readable_time( $query_totals['time'] );
			$total = number_format( (float) $query_totals['time'] , 4);
			$query_totals[][$total] = 'Total Query Execution Time';

		} else {
			$queries = 'There are no queries logged.';
		}

		$this->output['queries']      = $queries;
		$this->output['query_totals'] = $query_totals;
	}

	/*--------------------------------------------------------
	     CALL SQL EXPLAIN ON THE QUERY TO FIND MORE INFO
	----------------------------------------------------------*/

	function attempt_to_explain_query($query) {
		try {
			$sql = 'EXPLAIN '.$query['sql'];
			$rs = $this->db->query($sql);
		}
		catch(Exception $e) {}
		if($rs) {
			$row = mysql_fetch_array($rs, MYSQL_ASSOC);
			$query['explain'] = $row;
		}
		return $query;
	}

	// ------------------------------------------------------------------------

	/**
	 * Speed Data For Entire Page Load
	 * @access protected
	 * @return Profiler
	 */
	protected function _compile_speed_data()
	{
		$speed_totals = array(
			'total'   => static::get_readable_time( ( static::get_micro_time() - $this->start_time ) * 1000 ),
			'allowed' => ini_get("max_execution_time"),
		);
		$this->output['speed_totals'] = $speed_totals;
		return get_called_class();
	}

	// ------------------------------------------------------------------------

	/**
	 * Compile $_GET Data
	 * @access protected
	 * @return	Profiler
	 */
	protected function _compile_get()
	{
		$output = array();

		$get = $_GET;

		if (count($get) == 0 || $get === false)
		{
			$output = $output = 'No GET vars';
		}
		else
		{
			foreach ($get as $key => $val)
			{
				if (is_array($val))
				{
					$output[$key] = "<pre>" . htmlspecialchars(stripslashes(print_r($val, true))) . "</pre>";
				}
				else
				{
					$output[$key] = htmlspecialchars(stripslashes($val));
				}
			}
		}

		$this->output['get'] = $output;
		return get_called_class();
	}

	// --------------------------------------------------------------------

	/**
	 * Compile $_POST Data
	 * @access protected
	 * @return	Profiler
	 */
	protected function _compile_post()
	{
		$output = array();

		if (count($_POST) == 0)
		{
			$output = 'No POST vars';
		}
		else
		{
			foreach ($_POST as $key => $val)
			{
				if ( ! is_numeric($key))
				{
					$key = "'".$key."'";
				}

				if (is_array($val))
				{
					$output['&#36;_POST['. $key .']'] = '<pre>'. htmlspecialchars(stripslashes(print_r($val, TRUE))) . '</pre>';
				}
				else
				{
					$output['&#36;_POST['. $key .']'] = htmlspecialchars(stripslashes($val));
				}
			}
		}

		$this->output['post'] = $output;
		return get_called_class();
	}

	// --------------------------------------------------------------------


	// ------------------------------------------------------------------------
	// HELPER FUNCTIONS TO FORMAT DATA
	// ------------------------------------------------------------------------

	public function set_start_time( $time = 0 )
	{
		$this->start_time = $time;
		return $this;
	}

	/**
	 * Formats Microtime to a read-able format.
	 * @static
	 * @return string
	 */
	public static function get_micro_time()
	{
		$time = microtime();
		$time = explode(' ', $time);
		return $time[1] + $time[0];
	}

	// ------------------------------------------------------------------------

	/**
	 * Formats $size into a human readable filesize.
	 * @param  integer  $size
	 * @param  string  [$retstring]
	 * @return string
	 */
	public function get_readable_filesize( $size, $retstring = null )
	{
		// adapted from code at http://aidanlister.com/repos/v/function.size_readable.php

		$sizes = array('bytes', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
		if ( NULL === $retstring )
		{
			$retstring = '%01.2f %s';
		}

		$lastsizestring = end( $sizes );

		foreach ( $sizes as $sizestring )
		{
	       	if ( 1024 > $size )
	       	{
	       		break;
	       	}

	       	if ( $sizestring !== $lastsizestring )
	       	{
	       		$size /= 1024;
	       	}
		}

		if ( $sizestring === $sizes[0] )
		{
			$retstring = '%01d %s';
		} // Bytes aren't normally fractional
		return sprintf( $retstring, $size, $sizestring );
	}

	// ------------------------------------------------------------------------

	/**
	 * Returns human read-able time.
	 * @param  integer $time
	 * @return string
	 */
	public function get_readable_time( $time )
	{
		$ret = $time;
		$formatter = 0;
		$formats = array('ms', 's', 'm');
		if( $time >= 1000 && $time < 60000 )
		{
			$formatter = 1;
			$ret       = ($time / 1000);
		}
		if( $time >= 60000 )
		{
			$formatter = 2;
			$ret = ($time / 1000) / 60;
		}
		$ret = number_format($ret,3,'.','') . ' ' . $formats[$formatter];
		return $ret;
	}

	// --------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// DISPLAY TO THE SCREEN -- CALL WHEN CODE TERMINATING
	// ------------------------------------------------------------------------

	public function display()
	{
		static::_compile_console();
		static::_compile_file_data();
		static::_compile_memory_data();
		static::_compile_query_data();
		static::_compile_speed_data();
		static::_compile_post();
		static::_compile_get();
		//$this->gather_console_data();
		//$this->gather_file_data();
		//$this->gather_memory_data();
		//$this->gather_query_data();
		//$this->gather_speed_data();
		//$this->output['console'] = $this->_compile_console();
//		$this->output['get'] = $this->_compile_get();
//		$this->output['post'] = $this->_compile_post();

		if ( ! class_exists( 'View' ) )
		{
			throw new Exception("View class not loaded", 1);
		}
		$old_view_dir = View::get_old_view_dir();
		$view_dir     = View::get_view_dir();
		$changed      = FALSE;
		if ( strpos( $view_dir, 'admin/' ) !== FALSE )
		{
			View::set_view_dir( $old_view_dir );
			$changed = TRUE;
		}
		View::show( '_profiler', array( 'sections' => $this->output ));

		if ( TRUE === $changed )
		{
			View::set_view_dir( $view_dir );
		}
	}

}
