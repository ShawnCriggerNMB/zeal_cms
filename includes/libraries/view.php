<?php
/**
 *  Quick and dirty "template" class for lack of a better name, basically just a fast way to load HTML content from the same directory.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @subpackage View
 * @category core
 * @since  2.5
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */

if ( !defined('BASE') ) die('No Direct Script Access');

// ------------------------------------------------------------------------

class View {

	/**
	 * Template Tags
	 * @access protected
	 * @see View::set_tags()
	 * @var array
	 */
	protected static $tags = array();

	/**
	 * Directory view files are located under.
	 * @access protected
	 * @see View::set_view_directory()
	 * @see View::get_view_dir()
	 * @var string
	 */
	private static $view_dir = '/includes/themes/default/';

	/**
	 * Placeholder for original view file directory.
	 * @access protected
	 * @see View::set_view_directory()
	 * @see View::get_view_dir()
	 * @var string
	 */
	private static $old_view_dir = '/includes/themes/default/';


	// ------------------------------------------------------------------------

	/**
	 * View constructor
	 * @param array $tags
	 */
	public function __construct( $tags = array() )
	{
		self::$tags = $tags;
	}

	// ------------------------------------------------------------------------

	/**
	 * Sets view file dir
	 * @static
	 * @param string $dir
	 */
	public static function set_view_dir( $dir = NULL )
	{
		self::$old_view_dir = self::$view_dir;
		self::$view_dir = $dir;
		return get_called_class();
	}

	public static function get_view_dir()
	{
		return self::$view_dir;
	}

	public static function get_old_view_dir()
	{
		return self::$old_view_dir;
	}


	// ------------------------------------------------------------------------

	/**
	 * Sets Default Tags to be merged with any tags sent via View::show
	 * @static
	 * @param array $tags array of tags
	 */
	public static function set_tags( $tags = array() )
	{
		self::$tags = array_merge( self::$tags, $tags );
	}

	// ------------------------------------------------------------------------

	/**
	 * Loads $file from view directory, extracts $args and either echos or returns the content.
	 *
	 * Filename is filtered by "view_file_{$file}" and filename can be changed using that hook.
	 * File content is filtered by "{$file}_content" and file content can be changed using that hook.
	 *
	 * @static
	 * @param  string  $file [description]
	 * @param  array   $args [description]
	 * @param  boolean $echo [description]
	 * @return mixed
	 */
	public static function show( $file = '', $args = array(), $echo = TRUE )
	{
		global $siteurl, $sitedir, $current_user, $db;

		$_action = $file;
		$file   = site_dir( self::$view_dir . $file . '.php');

		$args   = array_merge( self::$tags, (array) $args );
		$module_name = 'pages';

		if ( isset( $_GET['tool'] ) )
		{
			$module_name = isset( $_GET['tool'] ) ? $_GET['tool'] : 'dashboard';
		} elseif ( isset( $args['module'] ) )
		{
			$module_name = $args['module'];
		}
		$module_name = str_replace( array( '..', '.', '/' ), '', $module_name );

		$module_path = site_dir( '/includes/modules/'.$module_name.'/views/' . $_action . '.php' );
		$module_id_path = $module_path;
		$file_id_path   = $module_path;
		if ( isset( $args['id'] ) )
		{
			$module_id_path = site_dir( "/includes/modules/{$module_name}/views/{$_action}-{$args['id']}.php" );
			$file_id_path   = site_dir( self::$view_dir . "{$_action}-{$args['id']}.php" );
		}

		if ( file_exists( $module_id_path ) )
		{
			$file = $module_path;
		} elseif ( file_exists( $module_path ) )
		{
			$file = $module_path;
		} elseif ( file_exists( $file_id_path ) )
		{
			$file = $file_id_path;
		}

		$file   = apply_filters( "view_file_{$_action}", $file );

		if ( ! file_exists( $file ) ) cms_die('view not found: ' . $file );
		//$args = array_merge( $tags, $args );
		extract( $args, EXTR_SKIP );

		ob_start();
		require( $file );
		$output = apply_filters( "{$_action}_content", ob_get_clean() );

		if ( function_exists( 'do_shortcode' ) )
		{
			$output = do_shortcode( $output );
		}

		if ( TRUE === $echo )
		{
			echo $output;
			return;
		}

		return $output;
	}

	// ------------------------------------------------------------------------

	/**
	 * Parses template tags, similar to View::show just replaces tags like {{tag}} with $tag, etc.
	 *
	 * @param  string    $file  view file to load
	 * @param  array     $args  template tags to parse.
	 * @param  [boolean] $echo  whether to return or echo.
	 * @return mixed
	 */
	public static function parse( $file = '', $args = array(), $echo = FALSE )
	{
		global $siteurl, $sitedir, $current_user, $db;

		$action = $file;
		$module_name = isset( $_GET['tool'] ) ? $_GET['tool'] : 'dashboard';
		$file   = site_dir( self::$view_dir . $file . '.php');
		$module_path = ABSPATH . '/includes/modules/{$module_name}/views/' . $action . '.php';
		if ( file_exists( $module_path ) )
		{
			$file = $module_path;
		}

		$file   = apply_filters( "view_file_{$action}", $file );

		if ( ! file_exists( $file ) ) cms_die('view not found: ' . $file );
		self::$tags['siteurl'] = $siteurl;
		self::$tags['sitedir'] = $sitedir;
		self::$tags['current_user'] = $current_user;

		$args   = array_merge( self::$tags, $args );

		ob_start();
		require( $file );
		$input  = apply_filters( "{$action}_content", ob_get_clean() );
		$search = preg_match_all( '/{.*?}/', $input, $matches );
		for($i = 0; $i < $search; $i++)
		{
			$matches[0][$i] = str_replace( array( '{', '}' ), NULL, $matches[0][$i] );
		}
		foreach($matches[0] as $value)
		{
			$input = str_replace( '{' . $value . '}', $args[ $value ], $input );
		}

		if ( TRUE === $echo )
		{
			echo $input;
			return;
		}

		return $input;

	}// parse_variables()

}
