<?php
/**
 * SC-Tools are a few random classes I wrote over the years to make my life easier.  If it makes your life easier consider buying me a beer or a coffee would be better.
 *
 * @author Shawn Crigger
 * @version 0.1.5
 * @copyright October, 25, 2014
 * @package SC-Tools
 **/

/**
 * A form helper class to create admin module forms quick and easy.
 *
 * @version 0.1.5
 * @package SC-Tools
 * @copyright October, 25, 2014
 * @author Shawn Crigger
 **/
class Form {


	// ------------------------------------------------------------------------

	/**
	 * Echos or Returns form input
	 *
	 *<code>
	 *
	 * 	$args = array(
	 * 		'name'        => 'input_name',     // form input's name
	 * 		'id'          => 'input_id',       // form input's id, if not set uses name.
	 * 		'label'       => 'Some text',      // text for label
	 * 		'value'       => 'value',          // input value
	 * // everything below here is optional.
	 * 		'type'        => 'text',           // optional input type, defaults to text, can be url, email, etc.
	 * 		'class'       => 'some-classes',   // classes for form input element
	 * 		'size'        => 45,               // size of form element, defaults to 45
	 * 		'tab_index'   => '2',              // tab-index of form element.
	 * 		'required'    => '',               // creates the required * in front of label.
	 * 		'label_class' => 'label classes',  // classes for form label element.
	 * 		'return'      => FALSE             // TRUE to return, FALSE or not set to echo.
	 * 	);
	 *
	 *  Form::text( $args );
	 *
	 *</code>
	 * @static
	 * @param  array  $args array of arguments required to create input.
	 * @return void|string
	 */
	public static function input( $args = array() )
	{
		$args = self::_clean_args( $args );
		extract( $args );

		if ( ! isset( $type ) )
		{
			$type = 'text';
		} else {
			$type = esc_attr( $type );
		}

		$html = <<<EOL
			  <div class="form-group">
			    <label for="{$id}" class="col-sm-3 control-label {$label_class}">{$required}{$label}</label>
			    <div class="col-sm-7">
			      <input {$style} {$data} name="{$name}" id="{$id}" value="{$value}" type="{$type}" class="{$class} form-control" {$placeholder} size="{$size}" {$tab_index} >
			      {$help_text}
			    </div>
			  </div>
EOL;
/*
		$html = "
		\t<tr>\n
      		\t\t<td>\t\t\t<label {$label_class} for=\"{$id}\">{$required}{$label}\t\t</label>\t</td>\n
        	\t\t<td>\t\t\t<input type=\"{$type}\" name=\"{$name}\" id=\"{$id}\" value=\"{$value}\" size=\"{$size}\" {$tab_index} {$class} {$placeholder} />\t\t</td>\n
      	\t\t{$help_text}\n
      	\t</tr>\n";
*/

      	if ( isset( $return ) && TRUE === $return )
      	{
      		return $html;
      	}

      	echo $html;
	}

	// ------------------------------------------------------------------------

	/**
	 * Creates select element by using $args['fields'], requires a array of objects in id=>name format, id will be checked to match $args['value']
	 * @param  array  $args
	 * @return void|string
	 */
	public static function select( $args = array() )
	{
		$args = self::_clean_args( $args );
		extract( $args );

		if ( ! isset( $fields ) OR ! is_array( $fields ) ) return FALSE;

		$options = '<option>Please select</option>';

		foreach ( $fields as $field )
		{
			$field = (array) $field;
			$key   = array_shift( $field );
			$value = array_shift( $field );

			$selected = selected( $args['value'], $key, FALSE );
			$options .= "\n\t\t\t\t\t<option {$selected} value=\"{$key}\">{$value}</option>\n";
		}

		unset( $selected );
/*
		$html = "
		\t<tr>\n
      		\t\t<td>\t\t\t<label {$label_class} for=\"{$id}\">{$required}{$label}\t\t</label>\t</td>\n
        	\t\t<td>\t\t\t<select name=\"{$name}\" id=\"{$id}\" {$tab_index} {$class} />\t\t{$options}</select>\n
      	\t\t{$help_text}</td>\n
      	\t</tr>\n";
*/

      	$html = <<<EOL
      		<div class="form-group">
      			<label class="{$label_class} col-sm-3 control-label">{$required} {$label}</label>
      			<div class="col-sm-6">
      			<select {$style} name="{$name}" id="{$id}" {$tab_index} class="{$class} form-control">
      				{$options}
      			</select>
      			{$help_text}
      			</div>
      		</div>
EOL;

      	if ( isset( $return ) && TRUE === $return )
      	{
      		return $html;
      	}

      	echo $html;

	}

	// ------------------------------------------------------------------------

	public static function textarea( $args = array() )
	{
		$args = self::_clean_args( $args );
		extract( $args );

		$rows = isset( $rows ) ? $rows : '5';
		$cols = isset( $cols ) ? $cols : '70';
		$html = <<<EOL
			  <div class="form-group">
			    <label for="{$id}" class="col-sm-3 control-label {$label_class}">{$required}{$label}</label>
			    <div class="col-sm-9">
			    	<textarea name="{$name}" id="{$id}" class="form-control {$class}" cols="{$cols}" rows="{$rows}" {$placeholder} {$tab_index} >{$value}</textarea>
			      {$help_text}
			    </div>
			  </div>
EOL;
      	if ( isset( $return ) && TRUE === $return )
      	{
      		return $html;
      	}

      	echo $html;

	}

	// ------------------------------------------------------------------------

	/**
	 * Creates hidden form input with $args['name'] and $args['value']
	 * @static
	 * @return void|string
	 */
	public static function hidden( $args = array() )
	{
		$args = self::_clean_args( $args );
		extract( $args );
		$html = "\t<input type=\"hidden\" name=\"{$name}\" value=\"{$value}\" />\n";
      	if ( isset( $return ) && TRUE === $return )
      	{
      		return $html;
      	}

      	echo $html;
	}

	// ------------------------------------------------------------------------

	public static function submit( $args = array() )
	{
		$args = self::_clean_args( $args );
		extract( $args );

		$name  = ( isset( $name )  && ! is_null( $name )  ) ? $name  : 'submit';
		$value = ( isset( $value ) && ! is_null( $value ) ) ? $value : 'Submit';
		$style = ( isset( $style ) && ! is_null( $style ) ) ? $style : 'padding:3px';
		$html = <<<EOL

		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-10">
				<input class="btn btn-success {$class}" type="submit" name="{$name}" id="{$id}" value="{$value}" {$tab_index} />
			</div>
		</div>
EOL;
      	if ( isset( $return ) && TRUE === $return )
      	{
      		return $html;
      	}

      	echo $html;

	}

	// ------------------------------------------------------------------------

	/**
	 * Creates the form openning tag, added args are method and enc_type
	 * @param  array  $args
	 * @return string|void
	 */
	public static function open( $args = array() )
	{
		$args = self::_clean_args( $args );
		extract( $args );

		if ( ! isset( $class ) OR empty( $class ) )
		{
			$class = ' form-horizontal ';
		}

		if ( ! isset( $name ) OR empty( $name ) )
		{
			$name = 'form';
		}

		$html = <<<EOL

			<form {$method} {$action} class="{$class}" name="{$name}" id="{$id}" {$enc_type} >
EOL;


      	if ( isset( $return ) && TRUE === $return )
      	{
      		return $html;
      	}

      	echo $html;
	}

	// ------------------------------------------------------------------------

	/**
	 * Creates the form closing tag.
	 * @return string|void
	 */
	public static function close()
	{
		$html = "\t\t</form>\n";
      	if ( isset( $return ) && TRUE === $return )
      	{
      		return $html;
      	}

      	echo $html;
	}

	// ------------------------------------------------------------------------

	/**
	 * Cleans and sets default arguments used internally in class only.
	 * @static
	 * @access private
	 * @param  array  $args
	 * @return array
	 */
	private static function _clean_args( $args = array() )
	{
		$args = (array) $args;

		if ( isset( $args['name'] ) )
		{
			$args['name']  = esc_attr( $args['name'] );
		} else {
			$args['name'] = NULL;
		}


		if ( isset( $args['label'] ) )
		{
			$args['label'] = esc_html( $args['label'] );
		}

		if ( isset( $args['value'] ) && ! is_null( $args['value'] ) )
		{
			$args['value'] = esc_html( $args['value'] );
		} else {
			$args['value'] = NULL;
		}

		if ( ! isset( $args['type'] ) )
		{
			$args['type'] = 'text';
		}
		if ( ! isset( $args['id'] ) )
		{
			$args['id'] = $args['name'];
		}
		if ( ! isset( $args['required'] ) )
		{
			$args['required'] = NULL;
		} else
		{
			$args['required'] = $args['required'];
		}

		if ( isset( $args['placeholder'] ) )
		{
			$args['placeholder'] = esc_attr( $args['placeholder'] );
			$args['placeholder'] = " placeholder=\"{$args['placeholder']}\" ";
		} else {
			$args['placeholder'] = NULL;
		}

		if ( isset( $args['class'] ) )
		{
			$args['class'] = esc_attr( $args['class'] );
//			$args['class'] = " class=\"{$args['class']}\" ";
		} else {
			$args['class'] = NULL;
		}

		if ( isset( $args['label_class'] ) )
		{
			$args['label_class'] = esc_attr( $args['label_class'] );
//			$args['label_class'] = " class=\"{$args['label_class']}\" ";
		} else {
			$args['label_class'] = NULL;
		}

		if ( isset( $args['help_text'] ) )
		{
			$args['help_text'] = "<span class=\"help-block\">{$args['help_text']}</span>";
		} else {
			$args['help_text'] = NULL;
		}


		if ( isset( $args['style'] ) )
		{
			$args['style'] = " style=\"{$args['style']}\" ";
		} else {
			$args['style'] = NULL;
		}


		if ( isset( $args['method'] ) )
		{
			$args['method'] = strtoupper( $args['method'] );
			$args['method'] = " method=\"{$args['method']}\" ";
		} else {
			$args['method'] = " method=\"POST\" ";
		}

		if ( isset( $args['action'] ) )
		{
			$args['action'] = " action=\"{$args['action']}\" ";
		} else {
			$args['action'] = NULL;
		}

		if ( isset( $args['enc_type'] ) )
		{
			$args['enc_type'] = " enctype=\"application/x-www-form-urlencoded\" ";
		} else {
			$args['enc_type'] = " enctype=\"multipart/form-data\" ";
		}

		if ( ! isset( $args['data'] ) )
		{
			$args['data'] = NULL;
		}

		$args['size']      = isset( $args['size'] )      ? $args['size'] : 45;
		$args['tab_index'] = isset( $args['tab_index'] ) ? ' tabindex="' . esc_attr( $args['tab_index'] ) . '" ' : NULL;;


		return $args;
	}

	// ------------------------------------------------------------------------


}// end Form()
