<?php if ( !defined('BASE') ) die('No Direct Script Access');

// basic init - include core files and establish a db session only.
// this is designed to work for non-web scripts also, so authentication needs to be handled separately for web pages
$sitedir = '/Users/shawnc/Sites/cmsv2';
if ( !defined('ABSPATH') )
	define( 'ABSPATH', $sitedir );
if ( !defined('BACKPRESS_PATH') )
	define( 'BACKPRESS_PATH', ABSPATH . '/includes/zeal/' );
if ( !defined('ZEAL_PATH') )
	define( 'ZEAL_PATH', ABSPATH . '/includes/zeal/' );

$time_start = microtime( true );

//require_once( ABSPATH . '/includes/misc.php' );
//time_since(true);

if ( is_file( ABSPATH . '/config.php' ) )
	include_once( ABSPATH . '/config.php' );
elseif ( is_file( dirname( dirname(__FILE__) ) . '/cms-config.php' ) )
	include_once( dirname( dirname(__FILE__) ) . '/cms-config.php' );
else
	define( 'IS_INSTALLING', true );

$error_level = error_reporting();

require( ABSPATH . '/includes/cms-vars.php' );
require( ABSPATH . '/includes/ChromeLog.php' );
require( ZEAL_PATH . '/functions.cms-load.php' );
//require_once( ABSPATH . '/lib/constants.php' );


// Verify server has correct PHP version and MySQL version to run this CMS.
global $cms_version, $cms_db_version, $required_php_version, $required_mysql_version;
require( ABSPATH . '/includes/version.php' );

// Check for the required PHP version and for the MySQL extension or a database drop-in.
cms_check_php_mysql_versions();

// Disable magic quotes at runtime. Magic quotes are added using wpdb later in wp-settings.php.
@ini_set( 'magic_quotes_runtime', 0 );
@ini_set( 'magic_quotes_sybase',  0 );

// WordPress calculates offsets from UTC.
date_default_timezone_set( 'UTC' );


// Turn register_globals off.
cms_unregister_GLOBALS();

// Standardize $_SERVER variables across setups.
cms_fix_server_vars();

// Check if we have received a request due to missing favicon.ico
cms_favicon_request();

// Start loading timer.
timer_start();

// Register action 'shutdown_action_hook' to be run directly before PHP script is finished on shutdown.
register_shutdown_function( 'shutdown_action_hook' );




require( ABSPATH . '/includes/wp-functions.php' );

// temporarily suppress deprecation warnings from backpress
//if ( defined( 'E_DEPRECATED' ) )
//	error_reporting( $error_level & ~E_DEPRECATED & ~E_STRICT );
error_reporting( E_ALL );

require( ZEAL_PATH . '/functions.compat.php' );
require( ZEAL_PATH . '/functions.formatting.php' );
require( ZEAL_PATH . '/functions.core.php' );
require( ZEAL_PATH . '/functions.kses.php' );
require( ZEAL_PATH . '/functions.plugin-api.php' );

require( ZEAL_PATH . '/class.cms-error.php' );

//Define the full path to the database class
if ( !defined( 'SP_DATABASE_CLASS_INCLUDE' ) ) {
	define( 'SP_DATABASE_CLASS_INCLUDE', ZEAL_PATH . '/class.bpdb-multi.php' );
}

// Define the name of the database class
if ( !defined( 'SP_DATABASE_CLASS' ) ) {
	define( 'SP_DATABASE_CLASS', 'BPDB_Multi' );
}

// Load the database class
if ( SP_DATABASE_CLASS_INCLUDE ) {
	require( SP_DATABASE_CLASS_INCLUDE );
}

if ( defined('DB_HOST') ) {
	$spdb_class = SP_DATABASE_CLASS;
	$ndb = new $spdb_class ( array(
		'name' => DB_NAME,
		'host' => DB_HOST,
		'user' => DB_USER,
		'password' => DB_PASSWORD,
		'errors' => 'suppress',
	) );


	$ndb->set_prefix($table_prefix);
	// standard prefix for SupportPress tables
	$ndb->set_prefix(
		$table_prefix,
		array(
			'options',
			'modules',
			'posts',
			'users',
			'usermeta',
			'pages',
		)
	);

	$ndb->field_types = array(
		'post_id' => '%d', 'team_id' => '%d',
		'comment_id' => '%d', 'uid' => '%d', 'id' => '%d',
	);

	// use cms_ for user tables
	if ( isset($user_table_prefix) )
		$ndb->set_prefix( $user_table_prefix, array('users', 'usermeta') );

	// optional separate config for user db
	if ( defined('USER_DB_HOST') ) {
		$ndb->add_db_server( 'user', array(
			'name' => USER_DB_NAME,
			'host' => USER_DB_HOST,
			'user' => USER_DB_USER,
			'password' => USER_DB_PASSWORD,
			'errors' => 'suppress',
		));

		// these two tables are in the user db
		$ndb->add_db_table( 'user', $ndb->users );
		$ndb->add_db_table( 'user', $ndb->usermeta );
	}
}

// Compatibility with WP function global object usage
global $wpdb;
$wpdb = &$ndb;
$GLOBALS['wpdb'] = $wpdb;

// Compatibility with BP function global object usage
global $bpdb;
$bpdb = &$ndb;

require( ZEAL_PATH . 'functions.cms-options.php' );
require( ZEAL_PATH . 'class.cms-options.php' );
require( ZEAL_PATH . 'class.cms-log.php' );


$memcache = NULL;
if ( class_exists('memcache') )
{
	$memcache = '-memcached';
}

require( ZEAL_PATH . "class.cms-object-cache{$memcache}.php" );
require( ZEAL_PATH . 'functions.cms-object-cache.php' );


require( ZEAL_PATH . 'class.cms-pass.php' );
require( ZEAL_PATH . 'class.cms-roles.php' );

// load core users class
require( ZEAL_PATH . 'class.cms-users.php' );

// load user class
require( ZEAL_PATH . 'class.cms-user.php' );

// auth code class
require( ZEAL_PATH . 'class.cms-auth.php' );

require( ABSPATH . '/includes/cms-meta.php' );
//require_once( ABSPATH . '/includes/wp-functions.php' );
require( ABSPATH . '/includes/script-loader.php' );
require( ZEAL_PATH . 'functions.shortcodes.php' );

require( ABSPATH . '/vendor/autoload.php' );
require( ABSPATH . '/includes/libraries/load.php' );

$null = NULL;

Load::library( 'view', $null, TRUE );
Load::library( 'form', $null, TRUE );
Load::library( 'notify', $null, TRUE );
Load::library( 'profiler', $GLOBALS['profiler'] );

$GLOBALS['profiler']->set_start_time( Profiler::get_micro_time() );

$GLOBALS['db'] = $db;

add_action( 'shutdown', function() {
	global $current_user;
	if ( ! is_object( $current_user ) ) return;
	if ( ! $current_user->has_cap( 'superadmin' ) ) return;
	$GLOBALS['profiler']->display();
});


/*
if ( array_key_exists('debug', $_GET ) )
{
}
*/
//require_once( ABSPATH . '/includes/form.php' );
//require_once( ABSPATH . '/includes/support-functions.php' );
//require_once( ABSPATH . '/includes/upgrade.php' );


// Load plugins
//require_once( 'includes/plugin.php' );
//sp_load_plugins( 'plugins/' );
//if ( defined( 'EXTRA_PLUGINS' ) && is_dir( EXTRA_PLUGINS ) )
//sp_load_plugins( EXTRA_PLUGINS );

//error_reporting( E_ALL );
cms_cache_init();







add_filter( 'sanitize_title', 'sanitize_title_with_dashes' );


// auth functions die if this isn't set
// when user password is encrypted it calls the following function.
cms_add_option( 'hash_function_name', 'sp_hash' );
//echo ( cms_get_option( 'hash_function_name' ) ); die();
//CMS_Options::test();
//die();


function __( $str ) {
	return $str;
}

function _e( $str ) {
	echo $str;
}

function _x( $str ) {
	return $str;
}

function _ex( $str ) {
	echo $str;
}

/**
 * Password hashing function, more secure then a normal un-hashed md5
 * @param  string $s string to encrypt
 * @return string
 */
function sp_hash( $s ) {
	return CMS_Pass::hash_password( $s );
}


/**
 * action fired after this file is loaded. for loading any files between this file and the init.php file.
 */
do_action( 'cms_loaded' );

//$wp_styles  = new WP_Styles();
//$wp_scripts = new WP_Scripts();
