<?php
	$fileCount  = count($sections['files']);
	$memoryUsed = $sections['memory_totals']['used'];
	$queryCount = $sections['query_totals']['count'];
	$speedTotal = $sections['speed_totals']['total'];

	$speedTotal = number_format( (float) $speedTotal , 2);
	$sections['console']['log_count'] = count($sections['console']['log_count']);

	/*
		The location of the profiler bar. Valid locations are:
			- bottom-left
			- bottom-right
			- top-left
			- top-right
			- bottom
			- top
	 */
	$bar_location = 'bottom-right';
?>

<style type="text/css">
	#zeal-profiler { clear: both; background: #222; padding: 0 5px; font-family: Helvetica, sans-serif; font-size: 10px !important; line-height: 12px; position: absolute; width: auto; min-width: 74em; max-width: 90%; z-index: 1000; display: none; }

	#zeal-profiler.bottom-right { position: fixed; bottom:0; right: 0; -webkit-border-top-left-radius: 7px; -moz-border-radius-topleft: 7px; border-top-left-radius: 7px; -webkit-box-shadow: -1px -1px 10px #999; -moz-box-shadow: -1px -1px 10px #999; box-shadow: -1px -1px 10px #999; }
	#zeal-profiler.bottom-left { position: fixed; bottom:0; left: 0; -webkit-border-top-right-radius: 7px; -moz-border-radius-topright: 7px; border-top-right-radius: 7px; -webkit-box-shadow: 1px -1px 10px #999; -moz-box-shadow: 1px -1px 10px #999; box-shadow: 1px -1px 10px #999; }
	#zeal-profiler.top-left { position: fixed; top:0; left: 0; -webkit-border-bottom-right-radius: 7px; -moz-border-radius-bottomright: 7px; border-bottom-right-radius: 7px;-webkit-box-shadow: 1px 1px 10px #999; -moz-box-shadow: 1px 1px 10px #999; box-shadow: 1px 1px 10px #999; }
	#zeal-profiler.top-right { position: fixed; top: 0; right: 0; -webkit-border-bottom-left-radius: 7px; -moz-border-radius-bottomleft: 7px; border-bottom-left-radius: 7px; -webkit-box-shadow: -1px 1px 10px #999; -moz-box-shadow: -1px 1px 10px #999; box-shadow: -1px 1px 10px #999; }
	#zeal-profiler.bottom { position: fixed; bottom: 0; left: 0; right: 0; width: 100%; max-width: 99.5%; -webkit-box-shadow: 0px 1px 10px #999; -moz-box-shadow: 0px 1px 10px #999; box-shadow: 0px 1px 10px #999; }
	#zeal-profiler.top { position: fixed; top: 0; left: 0; right: 0; width: 100%; max-width: 99.5%; -webkit-box-shadow: -1px 1px 10px #999; -moz-box-shadow: -1px 1px 10px #999; box-shadow: -1px 1px 10px #999; }

	.cms-profiler-box { padding: 10px; margin: 0 0 10px 0; max-height: 400px; overflow: auto; color: #fff; font-family: Monaco, 'Lucmsda Console', 'Courier New', monospace; font-size: 11px !important; }
	.cms-profiler-box h2 { font-family: Helvetica, sans-serif; font-weight: bold; font-size: 16px !important; padding: 0; line-height: 2.0; }

	#cms-profiler-vars a { text-decoration: none; }

	#cms-profiler-menu a:link, #cms-profiler-menu a:visited { display: inline-block; padding: 7px 0; margin: 0; color: #ccc; text-decoration: none; font-weight: lighter; cursor: pointer; text-align: center; width: 13%; border-bottom: 4px solid #444; }
	#cms-profiler-menu a:hover, #cms-profiler-menu a.current { background-color: #222; border-color: #999; }
	#cms-profiler-menu a span { display: block; font-weight: bold; font-size: 16px !important; line-height: 1.2; }

	#cms-profiler-menu-time span, #cms-profiler-benchmarks h2 { color: #B72F09; }
	#cms-profiler-menu-memory span, #cms-profiler-memory h2 { color: #953FA1; }
	#cms-profiler-menu-queries span, #cms-profiler-queries h2 { color: #3769A0; }
	#cms-profiler-menu-eloquent span, #cms-profiler-eloquent h2 { color: #f4726d; }
	#cms-profiler-menu-vars span, #cms-profiler-vars h2 { color: #D28C00; }
	#cms-profiler-menu-files span, #cms-profiler-files h2 { color: #5a8616; }
	#cms-profiler-menu-console span, #cms-profiler-console h2 { color: #5a8616; }

	#zeal-profiler table { width: 100%; }
	#zeal-profiler table.main td { padding: 7px 15px; text-align: left; vertical-align: top; color: #aaa; border-bottom: 1px dotted #444; line-height: 1.5; background: #101010 !important; font-size: 12px !important; }
	#zeal-profiler table.main tr:hover td { background: #292929 !important; }
	#zeal-profiler table.main code { font-family: inherit; padding: 0; background: transparent; border: 0; color: #fff; }

	#zeal-profiler table .hilight { color: #FFFD70 !important; }
	#zeal-profiler table .faded { color: #aaa !important; }
	#zeal-profiler table .small { font-size: 10px; letter-spacmsng: 1px; font-weight: lighter; }

	#cms-profiler-menu-exit { background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIhSURBVDjLlZPrThNRFIWJicmJz6BWiYbIkYDEG0JbBiitDQgm0PuFXqSAtKXtpE2hNuoPTXwSnwtExd6w0pl2OtPlrphKLSXhx07OZM769qy19wwAGLhM1ddC184+d18QMzoq3lfsD3LZ7Y3XbE5DL6Atzuyilc5Ciyd7IHVfgNcDYTQ2tvDr5crn6uLSvX+Av2Lk36FFpSVENDe3OxDZu8apO5rROJDLo30+Nlvj5RnTlVNAKs1aCVFr7b4BPn6Cls21AWgEQlz2+Dl1h7IdA+i97A/geP65WhbmrnZZ0GIJpr6OqZqYAd5/gJpKox4Mg7pD2YoC2b0/54rJQuJZdm6Izcgma4TW1WZ0h+y8BfbyJMwBmSxkjw+VObNanp5h/adwGhaTXF4NWbLj9gEONyCmUZmd10pGgf1/vwcgOT3tUQE0DdicwIod2EmSbwsKE1P8QoDkcHPJ5YESjgBJkYQpIEZ2KEB51Y6y3ojvY+P8XEDN7uKS0w0ltA7QGCWHCxSWWpwyaCeLy0BkA7UXyyg8fIzDoWHeBaDN4tQdSvAVdU1Aok+nsNTipIEVnkywo/FHatVkBoIhnFisOBoZxcGtQd4B0GYJNZsDSiAEadUBCkstPtN3Avs2Msa+Dt9XfxoFSNYF/Bh9gP0bOqHLAm2WUF1YQskwrVFYPWkf3h1iXwbvqGfFPSGW9Eah8HSS9fuZDnS32f71m8KFY7xs/QZyu6TH2+2+FAAAAABJRU5ErkJggg==) 0% 0% no-repeat; padding-left: 20px; position: absolute; right: 5px; top: 10px; display:none; }
	#cms-profiler-menu-open { background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAc9JREFUeNp8kr9O21AUxr9j06iqhGSBiqKypAMrIjtLBuaWNzBPEPIEJEvHInZEeAMyVJU6VMnUsY3asamULk1L/mAHSGzg3sO5l7hyAsmRvjj2Pb/vu+faYGbMq5sfL+oi76k1w1l2noEBRSyqLjJwML8O7K8mf0EPyLgQ0Wy6L2AVty4QPUNmu09P7cDU0qOtf132EQksIEeu1aKaGuHmy4qP60yVg+fQQQbKKFxqmLXw/Wtv4Qjx55c+lFPlyIGWVB07kk7g2GmrIRWgUNdjqq2++1VKj2AN4g/rOdb4Js2eFgM2cEyBjuBZEyvYqx7hdO2ktTd1BurKLfIteTY9ngB32OVrOhNQTOV+LAYjK7+zs/FbsPL/M1BD960KXZlXDAJJCUU92tJXyKuAGrovb7Mn6srzf2LWRXHqEHXo5JQBJ1IXVoeqQ1g7bhV4gIr+a0FgZAB4UwZKEjkBQ6oliXz50Jj91CpjjAp4zmvUFxSogaQP0JbEXR4iz5eUz35sNZPGV99/llNcLfljD1HSauZweExtm5gCk/qzuZFL3R7N7AAlfU5N7mFrpjFdh5Prnuym8ehDEtDMuy96M2lqptINbNYr8ryd/pDuBRgABwcgCJ3Gp98AAAAASUVORK5CYII%3D) 0% 0% no-repeat; z-index: 10000; }

	#cms-profiler-menu-open.bottom-right { position: fixed; right: -2px; bottom: 22px; }
	#cms-profiler-menu-open.bottom-left { position: fixed; left: 10px; bottom: 22px; }
	#cms-profiler-menu-open.top-left { position: fixed; left: 10px; top: 22px; }
	#cms-profiler-menu-open.top-right { position: fixed; right: -2px; top: 22px; }
</style>

<script type="text/javascript">
var cms_profiler_bar = {

	// current toolbar section thats open
	current: null,

	// current vars and config section open
	currentvar: null,

	// current config section open
	currentli: null,

	// toggle a toolbar section
	show : function(obj, el) {
		if (obj == cms_profiler_bar.current) {
			cms_profiler_bar.off(obj);
			cms_profiler_bar.current = null;
		} else {
			cms_profiler_bar.off(cms_profiler_bar.current);
			cms_profiler_bar.on(obj);
			cms_profiler_bar.remove_class(cms_profiler_bar.current, 'current');
			cms_profiler_bar.current = obj;
			//cms_profiler_bar.add_class(el, 'current');
		}
	},

	// turn an element on
	on : function(obj) {
		if (document.getElementById(obj) != null)
			document.getElementById(obj).style.display = '';
	},

	// turn an element off
	off : function(obj) {
		if (document.getElementById(obj) != null)
			document.getElementById(obj).style.display = 'none';
	},

	// toggle an element
	toggle : function(obj) {
		if (typeof obj == 'string')
			obj = document.getElementById(obj);

		if (obj)
			obj.style.display = obj.style.display == 'none' ? '' : 'none';
	},

	// open the toolbar
	open : function() {
		document.getElementById('cms-profiler-menu-open').style.display = 'none';
		document.getElementById('zeal-profiler').style.display = 'block';
		this.set_cookie('open');
	},

	// close the toolbar
	close : function() {
		document.getElementById('zeal-profiler').style.display = 'none';
		document.getElementById('cms-profiler-menu-open').style.display = 'block';
		this.set_cookie('closed');
	},

	// Add class to element
	add_class : function(obj, a_class) {
		alert(obj);
		document.getElementById(obj).className += " "+ a_class;
	},

	// Remove class from element
	remove_class : function(obj, r_class) {
		if (obj != undefined) {
			document.getElementById(obj).className = document.getElementById(obj).className.replace(/\bclass\b/, '');
		}
	},

	read_cookie : function() {
		var nameEQ = "Profiler=";
		var ca = document.cookie.split(';');
		for (var i=0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	},

	set_cookie : function(value) {
		var date = new Date();
		date.setTime(date.getTime() + (365*24*60*60*1000));
		var expires = "; expires=" + date.toGMTString();

		document.cookie = "Profiler=" + value + expires + "; path=/";
	},

	set_load_state : function() {
		var cookie_state = this.read_cookie();

		if (cookie_state == 'open') {
			this.open();
		} else {
			this.close();
		}
	},

	toggle_data_table : function(obj) {
		if (typeof obj == 'string') {
			obj = document.getElementById(obj + '_table');
		}

		if (obj) {
			obj.style.display = obj.style.display == 'none' ? '' : 'none';
		}
	}
};

window.onload = function() {
	cms_profiler_bar.set_load_state();
}
</script>

<a href="#" id="cms-profiler-menu-open" class="<?php echo $bar_location ?>" onclick="cms_profiler_bar.open(); return false;" style="width: 2em">&nbsp;</a>

<div id="zeal-profiler" class="<?php echo $bar_location ?>">

	<div id="cms-profiler-menu">

		<!-- Console -->
		<?php if ( isset( $sections['console'] ) ) : ?>
			<a href="#" id="cms-profiler-menu-console" onclick="cms_profiler_bar.show('cms-profiler-console', 'cms-profiler-menu-console'); return false;">
				<span><?php echo is_array($sections['console']) ? $sections['console']['log_count'] + $sections['console']['memory_count'] : 0 ?></span>
				Console
			</a>
		<?php endif; ?>

		<!-- Benchmarks -->
		<?php if (isset($sections['benchmarks'])) :?>
			<a href="#" id="cms-profiler-menu-time" onclick="cms_profiler_bar.show('cms-profiler-benchmarks', 'cms-profiler-menu-time'); return false;">
				<span><?php echo $speedTotal; ?> s</span>
				<?php //echo $this->benchmark->elapsed_time('total_execution_time_start', 'total_execution_time_end') ?>
				Load Time
			</a>
			<a href="#" id="cms-profiler-menu-memory" onclick="cms_profiler_bar.show('cms-profiler-memory', 'cms-profiler-menu-memory'); return false;">
				<span><?php echo (! function_exists('memory_get_usage')) ? '0' : round(memory_get_usage()/1024/1024, 2).' MB' ?></span>
				Memory Used
			</a>
		<?php endif; ?>

		<!-- Queries -->
		<?php if (isset($sections['queries'])) : ?>
			<a href="#" id="cms-profiler-menu-queries" onclick="cms_profiler_bar.show('cms-profiler-queries', 'cms-profiler-menu-queries'); return false;">
				<span><?php echo is_array($sections['queries']) ? (count($sections['queries']) - 1) : 0 ?> Queries</span>
				Database
			</a>
		<?php endif; ?>

		<!-- Vars and Config -->
		<?php if (isset($sections['http_headers']) || isset($sections['get']) || isset($sections['config']) || isset($sections['post']) || isset($sections['uri_string']) || isset($sections['controller_info'])) : ?>
			<a href="#" id="cms-profiler-menu-vars" onclick="cms_profiler_bar.show('cms-profiler-vars', 'cms-profiler-menu-vars'); return false;">
				<span>vars</span> &amp; Config
			</a>
		<?php endif; ?>

		<!-- Files -->
		<?php if (isset($sections['files'])) : ?>
			<a href="#" id="cms-profiler-menu-files" onclick="cms_profiler_bar.show('cms-profiler-files', 'cms-profiler-menu-files'); return false;">
				<span><?php echo is_array($sections['files']) ? count($sections['files']) : 0 ?></span> Files
			</a>
		<?php endif; ?>

		<a href="#" id="cms-profiler-menu-exit" onclick="cms_profiler_bar.close(); return false;" style="width: 2em; height: 2.1em"></a>
	</div>

<?php if (count($sections) > 0) : ?>

	<!-- Console -->
	<?php if (isset($sections['console'])) :?>
		<div id="cms-profiler-console" class="cms-profiler-box" style="display: none">
			<h2>Console</h2>

			<?php if (is_array($sections['console'])) : ?>

				<table class="main">
				<?php foreach ($sections['console']['console'] as $log) : ?>

					<?php if ($log['type'] == 'log') : ?>
						<tr>
							<td><?php echo $log['type'] ?></td>
							<td class="faded"><pre><?php echo $log['data'] ?></pre></td>
							<td></td>
						</tr>
					<?php elseif ($log['type'] == 'memory')  :?>
						<tr>
							<td><?php echo $log['type'] ?></td>
							<td>
								<em><?php echo $log['data_type'] ?></em>:
								<?php echo $log['name']; ?>
							</td>
							<td class="hilight" style="width: 9em"><?php echo $log['data'] ?></td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>
				</table>

			<?php else : ?>

				<?php echo $sections['console']; ?>

			<?php endif; ?>
		</div>
	<?php endif; ?>

	<!-- Memory -->
	<?php if (isset($sections['console'])) :?>
		<div id="cms-profiler-memory" class="cms-profiler-box" style="display: none">
			<h2>Memory Usage</h2>

			<?php if (is_array($sections['console'])) : ?>

				<table class="main">
				<?php foreach ($sections['console']['console'] as $log) : ?>

					<?php if ($log['type'] == 'memory')  :?>
						<tr>
							<td><?php echo $log['type'] ?></td>
							<td>
								<em><?php echo $log['data_type'] ?></em>:
								<?php echo $log['name']; ?>
							</td>
							<td class="hilight" style="width: 9em"><?php echo $log['data'] ?></td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>
				</table>

			<?php else : ?>

				<?php echo $sections['console']; ?>

			<?php endif; ?>
		</div>
	<?php endif; ?>

	<!-- Benchmarks -->
	<?php if (isset($sections['benchmarks'])) :?>
		<div id="cms-profiler-benchmarks" class="cms-profiler-box" style="display: none">
			<h2>Benchmarks</h2>

			<?php if (is_array($sections['benchmarks'])) : ?>

				<table class="main">
				<?php foreach ($sections['benchmarks'] as $key => $val) : ?>
					<tr><td><?php echo $key ?></td><td class="hilight"><?php echo $val ?></td></tr>
				<?php endforeach; ?>
				</table>

			<?php else : ?>

				<?php echo $sections['benchmarks']; ?>

			<?php endif; ?>
		</div>
	<?php endif; ?>

	<!-- Queries -->
	<?php if (isset($sections['queries'])) :?>
		<div id="cms-profiler-queries" class="cms-profiler-box" style="display: none">
			<h2>Queries</h2>

			<?php if (is_array($sections['queries'])) : ?>

				<table class="main" cellspacmsng="0">
				<?php foreach ($sections['queries'] as $key => $queries) : ?>
					<?php foreach ($queries as $time => $query): ?>
						<tr><td class="hilight"><?php echo $time ?></td><td><?php echo $query ?></td></tr>
					<?php endforeach; ?>
				<?php endforeach; ?>
				</table>

			<?php else : ?>

				<?php echo $sections['queries']; ?>

			<?php endif; ?>
		</div>
	<?php endif; ?>

	<!-- Vars and Config -->
	<?php if (isset($sections['http_headers']) || isset($sections['get']) || isset($sections['config']) || isset($sections['post']) || isset($sections['uri_string']) || isset($sections['controller_info']) || isset($sections['userdata'])) :?>
		<div id="cms-profiler-vars" class="cms-profiler-box" style="display: none">

			<!-- View Data -->
			<?php if (isset($sections['view_data'])) : ?>
				<a href="#" onclick="cms_profiler_bar.toggle_data_table('view_data'); return false;">
					<h2>VIEW DATA</h2>
				</a>

				<?php if (is_array($sections['view_data'])) : ?>

					<table class="main" id="view_data_table">
					<?php foreach ($sections['view_data'] as $key => $val) : ?>
						<tr><td class="hilight"><?php echo $key ?></td><td><?php echo $val ?></td></tr>
					<?php endforeach; ?>
					</table>

				<?php endif; ?>
			<?php endif; ?>

			<!-- User Data -->
			<?php if (isset($sections['userdata'])) :?>
					<a href="#" onclick="cms_profiler_bar.toggle_data_table('userdata'); return false;">
						<h2>SESSION USER DATA</h2>
					</a>

					<?php if (is_array($sections['userdata']) && count($sections['userdata'])) : ?>

						<table class="main" id="userdata_table">
						<?php foreach ($sections['userdata'] as $key => $val) : ?>
							<tr><td class="hilight"><?php echo $key ?></td><td><?php echo $val ?></td></tr>
						<?php endforeach; ?>
						</table>
					<?php endif; ?>
				<?php endif; ?>

			<!-- The Rest -->
			<?php foreach (array('get', 'post', 'uri_string', 'controller_info', 'headers', 'config') as $section) : ?>

				<?php if (isset($sections[$section])) :?>

					<?php $append = ($section == 'get' || $section == 'post') ? '_data' : '' ?>
					<a href="#" onclick="cms_profiler_bar.toggle_data_table('<?php echo $section ?>'); return false;">
						<h2><?php echo $section . $append ?></h2>
					</a>



						<table class="main" id="<?php echo $section ?>_table">
						<?php if (is_array($sections[$section])) : ?>
						<?php foreach ($sections[$section] as $key => $val) : ?>
							<tr><td class="hilight"><?php echo $key ?></td><td><?php echo htmlspecialchars($val) ?></td></tr>
						<?php endforeach; ?>
						<?php else : ?>
							<tr><td><?php echo $sections[$section]; ?></td></tr>
						<?php endif; ?>
						</table>
				<?php endif; ?>

			<?php endforeach; ?>
		</div>
	<?php endif; ?>

	<!-- Files -->
	<?php if (isset($sections['files'])) :?>
		<div id="cms-profiler-files" class="cms-profiler-box" style="display: none">
			<h2>Loaded Files</h2>

			<?php if (is_array($sections['files'])) : ?>

				<table class="main">
				<?php foreach ($sections['files'] as $file) : ?>
					<tr>
						<td class="hilight">
							<?php echo preg_replace("/\/.*\//", "", $file['name']) ?>
							<br/><strong><?=$file['size']?>)</strong>  <span class="faded small"><?php echo str_replace(ABSPATH, '', $file['name']) ?></span>
						</td>
					</tr>
				<?php endforeach; ?>
				</table>

			<?php else : ?>

				<?php echo $sections['files']; ?>

			<?php endif; ?>
		</div>
	<?php endif; ?>


<?php else: ?>

	<p class="cms-profiler-box">No data.</p>

<?php endif; ?>

</div>	<!-- /zeal_profiler -->



