<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
		<head>
				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
				<title><?= apply_filters( 'head_page_title', $meta_title ); ?></title>
				<meta name="description" content="<?= apply_filters( 'head_page_description', $meta_descr ); ?>">
				<meta name="viewport" content="width=device-width, initial-scale=1">

				<link rel="stylesheet" href="<?= site_url('/assets/css/bootstrap.min.css'); ?>">
				<style>
						body {
								padding-top: 50px;
								padding-bottom: 20px;
						}
				</style>
				<link rel="stylesheet" href="<?= site_url('/assets/css/bootstrap-theme.min.css'); ?>">
				<link rel="stylesheet" href="<?= site_url('/assets/css/main.css'); ?>">

				<script src="<?= site_url( '/assets/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js' )?>"></script>
		</head>
		<body>
				<!--[if lt IE 7]>
						<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
				<![endif]-->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><?=cms_get_option( 'site_name' ); ?></a>
				</div>
				<div class="navbar-collapse collapse">
					<form class="navbar-form navbar-right" role="form" method="post" action="<?= site_url(); ?>">
<?php if ( ! is_user_logged_in() ) : ?>
						<div class="form-group">
							<input type="text" name="username" placeholder="Email" class="form-control">
						</div>
						<div class="form-group">
							<input type="password" name="password" placeholder="Password" class="form-control">
						</div>
						<input type="hidden" value="login" name="action" />
						<button type="submit" class="btn btn-success">Sign in</button>
<?php else : ?>
						<input type="hidden" value="logout" name="action" />
						<button type="submit" class="btn btn-info">Logout</button>
<?php endif; ?>
					</form>
				</div><!--/.navbar-collapse -->
			</div>
		</div>

		<!-- Main jumbotron for a primary marketing message or call to action -->
		<div class="jumbotron">
			<div class="container">
				<h1>Hello, world!</h1>
				<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
				<p><a class="btn btn-primary btn-lg" role="button">Learn more &raquo;</a></p>
			</div>
		</div>

		<div class="container">
			<!-- Example row of columns -->
			<div class="row">
				<div class="col-md-12">
					<h2><?=$label?></h2>
					<?php //dump( get_defined_vars( ) ); ?>
					<p><?= $content ?></p>
					<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
				</div>
			</div>

			<hr>

			<footer>
				<p>&copy; Company 2014</p>
			</footer>
		</div> <!-- /container -->

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

				<script src="/assets/js/vendor/bootstrap.min.js"></script>

				<script src="/assets/js/plugins.js"></script>
				<script src="/assets/js/main.js"></script>

		</body>
</html>
