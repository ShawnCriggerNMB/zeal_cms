<?php if ( FALSE === $error ) : ?>
    <div class="alert alert-success fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <h4>Database tables successfully updated.</h4>
    </div>
<?php endif; ?>

	<form role="form" action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">

	  <div class="form-group">
	    <label for="site_name">Website Name</label>
	    <input type="text" class="form-control" name="site_name" id="site_name" placeholder="Some Company">
	  </div>

	  <div class="form-group">
	    <label for="site_url">Website URL</label>
	    <input type="text" class="form-control" id="site_url" name="site_url" placeholder="http://somewhere.com">
	  </div>

	  <div class="form-group">
	    <label for="site_dir">Åbsolute Path to Website</label>
	    <input type="text" class="form-control" id="site_dir" name="site_dir" placeholder="/home/username/public_html">
	  </div>

	  <input type="hidden" name="step" value="1">
	  <button type="submit" class="btn btn-default">Submit</button>
	</form>

</div>