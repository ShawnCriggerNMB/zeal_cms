<?php if ( FALSE === $error ) : ?>
    <div class="alert alert-success fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <h4>Zeal Technologies CMS successfully installed.</h4>
    </div>
<?php endif; ?>
