	<form role="form" action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">


	  <div class="form-group">
	    <label for="email">Admin email address</label>
	    <input type="email" class="form-control" id="email" name="email" placeholder="test@here.com">
	  </div>

	  <div class="form-group">
	    <label for="password">Password</label>
	    <input type="password" class="form-control" name="password" placeholder="Password">
	  </div>

	  <div class="form-group">
	    <label for="first_name">Admin First Name</label>
	    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Shawn">
	  </div>

	  <div class="form-group">
	    <label for="last_name">Admin Last Name</label>
	    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Doe">
	  </div>

	  <input type="hidden" name="step" value="2" />
	  <button type="submit" class="btn btn-default">Submit</button>
	</form>

</div>