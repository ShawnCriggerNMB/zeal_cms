<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Zeal Technologies CMS Installer">
        <meta name="author" content="Shawn Crigger">

        <title>Zeal Technologies CMS Installer</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/admin/assets/css/bootstrap.min.css" rel="stylesheet">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="container">

        <div class="page-header">
          <h1>Zeal Technologies CMS Installer <small>Step <?= $step ?> of <?= $max_steps ?></small></h1>
        </div>

<?php if ( is_string( $error ) ) : ?>

    <div class="alert alert-danger fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <h4>Please fix the following errors.</h4>
            <p>
                <ul><?= $error ?></ul>
            </p>
    </div>

<?php endif;