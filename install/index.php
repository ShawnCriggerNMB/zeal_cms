<?php

$dir = __DIR__ ;
define( 'BASE', TRUE );
/*
if ( ! defined( IS_INSTALLING ) )
{
	define( 'IS_INSTALLING', TRUE );
}
*/
error_reporting( E_ALL );
require( $dir . '/../includes/cms-load.php' );

create_role( 'administrator', 'Administrator' );
create_role( 'superadmin', 'Super Administrator' );

$reqs = array(
	'DB_NAME' => 'Database name is not set.',
	'DB_USER' => 'Database user is not set.',
	'DB_PASSWORD' => 'Database user password is not set.',
	'DB_HOST' => 'Database host is not set.',
	'DB_PREFIX' => 'Database prefix is not set.',
);
$error = FALSE;

foreach ($reqs as $key => $msg) {
	if ( ! defined( $key ) )
	{
		$error .= "\t<li>{$msg}</li>\n";
	}
}

$step = (int) @$_REQUEST['step'];
$max_steps = 3;



if ( ! is_post() OR ! isset( $_POST['step'] ) ) {
	$step = 0;
}


if ( 0 === $step )
{
	cms_upgrade();
	set_default_options();
}


if ( isset( $_POST['step'] ) )
{
	$step = (int) $_POST['step'];
	handle_submit( $step );
}



// ------------------------------------------------------------------------


require( $dir . '/views/head.php' );
require( $dir . "/views/step{$step}.php" );
require( $dir . '/views/footer.php' );


function test_db()
{
	global $db, $error;
	try {
		$db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASSWORD );
		return TRUE;
	} catch (Exception $e) {
		$error .= '<li>Could not connect to database, please check database settings.</li>';
		return FALSE;
	}
}

// ------------------------------------------------------------------------


function is_post()
{
	return (bool) ( 'POST' === $_SERVER['REQUEST_METHOD'] );
}

// ------------------------------------------------------------------------

function handle_submit()
{
	global $wpdb, $error, $step;
	if ( 0 === $step ) return FALSE;

	if ( ! is_post() ) return FALSE;

	if ( 1 === $step )
	{
		cms_update_option( 'site_name', $_POST['site_name'] );
		cms_update_option( 'site_url', $_POST['site_url'] );
		cms_update_option( 'site_dir', $_POST['site_dir'] );
		return TRUE;
	} elseif ( 2 === $step )
	{
		$GLOBALS['cms_users_object'] = new CMS_Users( $wpdb );
		if ( ! isset( $_POST['email'] ) OR empty( $_POST['email'] ) )
		{
			$error .= "<li>A admin email must be set.</li>";
		}
		if ( ! isset( $_POST['password'] ) OR empty( $_POST['password'] ) )
		{
			$error .= "<li>A admin email must be set.</li>";
		}
		if ( ! is_email( $_POST['email'] ) )
		{
			$error .= '<li>Invalid email: '.$_POST['email'] . '</li>';

		}
		if ( is_string( $error ) ) return FALSE;

		if ( !$GLOBALS['cms_users_object']->get_user( $_POST['email'] ) )
		{

			$r = $GLOBALS['cms_users_object']->new_user( array(
				'user_login'    => $_POST['email'],
				'user_nicename' => cms_get_option( 'sitename' ) . ' Administrator',
				'user_email'    => $_POST['email'],
				'user_pass'     => $_POST['password'],
				'user_status'   => 1,
			) );

			if ( is_cms_error($r) )
			{
				$error .= '<li>'.$r->get_error_message() .'</li>';
				return FALSE;
			} else {
				$user = new CMS_User( $_POST['email'] );
				$user->set_role('superadmin');
				update_usermeta( $user->ID, 'first_name', $_POST['first_name'], TRUE );
				update_usermeta( $user->ID, 'last_name', $_POST['last_name'], TRUE );
			}
		}
	}
}

// ------------------------------------------------------------------------



function cms_upgrade()
{

	global $wpdb, $table_prefix, $error;

	if ( ! test_db() ) return FALSE;

	require_once( __DIR__ . '/class.bp-sql-schema-parser.php');
	$schema = include( __DIR__ . '/schema/db-schema.php');

	if ( is_array($schema) ) {
		$parser = new BP_SQL_Schema_Parser();

		// fill in the table names
		foreach ( $schema as $t => $q )
			$schema[$t] = sprintf($q, $table_prefix . $t);

		$result = $parser->delta( $wpdb, $schema );
		if ( !empty($result['errors']) ) {
			echo '<pre>';
			echo "Upgrade error:\n";
			var_dump($result);
			echo '</pre>';
		}
	}


}



function set_default_options()
{
	global $wpdb;
	$contact_settings = array(
		'company_show_on_contact' => 0,
		'contact_email' => '',
		'contact_email_show' => 0,
		'contact_phone_1' => '',
		'contact_phone_1_type' => '',
		'contact_phone_1_show' => '',
		'contact_phone_2' => '',
		'contact_phone_1_type' => '',
		'contact_phone_2_show' => '',
		'contact_phone_3' => '',
		'contact_phone_1_type' => '',
		'contact_phone_3_show' => '',
		'contact_address' => '',
		'contact_city' => '',
		'contact_state' => '',
		'contact_zipcode' => '',
		'contact_address_type' => '',
		'contact_address_show' => '',
		'show_contact_info' => '',
		'display_contact' => '',
		'autosend_contactform_email' => '',
	);

	$contact_settings = serialize( $contact_settings );
	$default = array(
		'site_name' => 'Zeal Technologies CMS',
		'site_url'  => '',
		'site_dir'  => '',
		'form_email' => 'ithippyshawn@gmail.com',
		'company_name' => 'Zeal Technologies CMS',
		'company_type' => '',
		'business_hours' => '',
		'show_business_hours' => 0,
		'use_access_control' => 0,
		'use_seo_urls' => 0,
		'is_site_live' => 0,
		'google_code' => '',
		'default_keywords' => '',
		'default_title' => 'Zeal Technologies CMS',
		'default_description' => 'A simple CMS designed by Shawn Crigger.',
		'application_id'     => 1,
		'aplication_uri'     => '',
		'cron_uri'           => '',
		'cron_check'         => '',
		'wp_http_version'    => '1.0',
		'hash_function_name' => 'md5',
		'language_locale'    => '',
		'language_directory' => '',
		'charset'            => 'UTF-8',
		'gmt_offset'         => 0,
		'timezone_string'    => 'GMT',
		'module_contact_form' => $contact_settings,
	);

	foreach ($default as $key => $value) {
		$wpdb->insert( $wpdb->options, array( 'option_name' => $key, 'option_value' => $value ) );
	}
}
