<?php

return array(


'options' => "CREATE TABLE %s (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
)",


'modules' => "CREATE TABLE %s (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(25) NOT NULL DEFAULT '',
  `name` varchar(250) NOT NULL DEFAULT '',
  `installed` int(1) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '0',
  `display` int(1) NOT NULL DEFAULT '1' COMMENT '0=hide won''t show up in sidebar nav',
  `level` varchar(60) NOT NULL DEFAULT 'administrator',
  `description` text NOT NULL COMMENT 'instructions for home page',
  'icon' var(250) NOT NULL DEFAULT '',
  `notice_update` text NOT NULL COMMENT 'Instructions for Update Form',
  `notice_add` text NOT NULL COMMENT 'Instructions for Add Form',
  `weight` int(3) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`),
  KEY `identifier` (`identifier`)
)",


'users' => "CREATE TABLE %s (
  ID bigint(20) unsigned NOT NULL auto_increment,
  user_login varchar(60) NOT NULL default '',
  user_pass varchar(64) NOT NULL default '',
  user_nicename varchar(50) NOT NULL default '',
  user_email varchar(100) NOT NULL default '',
  user_url varchar(100) NOT NULL default '',
  user_registered datetime NOT NULL default '0000-00-00 00:00:00',
  user_activation_key varchar(60) NOT NULL default '',
  user_status int(11) NOT NULL default '0',
  display_name varchar(250) NOT NULL default '',
  PRIMARY KEY  (ID),
  KEY user_login_key (user_email),
  KEY user_login_key (user_login),
  KEY user_nicename (user_nicename)
)",

'usermeta' => "CREATE TABLE %s (
  umeta_id bigint(20) NOT NULL auto_increment,
  user_id bigint(20) NOT NULL default '0',
  meta_key varchar(255) default NULL,
  meta_value longtext,
  PRIMARY KEY  (umeta_id),
  KEY user_id (user_id),
  KEY meta_key (meta_key)
)"

);

?>
