# Zeal Technologies CMS




## Theory

- The current version will be 3.0.1, it is **MAJOR** rewrite which is why it is now version 3.
- The theory behind the current rewrite is that, with the current 2.0 versions, there is too much code duplication between the front end and the back-end, also it is extremely out of date, with JavaScript files dating back to 2008.   I think that both the front and back end should share a lot of the same code, thus preventing duplication.  JavaScript like jQuery should be loaded from the front-end location so that it will stay up-to-date.
- The HTML presentation layer will be re-written in HTML5, and designed with a responsive grid system, so that it will work on tablets, phones, or just little tiny computers.
- The "modules" folder will be moved out of the admin directory, and into "includes/modules" so that both the Admin functions for each modules and the front-end functions will be located in the same directory.  This way when you copy a module from one website to another it will just drop right in.
- By using Classes instead of procedural functions, we can reduce the amount of code that it takes to build a module, reducing the amount of code means the modules are built faster.
- CKEditor and CKFinder will be replaced with TinyMCE, as there are better OpenSource File Managers designed for TinyMCE compared to using a cracked version of CKFinder ( which is illegal )
- User Login will be handled in the Front-end, they will share 1 user table instead of 2 and just have roles and capabilities to detect if the user is a admin or a user.  In the future this can be expanded to say that Moderator rules can do this and this but not that, etc.






### Folder Structure

	public_html/includes           => will contain all files shared between the front and back-end.
	public_html/includes/zeal/     => Core classes and functions shared between front and back-end
	public_html/includes/modules/  => Will contain each indivdual module, both front and back end.
	public_html/assets             => JavaScript, CSS and Images for Front-end or shared between
	public_html/admin              => Will contain only the admin files, should only require the index.php and maybe a few other files.
	public_html/admin/assets       => Admin only Assets





### Resources Links

	[TinyMCE FileManager](http://www.responsivefilemanager.com/index.php)
	[TinyMCE Bootstrap Theme](https://github.com/gtraxx/tinymce-skin-bootstrap)


