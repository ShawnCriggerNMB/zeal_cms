<?php
/**
 * Abstract class for extending the Admin Modules from.
 *
 * @version 3.0
 * @package Zeal Technologies CMS
 * @category frontend
 * @since  3.0
 * @author Shawn Crigger <shawn@eaglewebdesigns.com>
 * @author Chuck Bunnell <support@eaglewebdesigns.com>
 * @copyright Zeal Technologies
 */


if ( ! function_exists( 'dump') ) :

  /**
    * Debug Helper
    *
    * Outputs the given variable(s) with formatting and location
    *
    * @access        public
    * @param        mixed    variables to be output
    */
  function dump()
  {
      list($callee) = debug_backtrace();
      $arguments = func_get_args();
      $total_arguments = count($arguments);

      echo '<fieldset style="background: #fefefe !important; border:2px red solid; padding:5px">';
      echo '<legend style="background:lightgrey; padding:5px;">'.$callee['file'].' @ line: '.$callee['line'].'</legend><pre>';
      $i = 0;
      foreach ($arguments as $argument)
      {
          echo '<br/><strong>Debug #'.(++$i).' of '.$total_arguments.'</strong>: ';
          var_dump($argument);
      }

      echo "</pre>";
      echo "</fieldset>";
  }

endif;

//if ( !defined('BASE') ) die('No Direct Script Access');

if ( ! defined( 'BASE' ) ) define( 'BASE', TRUE );

include_once( __DIR__ . '/config.php' );
//define( ABSPATH, __DIR__ );

require( ABSPATH . '/includes/cms-load.php' );
require( ABSPATH . '/includes/init.php' );


$post_type = isset( $_REQUEST['post_type'] ) ? $_REQUEST['post_type'] : 'pages';
$slug      = isset( $_REQUEST['page'] )      ? $_REQUEST['page']      : 'index';

Load::library( $post_type, $class );

$vars = $class->fetch( $slug );

View::show( 'index', $vars );
