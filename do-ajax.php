<?php
/**
 * All AJAX Processes should be sent to this file, with a POST var of 'action',
 * which will
 *
 * @package Zeal Tech CMS
 * @subpackage Administration
 *
 * @link http://codex.wordpress.org/AJAX_in_Plugins
 */

/**
 * Executing AJAX process.
 *
 * @since 2.1.0
 */
define( 'DOING_AJAX', true );
define( 'HOMEBASE', true );

/** Load ZealTech CMS Bootstrap */
//require_once( dirname( dirname( __FILE__ ) ) . '/wp-load.php' );
define( 'BASE', true );
require( __DIR__ . '/includes/cms-load.php' );
require( ABSPATH . '/includes/init.php' );

/** Allow for cross-domain requests (from the frontend). */
//send_origin_headers();
//@header( 'Access-Control-Allow-Origin: ' .  $origin );
//@header( 'Access-Control-Allow-Credentials: true' );

// Require an action parameter
if ( empty( $_REQUEST['action'] ) )
	die( '0' );

@header( 'Content-Type: text/html; charset=utf8' );
@header( 'X-Robots-Tag: noindex' );

send_nosniff_header();
nocache_headers();

do_action( 'admin_init' );

$core_actions_get = array();

$core_actions_post = array();

// Register core Ajax calls.
if ( ! empty( $_GET['action'] ) && in_array( $_GET['action'], $core_actions_get ) )
	add_action( 'cms_ajax_' . $_GET['action'], 'cms_ajax_' . str_replace( '-', '_', $_GET['action'] ), 1 );

if ( ! empty( $_POST['action'] ) && in_array( $_POST['action'], $core_actions_post ) )
	add_action( 'cms_ajax_' . $_POST['action'], 'cms_ajax_' . str_replace( '-', '_', $_POST['action'] ), 1 );

add_action( 'cms_ajax_nopriv_heartbeat', 'cms_ajax_nopriv_heartbeat', 1 );

if ( is_user_logged_in() ) {
	/**
	 * Fires authenticated AJAX actions for logged-in users.
	 *
	 * The dynamic portion of the hook name, $_REQUEST['action'],
	 * refers to the name of the AJAX action callback being fired.
	 *
	 * @since 2.1.0
	 */
	do_action( 'cms_ajax_' . $_REQUEST['action'] );
} else {
	/**
	 * Fires non-authenticated AJAX actions for logged-out users.
	 *
	 * The dynamic portion of the hook name, $_REQUEST['action'],
	 * refers to the name of the AJAX action callback being fired.
	 *
	 * @since 2.8.0
	 */
	do_action( 'cms_ajax_nopriv_' . $_REQUEST['action'] );
}
// Default status
die( '0' );
